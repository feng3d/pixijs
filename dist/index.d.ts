/// <reference types="feng3d" />
declare namespace feng3d.pixi {
    const Matrix: typeof feng3d.Matrix;
    const Transform: typeof feng3d.Transform;
    /**
     * Different types of environments for WebGL.
     *
     * @static
     * @name ENV
     * @enum {number}
     * @property {number} WEBGL_LEGACY - Used for older v1 WebGL devices. PixiJS will aim to ensure compatibility
     *  with older / less advanced devices. If you experience unexplained flickering prefer this environment.
     * @property {number} WEBGL - Version 1 of WebGL
     * @property {number} WEBGL2 - Version 2 of WebGL
     */
    enum ENV {
        WEBGL_LEGACY = 0,
        WEBGL = 1,
        WEBGL2 = 2
    }
    /**
     * Constant to identify the Renderer Type.
     *
     * @static
     * @name RENDERER_TYPE
     * @enum {number}
     * @property {number} UNKNOWN - Unknown render type.
     * @property {number} WEBGL - WebGL render type.
     * @property {number} CANVAS - Canvas render type.
     */
    enum RENDERER_TYPE {
        UNKNOWN = 0,
        WEBGL = 1,
        CANVAS = 2
    }
    /**
     * Bitwise OR of masks that indicate the buffers to be cleared.
     *
     * @static
     * @name BUFFER_BITS
     * @enum {number}
     * @property {number} COLOR - Indicates the buffers currently enabled for color writing.
     * @property {number} DEPTH - Indicates the depth buffer.
     * @property {number} STENCIL - Indicates the stencil buffer.
     */
    enum BUFFER_BITS {
        COLOR = 16384,
        DEPTH = 256,
        STENCIL = 1024
    }
    /**
     * Various blend modes supported by PIXI.
     *
     * IMPORTANT - The WebGL renderer only supports the NORMAL, ADD, MULTIPLY and SCREEN blend modes.
     * Anything else will silently act like NORMAL.
     *
     * @name BLEND_MODES
     * @enum {number}
     * @property {number} NORMAL
     * @property {number} ADD
     * @property {number} MULTIPLY
     * @property {number} SCREEN
     * @property {number} OVERLAY
     * @property {number} DARKEN
     * @property {number} LIGHTEN
     * @property {number} COLOR_DODGE
     * @property {number} COLOR_BURN
     * @property {number} HARD_LIGHT
     * @property {number} SOFT_LIGHT
     * @property {number} DIFFERENCE
     * @property {number} EXCLUSION
     * @property {number} HUE
     * @property {number} SATURATION
     * @property {number} COLOR
     * @property {number} LUMINOSITY
     * @property {number} NORMAL_NPM
     * @property {number} ADD_NPM
     * @property {number} SCREEN_NPM
     * @property {number} NONE
     * @property {number} SRC_IN
     * @property {number} SRC_OUT
     * @property {number} SRC_ATOP
     * @property {number} DST_OVER
     * @property {number} DST_IN
     * @property {number} DST_OUT
     * @property {number} DST_ATOP
     * @property {number} SUBTRACT
     * @property {number} SRC_OVER
     * @property {number} ERASE
     * @property {number} XOR
     */
    enum BLEND_MODES {
        NORMAL = 0,
        ADD = 1,
        MULTIPLY = 2,
        SCREEN = 3,
        OVERLAY = 4,
        DARKEN = 5,
        LIGHTEN = 6,
        COLOR_DODGE = 7,
        COLOR_BURN = 8,
        HARD_LIGHT = 9,
        SOFT_LIGHT = 10,
        DIFFERENCE = 11,
        EXCLUSION = 12,
        HUE = 13,
        SATURATION = 14,
        COLOR = 15,
        LUMINOSITY = 16,
        NORMAL_NPM = 17,
        ADD_NPM = 18,
        SCREEN_NPM = 19,
        NONE = 20,
        SRC_OVER = 0,
        SRC_IN = 21,
        SRC_OUT = 22,
        SRC_ATOP = 23,
        DST_OVER = 24,
        DST_IN = 25,
        DST_OUT = 26,
        DST_ATOP = 27,
        ERASE = 26,
        SUBTRACT = 28,
        XOR = 29
    }
    /**
     * Various webgl draw modes. These can be used to specify which GL drawMode to use
     * under certain situations and renderers.
     *
     * @static
     * @name DRAW_MODES
     * @enum {number}
     * @property {number} POINTS
     * @property {number} LINES
     * @property {number} LINE_LOOP
     * @property {number} LINE_STRIP
     * @property {number} TRIANGLES
     * @property {number} TRIANGLE_STRIP
     * @property {number} TRIANGLE_FAN
     */
    enum DRAW_MODES {
        POINTS = 0,
        LINES = 1,
        LINE_LOOP = 2,
        LINE_STRIP = 3,
        TRIANGLES = 4,
        TRIANGLE_STRIP = 5,
        TRIANGLE_FAN = 6
    }
    /**
     * Various GL texture/resources formats.
     *
     * @static
     * @name FORMATS
     * @enum {number}
     * @property {number} RGBA=6408
     * @property {number} RGB=6407
     * @property {number} ALPHA=6406
     * @property {number} LUMINANCE=6409
     * @property {number} LUMINANCE_ALPHA=6410
     * @property {number} DEPTH_COMPONENT=6402
     * @property {number} DEPTH_STENCIL=34041
     */
    enum FORMATS {
        RGBA = 6408,
        RGB = 6407,
        ALPHA = 6406,
        LUMINANCE = 6409,
        LUMINANCE_ALPHA = 6410,
        DEPTH_COMPONENT = 6402,
        DEPTH_STENCIL = 34041
    }
    /**
     * Various GL target types.
     *
     * @static
     * @name TARGETS
     * @enum {number}
     * @property {number} TEXTURE_2D=3553
     * @property {number} TEXTURE_CUBE_MAP=34067
     * @property {number} TEXTURE_2D_ARRAY=35866
     * @property {number} TEXTURE_CUBE_MAP_POSITIVE_X=34069
     * @property {number} TEXTURE_CUBE_MAP_NEGATIVE_X=34070
     * @property {number} TEXTURE_CUBE_MAP_POSITIVE_Y=34071
     * @property {number} TEXTURE_CUBE_MAP_NEGATIVE_Y=34072
     * @property {number} TEXTURE_CUBE_MAP_POSITIVE_Z=34073
     * @property {number} TEXTURE_CUBE_MAP_NEGATIVE_Z=34074
     */
    enum TARGETS {
        TEXTURE_2D = 3553,
        TEXTURE_CUBE_MAP = 34067,
        TEXTURE_2D_ARRAY = 35866,
        TEXTURE_CUBE_MAP_POSITIVE_X = 34069,
        TEXTURE_CUBE_MAP_NEGATIVE_X = 34070,
        TEXTURE_CUBE_MAP_POSITIVE_Y = 34071,
        TEXTURE_CUBE_MAP_NEGATIVE_Y = 34072,
        TEXTURE_CUBE_MAP_POSITIVE_Z = 34073,
        TEXTURE_CUBE_MAP_NEGATIVE_Z = 34074
    }
    /**
     * Various GL data format types.
     *
     * @static
     * @name TYPES
     * @enum {number}
     * @property {number} UNSIGNED_BYTE=5121
     * @property {number} UNSIGNED_SHORT=5123
     * @property {number} UNSIGNED_SHORT_5_6_5=33635
     * @property {number} UNSIGNED_SHORT_4_4_4_4=32819
     * @property {number} UNSIGNED_SHORT_5_5_5_1=32820
     * @property {number} FLOAT=5126
     * @property {number} HALF_FLOAT=36193
     */
    enum TYPES {
        UNSIGNED_BYTE = 5121,
        UNSIGNED_SHORT = 5123,
        UNSIGNED_SHORT_5_6_5 = 33635,
        UNSIGNED_SHORT_4_4_4_4 = 32819,
        UNSIGNED_SHORT_5_5_5_1 = 32820,
        FLOAT = 5126,
        HALF_FLOAT = 36193
    }
    /**
     * The scale modes that are supported by pixi.
     *
     * The {@link PIXI.settings.SCALE_MODE} scale mode affects the default scaling mode of future operations.
     * It can be re-assigned to either LINEAR or NEAREST, depending upon suitability.
     *
     * @static
     * @name SCALE_MODES
     * @enum {number}
     * @property {number} LINEAR Smooth scaling
     * @property {number} NEAREST Pixelating scaling
     */
    enum SCALE_MODES {
        NEAREST = 0,
        LINEAR = 1
    }
    /**
     * The wrap modes that are supported by pixi.
     *
     * The {@link PIXI.settings.WRAP_MODE} wrap mode affects the default wrapping mode of future operations.
     * It can be re-assigned to either CLAMP or REPEAT, depending upon suitability.
     * If the texture is non power of two then clamp will be used regardless as WebGL can
     * only use REPEAT if the texture is po2.
     *
     * This property only affects WebGL.
     *
     * @name WRAP_MODES
     * @static
     * @enum {number}
     * @property {number} CLAMP - The textures uvs are clamped
     * @property {number} REPEAT - The texture uvs tile and repeat
     * @property {number} MIRRORED_REPEAT - The texture uvs tile and repeat with mirroring
     */
    enum WRAP_MODES {
        CLAMP = 33071,
        REPEAT = 10497,
        MIRRORED_REPEAT = 33648
    }
    /**
     * Mipmap filtering modes that are supported by pixi.
     *
     * The {@link PIXI.settings.MIPMAP_TEXTURES} affects default texture filtering.
     * Mipmaps are generated for a baseTexture if its `mipmap` field is `ON`,
     * or its `POW2` and texture dimensions are powers of 2.
     * Due to platform restriction, `ON` option will work like `POW2` for webgl-1.
     *
     * This property only affects WebGL.
     *
     * @name MIPMAP_MODES
     * @static
     * @enum {number}
     * @property {number} OFF - No mipmaps
     * @property {number} POW2 - Generate mipmaps if texture dimensions are pow2
     * @property {number} ON - Always generate mipmaps
     * @property {number} ON_MANUAL - Use mipmaps, but do not auto-generate them; this is used with a resource
     *   that supports buffering each level-of-detail.
     */
    enum MIPMAP_MODES {
        OFF = 0,
        POW2 = 1,
        ON = 2,
        ON_MANUAL = 3
    }
    /**
     * How to treat textures with premultiplied alpha
     *
     * @name ALPHA_MODES
     * @static
     * @enum {number}
     * @property {number} NO_PREMULTIPLIED_ALPHA - Source is not premultiplied, leave it like that.
     *  Option for compressed and data textures that are created from typed arrays.
     * @property {number} PREMULTIPLY_ON_UPLOAD - Source is not premultiplied, premultiply on upload.
     *  Default option, used for all loaded images.
     * @property {number} PREMULTIPLIED_ALPHA - Source is already premultiplied
     *  Example: spine atlases with `_pma` suffix.
     * @property {number} NPM - Alias for NO_PREMULTIPLIED_ALPHA.
     * @property {number} UNPACK - Default option, alias for PREMULTIPLY_ON_UPLOAD.
     * @property {number} PMA - Alias for PREMULTIPLIED_ALPHA.
     */
    enum ALPHA_MODES {
        NPM = 0,
        UNPACK = 1,
        PMA = 2,
        NO_PREMULTIPLIED_ALPHA = 0,
        PREMULTIPLY_ON_UPLOAD = 1,
        PREMULTIPLY_ALPHA = 2
    }
    /**
     * Configure whether filter textures are cleared after binding.
     *
     * Filter textures need not be cleared if the filter does not use pixel blending. {@link CLEAR_MODES.BLIT} will detect
     * this and skip clearing as an optimization.
     *
     * @name CLEAR_MODES
     * @static
     * @enum {number}
     * @property {number} BLEND - Do not clear the filter texture. The filter's output will blend on top of the output texture.
     * @property {number} CLEAR - Always clear the filter texture.
     * @property {number} BLIT - Clear only if {@link FilterSystem.forceClear} is set or if the filter uses pixel blending.
     * @property {number} NO - Alias for BLEND, same as `false` in earlier versions
     * @property {number} YES - Alias for CLEAR, same as `true` in earlier versions
     * @property {number} AUTO - Alias for BLIT
     */
    enum CLEAR_MODES {
        NO = 0,
        YES = 1,
        AUTO = 2,
        BLEND = 0,
        CLEAR = 1,
        BLIT = 2
    }
    /**
     * The gc modes that are supported by pixi.
     *
     * The {@link PIXI.settings.GC_MODE} Garbage Collection mode for PixiJS textures is AUTO
     * If set to GC_MODE, the renderer will occasionally check textures usage. If they are not
     * used for a specified period of time they will be removed from the GPU. They will of course
     * be uploaded again when they are required. This is a silent behind the scenes process that
     * should ensure that the GPU does not  get filled up.
     *
     * Handy for mobile devices!
     * This property only affects WebGL.
     *
     * @name GC_MODES
     * @enum {number}
     * @static
     * @property {number} AUTO - Garbage collection will happen periodically automatically
     * @property {number} MANUAL - Garbage collection will need to be called manually
     */
    enum GC_MODES {
        AUTO = 0,
        MANUAL = 1
    }
    /**
     * Constants that specify float precision in shaders.
     *
     * @name PRECISION
     * @constant
     * @static
     * @enum {string}
     * @property {string} LOW='lowp'
     * @property {string} MEDIUM='mediump'
     * @property {string} HIGH='highp'
     */
    enum PRECISION {
        LOW = "lowp",
        MEDIUM = "mediump",
        HIGH = "highp"
    }
    /**
     * Constants for mask implementations.
     * We use `type` suffix because it leads to very different behaviours
     *
     * @name MASK_TYPES
     * @static
     * @enum {number}
     * @property {number} NONE - Mask is ignored
     * @property {number} SCISSOR - Scissor mask, rectangle on screen, cheap
     * @property {number} STENCIL - Stencil mask, 1-bit, medium, works only if renderer supports stencil
     * @property {number} SPRITE - Mask that uses SpriteMaskFilter, uses temporary RenderTexture
     */
    enum MASK_TYPES {
        NONE = 0,
        SCISSOR = 1,
        STENCIL = 2,
        SPRITE = 3
    }
    /**
     * Constants for multi-sampling antialiasing.
     *
     * @see PIXI.Framebuffer#multisample
     *
     * @name MSAA_QUALITY
     * @static
     * @enum {number}
     * @property {number} NONE - No multisampling for this renderTexture
     * @property {number} LOW - Try 2 samples
     * @property {number} MEDIUM - Try 4 samples
     * @property {number} HIGH - Try 8 samples
     */
    enum MSAA_QUALITY {
        NONE = 0,
        LOW = 2,
        MEDIUM = 4,
        HIGH = 8
    }
}
declare namespace feng3d.pixi {
    /**
     * Uploading the same buffer multiple times in a single frame can cause performance issues.
     * Apparent on iOS so only check for that at the moment
     * This check may become more complex if this issue pops up elsewhere.
     *
     * @private
     * @returns {boolean}
     */
    function canUploadSameBuffer(): boolean;
}
declare namespace feng3d.pixi {
    type UserAgent = string;
    type Navigator = {
        userAgent: string;
        platform: string;
        maxTouchPoints?: number;
    };
    type isMobileResult = {
        apple: {
            phone: boolean;
            ipod: boolean;
            tablet: boolean;
            universal: boolean;
            device: boolean;
        };
        amazon: {
            phone: boolean;
            tablet: boolean;
            device: boolean;
        };
        android: {
            phone: boolean;
            tablet: boolean;
            device: boolean;
        };
        windows: {
            phone: boolean;
            tablet: boolean;
            device: boolean;
        };
        other: {
            blackberry: boolean;
            blackberry10: boolean;
            opera: boolean;
            firefox: boolean;
            chrome: boolean;
            device: boolean;
        };
        phone: boolean;
        tablet: boolean;
        any: boolean;
    };
    type IsMobileParameter = UserAgent | Navigator;
    function isMobile(param?: IsMobileParameter): isMobileResult;
    const ismobileResult: isMobileResult;
}
declare namespace feng3d.pixi {
    /**
     * The maximum recommended texture units to use.
     * In theory the bigger the better, and for desktop we'll use as many as we can.
     * But some mobile devices slow down if there is to many branches in the shader.
     * So in practice there seems to be a sweet spot size that varies depending on the device.
     *
     * In v4, all mobile devices were limited to 4 texture units because for this.
     * In v5, we allow all texture units to be used on modern Apple or Android devices.
     *
     * @private
     * @param {number} max
     * @returns {number}
     */
    function maxRecommendedTextures(max: number): number;
}
declare namespace feng3d.pixi {
    interface IRenderOptions {
        view: HTMLCanvasElement;
        antialias: boolean;
        autoDensity: boolean;
        backgroundColor: number;
        backgroundAlpha: number;
        useContextAlpha: boolean | 'notMultiplied';
        clearBeforeRender: boolean;
        preserveDrawingBuffer: boolean;
        width: number;
        height: number;
        legacy: boolean;
    }
    interface ISettings {
        MIPMAP_TEXTURES: number;
        ANISOTROPIC_LEVEL: number;
        RESOLUTION: number;
        FILTER_RESOLUTION: number;
        SPRITE_MAX_TEXTURES: number;
        SPRITE_BATCH_SIZE: number;
        RENDER_OPTIONS: IRenderOptions;
        GC_MODE: number;
        GC_MAX_IDLE: number;
        GC_MAX_CHECK_COUNT: number;
        WRAP_MODE: number;
        SCALE_MODE: number;
        PRECISION_VERTEX: string;
        PRECISION_FRAGMENT: string;
        CAN_UPLOAD_SAME_BUFFER: boolean;
        CREATE_IMAGE_BITMAP: boolean;
        ROUND_PIXELS: boolean;
        RETINA_PREFIX?: RegExp;
        FAIL_IF_MAJOR_PERFORMANCE_CAVEAT?: boolean;
        UPLOADS_PER_FRAME?: number;
        SORTABLE_CHILDREN?: boolean;
        PREFER_ENV?: number;
        STRICT_TEXTURE_CACHE?: boolean;
        MESH_CANVAS_PADDING?: number;
        TARGET_FPMS?: number;
    }
    /**
     * User's customizable globals for overriding the default PIXI settings, such
     * as a renderer's default resolution, framerate, float precision, etc.
     * @example
     * // Use the native window resolution as the default resolution
     * // will support high-density displays when rendering
     * PIXI.settings.RESOLUTION = window.devicePixelRatio;
     *
     * // Disable interpolation when scaling, will make texture be pixelated
     * PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
     * @namespace feng3d.pixi.settings
     */
    const settings: ISettings;
}
declare namespace feng3d.pixi {
    /**
     * Regexp for data URI.
     * Based on: {@link https://github.com/ragingwind/data-uri-regex}
     *
     * @static
     * @constant {RegExp|string} DATA_URI
     * @example data:image/png;base64
     */
    const DATA_URI: RegExp;
}
declare namespace feng3d.pixi {
}
declare namespace feng3d.pixi {
    /**
     * parses a URL query string into a collection of key and value pairs
     *
     * @param qs The URL query string to parse
     * @param sep The substring used to delimit key and value pairs in the query string
     * @param eq The substring used to delimit keys and values in the query string
     * @param options.decodeURIComponent The function to use when decoding percent-encoded characters in the query string
     * @param options.maxKeys Specifies the maximum number of keys to parse. Specify 0 to remove key counting limitations default 1000
     */
    function decodeFuncType(qs?: string, sep?: string, eq?: string, options?: {
        decodeURIComponent?: Function;
        maxKeys?: number;
    }): Record<any, unknown>;
    /**
    * It serializes passed object into string
    * The numeric values must be finite.
    * Any other input values will be coerced to empty strings.
    *
    * @param obj The object to serialize into a URL query string
    * @param sep The substring used to delimit key and value pairs in the query string
    * @param eq The substring used to delimit keys and values in the query string
    * @param name
    */
    function encodeFuncType(obj?: Record<any, unknown>, sep?: string, eq?: string, name?: any): string;
    export const querystring: {
        decode: typeof decodeFuncType;
        parse: typeof decodeFuncType;
        encode: typeof encodeFuncType;
        stringify: typeof encodeFuncType;
    };
    export {};
}
declare namespace feng3d.pixi {
    function Url(): void;
    function urlParse(url: any, parseQueryString?: any, slashesDenoteHost?: any): any;
    function urlFormat(obj: any): any;
    function urlResolve(source: any, relative: any): any;
    function urlResolveObject(source: any, relative: any): any;
    export const url: {
        parse: typeof urlParse;
        resolve: typeof urlResolve;
        resolveObject: typeof urlResolveObject;
        format: typeof urlFormat;
        Url: typeof Url;
    };
    export {};
}
declare namespace feng3d.pixi {
    /**
     * Creates an array containing the numeric code points of each Unicode
     * character in the string. While JavaScript uses UCS-2 internally,
     * this function will convert a pair of surrogate halves (each of which
     * UCS-2 exposes as separate characters) into a single code point,
     * matching UTF-16.
     * @see `punycode.ucs2.encode`
     * @see <https://mathiasbynens.be/notes/javascript-encoding>
     * @memberOf punycode.ucs2
     * @name decode
     * @param {String} string The Unicode input string (UCS-2).
     * @returns {Array} The new array of code points.
     */
    function ucs2decode(string: any): any[];
    /** Define the public API */
    export const punycode: {
        /**
         * A string representing the current Punycode.js version number.
         * @memberOf punycode
         * @type String
         */
        version: string;
        /**
         * An object of methods to convert from JavaScript's internal character
         * representation (UCS-2) to Unicode code points, and back.
         * @see <https://mathiasbynens.be/notes/javascript-encoding>
         * @memberOf punycode
         * @type Object
         */
        ucs2: {
            decode: typeof ucs2decode;
            encode: (array: any) => string;
        };
        decode: (input: any) => string;
        encode: (input: any) => string;
        toASCII: (input: any) => string;
        toUnicode: (input: any) => string;
    };
    export {};
}
declare namespace feng3d.pixi {
    interface ParsedUri {
        source?: string;
        protocol?: string;
        authority?: string;
        userInfo?: string;
        user?: string;
        password?: string;
        host?: string;
        port?: string;
        relative?: string;
        path?: string;
        directory?: string;
        file?: string;
        query?: string;
        anchor?: string;
    }
    interface Options {
        strictMode?: boolean;
    }
    export function parseUri(str: string, opts?: Options): ParsedUri;
    export {};
}
/**
 * Port from https://github.com/mapbox/earcut (v2.2.2)
 */
declare namespace feng3d.pixi {
    /**
     * 三角化形状
     *
     * @param data 形状顶点数据
     * @param holeIndices 空洞的起始顶点索引列表
     * @param dim 相邻顶点数据之间的步长
     */
    function earcut(data: number[], holeIndices: number[], dim?: number): number[];
}
declare namespace feng3d.pixi {
    /**
     * Skips the hello message of renderers that are created after this is run.
     *
     * @function skipHello.utils
     */
    function skipHello(): void;
    /**
     * Logs out the version and renderer information for this running instance of PIXI.
     * If you don't want to see this message you can run `PIXI.utils.skipHello()` before
     * creating your renderer. Keep in mind that doing that will forever make you a jerk face.
     *
     * @static
     * @function sayHello.utils
     * @param {string} type - The string renderer type to log.
     */
    function sayHello(type: string): void;
}
declare namespace feng3d.pixi {
    /**
     * Helper for checking for WebGL support.
     *.utils
     * @function isWebGLSupported
     * @return {boolean} Is WebGL supported.
     */
    function isWebGLSupported(): boolean;
}
declare namespace feng3d.pixi {
    const cssColorNames: {
        aliceblue: string;
        antiquewhite: string;
        aqua: string;
        aquamarine: string;
        azure: string;
        beige: string;
        bisque: string;
        black: string;
        blanchedalmond: string;
        blue: string;
        blueviolet: string;
        brown: string;
        burlywood: string;
        cadetblue: string;
        chartreuse: string;
        chocolate: string;
        coral: string;
        cornflowerblue: string;
        cornsilk: string;
        crimson: string;
        cyan: string;
        darkblue: string;
        darkcyan: string;
        darkgoldenrod: string;
        darkgray: string;
        darkgreen: string;
        darkgrey: string;
        darkkhaki: string;
        darkmagenta: string;
        darkolivegreen: string;
        darkorange: string;
        darkorchid: string;
        darkred: string;
        darksalmon: string;
        darkseagreen: string;
        darkslateblue: string;
        darkslategray: string;
        darkslategrey: string;
        darkturquoise: string;
        darkviolet: string;
        deeppink: string;
        deepskyblue: string;
        dimgray: string;
        dimgrey: string;
        dodgerblue: string;
        firebrick: string;
        floralwhite: string;
        forestgreen: string;
        fuchsia: string;
        gainsboro: string;
        ghostwhite: string;
        goldenrod: string;
        gold: string;
        gray: string;
        green: string;
        greenyellow: string;
        grey: string;
        honeydew: string;
        hotpink: string;
        indianred: string;
        indigo: string;
        ivory: string;
        khaki: string;
        lavenderblush: string;
        lavender: string;
        lawngreen: string;
        lemonchiffon: string;
        lightblue: string;
        lightcoral: string;
        lightcyan: string;
        lightgoldenrodyellow: string;
        lightgray: string;
        lightgreen: string;
        lightgrey: string;
        lightpink: string;
        lightsalmon: string;
        lightseagreen: string;
        lightskyblue: string;
        lightslategray: string;
        lightslategrey: string;
        lightsteelblue: string;
        lightyellow: string;
        lime: string;
        limegreen: string;
        linen: string;
        magenta: string;
        maroon: string;
        mediumaquamarine: string;
        mediumblue: string;
        mediumorchid: string;
        mediumpurple: string;
        mediumseagreen: string;
        mediumslateblue: string;
        mediumspringgreen: string;
        mediumturquoise: string;
        mediumvioletred: string;
        midnightblue: string;
        mintcream: string;
        mistyrose: string;
        moccasin: string;
        navajowhite: string;
        navy: string;
        oldlace: string;
        olive: string;
        olivedrab: string;
        orange: string;
        orangered: string;
        orchid: string;
        palegoldenrod: string;
        palegreen: string;
        paleturquoise: string;
        palevioletred: string;
        papayawhip: string;
        peachpuff: string;
        peru: string;
        pink: string;
        plum: string;
        powderblue: string;
        purple: string;
        rebeccapurple: string;
        red: string;
        rosybrown: string;
        royalblue: string;
        saddlebrown: string;
        salmon: string;
        sandybrown: string;
        seagreen: string;
        seashell: string;
        sienna: string;
        silver: string;
        skyblue: string;
        slateblue: string;
        slategray: string;
        slategrey: string;
        snow: string;
        springgreen: string;
        steelblue: string;
        tan: string;
        teal: string;
        thistle: string;
        tomato: string;
        turquoise: string;
        violet: string;
        wheat: string;
        white: string;
        whitesmoke: string;
        yellow: string;
        yellowgreen: string;
    };
    /**
     * Converts a hexadecimal color number to an [R, G, B] array of normalized floats (numbers from 0.0 to 1.0).
     *
     * @example
     * PIXI.utils.hex2rgb(0xffffff); // returns [1, 1, 1].utils
     * @function hex2rgb
     * @param {number} hex - The hexadecimal number to convert
     * @param  {number[]} [out=[]] - If supplied, this array will be used rather than returning a new one
     * @return {number[]} An array representing the [R, G, B] of the color where all values are floats.
     */
    function hex2rgb(hex: number, out?: Array<number> | Float32Array): Array<number> | Float32Array;
    /**
     * Converts a hexadecimal color number to a string.
     *
     * @example
     * PIXI.utils.hex2string(0xffffff); // returns "#ffffff".utils
     * @function hex2string
     * @param {number} hex - Number in hex (e.g., `0xffffff`)
     * @return {string} The string color (e.g., `"#ffffff"`).
     */
    function hex2string(hex: number): string;
    /**
     * Converts a string to a hexadecimal color number.
     * It can handle:
     *  hex strings starting with #: "#ffffff"
     *  hex strings starting with 0x: "0xffffff"
     *  hex strings without prefix: "ffffff"
     *  css colors: "black"
     *
     * @example
     * PIXI.utils.string2hex("#ffffff"); // returns 0xffffff.utils
     * @function string2hex
     * @param {string} string - The string color (e.g., `"#ffffff"`)
     * @return {number} Number in hexadecimal.
     */
    function string2hex(string: string): number;
    /**
     * Converts a color as an [R, G, B] array of normalized floats to a hexadecimal number.
     *
     * @example
     * PIXI.utils.rgb2hex([1, 1, 1]); // returns 0xffffff.utils
     * @function rgb2hex
     * @param {number[]} rgb - Array of numbers where all values are normalized floats from 0.0 to 1.0.
     * @return {number} Number in hexadecimal.
     */
    function rgb2hex(rgb: number[] | Float32Array): number;
}
declare namespace feng3d.pixi {
    /**
     * maps premultiply flag and blendMode to adjusted blendMode.utils
     * @const premultiplyBlendMode
     * @type {Array<number[]>}
     */
    const premultiplyBlendMode: number[][];
    /**
     * changes blendMode according to texture format
     *.utils
     * @function correctBlendMode
     * @param {number} blendMode - supposed blend mode
     * @param {boolean} premultiplied - whether source is premultiplied
     * @returns {number} true blend mode for this texture
     */
    function correctBlendMode(blendMode: number, premultiplied: boolean): number;
    /**
     * combines rgb and alpha to out array
     *.utils
     * @function premultiplyRgba
     * @param {Float32Array|number[]} rgb - input rgb
     * @param {number} alpha - alpha param
     * @param {Float32Array} [out] - output
     * @param {boolean} [premultiply=true] - do premultiply it
     * @returns {Float32Array} vec4 rgba
     */
    function premultiplyRgba(rgb: Float32Array | number[], alpha: number, out?: Float32Array, premultiply?: boolean): Float32Array;
    /**
     * premultiplies tint
     *.utils
     * @function premultiplyTint
     * @param {number} tint - integer RGB
     * @param {number} alpha - floating point alpha (0.0-1.0)
     * @returns {number} tint multiplied by alpha
     */
    function premultiplyTint(tint: number, alpha: number): number;
    /**
     * converts integer tint and float alpha to vec4 form, premultiplies by default
     *.utils
     * @function premultiplyTintToRgba
     * @param {number} tint - input tint
     * @param {number} alpha - alpha param
     * @param {Float32Array} [out] - output
     * @param {boolean} [premultiply=true] - do premultiply it
     * @returns {Float32Array} vec4 rgba
     */
    function premultiplyTintToRgba(tint: number, alpha: number, out: Float32Array, premultiply?: boolean): Float32Array;
}
declare namespace feng3d.pixi {
    /**
     * Helper for warning developers about deprecated features & settings.
     * A stack track for warnings is given; useful for tracking-down where
     * deprecated methods/properties/classes are being used within the code.
     *.utils
     * @function deprecation
     * @param {string} version - The version where the feature became deprecated
     * @param {string} message - Message should include what is deprecated, where, and the new solution
     * @param {number} [ignoreDepth=3] - The number of steps to ignore at the top of the error stack
     *        this is mostly to ignore internal deprecation calls.
     */
    function deprecation(version: string, message: string, ignoreDepth?: number): void;
}
declare namespace feng3d.pixi {
    type ArrayFixed<T, L extends number> = [T, ...Array<T>] & {
        length: L;
    };
    type Dict<T> = {
        [key: string]: T;
    };
}
declare namespace feng3d.pixi {
    /**
     * Generic Mask Stack data structure
     *.utils
     * @function createIndicesForQuads
     * @param {number} size - Number of quads
     * @param {Uint16Array|Uint32Array} [outBuffer] - Buffer for output, length has to be `6 * size`
     * @return {Uint16Array|Uint32Array} - Resulting index buffer
     */
    function createIndicesForQuads(size: number, outBuffer?: Uint16Array | Uint32Array): Uint16Array | Uint32Array;
}
declare namespace feng3d.pixi {
    function getBufferType(array: ITypedArray): 'Float32Array' | 'Uint32Array' | 'Int32Array' | 'Uint16Array' | 'Uint8Array' | null;
}
declare namespace feng3d.pixi {
    type PackedArray = Float32Array | Uint32Array | Int32Array | Uint8Array;
    export function interleaveTypedArrays(arrays: PackedArray[], sizes: number[]): Float32Array;
    export {};
}
declare namespace feng3d.pixi {
    /**
     * Rounds to next power of two.
     *
     * @function nextPow2.utils
     * @param {number} v - input value
     * @return {number}
     */
    function nextPow2(v: number): number;
    /**
     * Checks if a number is a power of two.
     *
     * @function isPow2.utils
     * @param {number} v - input value
     * @return {boolean} `true` if value is power of two
     */
    function isPow2(v: number): boolean;
    /**
     * Computes ceil of log base 2
     *
     * @function log2.utils
     * @param {number} v - input value
     * @return {number} logarithm base 2
     */
    function log2(v: number): number;
}
declare namespace feng3d.pixi {
    /**
     * Returns sign of number
     *.utils
     * @function sign
     * @param {number} n - the number to check the sign of
     * @returns {number} 0 if `n` is 0, -1 if `n` is negative, 1 if `n` is positive
     */
    function sign(n: number): -1 | 0 | 1;
}
declare namespace feng3d.pixi {
    /**
     * Gets the next unique identifier
     *.utils
     * @function uid
     * @return {number} The next unique identifier to use.
     */
    function uid(): number;
}
declare namespace feng3d.pixi {
    /**
     * @todo Describe property usage
     *
     * @static
     * @name ProgramCache.utils
     * @type {Object}
     */
    const ProgramCache: {
        [key: string]: Program;
    };
    /**
     * @todo Describe property usage
     *
     * @static
     * @name TextureCache.utils
     * @type {Object}
     */
    const TextureCache: {
        [key: string]: Texture;
    };
    /**
     * @todo Describe property usage
     *
     * @static
     * @name BaseTextureCache.utils
     * @type {Object}
     */
    const BaseTextureCache: {
        [key: string]: BaseTexture;
    };
    /**
     * Destroys all texture in the cache
     *.utils
     * @function destroyTextureCache
     */
    function destroyTextureCache(): void;
    /**
     * Removes all textures from cache, but does not destroy them
     *.utils
     * @function clearTextureCache
     */
    function clearTextureCache(): void;
}
declare namespace feng3d.pixi {
    /**
     * Creates a Canvas element of the given size to be used as a target for rendering to.
     *.utils
     */
    class CanvasRenderTarget {
        /** The Canvas object that belongs to this CanvasRenderTarget. */
        canvas: HTMLCanvasElement;
        /** A CanvasRenderingContext2D object representing a two-dimensional rendering context. */
        context: CanvasRenderingContext2D;
        /**
         * The resolution / device pixel ratio of the canvas
         * @default 1
         */
        resolution: number;
        /**
         * @param width - the width for the newly created canvas
         * @param height - the height for the newly created canvas
         * @param {number} [resolution=1] - The resolution / device pixel ratio of the canvas
         */
        constructor(width: number, height: number, resolution?: number);
        /**
         * Clears the canvas that was created by the CanvasRenderTarget class.
         *
         * @private
         */
        clear(): void;
        /**
         * Resizes the canvas to the specified width and height.
         *
         * @param width - the new width of the canvas
         * @param height - the new height of the canvas
         */
        resize(width: number, height: number): void;
        /** Destroys this canvas. */
        destroy(): void;
        /**
         * The width of the canvas buffer in pixels.
         *
         * @member {number}
         */
        get width(): number;
        set width(val: number);
        /**
         * The height of the canvas buffer in pixels.
         *
         * @member {number}
         */
        get height(): number;
        set height(val: number);
    }
}
declare namespace feng3d.pixi {
    /**
     * Trim transparent borders from a canvas
     *.utils
     * @function trimCanvas
     * @param {HTMLCanvasElement} canvas - the canvas to trim
     * @returns {object} Trim data
     */
    function trimCanvas(canvas: HTMLCanvasElement): {
        width: number;
        height: number;
        data?: ImageData;
    };
}
declare namespace feng3d.pixi {
    interface DecomposedDataUri {
        mediaType: string;
        subType: string;
        charset: string;
        encoding: string;
        data: string;
    }
    /**.utils
     * @interface DecomposedDataUri
     */
    /**
     * type, eg. `image`.utils.DecomposedDataUri#
     * @member {string} mediaType
     */
    /**
     * Sub type, eg. `png`.utils.DecomposedDataUri#
     * @member {string} subType
     */
    /**.utils.DecomposedDataUri#
     * @member {string} charset
     */
    /**
     * Data encoding, eg. `base64`.utils.DecomposedDataUri#
     * @member {string} encoding
     */
    /**
     * The actual data.utils.DecomposedDataUri#
     * @member {string} data
     */
    /**
     * Split a data URI into components. Returns undefined if
     * parameter `dataUri` is not a valid data URI.
     *.utils
     * @function decomposeDataUri
     * @param {string} dataUri - the data URI to check
     * @return {PIXI.utils.DecomposedDataUri|undefined} The decomposed data uri or undefined
     */
    function decomposeDataUri(dataUri: string): DecomposedDataUri;
}
declare namespace feng3d.pixi {
    /**
     * Sets the `crossOrigin` property for this resource based on if the url
     * for this resource is cross-origin. If crossOrigin was manually set, this
     * function does nothing.
     * Nipped from the resource loader!
     *
     * @ignore
     * @param {string} url - The url to test.
     * @param {object} [loc=window.location] - The location object to test against.
     * @return {string} The crossOrigin value to use (or empty string for none).
     */
    function determineCrossOrigin(url: string, loc?: Location): string;
}
declare namespace feng3d.pixi {
    /**
     * get the resolution / device pixel ratio of an asset by looking for the prefix
     * used by spritesheets and image urls
     *.utils
     * @function getResolutionOfUrl
     * @param {string} url - the image path
     * @param {number} [defaultValue=1] - the defaultValue if no filename prefix is set.
     * @return {number} resolution / device pixel ratio of an asset
     */
    function getResolutionOfUrl(url: string, defaultValue?: number): number;
}
declare namespace feng3d.pixi {
    const utils: {
        rgb2hex: typeof rgb2hex;
        hex2rgb: typeof hex2rgb;
        premultiplyTint: typeof premultiplyTint;
        premultiplyTintToRgba: typeof premultiplyTintToRgba;
        correctBlendMode: typeof correctBlendMode;
        getResolutionOfUrl: typeof getResolutionOfUrl;
        premultiplyBlendMode: number[][];
        EventEmitter: typeof EventEmitter;
    };
}
declare namespace feng3d.pixi {
    /**
     * A Runner is a highly performant and simple alternative to signals. Best used in situations
     * where events are dispatched to many objects at high frequency (say every frame!)
     *
     *
     * like a signal..
     * ```
     * import { Runner } from '@pixi/runner';
     *
     * const myObject = {
     *     loaded: new Runner('loaded')
     * }
     *
     * const listener = {
     *     loaded: function(){
     *         // thin
     *     }
     * }
     *
     * myObject.update.add(listener);
     *
     * myObject.loaded.emit();
     * ```
     *
     * Or for handling calling the same function on many items
     * ```
     * import { Runner } from '@pixi/runner';
     *
     * const myGame = {
     *     update: new Runner('update')
     * }
     *
     * const gameObject = {
     *     update: function(time){
     *         // update my gamey state
     *     }
     * }
     *
     * myGame.update.add(gameObject1);
     *
     * myGame.update.emit(time);
     * ```
     */
    class Runner {
        items: any[];
        private _name;
        private _aliasCount;
        /**
         *  @param {string} name - the function name that will be executed on the listeners added to this Runner.
         */
        constructor(name: string);
        /**
         * Dispatch/Broadcast Runner to all listeners added to the queue.
         * @param {...any} params - optional parameters to pass to each listener
         * @return {PIXI.Runner}
         */
        emit(a0?: unknown, a1?: unknown, a2?: unknown, a3?: unknown, a4?: unknown, a5?: unknown, a6?: unknown, a7?: unknown): this;
        private ensureNonAliasedItems;
        /**
         * Add a listener to the Runner
         *
         * Runners do not need to have scope or functions passed to them.
         * All that is required is to pass the listening object and ensure that it has contains a function that has the same name
         * as the name provided to the Runner when it was created.
         *
         * Eg A listener passed to this Runner will require a 'complete' function.
         *
         * ```
         * import { Runner } from '@pixi/runner';
         *
         * const complete = new Runner('complete');
         * ```
         *
         * The scope used will be the object itself.
         *
         * @param {any} item - The object that will be listening.
         * @return {PIXI.Runner}
         */
        add(item: unknown): this;
        /**
         * Remove a single listener from the dispatch queue.
         * @param {any} item - The listener that you would like to remove.
         * @return {PIXI.Runner}
         */
        remove(item: unknown): this;
        /**
         * Check to see if the listener is already in the Runner
         * @param {any} item - The listener that you would like to check.
         */
        contains(item: unknown): boolean;
        /**
         * Remove all listeners from the Runner
         * @return {PIXI.Runner}
         */
        removeAll(): this;
        /**
         * Remove all references, don't use after this.
         */
        destroy(): void;
        /**
         * `true` if there are no this Runner contains no listeners
         *
         * @member {boolean}
         * @readonly
         */
        get empty(): boolean;
        /**
         * The name of the runner.
         *
         * @member {string}
         * @readonly
         */
        get name(): string;
    }
}
declare namespace feng3d.pixi {
    /**
     * The Circle object is used to help draw graphics and can also be used to specify a hit area for displayObjects.
     *
     */
    class Circle {
        x: number;
        y: number;
        radius: number;
        readonly type: SHAPES.CIRC;
        /**
         * @param {number} [x=0] - The X coordinate of the center of this circle
         * @param {number} [y=0] - The Y coordinate of the center of this circle
         * @param {number} [radius=0] - The radius of the circle
         */
        constructor(x?: number, y?: number, radius?: number);
        /**
         * Creates a clone of this Circle instance
         *
         * @return {PIXI.Circle} a copy of the Circle
         */
        clone(): Circle;
        /**
         * Checks whether the x and y coordinates given are contained within this circle
         *
         * @param {number} x - The X coordinate of the point to test
         * @param {number} y - The Y coordinate of the point to test
         * @return {boolean} Whether the x/y coordinates are within this Circle
         */
        contains(x: number, y: number): boolean;
        /**
        * Returns the framing rectangle of the circle as a Rectangle object
        *
        * @return {PIXI.Rectangle} the framing rectangle
        */
        getBounds(): Rectangle;
        toString(): string;
    }
}
declare namespace feng3d.pixi {
    /**
     * The Ellipse object is used to help draw graphics and can also be used to specify a hit area for displayObjects.
     *
     */
    class Ellipse {
        x: number;
        y: number;
        width: number;
        height: number;
        readonly type: SHAPES.ELIP;
        /**
         * @param {number} [x=0] - The X coordinate of the center of this ellipse
         * @param {number} [y=0] - The Y coordinate of the center of this ellipse
         * @param {number} [halfWidth=0] - The half width of this ellipse
         * @param {number} [halfHeight=0] - The half height of this ellipse
         */
        constructor(x?: number, y?: number, halfWidth?: number, halfHeight?: number);
        /**
         * Creates a clone of this Ellipse instance
         *
         * @return {PIXI.Ellipse} a copy of the ellipse
         */
        clone(): Ellipse;
        /**
         * Checks whether the x and y coordinates given are contained within this ellipse
         *
         * @param {number} x - The X coordinate of the point to test
         * @param {number} y - The Y coordinate of the point to test
         * @return {boolean} Whether the x/y coords are within this ellipse
         */
        contains(x: number, y: number): boolean;
        /**
         * Returns the framing rectangle of the ellipse as a Rectangle object
         *
         * @return {PIXI.Rectangle} the framing rectangle
         */
        getBounds(): Rectangle;
        toString(): string;
    }
}
declare namespace feng3d.pixi {
    /**
     * A class to define a shape via user defined coordinates.
     *
     */
    class Polygon {
        points: number[];
        closeStroke: boolean;
        readonly type: SHAPES.POLY;
        constructor(points: Vector2[] | number[]);
        constructor(...points: Vector2[] | number[]);
        /**
         * Creates a clone of this polygon
         *
         * @return {PIXI.Polygon} a copy of the polygon
         */
        clone(): Polygon;
        /**
         * Checks whether the x and y coordinates passed to this function are contained within this polygon
         *
         * @param {number} x - The X coordinate of the point to test
         * @param {number} y - The Y coordinate of the point to test
         * @return {boolean} Whether the x/y coordinates are within this polygon
         */
        contains(x: number, y: number): boolean;
        toString(): string;
    }
}
declare namespace feng3d.pixi {
    /**
     * Size object, contains width and height
     *
     * @typedef {object} ISize
     * @property {number} width - Width component
     * @property {number} height - Height component
     */
    /**
     * Rectangle object is an area defined by its position, as indicated by its top-left corner
     * point (x, y) and by its width and its height.
     *
     */
    class Rectangle {
        x: number;
        y: number;
        width: number;
        height: number;
        readonly type: SHAPES.RECT;
        /**
         * @param {number} [x=0] - The X coordinate of the upper-left corner of the rectangle
         * @param {number} [y=0] - The Y coordinate of the upper-left corner of the rectangle
         * @param {number} [width=0] - The overall width of this rectangle
         * @param {number} [height=0] - The overall height of this rectangle
         */
        constructor(x?: number, y?: number, width?: number, height?: number);
        /**
         * returns the left edge of the rectangle
         *
         * @member {number}
         */
        get left(): number;
        /**
         * returns the right edge of the rectangle
         *
         * @member {number}
         */
        get right(): number;
        /**
         * returns the top edge of the rectangle
         *
         * @member {number}
         */
        get top(): number;
        /**
         * returns the bottom edge of the rectangle
         *
         * @member {number}
         */
        get bottom(): number;
        /**
         * A constant empty rectangle.
         *
         * @static
         * @constant
         * @member {PIXI.Rectangle}
         * @return {PIXI.Rectangle} An empty rectangle
         */
        static get EMPTY(): Rectangle;
        /**
         * Creates a clone of this Rectangle
         *
         * @return {PIXI.Rectangle} a copy of the rectangle
         */
        clone(): Rectangle;
        /**
         * Copies another rectangle to this one.
         *
         * @param {PIXI.Rectangle} rectangle - The rectangle to copy from.
         * @return {PIXI.Rectangle} Returns itself.
         */
        copyFrom(rectangle: Rectangle): Rectangle;
        /**
         * Copies this rectangle to another one.
         *
         * @param {PIXI.Rectangle} rectangle - The rectangle to copy to.
         * @return {PIXI.Rectangle} Returns given parameter.
         */
        copyTo(rectangle: Rectangle): Rectangle;
        /**
         * Checks whether the x and y coordinates given are contained within this Rectangle
         *
         * @param {number} x - The X coordinate of the point to test
         * @param {number} y - The Y coordinate of the point to test
         * @return {boolean} Whether the x/y coordinates are within this Rectangle
         */
        contains(x: number, y: number): boolean;
        /**
         * Pads the rectangle making it grow in all directions.
         * If paddingY is omitted, both paddingX and paddingY will be set to paddingX.
         *
         * @param {number} [paddingX=0] - The horizontal padding amount.
         * @param {number} [paddingY=0] - The vertical padding amount.
         * @return {PIXI.Rectangle} Returns itself.
         */
        pad(paddingX?: number, paddingY?: number): this;
        /**
         * Fits this rectangle around the passed one.
         *
         * @param {PIXI.Rectangle} rectangle - The rectangle to fit.
         * @return {PIXI.Rectangle} Returns itself.
         */
        fit(rectangle: Rectangle): this;
        /**
         * Enlarges rectangle that way its corners lie on grid
         *
         * @param {number} [resolution=1] - resolution
         * @param {number} [eps=0.001] - precision
         * @return {PIXI.Rectangle} Returns itself.
         */
        ceil(resolution?: number, eps?: number): this;
        /**
         * Enlarges this rectangle to include the passed rectangle.
         *
         * @param {PIXI.Rectangle} rectangle - The rectangle to include.
         * @return {PIXI.Rectangle} Returns itself.
         */
        enlarge(rectangle: Rectangle): this;
        toString(): string;
    }
}
declare namespace feng3d.pixi {
    /**
     * The Rounded Rectangle object is an area that has nice rounded corners, as indicated by its
     * top-left corner point (x, y) and by its width and its height and its radius.
     *
     */
    class RoundedRectangle {
        x: number;
        y: number;
        width: number;
        height: number;
        radius: number;
        readonly type: SHAPES.RREC;
        /**
         * @param {number} [x=0] - The X coordinate of the upper-left corner of the rounded rectangle
         * @param {number} [y=0] - The Y coordinate of the upper-left corner of the rounded rectangle
         * @param {number} [width=0] - The overall width of this rounded rectangle
         * @param {number} [height=0] - The overall height of this rounded rectangle
         * @param {number} [radius=20] - Controls the radius of the rounded corners
         */
        constructor(x?: number, y?: number, width?: number, height?: number, radius?: number);
        /**
         * Creates a clone of this Rounded Rectangle
         *
         * @return {PIXI.RoundedRectangle} a copy of the rounded rectangle
         */
        clone(): RoundedRectangle;
        /**
         * Checks whether the x and y coordinates given are contained within this Rounded Rectangle
         *
         * @param {number} x - The X coordinate of the point to test
         * @param {number} y - The Y coordinate of the point to test
         * @return {boolean} Whether the x/y coordinates are within this Rounded Rectangle
         */
        contains(x: number, y: number): boolean;
        toString(): string;
    }
}
declare namespace feng3d.pixi {
    /**
     * Two Pi.
     *
     * @static
     * @member {number}
     */
    const PI_2: number;
    /**
     * Conversion factor for converting radians to degrees.
     *
     * @static
     * @member {number} RAD_TO_DEG
     */
    const RAD_TO_DEG: number;
    /**
     * Conversion factor for converting degrees to radians.
     *
     * @static
     * @member {number}
     */
    const DEG_TO_RAD: number;
    /**
     * Constants that identify shapes, mainly to prevent `instanceof` calls.
     *
     * @static
     * @enum {number}
     * @property {number} POLY Polygon
     * @property {number} RECT Rectangle
     * @property {number} CIRC Circle
     * @property {number} ELIP Ellipse
     * @property {number} RREC Rounded Rectangle
     */
    enum SHAPES {
        POLY = 0,
        RECT = 1,
        CIRC = 2,
        ELIP = 3,
        RREC = 4
    }
}
declare namespace feng3d.pixi {
    type IShape = Circle | Ellipse | Polygon | Rectangle | RoundedRectangle;
    interface ISize {
        width: number;
        height: number;
    }
}
declare namespace feng3d.pixi {
    type GD8Symmetry = number;
    /**
     * @typedef {number} GD8Symmetry
     * @see PIXI.groupD8
     */
    /**
     * Implements the dihedral group D8, which is similar to
     * [group D4]{@link http://mathworld.wolfram.com/DihedralGroupD4.html};
     * D8 is the same but with diagonals, and it is used for texture
     * rotations.
     *
     * The directions the U- and V- axes after rotation
     * of an angle of `a: GD8Constant` are the vectors `(uX(a), uY(a))`
     * and `(vX(a), vY(a))`. These aren't necessarily unit vectors.
     *
     * **Origin:**<br>
     *  This is the small part of gameofbombs.com portal system. It works.
     *
     * @see PIXI.groupD8.E
     * @see PIXI.groupD8.SE
     * @see PIXI.groupD8.S
     * @see PIXI.groupD8.SW
     * @see PIXI.groupD8.W
     * @see PIXI.groupD8.NW
     * @see PIXI.groupD8.N
     * @see PIXI.groupD8.NE
     * @author Ivan @ivanpopelyshev
     * @namespace feng3d.pixi.groupD8
     */
    export const groupD8: {
        /**
         * | Rotation | Direction |
         * |----------|-----------|
         * | 0°       | East      |
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        E: number;
        /**
         * | Rotation | Direction |
         * |----------|-----------|
         * | 45°↻     | Southeast |
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        SE: number;
        /**
         * | Rotation | Direction |
         * |----------|-----------|
         * | 90°↻     | South     |
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        S: number;
        /**
         * | Rotation | Direction |
         * |----------|-----------|
         * | 135°↻    | Southwest |
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        SW: number;
        /**
         * | Rotation | Direction |
         * |----------|-----------|
         * | 180°     | West      |
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        W: number;
        /**
         * | Rotation    | Direction    |
         * |-------------|--------------|
         * | -135°/225°↻ | Northwest    |
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        NW: number;
        /**
         * | Rotation    | Direction    |
         * |-------------|--------------|
         * | -90°/270°↻  | North        |
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        N: number;
        /**
         * | Rotation    | Direction    |
         * |-------------|--------------|
         * | -45°/315°↻  | Northeast    |
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        NE: number;
        /**
         * Reflection about Y-axis.
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        MIRROR_VERTICAL: number;
        /**
         * Reflection about the main diagonal.
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        MAIN_DIAGONAL: number;
        /**
         * Reflection about X-axis.
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        MIRROR_HORIZONTAL: number;
        /**
         * Reflection about reverse diagonal.
         *
         * @memberof PIXI.groupD8
         * @constant {PIXI.GD8Symmetry}
         */
        REVERSE_DIAGONAL: number;
        /**
         * @memberof PIXI.groupD8
         * @param {PIXI.GD8Symmetry} ind - sprite rotation angle.
         * @return {PIXI.GD8Symmetry} The X-component of the U-axis
         *    after rotating the axes.
         */
        uX: (ind: GD8Symmetry) => GD8Symmetry;
        /**
         * @memberof PIXI.groupD8
         * @param {PIXI.GD8Symmetry} ind - sprite rotation angle.
         * @return {PIXI.GD8Symmetry} The Y-component of the U-axis
         *    after rotating the axes.
         */
        uY: (ind: GD8Symmetry) => GD8Symmetry;
        /**
         * @memberof PIXI.groupD8
         * @param {PIXI.GD8Symmetry} ind - sprite rotation angle.
         * @return {PIXI.GD8Symmetry} The X-component of the V-axis
         *    after rotating the axes.
         */
        vX: (ind: GD8Symmetry) => GD8Symmetry;
        /**
         * @memberof PIXI.groupD8
         * @param {PIXI.GD8Symmetry} ind - sprite rotation angle.
         * @return {PIXI.GD8Symmetry} The Y-component of the V-axis
         *    after rotating the axes.
         */
        vY: (ind: GD8Symmetry) => GD8Symmetry;
        /**
         * @memberof PIXI.groupD8
         * @param {PIXI.GD8Symmetry} rotation - symmetry whose opposite
         *   is needed. Only rotations have opposite symmetries while
         *   reflections don't.
         * @return {PIXI.GD8Symmetry} The opposite symmetry of `rotation`
         */
        inv: (rotation: GD8Symmetry) => GD8Symmetry;
        /**
         * Composes the two D8 operations.
         *
         * Taking `^` as reflection:
         *
         * |       | E=0 | S=2 | W=4 | N=6 | E^=8 | S^=10 | W^=12 | N^=14 |
         * |-------|-----|-----|-----|-----|------|-------|-------|-------|
         * | E=0   | E   | S   | W   | N   | E^   | S^    | W^    | N^    |
         * | S=2   | S   | W   | N   | E   | S^   | W^    | N^    | E^    |
         * | W=4   | W   | N   | E   | S   | W^   | N^    | E^    | S^    |
         * | N=6   | N   | E   | S   | W   | N^   | E^    | S^    | W^    |
         * | E^=8  | E^  | N^  | W^  | S^  | E    | N     | W     | S     |
         * | S^=10 | S^  | E^  | N^  | W^  | S    | E     | N     | W     |
         * | W^=12 | W^  | S^  | E^  | N^  | W    | S     | E     | N     |
         * | N^=14 | N^  | W^  | S^  | E^  | N    | W     | S     | E     |
         *
         * [This is a Cayley table]{@link https://en.wikipedia.org/wiki/Cayley_table}
         * @memberof PIXI.groupD8
         * @param {PIXI.GD8Symmetry} rotationSecond - Second operation, which
         *   is the row in the above cayley table.
         * @param {PIXI.GD8Symmetry} rotationFirst - First operation, which
         *   is the column in the above cayley table.
         * @return {PIXI.GD8Symmetry} Composed operation
         */
        add: (rotationSecond: GD8Symmetry, rotationFirst: GD8Symmetry) => GD8Symmetry;
        /**
         * Reverse of `add`.
         *
         * @memberof PIXI.groupD8
         * @param {PIXI.GD8Symmetry} rotationSecond - Second operation
         * @param {PIXI.GD8Symmetry} rotationFirst - First operation
         * @return {PIXI.GD8Symmetry} Result
         */
        sub: (rotationSecond: GD8Symmetry, rotationFirst: GD8Symmetry) => GD8Symmetry;
        /**
         * Adds 180 degrees to rotation, which is a commutative
         * operation.
         *
         * @memberof PIXI.groupD8
         * @param {number} rotation - The number to rotate.
         * @returns {number} Rotated number
         */
        rotate180: (rotation: number) => number;
        /**
         * Checks if the rotation angle is vertical, i.e. south
         * or north. It doesn't work for reflections.
         *
         * @memberof PIXI.groupD8
         * @param {PIXI.GD8Symmetry} rotation - The number to check.
         * @returns {boolean} Whether or not the direction is vertical
         */
        isVertical: (rotation: GD8Symmetry) => boolean;
        /**
         * Approximates the vector `V(dx,dy)` into one of the
         * eight directions provided by `groupD8`.
         *
         * @memberof PIXI.groupD8
         * @param {number} dx - X-component of the vector
         * @param {number} dy - Y-component of the vector
         * @return {PIXI.GD8Symmetry} Approximation of the vector into
         *  one of the eight symmetries.
         */
        byDirection: (dx: number, dy: number) => GD8Symmetry;
        /**
         * Helps sprite to compensate texture packer rotation.
         *
         * @memberof PIXI.groupD8
         * @param {PIXI.Matrix} matrix - sprite world matrix
         * @param {PIXI.GD8Symmetry} rotation - The rotation factor to use.
         * @param {number} tx - sprite anchoring
         * @param {number} ty - sprite anchoring
         */
        matrixAppendRotationInv: (matrix: Matrix, rotation: GD8Symmetry, tx?: number, ty?: number) => void;
    };
    export {};
}
declare namespace feng3d.pixi {
    /**
     * Represents the update priorities used by internal PIXI classes when registered with
     * the {@link PIXI.Ticker} object. Higher priority items are updated first and lower
     * priority items, such as render, should go later.
     *
     * @static
     * @constant
     * @name UPDATE_PRIORITY
     * @enum {number}
     * @property {number} INTERACTION=50 Highest priority, used for {@link PIXI.InteractionManager}
     * @property {number} HIGH=25 High priority updating, {@link PIXI.VideoBaseTexture} and {@link PIXI.AnimatedSprite}
     * @property {number} NORMAL=0 Default priority for ticker events, see {@link PIXI.Ticker#add}.
     * @property {number} LOW=-25 Low priority used for {@link PIXI.Application} rendering.
     * @property {number} UTILITY=-50 Lowest priority used for {@link PIXI.BasePrepare} utility.
     */
    enum UPDATE_PRIORITY {
        INTERACTION = 50,
        HIGH = 25,
        NORMAL = 0,
        LOW = -25,
        UTILITY = -50
    }
    interface Application {
        ticker: Ticker;
        stop(): void;
        start(): void;
    }
    interface IApplicationOptions {
        autoStart?: boolean;
        sharedTicker?: boolean;
    }
}
declare namespace feng3d.pixi {
}
declare namespace feng3d.pixi {
    type TickerCallback<T> = (this: T, dt: number) => any;
    /**
     * A Ticker class that runs an update loop that other objects listen to.
     *
     * This class is composed around listeners meant for execution on the next requested animation frame.
     * Animation frames are requested only when necessary, e.g. When the ticker is started and the emitter has listeners.
     *
     */
    class Ticker {
        /** The private shared ticker instance */
        private static _shared;
        /** The private system ticker instance  */
        private static _system;
        /**
         * Whether or not this ticker should invoke the method
         * {@link PIXI.Ticker#start} automatically
         * when a listener is added.
         */
        autoStart: boolean;
        /**
         * Scalar time value from last frame to this frame.
         * This value is capped by setting {@link PIXI.Ticker#minFPS}
         * and is scaled with {@link PIXI.Ticker#speed}.
         * **Note:** The cap may be exceeded by scaling.
         */
        deltaTime: number;
        /**
         * Scaler time elapsed in milliseconds from last frame to this frame.
         * This value is capped by setting {@link PIXI.Ticker#minFPS}
         * and is scaled with {@link PIXI.Ticker#speed}.
         * **Note:** The cap may be exceeded by scaling.
         * If the platform supports DOMHighResTimeStamp,
         * this value will have a precision of 1 µs.
         * Defaults to target frame time
         * @default 16.66
         */
        deltaMS: number;
        /**
         * Time elapsed in milliseconds from last frame to this frame.
         * Opposed to what the scalar {@link PIXI.Ticker#deltaTime}
         * is based, this value is neither capped nor scaled.
         * If the platform supports DOMHighResTimeStamp,
         * this value will have a precision of 1 µs.
         * Defaults to target frame time
         * @default 16.66
         */
        elapsedMS: number;
        /**
         * The last time {@link PIXI.Ticker#update} was invoked.
         * This value is also reset internally outside of invoking
         * update, but only when a new animation frame is requested.
         * If the platform supports DOMHighResTimeStamp,
         * this value will have a precision of 1 µs.
         */
        lastTime: number;
        /**
         * Factor of current {@link PIXI.Ticker#deltaTime}.
         * @example
         * // Scales ticker.deltaTime to what would be
         * // the equivalent of approximately 120 FPS
         * ticker.speed = 2;
         */
        speed: number;
        /**
         * Whether or not this ticker has been started.
         * `true` if {@link PIXI.Ticker#start} has been called.
         * `false` if {@link PIXI.Ticker#stop} has been called.
         * While `false`, this value may change to `true` in the
         * event of {@link PIXI.Ticker#autoStart} being `true`
         * and a listener is added.
         */
        started: boolean;
        /** The first listener. All new listeners added are chained on this. */
        private _head;
        /** Internal current frame request ID */
        private _requestId;
        /**
         * Internal value managed by minFPS property setter and getter.
         * This is the maximum allowed milliseconds between updates.
         */
        private _maxElapsedMS;
        /**
         * Internal value managed by minFPS property setter and getter.
         * This is the maximum allowed milliseconds between updates.
         */
        private _minElapsedMS;
        /** If enabled, deleting is disabled.*/
        private _protected;
        /**
         * The last time keyframe was executed.
         * Maintains a relatively fixed interval with the previous value.
         */
        private _lastFrame;
        /**
         * Internal tick method bound to ticker instance.
         * This is because in early 2015, Function.bind
         * is still 60% slower in high performance scenarios.
         * Also separating frame requests from update method
         * so listeners may be called at any time and with
         * any animation API, just invoke ticker.update(time).
         *
         * @param time - Time since last tick.
         */
        private _tick;
        constructor();
        /**
         * Conditionally requests a new animation frame.
         * If a frame has not already been requested, and if the internal
         * emitter has listeners, a new frame is requested.
         *
         * @private
         */
        private _requestIfNeeded;
        /**
         * Conditionally cancels a pending animation frame.
         * @private
         */
        private _cancelIfNeeded;
        /**
         * Conditionally requests a new animation frame.
         * If the ticker has been started it checks if a frame has not already
         * been requested, and if the internal emitter has listeners. If these
         * conditions are met, a new frame is requested. If the ticker has not
         * been started, but autoStart is `true`, then the ticker starts now,
         * and continues with the previous conditions to request a new frame.
         *
         * @private
         */
        private _startIfPossible;
        /**
         * Register a handler for tick events. Calls continuously unless
         * it is removed or the ticker is stopped.
         *
         * @param fn - The listener function to be added for updates
         * @param context - The listener context
         * @param {number} [priority=PIXI.UPDATE_PRIORITY.NORMAL] - The priority for emitting
         * @returns This instance of a ticker
         */
        add<T = any>(fn: TickerCallback<T>, context?: T, priority?: UPDATE_PRIORITY): this;
        /**
         * Add a handler for the tick event which is only execute once.
         *
         * @param fn - The listener function to be added for one update
         * @param context - The listener context
         * @param {number} [priority=PIXI.UPDATE_PRIORITY.NORMAL] - The priority for emitting
         * @returns This instance of a ticker
         */
        addOnce<T = any>(fn: TickerCallback<T>, context?: T, priority?: UPDATE_PRIORITY): this;
        /**
         * Internally adds the event handler so that it can be sorted by priority.
         * Priority allows certain handler (user, AnimatedSprite, Interaction) to be run
         * before the rendering.
         *
         * @private
         * @param listener - Current listener being added.
         * @returns This instance of a ticker
         */
        private _addListener;
        /**
         * Removes any handlers matching the function and context parameters.
         * If no handlers are left after removing, then it cancels the animation frame.
         *
         * @param fn - The listener function to be removed
         * @param context - The listener context to be removed
         * @returns This instance of a ticker
         */
        remove<T = any>(fn: TickerCallback<T>, context?: T): this;
        /**
         * The number of listeners on this ticker, calculated by walking through linked list
         *
         * @readonly
         * @member {number}
         */
        get count(): number;
        /**
         * Starts the ticker. If the ticker has listeners
         * a new animation frame is requested at this point.
         */
        start(): void;
        /**
         * Stops the ticker. If the ticker has requested
         * an animation frame it is canceled at this point.
         */
        stop(): void;
        /**
         * Destroy the ticker and don't use after this. Calling
         * this method removes all references to internal events.
         */
        destroy(): void;
        /**
         * Triggers an update. An update entails setting the
         * current {@link PIXI.Ticker#elapsedMS},
         * the current {@link PIXI.Ticker#deltaTime},
         * invoking all listeners with current deltaTime,
         * and then finally setting {@link PIXI.Ticker#lastTime}
         * with the value of currentTime that was provided.
         * This method will be called automatically by animation
         * frame callbacks if the ticker instance has been started
         * and listeners are added.
         *
         * @param {number} [currentTime=performance.now()] - the current time of execution
         */
        update(currentTime?: number): void;
        /**
         * The frames per second at which this ticker is running.
         * The default is approximately 60 in most modern browsers.
         * **Note:** This does not factor in the value of
         * {@link PIXI.Ticker#speed}, which is specific
         * to scaling {@link PIXI.Ticker#deltaTime}.
         *
         * @member {number}
         * @readonly
         */
        get FPS(): number;
        /**
         * Manages the maximum amount of milliseconds allowed to
         * elapse between invoking {@link PIXI.Ticker#update}.
         * This value is used to cap {@link PIXI.Ticker#deltaTime},
         * but does not effect the measured value of {@link PIXI.Ticker#FPS}.
         * When setting this property it is clamped to a value between
         * `0` and `PIXI.settings.TARGET_FPMS * 1000`.
         *
         * @member {number}
         * @default 10
         */
        get minFPS(): number;
        set minFPS(fps: number);
        /**
         * Manages the minimum amount of milliseconds required to
         * elapse between invoking {@link PIXI.Ticker#update}.
         * This will effect the measured value of {@link PIXI.Ticker#FPS}.
         * If it is set to `0`, then there is no limit; PixiJS will render as many frames as it can.
         * Otherwise it will be at least `minFPS`
         *
         * @member {number}
         * @default 0
         */
        get maxFPS(): number;
        set maxFPS(fps: number);
        /**
         * The shared ticker instance used by {@link PIXI.AnimatedSprite} and by
         * {@link PIXI.VideoResource} to update animation frames / video textures.
         *
         * It may also be used by {@link PIXI.Application} if created with the `sharedTicker` option property set to true.
         *
         * The property {@link PIXI.Ticker#autoStart} is set to `true` for this instance.
         * Please follow the examples for usage, including how to opt-out of auto-starting the shared ticker.
         *
         * @example
         * let ticker = PIXI.Ticker.shared;
         * // Set this to prevent starting this ticker when listeners are added.
         * // By default this is true only for the PIXI.Ticker.shared instance.
         * ticker.autoStart = false;
         * // FYI, call this to ensure the ticker is stopped. It should be stopped
         * // if you have not attempted to render anything yet.
         * ticker.stop();
         * // Call this when you are ready for a running shared ticker.
         * ticker.start();
         *
         * @example
         * // You may use the shared ticker to render...
         * let renderer = PIXI.autoDetectRenderer();
         * let stage = new PIXI.Container();
         * document.body.appendChild(renderer.view);
         * ticker.add(function (time) {
         *     renderer.render(stage);
         * });
         *
         * @example
         * // Or you can just update it manually.
         * ticker.autoStart = false;
         * ticker.stop();
         * function animate(time) {
         *     ticker.update(time);
         *     renderer.render(stage);
         *     requestAnimationFrame(animate);
         * }
         * animate(performance.now());
         *
         * @member {PIXI.Ticker}
         * @static
         */
        static get shared(): Ticker;
        /**
         * The system ticker instance used by {@link PIXI.InteractionManager} and by
         * {@link PIXI.BasePrepare} for core timing functionality that shouldn't usually need to be paused,
         * unlike the `shared` ticker which drives visual animations and rendering which may want to be paused.
         *
         * The property {@link PIXI.Ticker#autoStart} is set to `true` for this instance.
         *
         * @member {PIXI.Ticker}
         * @static
         */
        static get system(): Ticker;
    }
}
declare namespace feng3d.pixi {
    /**
     * Internal class for handling the priority sorting of ticker handlers.
     *
     * @private
     */
    class TickerListener<T = any> {
        /** The current priority. */
        priority: number;
        /** The next item in chain. */
        next: TickerListener;
        /** The previous item in chain. */
        previous: TickerListener;
        /** The handler function to execute. */
        private fn;
        /** The calling to execute. */
        private context;
        /** If this should only execute once. */
        private once;
        /** `true` if this listener has been destroyed already. */
        private _destroyed;
        /**
         * Constructor
         * @private
         * @param fn - The listener function to be added for one update
         * @param context - The listener context
         * @param priority - The priority for emitting
         * @param once - If the handler should fire once
         */
        constructor(fn: TickerCallback<T>, context?: T, priority?: number, once?: boolean);
        /**
         * Simple compare function to figure out if a function and context match.
         * @private
         * @param fn - The listener function to be added for one update
         * @param context - The listener context
         * @return `true` if the listener match the arguments
         */
        match(fn: TickerCallback<T>, context?: any): boolean;
        /**
         * Emit by calling the current function.
         * @private
         * @param deltaTime - time since the last emit.
         * @return Next ticker
         */
        emit(deltaTime: number): TickerListener;
        /**
         * Connect to the list.
         * @private
         * @param previous - Input node, previous listener
         */
        connect(previous: TickerListener): void;
        /**
         * Destroy and don't use after this.
         * @private
         * @param hard - `true` to remove the `next` reference, this
         *        is considered a hard destroy. Soft destroy maintains the next reference.
         * @return The listener to redirect while emitting or removing.
         */
        destroy(hard?: boolean): TickerListener;
    }
}
declare namespace feng3d.pixi {
    /**
     * Middleware for for Application Ticker.
     *
     * @example
     * import {TickerPlugin} from '@pixi/ticker';
     * import {Application} from '@pixi/app';
     * Application.registerPlugin(TickerPlugin);
     *
     */
    class TickerPlugin {
        static start: () => void;
        static stop: () => void;
        static _ticker: Ticker;
        static ticker: Ticker;
        /**
         * Initialize the plugin with scope of application instance
         *
         * @static
         * @private
         * @param {object} [options] - See application options
         */
        static init(options?: IApplicationOptions): void;
        /**
         * Clean up the ticker, scoped to application.
         *
         * @static
         * @private
         */
        static destroy(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * 'Builder' pattern for bounds rectangles.
     *
     * This could be called an Axis-Aligned Bounding Box.
     * It is not an actual shape. It is a mutable thing; no 'EMPTY' or those kind of problems.
     *
     */
    class Bounds {
        minX: number;
        minY: number;
        maxX: number;
        maxY: number;
        rect: Rectangle;
        updateID: number;
        constructor();
        /**
         * Checks if bounds are empty.
         *
         * @return {boolean} True if empty.
         */
        isEmpty(): boolean;
        /**
         * Clears the bounds and resets.
         *
         */
        clear(): void;
        /**
         * Can return Rectangle.EMPTY constant, either construct new rectangle, either use your rectangle
         * It is not guaranteed that it will return tempRect
         *
         * @param {PIXI.Rectangle} rect - temporary object will be used if AABB is not empty
         * @returns {PIXI.Rectangle} A rectangle of the bounds
         */
        getRectangle(rect?: Rectangle): Rectangle;
        /**
         * This function should be inlined when its possible.
         *
         * @param {PIXI.IPointData} point - The point to add.
         */
        addPoint(point: Vector2): void;
        /**
         * Adds a point, after transformed. This should be inlined when its possible.
         *
         * @param matrix
         * @param point
         */
        addPointMatrix(matrix: Matrix, point: Vector2): void;
        /**
         * Adds a quad, not transformed
         *
         * @param {Float32Array} vertices - The verts to add.
         */
        addQuad(vertices: Float32Array): void;
        /**
         * Adds sprite frame, transformed.
         *
         * @param {PIXI.Transform} transform - transform to apply
         * @param {number} x0 - left X of frame
         * @param {number} y0 - top Y of frame
         * @param {number} x1 - right X of frame
         * @param {number} y1 - bottom Y of frame
         */
        addFrame(transform: Transform, x0: number, y0: number, x1: number, y1: number): void;
        /**
         * Adds sprite frame, multiplied by matrix
         *
         * @param {PIXI.Matrix} matrix - matrix to apply
         * @param {number} x0 - left X of frame
         * @param {number} y0 - top Y of frame
         * @param {number} x1 - right X of frame
         * @param {number} y1 - bottom Y of frame
         */
        addFrameMatrix(matrix: Matrix, x0: number, y0: number, x1: number, y1: number): void;
        /**
         * Adds screen vertices from array
         *
         * @param {Float32Array} vertexData - calculated vertices
         * @param {number} beginOffset - begin offset
         * @param {number} endOffset - end offset, excluded
         */
        addVertexData(vertexData: Float32Array, beginOffset: number, endOffset: number): void;
        /**
         * Add an array of mesh vertices
         *
         * @param {PIXI.Transform} transform - mesh transform
         * @param {Float32Array} vertices - mesh coordinates in array
         * @param {number} beginOffset - begin offset
         * @param {number} endOffset - end offset, excluded
         */
        addVertices(transform: Transform, vertices: Float32Array, beginOffset: number, endOffset: number): void;
        /**
         * Add an array of mesh vertices.
         *
         * @param {PIXI.Matrix} matrix - mesh matrix
         * @param {Float32Array} vertices - mesh coordinates in array
         * @param {number} beginOffset - begin offset
         * @param {number} endOffset - end offset, excluded
         * @param {number} [padX=0] - x padding
         * @param {number} [padY=0] - y padding
         */
        addVerticesMatrix(matrix: Matrix, vertices: Float32Array, beginOffset: number, endOffset: number, padX?: number, padY?: number): void;
        /**
         * Adds other Bounds.
         *
         * @param {PIXI.Bounds} bounds - The Bounds to be added
         */
        addBounds(bounds: Bounds): void;
        /**
         * Adds other Bounds, masked with Bounds.
         *
         * @param {PIXI.Bounds} bounds - The Bounds to be added.
         * @param {PIXI.Bounds} mask - TODO
         */
        addBoundsMask(bounds: Bounds, mask: Bounds): void;
        /**
         * Adds other Bounds, multiplied by matrix. Bounds shouldn't be empty.
         *
         * @param {PIXI.Bounds} bounds - other bounds
         * @param {PIXI.Matrix} matrix - multiplicator
         */
        addBoundsMatrix(bounds: Bounds, matrix: Matrix): void;
        /**
         * Adds other Bounds, masked with Rectangle.
         *
         * @param {PIXI.Bounds} bounds - TODO
         * @param {PIXI.Rectangle} area - TODO
         */
        addBoundsArea(bounds: Bounds, area: Rectangle): void;
        /**
         * Pads bounds object, making it grow in all directions.
         * If paddingY is omitted, both paddingX and paddingY will be set to paddingX.
         *
         * @param {number} [paddingX=0] - The horizontal padding amount.
         * @param {number} [paddingY=0] - The vertical padding amount.
         */
        pad(paddingX?: number, paddingY?: number): void;
        /**
         * Adds padded frame. (x0, y0) should be strictly less than (x1, y1)
         *
         * @param {number} x0 - left X of frame
         * @param {number} y0 - top Y of frame
         * @param {number} x1 - right X of frame
         * @param {number} y1 - bottom Y of frame
         * @param {number} padX - padding X
         * @param {number} padY - padding Y
         */
        addFramePad(x0: number, y0: number, x1: number, y1: number, padX: number, padY: number): void;
    }
}
declare namespace feng3d.pixi {
    class Container extends feng3d.Node2D {
        static create(name?: string): Container;
        parent: Container;
        children: Container[];
        private _mask;
        /**
         * Sets a mask for the displayObject. A mask is an object that limits the visibility of an
         * object to the shape of the mask applied to it. In PixiJS a regular mask must be a
         * {@link PIXI.Graphics} or a {@link PIXI.Sprite} object. This allows for much faster masking in canvas as it
         * utilities shape clipping. To remove a mask, set this property to `null`.
         *
         * For sprite mask both alpha and red channel are used. Black mask is the same as transparent mask.
         *
         * @example
         * const graphics = new PIXI.Graphics();
         * graphics.beginFill(0xFF3300);
         * graphics.drawRect(50, 250, 100, 100);
         * graphics.endFill();
         *
         * const sprite = new PIXI.Sprite(texture);
         * sprite.mask = graphics;
         *
         * @todo At the moment, PIXI.CanvasRenderer doesn't support PIXI.Sprite as mask.
         * @member {PIXI.Container|PIXI.MaskData|null}
         */
        get mask(): Container | MaskData | null;
        set mask(value: Container | MaskData | null);
        constructor();
        /**
         * @protected
         * @member {PIXI.Container}
         */
        get _tempDisplayObjectParent(): Node2D<Component2DEventMap>;
        /**
         * Can this object be rendered, if false the object will not be drawn but the updateTransform
         * methods will still be called.
         *
         * Only affects recursive calls from parent. You can ask for bounds manually.
         *
         * @member {boolean}
         */
        renderable: boolean;
        /**
         * The area the filter is applied to. This is used as more of an optimization
         * rather than figuring out the dimensions of the displayObject each frame you can set this rectangle.
         *
         * Also works as an interaction mask.
         *
         * @member {?PIXI.Rectangle}
         */
        filterArea: pixi.Rectangle;
        /**
         * Sets the filters for the displayObject.
         * * IMPORTANT: This is a WebGL only feature and will be ignored by the canvas renderer.
         * To remove filters simply set this property to `'null'`.
         *
         * @member {?PIXI.Filter[]}
         */
        filters: pixi.Filter[];
        /**
         * used to fast check if a sprite is.. a sprite!
         * @member {boolean}
         */
        isSprite: boolean;
        /**
         * Does any other displayObject use this object as a mask?
         * @member {boolean}
         */
        isMask: boolean;
        /**
         * Which index in the children array the display component was before the previous zIndex sort.
         * Used by containers to help sort objects with the same zIndex, by using previous array index as the decider.
         *
         * @member {number}
         * @protected
         */
        _lastSortedIndex: number;
        /**
         * The bounds object, this is used to calculate and store the bounds of the displayObject.
         *
         * @member {PIXI.Bounds}
         */
        _bounds: Bounds;
        /**
         * Local bounds object, swapped with `_bounds` when using `getLocalBounds()`.
         *
         * @member {PIXI.Bounds}
         */
        _localBounds: pixi.Bounds;
        /**
         * If set to true, the container will sort its children by zIndex value
         * when updateTransform() is called, or manually if sortChildren() is called.
         *
         * This actually changes the order of elements in the array, so should be treated
         * as a basic solution that is not performant compared to other solutions,
         * such as @link https://github.com/pixijs/pixi-display
         *
         * Also be aware of that this may not work nicely with the addChildAt() function,
         * as the zIndex sorting may cause the child to automatically sorted to another position.
         *
         * @see PIXI.settings.SORTABLE_CHILDREN
         *
         * @member {boolean}
         */
        sortableChildren: boolean;
        /**
         * Should children be sorted by zIndex at the next updateTransform call.
         *
         * Will get automatically set to true if a new child is added, or if a child's zIndex changes.
         *
         * @member {boolean}
         */
        sortDirty: boolean;
        containerUpdateTransform: () => void;
        /**
         * The zIndex of the displayObject.
         *
         * If a container has the sortableChildren property set to true, children will be automatically
         * sorted by zIndex value; a higher value will mean it will be moved towards the end of the array,
         * and thus rendered on top of other display objects within the same container.
         *
         * @member {number}
         * @see PIXI.Container#sortableChildren
         */
        get zIndex(): number;
        set zIndex(value: number);
        /**
         * The zIndex of the displayObject.
         * A higher value will mean it will be rendered on top of other displayObjects within the same container.
         *
         * @member {number}
         * @protected
         */
        protected _zIndex: number;
        get width(): number;
        set width(value: number);
        get height(): number;
        set height(value: number);
        _width: number;
        _height: number;
        /**
         * Currently enabled filters
         * @member {PIXI.Filter[]}
         * @protected
         */
        protected _enabledFilters: pixi.Filter[];
        /**
         * Flags the cached bounds as dirty.
         *
         * @member {number}
         * @protected
         */
        protected _boundsID: number;
        /**
         * Cache of this display-object's bounds-rectangle.
         *
         * @member {PIXI.Bounds}
         * @protected
         */
        protected _boundsRect: pixi.Rectangle;
        /**
         * Cache of this display-object's local-bounds rectangle.
         *
         * @member {PIXI.Bounds}
         * @protected
         */
        protected _localBoundsRect: pixi.Rectangle;
        /**
         * Calculates and returns the (world) bounds of the display object as a [Rectangle]{@link PIXI.Rectangle}.
         *
         * This method is expensive on containers with a large subtree (like the stage). This is because the bounds
         * of a container depend on its children's bounds, which recursively causes all bounds in the subtree to
         * be recalculated. The upside, however, is that calling `getBounds` once on a container will indeed update
         * the bounds of all children (the whole subtree, in fact). This side effect should be exploited by using
         * `displayObject._bounds.getRectangle()` when traversing through all the bounds in a scene graph. Otherwise,
         * calling `getBounds` on each object in a subtree will cause the total cost to increase quadratically as
         * its height increases.
         *
         * * The transforms of all objects in a container's **subtree** and of all **ancestors** are updated.
         * * The world bounds of all display objects in a container's **subtree** will also be recalculated.
         *
         * The `_bounds` object stores the last calculation of the bounds. You can use to entirely skip bounds
         * calculation if needed.
         *
         * ```js
         * const lastCalculatedBounds = displayObject._bounds.getRectangle(optionalRect);
         * ```
         *
         * Do know that usage of `getLocalBounds` can corrupt the `_bounds` of children (the whole subtree, actually). This
         * is a known issue that has not been solved. See [getLocalBounds]{@link feng3d.Node2D#getLocalBounds} for more
         * details.
         *
         * `getBounds` should be called with `skipUpdate` equal to `true` in a render() call. This is because the transforms
         * are guaranteed to be update-to-date. In fact, recalculating inside a render() call may cause corruption in certain
         * cases.
         *
         * @param {boolean} [skipUpdate] - Setting to `true` will stop the transforms of the scene graph from
         *  being updated. This means the calculation returned MAY be out of date BUT will give you a
         *  nice performance boost.
         * @param {PIXI.Rectangle} [rect] - Optional rectangle to store the result of the bounds calculation.
         * @return {PIXI.Rectangle} The minimum axis-aligned rectangle in world space that fits around this object.
         */
        getBounds(skipUpdate?: boolean, rect?: Rectangle): Rectangle;
        /**
         * Used in Renderer, cacheAsBitmap and other places where you call an `updateTransform` on root
         *
         * ```
         * const cacheParent = elem.enableTempParent();
         * elem.updateTransform();
         * elem.disableTempParent(cacheParent);
         * ```
         *
         * @returns {feng3d.Node2D} current parent
         */
        enableTempParent(): Container;
        /**
         * Pair method for `enableTempParent`
         *
         * @param {feng3d.Node2D} cacheParent - Actual parent of element
         */
        disableTempParent(cacheParent: Container): void;
        /**
         * Sorts children by zIndex. Previous order is maintained for 2 children with the same zIndex.
         */
        sortChildren(): void;
        updateTransform(): void;
        /**
         * Recalculates the bounds of the container.
         *
         * This implementation will automatically fit the children's bounds into the calculation. Each child's bounds
         * is limited to its mask's bounds or filterArea, if any is applied.
         */
        calculateBounds(): void;
        /**
         * Retrieves the local bounds of the displayObject as a rectangle object.
         *
         * Calling `getLocalBounds` may invalidate the `_bounds` of the whole subtree below. If using it inside a render()
         * call, it is advised to call `getBounds()` immediately after to recalculate the world bounds of the subtree.
         *
         * @param {PIXI.Rectangle} [rect] - Optional rectangle to store the result of the bounds calculation.
         * @param {boolean} [skipChildrenUpdate=false] - Setting to `true` will stop re-calculation of children transforms,
         *  it was default behaviour of pixi 4.0-5.2 and caused many problems to users.
         * @return {PIXI.Rectangle} The rectangular bounding area.
         */
        getLocalBounds(rect?: Rectangle, skipChildrenUpdate?: boolean): Rectangle;
        /**
         * Recalculates the content bounds of this object. This should be overriden to
         * calculate the bounds of this specific object (not including children).
         *
         * @protected
         */
        protected _calculateBounds(): void;
        /**
         * Renders the object using the WebGL renderer.
         *
         * The [_render]{@link PIXI.Container#_render} method is be overriden for rendering the contents of the
         * container itself. This `render` method will invoke it, and also invoke the `render` methods of all
         * children afterward.
         *
         * If `renderable` or `visible` is false or if `worldAlpha` is not positive, this implementation will entirely
         * skip rendering. See {@link feng3d.Node2D} for choosing between `renderable` or `visible`. Generally,
         * setting alpha to zero is not recommended for purely skipping rendering.
         *
         * When your scene becomes large (especially when it is larger than can be viewed in a single screen), it is
         * advised to employ **culling** to automatically skip rendering objects outside of the current screen. The
         * [@pixi-essentials/cull]{@link https://www.npmjs.com/package/@pixi-essentials/cull} and
         * [pixi-cull]{@link https://www.npmjs.com/package/pixi-cull} packages do this out of the box.
         *
         * The [renderAdvanced]{@link PIXI.Container#renderAdvanced} method is internally used when when masking or
         * filtering is applied on a container. This does, however, break batching and can affect performance when
         * masking and filtering is applied extensively throughout the scene graph.
         *
         * @param {PIXI.Renderer} renderer - The renderer
         */
        render(renderer: Renderer): void;
        /**
         * Render the object using the WebGL renderer and advanced features.
         *
         * @protected
         * @param {PIXI.Renderer} renderer - The renderer
         */
        renderAdvanced(renderer: Renderer): void;
        /**
         *
         * @param {PIXI.Renderer} renderer - The renderer
         */
        _render(_renderer: Renderer): void;
        /**
         * Removes all internal references and listeners as well as removes children from the display list.
         * Do not use a Container after calling `destroy`.
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
         *  have been set to that value
         * @param {boolean} [options.children=false] - if set to true, all the children will have their destroy
         *  method called as well. 'options' will be passed on to those calls.
         * @param {boolean} [options.texture=false] - Only used for child Sprites if options.children is set to true
         *  Should it destroy the texture of the child sprite
         * @param {boolean} [options.baseTexture=false] - Only used for child Sprites if options.children is set to true
         *  Should it destroy the base texture of the child sprite
         */
        destroy(options?: IDestroyOptions | boolean): void;
        containsPoint(point: Vector2): boolean;
        /**
         * Mixes all enumerable properties and methods from a source object to Node2D.
         *
         * @param {object} source - The source of properties and methods to mix in.
         */
        static mixin(source: pixi.Dict<any>): void;
    }
}
declare namespace feng3d.pixi {
}
declare namespace feng3d.pixi {
    type InteractivePointerEvent = PointerEvent | TouchEvent | MouseEvent;
    /**
     * Holds all information related to an Interaction event
     *
     */
    class InteractionData {
        global: Vector2;
        target: Container;
        originalEvent: InteractivePointerEvent;
        identifier: number;
        isPrimary: boolean;
        button: number;
        buttons: number;
        width: number;
        height: number;
        tiltX: number;
        tiltY: number;
        pointerType: string;
        pressure: number;
        rotationAngle: number;
        twist: number;
        tangentialPressure: number;
        constructor();
        /**
         * The unique identifier of the pointer. It will be the same as `identifier`.
         * @readonly
         * @member {number}
         * @see https://developer.mozilla.org/en-US/docs/Web/API/PointerEvent/pointerId
         */
        get pointerId(): number;
        /**
         * This will return the local coordinates of the specified displayObject for this InteractionData
         *
         * @param {feng3d.Node2D} displayObject - The Node2D that you would like the local
         *  coords off
         * @param {PIXI.Point} [point] - A Point object in which to store the value, optional (otherwise
         *  will create a new point)
         * @param {PIXI.Point} [globalPos] - A Point object containing your custom global coords, optional
         *  (otherwise will use the current global coords)
         * @return {PIXI.Point} A point containing the coordinates of the InteractionData position relative
         *  to the Node2D
         */
        getLocalPosition(displayObject: Container, point?: Vector2, globalPos?: Vector2): Vector2;
        /**
         * Copies properties from normalized event data.
         *
         * @param {Touch|MouseEvent|PointerEvent} event - The normalized event data
         */
        copyEvent(event: Touch | InteractivePointerEvent): void;
        /**
         * Resets the data for pooling.
         */
        reset(): void;
    }
}
declare namespace feng3d.pixi {
    type InteractionCallback = (interactionEvent: InteractionEvent, displayObject: Container, hit?: boolean) => void;
    /**
     * Event class that mimics native DOM events.
     *
     */
    class InteractionEvent {
        stopped: boolean;
        stopsPropagatingAt: Container;
        stopPropagationHint: boolean;
        target: Container;
        currentTarget: Container;
        type: string;
        data: InteractionData;
        constructor();
        /**
         * Prevents event from reaching any objects other than the current object.
         *
         */
        stopPropagation(): void;
        /**
         * Resets the event.
         */
        reset(): void;
    }
}
declare namespace feng3d.pixi {
    interface Container extends Partial<pixi.InteractiveTarget> {
    }
    type Cursor = 'auto' | 'default' | 'none' | 'context-menu' | 'help' | 'pointer' | 'progress' | 'wait' | 'cell' | 'crosshair' | 'text' | 'vertical-text' | 'alias' | 'copy' | 'move' | 'no-drop' | 'not-allowed' | 'e-resize' | 'n-resize' | 'ne-resize' | 'nw-resize' | 's-resize' | 'se-resize' | 'sw-resize' | 'w-resize' | 'ns-resize' | 'ew-resize' | 'nesw-resize' | 'col-resize' | 'nwse-resize' | 'row-resize' | 'all-scroll' | 'zoom-in' | 'zoom-out' | 'grab' | 'grabbing';
    interface IHitArea {
        contains(x: number, y: number): boolean;
    }
    interface InteractiveTarget {
        interactive: boolean;
        interactiveChildren: boolean;
        hitArea: IHitArea;
        cursor: Cursor | string;
        buttonMode: boolean;
        trackedPointers: {
            [x: number]: InteractionTrackingData;
        };
        _trackedPointers: {
            [x: number]: InteractionTrackingData;
        };
    }
    /**
     * Interface for classes that represent a hit area.
     *
     * It is implemented by the following classes:
     * - {@link PIXI.Circle}
     * - {@link PIXI.Ellipse}
     * - {@link PIXI.Polygon}
     * - {@link PIXI.RoundedRectangle}
     *
     * @interface IHitArea
     */
    /**
     * Checks whether the x and y coordinates given are contained within this area
     *
     * @method
     * @name contains.IHitArea#
     * @param {number} x - The X coordinate of the point to test
     * @param {number} y - The Y coordinate of the point to test
     * @return {boolean} Whether the x/y coordinates are within this area
     */
    /**
     * Default property values of interactive objects
     * Used by {@link PIXI.InteractionManager} to automatically give all DisplayObjects these properties
     *
     * @private
     * @name interactiveTarget
     * @type {Object}
     * @example
     *      function MyObject() {}
     *
     *      Object.assign(
     *          DisplayObject.prototype,
     *          PIXI.interactiveTarget
     *      );
     */
    const interactiveTarget: InteractiveTarget;
}
declare namespace feng3d.pixi {
    interface InteractionTrackingFlags {
        OVER: number;
        LEFT_DOWN: number;
        RIGHT_DOWN: number;
        NONE: number;
    }
    /**
     * DisplayObjects with the {@link PIXI.interactiveTarget} mixin use this class to track interactions
     *
     * @private
     */
    class InteractionTrackingData {
        static FLAGS: Readonly<InteractionTrackingFlags>;
        private readonly _pointerId;
        private _flags;
        /**
         * @param {number} pointerId - Unique pointer id of the event
         * @private
         */
        constructor(pointerId: number);
        /**
         *
         * @private
         * @param {number} flag - The interaction flag to set
         * @param {boolean} yn - Should the flag be set or unset
         */
        private _doSet;
        /**
         * Unique pointer id of the event
         *
         * @readonly
         * @private
         * @member {number}
         */
        get pointerId(): number;
        /**
         * State of the tracking data, expressed as bit flags
         *
         * @private
         * @member {number}
         */
        get flags(): number;
        set flags(flags: number);
        /**
         * Is the tracked event inactive (not over or down)?
         *
         * @private
         * @member {number}
         */
        get none(): boolean;
        /**
         * Is the tracked event over the DisplayObject?
         *
         * @private
         * @member {boolean}
         */
        get over(): boolean;
        set over(yn: boolean);
        /**
         * Did the right mouse button come down in the DisplayObject?
         *
         * @private
         * @member {boolean}
         */
        get rightDown(): boolean;
        set rightDown(yn: boolean);
        /**
         * Did the left mouse button come down in the DisplayObject?
         *
         * @private
         * @member {boolean}
         */
        get leftDown(): boolean;
        set leftDown(yn: boolean);
    }
}
declare namespace feng3d {
    interface Component2DEventMap extends pixi.InteractionEventMap {
    }
}
declare namespace feng3d.pixi {
    export interface InteractionManagerOptions {
        autoPreventDefault?: boolean;
        interactionFrequency?: number;
        useSystemTicker?: boolean;
    }
    export interface DelayedEvent {
        displayObject: Container;
        eventString: InteractionEventKeys;
        eventData: InteractionEvent;
    }
    export interface InteractionEventMap {
        pointercancel: InteractionData;
        pointerover: InteractionData;
        pointerdown: InteractionData;
        touchstart: InteractionData;
        rightdown: InteractionData;
        mousedown: InteractionData;
        touchcancel: InteractionData;
        rightup: InteractionData;
        mouseup: InteractionData;
        rightclick: InteractionData;
        click: InteractionData;
        rightupoutside: InteractionData;
        mouseupoutside: InteractionData;
        pointerup: InteractionData;
        touchend: InteractionData;
        pointertap: InteractionData;
        pointerupoutside: InteractionData;
        tap: InteractionData;
        touchendoutside: InteractionData;
        pointermove: InteractionData;
        touchmove: InteractionData;
        mousemove: InteractionData;
        mouseover: InteractionData;
        pointerout: InteractionData;
        mouseout: InteractionData;
    }
    type InteractionEventKeys = keyof InteractionEventMap;
    /**
     * The interaction manager deals with mouse, touch and pointer events.
     *
     * Any Node2D can be interactive if its `interactive` property is set to true.
     *
     * This manager also supports multitouch.
     *
     * An instance of this class is automatically created by default, and can be found at `renderer.plugins.interaction`
     *
     */
    export class InteractionManager extends EventEmitter {
        readonly activeInteractionData: {
            [key: number]: InteractionData;
        };
        readonly supportsTouchEvents: boolean;
        readonly supportsPointerEvents: boolean;
        interactionDataPool: InteractionData[];
        cursor: string;
        delayedEvents: DelayedEvent[];
        search: TreeSearch;
        renderer: AbstractRenderer;
        autoPreventDefault: boolean;
        interactionFrequency: number;
        mouse: InteractionData;
        eventData: InteractionEvent;
        moveWhenInside: boolean;
        cursorStyles: Dict<string | ((mode: string) => void) | CSSStyleDeclaration>;
        currentCursorMode: string;
        resolution: number;
        protected interactionDOMElement: HTMLElement;
        protected eventsAdded: boolean;
        protected tickerAdded: boolean;
        protected mouseOverRenderer: boolean;
        private _useSystemTicker;
        private _deltaTime;
        private _didMove;
        private _tempDisplayObject;
        /**
         * @param {PIXI.CanvasRenderer|PIXI.Renderer} renderer - A reference to the current renderer
         * @param {object} [options] - The options for the manager.
         * @param {boolean} [options.autoPreventDefault=true] - Should the manager automatically prevent default browser actions.
         * @param {number} [options.interactionFrequency=10] - Maximum frequency (ms) at pointer over/out states will be checked.
         * @param {number} [options.useSystemTicker=true] - Whether to add {@link tickerUpdate} to {@link PIXI.Ticker.system}.
         */
        constructor(renderer: AbstractRenderer, options?: InteractionManagerOptions);
        /**
         * Should the InteractionManager automatically add {@link tickerUpdate} to {@link PIXI.Ticker.system}.
         *
         * @member {boolean}
         * @default true
         */
        get useSystemTicker(): boolean;
        set useSystemTicker(useSystemTicker: boolean);
        /**
         * Last rendered object or temp object
         * @readonly
         * @protected
         * @member {feng3d.Node2D}
         */
        get lastObjectRendered(): Container;
        /**
         * Hit tests a point against the display tree, returning the first interactive object that is hit.
         *
         * @param {PIXI.Point} globalPoint - A point to hit test with, in global space.
         * @param {PIXI.Container} [root] - The root display object to start from. If omitted, defaults
         * to the last rendered root of the associated renderer.
         * @return {feng3d.Node2D} The hit display object, if any.
         */
        hitTest(globalPoint: Vector2, root?: Container): Container;
        /**
         * Sets the DOM element which will receive mouse/touch events. This is useful for when you have
         * other DOM elements on top of the renderers Canvas element. With this you'll be bale to delegate
         * another DOM element to receive those events.
         *
         * @param {HTMLElement} element - the DOM element which will receive mouse and touch events.
         * @param {number} [resolution=1] - The resolution / device pixel ratio of the new element (relative to the canvas).
         */
        setTargetElement(element: HTMLElement, resolution?: number): void;
        /**
         * Add the ticker listener
         *
         * @private
         */
        private addTickerListener;
        /**
         * Remove the ticker listener
         *
         * @private
         */
        private removeTickerListener;
        /**
         * Registers all the DOM events
         *
         * @private
         */
        private addEvents;
        /**
         * Removes all the DOM events that were previously registered
         *
         * @private
         */
        private removeEvents;
        /**
         * Updates the state of interactive objects if at least {@link interactionFrequency}
         * milliseconds have passed since the last invocation.
         *
         * Invoked by a throttled ticker update from {@link PIXI.Ticker.system}.
         *
         * @param {number} deltaTime - time delta since the last call
         */
        tickerUpdate(deltaTime: number): void;
        /**
         * Updates the state of interactive objects.
         */
        update(): void;
        /**
         * Sets the current cursor mode, handling any callbacks or CSS style changes.
         *
         * @param {string} mode - cursor mode, a key from the cursorStyles dictionary
         */
        setCursorMode(mode: string): void;
        /**
         * Dispatches an event on the display object that was interacted with
         *
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - the display object in question
         * @param {string} eventString - the name of the event (e.g, mousedown)
         * @param {PIXI.InteractionEvent} eventData - the event data object
         * @private
         */
        private dispatchEvent;
        /**
         * Puts a event on a queue to be dispatched later. This is used to guarantee correct
         * ordering of over/out events.
         *
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - the display object in question
         * @param {string} eventString - the name of the event (e.g, mousedown)
         * @param {object} eventData - the event data object
         * @private
         */
        private delayDispatchEvent;
        /**
         * Maps x and y coords from a DOM object and maps them correctly to the PixiJS view. The
         * resulting value is stored in the point. This takes into account the fact that the DOM
         * element could be scaled and positioned anywhere on the screen.
         *
         * @param  {PIXI.IPointData} point - the point that the result will be stored in
         * @param  {number} x - the x coord of the position to map
         * @param  {number} y - the y coord of the position to map
         */
        mapPositionToPoint(point: Vector2, x: number, y: number): void;
        /**
         * This function is provides a neat way of crawling through the scene graph and running a
         * specified function on all interactive objects it finds. It will also take care of hit
         * testing the interactive objects and passes the hit across in the function.
         *
         * @protected
         * @param {PIXI.InteractionEvent} interactionEvent - event containing the point that
         *  is tested for collision
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - the displayObject
         *  that will be hit test (recursively crawls its children)
         * @param {Function} [func] - the function that will be called on each interactive object. The
         *  interactionEvent, displayObject and hit will be passed to the function
         * @param {boolean} [hitTest] - indicates whether we want to calculate hits
         *  or just iterate through all interactive objects
         */
        processInteractive(interactionEvent: InteractionEvent, displayObject: Container, func?: InteractionCallback, hitTest?: boolean): void;
        /**
         * Is called when the pointer button is pressed down on the renderer element
         *
         * @private
         * @param {PointerEvent} originalEvent - The DOM event of a pointer button being pressed down
         */
        private onPointerDown;
        /**
         * Processes the result of the pointer down check and dispatches the event if need be
         *
         * @private
         * @param {PIXI.InteractionEvent} interactionEvent - The interaction event wrapping the DOM event
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - The display object that was tested
         * @param {boolean} hit - the result of the hit test on the display object
         */
        private processPointerDown;
        /**
         * Is called when the pointer button is released on the renderer element
         *
         * @private
         * @param {PointerEvent} originalEvent - The DOM event of a pointer button being released
         * @param {boolean} cancelled - true if the pointer is cancelled
         * @param {Function} func - Function passed to {@link processInteractive}
         */
        private onPointerComplete;
        /**
         * Is called when the pointer button is cancelled
         *
         * @private
         * @param {PointerEvent} event - The DOM event of a pointer button being released
         */
        private onPointerCancel;
        /**
         * Processes the result of the pointer cancel check and dispatches the event if need be
         *
         * @private
         * @param {PIXI.InteractionEvent} interactionEvent - The interaction event wrapping the DOM event
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - The display object that was tested
         */
        private processPointerCancel;
        /**
         * Is called when the pointer button is released on the renderer element
         *
         * @private
         * @param {PointerEvent} event - The DOM event of a pointer button being released
         */
        private onPointerUp;
        /**
         * Processes the result of the pointer up check and dispatches the event if need be
         *
         * @private
         * @param {PIXI.InteractionEvent} interactionEvent - The interaction event wrapping the DOM event
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - The display object that was tested
         * @param {boolean} hit - the result of the hit test on the display object
         */
        private processPointerUp;
        /**
         * Is called when the pointer moves across the renderer element
         *
         * @private
         * @param {PointerEvent} originalEvent - The DOM event of a pointer moving
         */
        private onPointerMove;
        /**
         * Processes the result of the pointer move check and dispatches the event if need be
         *
         * @private
         * @param {PIXI.InteractionEvent} interactionEvent - The interaction event wrapping the DOM event
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - The display object that was tested
         * @param {boolean} hit - the result of the hit test on the display object
         */
        private processPointerMove;
        /**
         * Is called when the pointer is moved out of the renderer element
         *
         * @private
         * @param {PointerEvent} originalEvent - The DOM event of a pointer being moved out
         */
        private onPointerOut;
        /**
         * Processes the result of the pointer over/out check and dispatches the event if need be
         *
         * @private
         * @param {PIXI.InteractionEvent} interactionEvent - The interaction event wrapping the DOM event
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - The display object that was tested
         * @param {boolean} hit - the result of the hit test on the display object
         */
        private processPointerOverOut;
        /**
         * Is called when the pointer is moved into the renderer element
         *
         * @private
         * @param {PointerEvent} originalEvent - The DOM event of a pointer button being moved into the renderer view
         */
        private onPointerOver;
        /**
         * Get InteractionData for a given pointerId. Store that data as well
         *
         * @private
         * @param {PointerEvent} event - Normalized pointer event, output from normalizeToPointerData
         * @return {PIXI.InteractionData} - Interaction data for the given pointer identifier
         */
        private getInteractionDataForPointerId;
        /**
         * Return unused InteractionData to the pool, for a given pointerId
         *
         * @private
         * @param {number} pointerId - Identifier from a pointer event
         */
        private releaseInteractionDataForPointerId;
        /**
         * Configure an InteractionEvent to wrap a DOM PointerEvent and InteractionData
         *
         * @private
         * @param {PIXI.InteractionEvent} interactionEvent - The event to be configured
         * @param {PointerEvent} pointerEvent - The DOM event that will be paired with the InteractionEvent
         * @param {PIXI.InteractionData} interactionData - The InteractionData that will be paired
         *        with the InteractionEvent
         * @return {PIXI.InteractionEvent} the interaction event that was passed in
         */
        private configureInteractionEventForDOMEvent;
        /**
         * Ensures that the original event object contains all data that a regular pointer event would have
         *
         * @private
         * @param {TouchEvent|MouseEvent|PointerEvent} event - The original event data from a touch or mouse event
         * @return {PointerEvent[]} An array containing a single normalized pointer event, in the case of a pointer
         *  or mouse event, or a multiple normalized pointer events if there are multiple changed touches
         */
        private normalizeToPointerData;
        /**
         * Destroys the interaction manager
         *
         */
        destroy(): void;
    }
    export {};
}
declare namespace feng3d.pixi {
    /**
     * Strategy how to search through stage tree for interactive objects
     *
     * @private
     */
    class TreeSearch {
        private readonly _tempPoint;
        constructor();
        /**
         * Recursive implementation for findHit
         *
         * @private
         * @param {PIXI.InteractionEvent} interactionEvent - event containing the point that
         *  is tested for collision
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - the displayObject
         *  that will be hit test (recursively crawls its children)
         * @param {Function} [func] - the function that will be called on each interactive object. The
         *  interactionEvent, displayObject and hit will be passed to the function
         * @param {boolean} [hitTest] - this indicates if the objects inside should be hit test against the point
         * @param {boolean} [interactive] - Whether the displayObject is interactive
         * @return {boolean} returns true if the displayObject hit the point
         */
        recursiveFindHit(interactionEvent: InteractionEvent, displayObject: Container, func?: InteractionCallback, hitTest?: boolean, interactive?: boolean): boolean;
        /**
         * This function is provides a neat way of crawling through the scene graph and running a
         * specified function on all interactive objects it finds. It will also take care of hit
         * testing the interactive objects and passes the hit across in the function.
         *
         * @private
         * @param {PIXI.InteractionEvent} interactionEvent - event containing the point that
         *  is tested for collision
         * @param {PIXI.Container|PIXI.Sprite|PIXI.TilingSprite} displayObject - the displayObject
         *  that will be hit test (recursively crawls its children)
         * @param {Function} [func] - the function that will be called on each interactive object. The
         *  interactionEvent, displayObject and hit will be passed to the function
         * @param {boolean} [hitTest] - this indicates if the objects inside should be hit test against the point
         * @return {boolean} returns true if the displayObject hit the point
         */
        findHit(interactionEvent: InteractionEvent, displayObject: Container, func?: InteractionCallback, hitTest?: boolean): void;
    }
}
declare namespace feng3d.pixi {
    interface IRendererOptions {
        width?: number;
        height?: number;
        view?: HTMLCanvasElement;
        useContextAlpha?: boolean | 'notMultiplied';
        /**
         * Use `backgroundAlpha` instead.
         * @deprecated
         */
        transparent?: boolean;
        autoDensity?: boolean;
        antialias?: boolean;
        resolution?: number;
        preserveDrawingBuffer?: boolean;
        clearBeforeRender?: boolean;
        backgroundColor?: number;
        backgroundAlpha?: number;
        powerPreference?: WebGLPowerPreference;
        context?: IRenderingContext;
    }
    interface IRendererPlugins {
        [key: string]: any;
    }
    interface IRendererRenderOptions {
        renderTexture?: RenderTexture;
        clear?: boolean;
        transform?: Matrix;
        skipUpdateTransform?: boolean;
    }
    /**
     * The AbstractRenderer is the base for a PixiJS Renderer. It is extended by the {@link PIXI.CanvasRenderer}
     * and {@link PIXI.Renderer} which can be used for rendering a PixiJS scene.
     *
     * @abstract
     */
    abstract class AbstractRenderer extends EventEmitter {
        resolution: number;
        clearBeforeRender?: boolean;
        readonly options: IRendererOptions;
        readonly type: RENDERER_TYPE;
        readonly screen: Rectangle;
        readonly view: HTMLCanvasElement;
        readonly plugins: IRendererPlugins;
        readonly useContextAlpha: boolean | 'notMultiplied';
        readonly autoDensity: boolean;
        readonly preserveDrawingBuffer: boolean;
        protected _backgroundColor: number;
        protected _backgroundColorString: string;
        _backgroundColorRgba: number[];
        _lastObjectRendered: IRenderableObject;
        /**
         * @param system - The name of the system this renderer is for.
         * @param [options] - The optional renderer parameters.
         * @param {number} [options.width=800] - The width of the screen.
         * @param {number} [options.height=600] - The height of the screen.
         * @param {HTMLCanvasElement} [options.view] - The canvas to use as a view, optional.
         * @param {boolean} [options.useContextAlpha=true] - Pass-through value for canvas' context `alpha` property.
         *   If you want to set transparency, please use `backgroundAlpha`. This option is for cases where the
         *   canvas needs to be opaque, possibly for performance reasons on some older devices.
         * @param {boolean} [options.autoDensity=false] - Resizes renderer view in CSS pixels to allow for
         *   resolutions other than 1.
         * @param {boolean} [options.antialias=false] - Sets antialias
         * @param {number} [options.resolution=1] - The resolution / device pixel ratio of the renderer. The
         *  resolution of the renderer retina would be 2.
         * @param {boolean} [options.preserveDrawingBuffer=false] - Enables drawing buffer preservation,
         *  enable this if you need to call toDataUrl on the WebGL context.
         * @param {boolean} [options.clearBeforeRender=true] - This sets if the renderer will clear the canvas or
         *      not before the new render pass.
         * @param {number} [options.backgroundColor=0x000000] - The background color of the rendered area
         *  (shown if not transparent).
         * @param {number} [options.backgroundAlpha=1] - Value from 0 (fully transparent) to 1 (fully opaque).
         */
        constructor(type?: RENDERER_TYPE, options?: IRendererOptions);
        /**
         * Initialize the plugins.
         *
         * @protected
         * @param {object} staticMap - The dictionary of statically saved plugins.
         */
        initPlugins(staticMap: IRendererPlugins): void;
        /**
         * Same as view.width, actual number of pixels in the canvas by horizontal.
         *
         * @member {number}
         * @readonly
         * @default 800
         */
        get width(): number;
        /**
         * Same as view.height, actual number of pixels in the canvas by vertical.
         *
         * @member {number}
         * @readonly
         * @default 600
         */
        get height(): number;
        /**
         * Resizes the screen and canvas to the specified width and height.
         * Canvas dimensions are multiplied by resolution.
         *
         * @param screenWidth - The new width of the screen.
         * @param screenHeight - The new height of the screen.
         */
        resize(screenWidth: number, screenHeight: number): void;
        /**
         * Useful function that returns a texture of the display object that can then be used to create sprites
         * This can be quite useful if your displayObject is complicated and needs to be reused multiple times.
         *
         * @param displayObject - The displayObject the object will be generated from.
         * @param scaleMode - The scale mode of the texture.
         * @param resolution - The resolution / device pixel ratio of the texture being generated.
         * @param [region] - The region of the displayObject, that shall be rendered,
         *        if no region is specified, defaults to the local bounds of the displayObject.
         * @return A texture of the graphics object.
         */
        generateTexture(displayObject: IRenderableObject, scaleMode?: SCALE_MODES, resolution?: number, region?: Rectangle): RenderTexture;
        abstract render(displayObject: IRenderableObject, options?: IRendererRenderOptions): void;
        /**
         * Removes everything from the renderer and optionally removes the Canvas DOM element.
         *
         * @param [removeView=false] - Removes the Canvas element from the DOM.
         */
        destroy(removeView?: boolean): void;
        /**
         * The background color to fill if not transparent
         *
         * @member {number}
         */
        get backgroundColor(): number;
        set backgroundColor(value: number);
        /**
         * The background color alpha. Setting this to 0 will make the canvas transparent.
         *
         * @member {number}
         */
        get backgroundAlpha(): number;
        set backgroundAlpha(value: number);
    }
}
declare namespace feng3d.pixi {
    interface IRendererOptionsAuto extends IRendererOptions {
        forceCanvas?: boolean;
    }
    /**
     * This helper function will automatically detect which renderer you should be using.
     * WebGL is the preferred renderer as it is a lot faster. If WebGL is not supported by
     * the browser then this function will return a canvas renderer
     *
     * @function autoDetectRenderer
     * @param {object} [options] - The optional renderer parameters
     * @param {number} [options.width=800] - the width of the renderers view
     * @param {number} [options.height=600] - the height of the renderers view
     * @param {HTMLCanvasElement} [options.view] - the canvas to use as a view, optional
     * @param {boolean} [options.useContextAlpha=true] - Pass-through value for canvas' context `alpha` property.
     *   If you want to set transparency, please use `backgroundAlpha`. This option is for cases where the
     *   canvas needs to be opaque, possibly for performance reasons on some older devices.
     * @param {boolean} [options.autoDensity=false] - Resizes renderer view in CSS pixels to allow for
     *   resolutions other than 1
     * @param {boolean} [options.antialias=false] - sets antialias
     * @param {boolean} [options.preserveDrawingBuffer=false] - enables drawing buffer preservation, enable this if you
     *  need to call toDataUrl on the webgl context
     * @param {number} [options.backgroundColor=0x000000] - The background color of the rendered area
     *  (shown if not transparent).
     * @param {number} [options.backgroundAlpha=1] - Value from 0 (fully transparent) to 1 (fully opaque).
     * @param {boolean} [options.clearBeforeRender=true] - This sets if the renderer will clear the canvas or
     *   not before the new render pass.
     * @param {number} [options.resolution=1] - The resolution / device pixel ratio of the renderer, retina would be 2
     * @param {boolean} [options.forceCanvas=false] - prevents selection of WebGL renderer, even if such is present, this
     *   option only is available when using **pixi.js-legacy** or **@pixi/canvas-renderer** modules, otherwise
     *   it is ignored.
     * @param {string} [options.powerPreference] - Parameter passed to webgl context, set to "high-performance"
     *  for devices with dual graphics card **webgl only**
     * @return {PIXI.Renderer|PIXI.CanvasRenderer} Returns WebGL renderer if available, otherwise CanvasRenderer
     */
    function autoDetectRenderer(options?: IRendererOptionsAuto): AbstractRenderer;
}
declare namespace feng3d.pixi {
    /**
     * Interface for DisplayObject to interface with Renderer.
     * The minimum APIs needed to implement a renderable object.
     */
    interface IRenderableObject {
        /** Object must have a parent container */
        parent: IRenderableContainer;
        /** Before method for transform updates */
        enableTempParent(): IRenderableContainer;
        /** Update the transforms */
        updateTransform(): void;
        /** After method for transform updates */
        disableTempParent(parent: IRenderableContainer): void;
        /** Render object directly */
        render(renderer: Renderer): void;
    }
    /**
     * Interface for Container to interface with Renderer.
     */
    interface IRenderableContainer extends IRenderableObject {
        /** Get Local bounds for container */
        getLocalBounds(rect?: Rectangle, skipChildrenUpdate?: boolean): Rectangle;
    }
}
declare namespace feng3d.pixi {
    /**
     * Mixed WebGL1/WebGL2 Rendering Context.
     * Either its WebGL2, either its WebGL1 with PixiJS polyfills on it
     */
    interface IRenderingContext extends WebGL2RenderingContext {
    }
}
declare namespace feng3d.pixi {
    interface IRendererPluginConstructor {
        new (renderer: Renderer, options?: any): IRendererPlugin;
    }
    interface IRendererPlugin {
        destroy(): void;
    }
    /**
     * The Renderer draws the scene and all its content onto a WebGL enabled canvas.
     *
     * This renderer should be used for browsers that support WebGL.
     *
     * This renderer works by automatically managing WebGLBatchesm, so no need for Sprite Batches or Sprite Clouds.
     * Don't forget to add the view to your DOM or you will not see anything!
     *
     * Renderer is composed of systems that manage specific tasks. The following systems are added by default
     * whenever you create a renderer:
     *
     * | System                               | Description                                                                   |
     * | ------------------------------------ | ----------------------------------------------------------------------------- |
     * | {@link PIXI.BatchSystem}             | This manages object renderers that defer rendering until a flush.             |
     * | {@link PIXI.ContextSystem}           | This manages the WebGL context and extensions.                                |
     * | {@link PIXI.FilterSystem}            | This manages the filtering pipeline for post-processing effects.              |
     * | {@link PIXI.FramebufferSystem}       | This manages framebuffers, which are used for offscreen rendering.            |
     * | {@link PIXI.GeometrySystem}          | This manages geometries & buffers, which are used to draw object meshes.      |
     * | {@link PIXI.MaskSystem}              | This manages masking operations.                                              |
     * | {@link PIXI.ProjectionSystem}        | This manages the `projectionMatrix`, used by shaders to get NDC coordinates.  |
     * | {@link PIXI.RenderTextureSystem}     | This manages render-textures, which are an abstraction over framebuffers.     |
     * | {@link PIXI.ScissorSystem}           | This handles scissor masking, and is used internally by {@link MaskSystem}    |
     * | {@link PIXI.ShaderSystem}            | This manages shaders, programs that run on the GPU to calculate 'em pixels.   |
     * | {@link PIXI.StateSystem}             | This manages the WebGL state variables like blend mode, depth testing, etc.   |
     * | {@link PIXI.StencilSystem}           | This handles stencil masking, and is used internally by {@link MaskSystem}    |
     * | {@link PIXI.TextureSystem}           | This manages textures and their resources on the GPU.                         |
     * | {@link PIXI.TextureGCSystem}         | This will automatically remove textures from the GPU if they are not used.    |
     *
     * The breadth of the API surface provided by the renderer is contained within these systems.
     *
     */
    class Renderer extends AbstractRenderer {
        gl: IRenderingContext;
        globalUniforms: UniformGroup;
        CONTEXT_UID: number;
        renderingToScreen: boolean;
        mask: MaskSystem;
        context: ContextSystem;
        state: StateSystem;
        shader: ShaderSystem;
        texture: TextureSystem;
        geometry: GeometrySystem;
        framebuffer: FramebufferSystem;
        scissor: ScissorSystem;
        stencil: StencilSystem;
        projection: ProjectionSystem;
        textureGC: TextureGCSystem;
        filter: FilterSystem;
        renderTexture: RenderTextureSystem;
        batch: BatchSystem;
        runners: {
            [key: string]: Runner;
        };
        /**
         * Create renderer if WebGL is available. Overrideable
         * by the **@pixi/canvas-renderer** package to allow fallback.
         * throws error if WebGL is not available.
         * @static
         * @private
         */
        static create(options?: IRendererOptions): AbstractRenderer;
        /**
         * @param [options] - The optional renderer parameters.
         * @param {number} [options.width=800] - The width of the screen.
         * @param {number} [options.height=600] - The height of the screen.
         * @param {HTMLCanvasElement} [options.view] - The canvas to use as a view, optional.
         * @param {boolean} [options.useContextAlpha=true] - Pass-through value for canvas' context `alpha` property.
         *   If you want to set transparency, please use `backgroundAlpha`. This option is for cases where the
         *   canvas needs to be opaque, possibly for performance reasons on some older devices.
         * @param {boolean} [options.autoDensity=false] - Resizes renderer view in CSS pixels to allow for
         *   resolutions other than 1.
         * @param {boolean} [options.antialias=false] - Sets antialias. If not available natively then FXAA
         *  antialiasing is used.
         * @param {number} [options.resolution=1] - The resolution / device pixel ratio of the renderer.
         *  The resolution of the renderer retina would be 2.
         * @param {boolean} [options.clearBeforeRender=true] - This sets if the renderer will clear
         *  the canvas or not before the new render pass. If you wish to set this to false, you *must* set
         *  preserveDrawingBuffer to `true`.
         * @param {boolean} [options.preserveDrawingBuffer=false] - Enables drawing buffer preservation,
         *  enable this if you need to call toDataUrl on the WebGL context.
         * @param {number} [options.backgroundColor=0x000000] - The background color of the rendered area
         *  (shown if not transparent).
         * @param {number} [options.backgroundAlpha=1] - Value from 0 (fully transparent) to 1 (fully opaque).
         * @param {string} [options.powerPreference] - Parameter passed to WebGL context, set to "high-performance"
         *  for devices with dual graphics card.
         * @param {object} [options.context] - If WebGL context already exists, all parameters must be taken from it.
         * @public
         */
        constructor(options?: IRendererOptions);
        /**
         * Add a new system to the renderer.
         * @param ClassRef - Class reference
         * @param [name] - Property name for system, if not specified
         *        will use a static `name` property on the class itself. This
         *        name will be assigned as s property on the Renderer so make
         *        sure it doesn't collide with properties on Renderer.
         * @return {PIXI.Renderer} Return instance of renderer
         */
        addSystem<T extends System>(ClassRef: {
            new (renderer: Renderer): T;
        }, name: string): this;
        /**
         * Renders the object to its WebGL view.
         *
         * @param displayObject - The object to be rendered.
         * @param {object} [options] - Object to use for render options.
         * @param {PIXI.RenderTexture} [options.renderTexture] - The render texture to render to.
         * @param {boolean} [options.clear=true] - Should the canvas be cleared before the new render.
         * @param {PIXI.Matrix} [options.transform] - A transform to apply to the render texture before rendering.
         * @param {boolean} [options.skipUpdateTransform=false] - Should we skip the update transform pass?
         */
        render(displayObject: IRenderableObject, options?: IRendererRenderOptions): void;
        /**
         * Please use the `option` render arguments instead.
         *
         * @deprecated Since 6.0.0
         * @param displayObject
         * @param renderTexture
         * @param clear
         * @param transform
         * @param skipUpdateTransform
         */
        render(displayObject: IRenderableObject, renderTexture?: RenderTexture, clear?: boolean, transform?: Matrix, skipUpdateTransform?: boolean): void;
        /**
         * Resizes the WebGL view to the specified width and height.
         *
         * @param screenWidth - The new width of the screen.
         * @param screenHeight - The new height of the screen.
         */
        resize(screenWidth: number, screenHeight: number): void;
        /**
         * Resets the WebGL state so you can render things however you fancy!
         *
         * @return {PIXI.Renderer} Returns itself.
         */
        reset(): this;
        /**
         * Clear the frame buffer
         */
        clear(): void;
        /**
         * Removes everything from the renderer (event listeners, spritebatch, etc...)
         *
         * @param [removeView=false] - Removes the Canvas element from the DOM.
         *  See: https://github.com/pixijs/pixi.js/issues/2233
         */
        destroy(removeView?: boolean): void;
        /**
         * Please use `plugins.extract` instead.
         * @member {PIXI.Extract} extract
         * @deprecated since 6.0.0
         * @readonly
         */
        get extract(): any;
        /**
         * Collection of installed plugins. These are included by default in PIXI, but can be excluded
         * by creating a custom build. Consult the README for more information about creating custom
         * builds and excluding plugins.
         * @name plugins
         * @type {object}
         * @readonly
         * @property {PIXI.AccessibilityManager} accessibility Support tabbing interactive elements.
         * @property {PIXI.Extract} extract Extract image data from renderer.
         * @property {PIXI.InteractionManager} interaction Handles mouse, touch and pointer events.
         * @property {PIXI.ParticleRenderer} particle Renderer for ParticleContainer objects.
         * @property {PIXI.Prepare} prepare Pre-render display objects.
         * @property {PIXI.BatchRenderer} batch Batching of Sprite, Graphics and Mesh objects.
         * @property {PIXI.TilingSpriteRenderer} tilingSprite Renderer for TilingSprite objects.
         */
        static __plugins: IRendererPlugins;
        /**
         * Adds a plugin to the renderer.
         *
         * @method
         * @param pluginName - The name of the plugin.
         * @param ctor - The constructor function or class for the plugin.
         */
        static registerPlugin(pluginName: string, ctor: IRendererPluginConstructor): void;
    }
    const WebGLRenderer: typeof Renderer;
}
declare namespace feng3d.pixi {
}
declare namespace feng3d.pixi {
    interface ISystems {
    }
    const systems: ISystems;
    /**
     * System is a base class used for extending systems used by the {@link PIXI.Renderer}
     *
     * @see PIXI.Renderer#addSystem
     */
    class System {
        renderer: Renderer;
        /**
         * @param {Renderer} renderer - The renderer this manager works for.
         */
        constructor(renderer: Renderer);
        /**
         * Generic destroy methods to be overridden by the subclass
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage specific types of masking operations.
     *
     */
    class AbstractMaskSystem extends System {
        protected maskStack: Array<MaskData>;
        protected glConst: number;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * gets count of masks of certain type
         * @returns {number}
         */
        getStackLength(): number;
        /**
         * Changes the mask stack that is used by this System.
         *
         * @param {PIXI.MaskData[]} maskStack - The mask stack
         */
        setMaskStack(maskStack: Array<MaskData>): void;
        /**
         * Setup renderer to use the current mask data.
         * @private
         */
        protected _useCurrent(): void;
        /**
         * Destroys the mask stack.
         *
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    interface IMaskTarget extends IFilterTarget {
        renderable: boolean;
        isSprite?: boolean;
        worldTransform: Matrix;
        isFastRect?(): boolean;
        getBounds(skipUpdate?: boolean): Rectangle;
        render(renderer: Renderer): void;
    }
    /**
     * Component for masked elements
     *
     * Holds mask mode and temporary data about current mask
     *
     */
    class MaskData {
        type: MASK_TYPES;
        autoDetect: boolean;
        maskObject: IMaskTarget;
        pooled: boolean;
        isMaskData: true;
        _stencilCounter: number;
        _scissorCounter: number;
        _scissorRect: Rectangle;
        _target: IMaskTarget;
        /**
         * Create MaskData
         *
         * @param {PIXI.DisplayObject} [maskObject=null] - object that describes the mask
         */
        constructor(maskObject?: IMaskTarget);
        /**
         * resets the mask data after popMask()
         */
        reset(): void;
        /**
         * copies counters from maskData above, called from pushMask()
         * @param {PIXI.MaskData|null} maskAbove
         */
        copyCountersOrReset(maskAbove?: MaskData): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage masks.
     *
     * There are three built-in types of masking:
     * * **Scissor Masking**: Scissor masking discards pixels that are outside of a rectangle called the scissor box. It is
     *  the most performant as the scissor test is inexpensive. However, it can only be used when the mask is rectangular.
     * * **Stencil Masking**: Stencil masking discards pixels that don't overlap with the pixels rendered into the stencil
     *  buffer. It is the next fastest option as it does not require rendering into a separate framebuffer. However, it does
     *  cause the mask to be rendered **twice** for each masking operation; hence, minimize the rendering cost of your masks.
     * * **Sprite Mask Filtering**: Sprite mask filtering discards pixels based on the red channel of the sprite-mask's
     *  texture. (Generally, the masking texture is grayscale). Using advanced techniques, you might be able to embed this
     *  type of masking in a custom shader - and hence, bypassing the masking system fully for performance wins.
     *
     * The best type of masking is auto-detected when you `push` one. To use scissor masking, you must pass in a `Graphics`
     * object with just a rectangle drawn.
     *
     * ## Mask Stacks
     *
     * In the scene graph, masks can be applied recursively, i.e. a mask can be applied during a masking operation. The mask
     * stack stores the currently applied masks in order. Each {@link PIXI.BaseRenderTexture} holds its own mask stack, i.e.
     * when you switch render-textures, the old masks only applied when you switch back to rendering to the old render-target.
     *
     */
    class MaskSystem extends System {
        enableScissor: boolean;
        protected readonly alphaMaskPool: Array<SpriteMaskFilter[]>;
        protected alphaMaskIndex: number;
        private readonly maskDataPool;
        private maskStack;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Changes the mask stack that is used by this System.
         *
         * @param {PIXI.MaskData[]} maskStack - The mask stack
         */
        setMaskStack(maskStack: Array<MaskData>): void;
        /**
         * Enables the mask and appends it to the current mask stack.
         *
         * NOTE: The batch renderer should be flushed beforehand to prevent pending renders from being masked.
         *
         * @param {PIXI.DisplayObject} target - Display Object to push the mask to
         * @param {PIXI.MaskData|PIXI.Sprite|PIXI.Graphics|PIXI.DisplayObject} maskData - The masking data.
         */
        push(target: IMaskTarget, maskDataOrTarget: MaskData | IMaskTarget): void;
        /**
         * Removes the last mask from the mask stack and doesn't return it.
         *
         * NOTE: The batch renderer should be flushed beforehand to render the masked contents before the mask is removed.
         *
         * @param {PIXI.DisplayObject} target - Display Object to pop the mask from
         */
        pop(target: IMaskTarget): void;
        /**
         * Sets type of MaskData based on its maskObject
         * @param {PIXI.MaskData} maskData
         */
        detect(maskData: MaskData): void;
        /**
         * Applies the Mask and adds it to the current filter stack.
         *
         * @param {PIXI.MaskData} maskData - Sprite to be used as the mask
         */
        pushSpriteMask(maskData: MaskData): void;
        /**
         * Removes the last filter from the filter stack and doesn't return it.
         */
        popSpriteMask(): void;
    }
    interface ISystems {
        MaskSystem: new (...args: any) => MaskSystem;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage scissor masking.
     *
     * Scissor masking discards pixels outside of a rectangle called the scissor box. The scissor box is in the framebuffer
     * viewport's space; however, the mask's rectangle is projected from world-space to viewport space automatically
     * by this system.
     *
     */
    class ScissorSystem extends AbstractMaskSystem {
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        getStackLength(): number;
        /**
         * Applies the Mask and adds it to the current stencil stack.
         *
         * @author alvin
         * @param {PIXI.MaskData} maskData - The mask data
         */
        push(maskData: MaskData): void;
        /**
         * This should be called after a mask is popped off the mask stack. It will rebind the scissor box to be latest with the
         * last mask in the stack.
         *
         * This can also be called when you directly modify the scissor box and want to restore PixiJS state.
         */
        pop(): void;
        /**
         * Setup renderer to use the current scissor data.
         * @private
         */
        _useCurrent(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage stencils (used for masks).
     *
     */
    class StencilSystem extends AbstractMaskSystem {
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        getStackLength(): number;
        /**
         * Applies the Mask and adds it to the current stencil stack.
         *
         * @param {PIXI.MaskData} maskData - The mask data
         */
        push(maskData: MaskData): void;
        /**
         * Pops stencil mask. MaskData is already removed from stack
         *
         * @param {PIXI.DisplayObject} maskObject - object of popped mask data
         */
        pop(maskObject: IMaskTarget): void;
        /**
         * Setup renderer to use the current stencil data.
         * @private
         */
        _useCurrent(): void;
        /**
         * Fill 1s equal to the number of acitve stencil masks.
         * @private
         * @return {number} The bitwise mask.
         */
        _getBitwiseMask(): number;
    }
}
declare namespace feng3d.pixi {
    /**
     * A framebuffer can be used to render contents off of the screen. {@link PIXI.BaseRenderTexture} uses
     * one internally to render into itself. You can attach a depth or stencil buffer to a framebuffer.
     *
     * On WebGL 2 machines, shaders can output to multiple textures simultaneously with GLSL 300 ES.
     *
     */
    class Framebuffer {
        width: number;
        height: number;
        multisample: MSAA_QUALITY;
        stencil: boolean;
        depth: boolean;
        dirtyId: number;
        dirtyFormat: number;
        dirtySize: number;
        depthTexture: BaseTexture;
        colorTextures: Array<BaseTexture>;
        glFramebuffers: {
            [key: string]: GLFramebuffer;
        };
        disposeRunner: Runner;
        /**
         * @param {number} width - Width of the frame buffer
         * @param {number} height - Height of the frame buffer
         */
        constructor(width: number, height: number);
        /**
         * Reference to the colorTexture.
         *
         * @member {PIXI.BaseTexture[]}
         * @readonly
         */
        get colorTexture(): BaseTexture;
        /**
         * Add texture to the colorTexture array
         *
         * @param {number} [index=0] - Index of the array to add the texture to
         * @param {PIXI.BaseTexture} [texture] - Texture to add to the array
         */
        addColorTexture(index?: number, texture?: BaseTexture): this;
        /**
         * Add a depth texture to the frame buffer
         *
         * @param {PIXI.BaseTexture} [texture] - Texture to add
         */
        addDepthTexture(texture?: BaseTexture): this;
        /**
         * Enable depth on the frame buffer
         */
        enableDepth(): this;
        /**
         * Enable stencil on the frame buffer
         */
        enableStencil(): this;
        /**
         * Resize the frame buffer
         *
         * @param {number} width - Width of the frame buffer to resize to
         * @param {number} height - Height of the frame buffer to resize to
         */
        resize(width: number, height: number): void;
        /**
         * Disposes WebGL resources that are connected to this geometry
         */
        dispose(): void;
        /**
         * Destroys and removes the depth texture added to this framebuffer.
         */
        destroyDepthTexture(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage framebuffers.
     *
     */
    class FramebufferSystem extends System {
        readonly managedFramebuffers: Array<Framebuffer>;
        current: Framebuffer;
        viewport: Rectangle;
        hasMRT: boolean;
        writeDepthTexture: boolean;
        protected CONTEXT_UID: number;
        protected gl: IRenderingContext;
        protected unknownFramebuffer: Framebuffer;
        protected msaaSamples: Array<number>;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Sets up the renderer context and necessary buffers.
         */
        protected contextChange(): void;
        /**
         * Bind a framebuffer
         *
         * @param {PIXI.Framebuffer} [framebuffer]
         * @param {PIXI.Rectangle} [frame] - frame, default is framebuffer size
         */
        bind(framebuffer?: Framebuffer, frame?: Rectangle): void;
        /**
         * Set the WebGLRenderingContext's viewport.
         *
         * @param {Number} x - X position of viewport
         * @param {Number} y - Y position of viewport
         * @param {Number} width - Width of viewport
         * @param {Number} height - Height of viewport
         */
        setViewport(x: number, y: number, width: number, height: number): void;
        /**
         * Get the size of the current width and height. Returns object with `width` and `height` values.
         *
         * @member {object}
         * @readonly
         */
        get size(): {
            x: number;
            y: number;
            width: number;
            height: number;
        };
        /**
         * Clear the color of the context
         *
         * @param {Number} r - Red value from 0 to 1
         * @param {Number} g - Green value from 0 to 1
         * @param {Number} b - Blue value from 0 to 1
         * @param {Number} a - Alpha value from 0 to 1
         * @param {PIXI.BUFFER_BITS} [mask=BUFFER_BITS.COLOR | BUFFER_BITS.DEPTH] - Bitwise OR of masks
         *  that indicate the buffers to be cleared, by default COLOR and DEPTH buffers.
         */
        clear(r: number, g: number, b: number, a: number, mask?: BUFFER_BITS): void;
        /**
         * Initialize framebuffer for this context
         *
         * @protected
         * @param {PIXI.Framebuffer} framebuffer
         * @returns {PIXI.GLFramebuffer} created GLFramebuffer
         */
        initFramebuffer(framebuffer: Framebuffer): GLFramebuffer;
        /**
         * Resize the framebuffer
         *
         * @protected
         * @param {PIXI.Framebuffer} framebuffer
         */
        resizeFramebuffer(framebuffer: Framebuffer): void;
        /**
         * Update the framebuffer
         *
         * @protected
         * @param {PIXI.Framebuffer} framebuffer
         */
        updateFramebuffer(framebuffer: Framebuffer): void;
        /**
         * Detects number of samples that is not more than a param but as close to it as possible
         *
         * @param {PIXI.MSAA_QUALITY} samples - number of samples
         * @returns {PIXI.MSAA_QUALITY} - recommended number of samples
         */
        protected detectSamples(samples: MSAA_QUALITY): MSAA_QUALITY;
        /**
         * Only works with WebGL2
         *
         * blits framebuffer to another of the same or bigger size
         * after that target framebuffer is bound
         *
         * Fails with WebGL warning if blits multisample framebuffer to different size
         *
         * @param {PIXI.Framebuffer} [framebuffer] - by default it blits "into itself", from renderBuffer to texture.
         * @param {PIXI.Rectangle} [sourcePixels] - source rectangle in pixels
         * @param {PIXI.Rectangle} [destPixels] - dest rectangle in pixels, assumed to be the same as sourcePixels
         */
        blit(framebuffer?: Framebuffer, sourcePixels?: Rectangle, destPixels?: Rectangle): void;
        /**
         * Disposes framebuffer
         * @param {PIXI.Framebuffer} framebuffer - framebuffer that has to be disposed of
         * @param {boolean} [contextLost=false] - If context was lost, we suppress all delete function calls
         */
        disposeFramebuffer(framebuffer: Framebuffer, contextLost?: boolean): void;
        /**
         * Disposes all framebuffers, but not textures bound to them
         * @param {boolean} [contextLost=false] - If context was lost, we suppress all delete function calls
         */
        disposeAll(contextLost?: boolean): void;
        /**
         * Forcing creation of stencil buffer for current framebuffer, if it wasn't done before.
         * Used by MaskSystem, when its time to use stencil mask for Graphics element.
         *
         * Its an alternative for public lazy `framebuffer.enableStencil`, in case we need stencil without rebind.
         *
         * @private
         */
        forceStencil(): void;
        /**
         * resets framebuffer stored state, binds screen framebuffer
         *
         * should be called before renderTexture reset()
         */
        reset(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Internal framebuffer for WebGL context
     */
    class GLFramebuffer {
        framebuffer: WebGLFramebuffer;
        stencil: WebGLRenderbuffer;
        multisample: MSAA_QUALITY;
        msaaBuffer: WebGLRenderbuffer;
        blitFramebuffer: Framebuffer;
        dirtyId: number;
        dirtyFormat: number;
        dirtySize: number;
        constructor(framebuffer: WebGLTexture);
    }
}
declare namespace feng3d.pixi {
    /**
     * Base resource class for textures that manages validation and uploading, depending on its type.
     *
     * Uploading of a base texture to the GPU is required.
     *
     */
    abstract class Resource {
        destroyed: boolean;
        internal: boolean;
        protected _width: number;
        protected _height: number;
        protected onResize: Runner;
        protected onUpdate: Runner;
        protected onError: Runner;
        /**
         * @param {number} [width=0] - Width of the resource
         * @param {number} [height=0] - Height of the resource
         */
        constructor(width?: number, height?: number);
        /**
         * Bind to a parent BaseTexture
         *
         * @param {PIXI.BaseTexture} baseTexture - Parent texture
         */
        bind(baseTexture: BaseTexture): void;
        /**
         * Unbind to a parent BaseTexture
         *
         * @param {PIXI.BaseTexture} baseTexture - Parent texture
         */
        unbind(baseTexture: BaseTexture): void;
        /**
         * Trigger a resize event
         * @param {number} width - X dimension
         * @param {number} height - Y dimension
         */
        resize(width: number, height: number): void;
        /**
         * Has been validated
         * @readonly
         * @member {boolean}
         */
        get valid(): boolean;
        /**
         * Has been updated trigger event
         */
        update(): void;
        /**
         * This can be overridden to start preloading a resource
         * or do any other prepare step.
         * @protected
         * @return {Promise<void>} Handle the validate event
         */
        load(): Promise<Resource>;
        /**
         * The width of the resource.
         *
         * @member {number}
         * @readonly
         */
        get width(): number;
        /**
         * The height of the resource.
         *
         * @member {number}
         * @readonly
         */
        get height(): number;
        /**
         * Uploads the texture or returns false if it cant for some reason. Override this.
         *
         * @param {PIXI.Renderer} renderer - yeah, renderer!
         * @param {PIXI.BaseTexture} baseTexture - the texture
         * @param {PIXI.GLTexture} glTexture - texture instance for this webgl context
         * @returns {boolean} true is success
         */
        abstract upload(renderer: Renderer, baseTexture: BaseTexture, glTexture: GLTexture): boolean;
        /**
         * Set the style, optional to override
         *
         * @param {PIXI.Renderer} renderer - yeah, renderer!
         * @param {PIXI.BaseTexture} baseTexture - the texture
         * @param {PIXI.GLTexture} glTexture - texture instance for this webgl context
         * @returns {boolean} `true` is success
         */
        style(_renderer: Renderer, _baseTexture: BaseTexture, _glTexture: GLTexture): boolean;
        /**
         * Clean up anything, this happens when destroying is ready.
         *
         * @protected
         */
        dispose(): void;
        /**
         * Call when destroying resource, unbind any BaseTexture object
         * before calling this method, as reference counts are maintained
         * internally.
         */
        destroy(): void;
        /**
         * Abstract, used to auto-detect resource type
         *
         * @static
         * @param {*} source - The source object
         * @param {string} extension - The extension of source, if set
         */
        static test(_source: unknown, _extension?: string): boolean;
    }
}
declare namespace feng3d.pixi {
    /**
     * Resource that can manage several resource (items) inside.
     * All resources need to have the same pixel size.
     * Parent class for CubeResource and ArrayResource
     *
     */
    abstract class AbstractMultiResource extends Resource {
        readonly length: number;
        items: Array<BaseTexture>;
        itemDirtyIds: Array<number>;
        private _load;
        baseTexture: BaseTexture;
        /**
         * @param {number} length
         * @param {object} [options] - Options to for Resource constructor
         * @param {number} [options.width] - Width of the resource
         * @param {number} [options.height] - Height of the resource
         */
        constructor(length: number, options?: ISize);
        /**
         * used from ArrayResource and CubeResource constructors
         * @param {Array<*>} resources - Can be resources, image elements, canvas, etc. ,
         *  length should be same as constructor length
         * @param {object} [options] - detect options for resources
         * @protected
         */
        protected initFromArray(resources: Array<any>, options?: IAutoDetectOptions): void;
        /**
         * Destroy this BaseImageResource
         * @override
         */
        dispose(): void;
        /**
         * Set a baseTexture by ID
         *
         * @param {PIXI.BaseTexture} baseTexture
         * @param {number} index - Zero-based index of resource to set
         * @return {PIXI.AbstractMultiResource} Instance for chaining
         */
        abstract addBaseTextureAt(baseTexture: BaseTexture, index: number): this;
        /**
         * Set a resource by ID
         *
         * @param {PIXI.Resource} resource
         * @param {number} index - Zero-based index of resource to set
         * @return {PIXI.ArrayResource} Instance for chaining
         */
        addResourceAt(resource: Resource, index: number): this;
        /**
         * Set the parent base texture
         * @member {PIXI.BaseTexture}
         * @override
         */
        bind(baseTexture: BaseTexture): void;
        /**
         * Unset the parent base texture
         * @member {PIXI.BaseTexture}
         * @override
         */
        unbind(baseTexture: BaseTexture): void;
        /**
         * Load all the resources simultaneously
         * @override
         * @return {Promise<void>} When load is resolved
         */
        load(): Promise<this>;
    }
}
declare namespace feng3d.pixi {
    /**
     * A resource that contains a number of sources.
     *
     */
    class ArrayResource extends AbstractMultiResource {
        /**
         * @param {number|Array<*>} source - Number of items in array or the collection
         *        of image URLs to use. Can also be resources, image elements, canvas, etc.
         * @param {object} [options] - Options to apply to {@link PIXI.autoDetectResource}
         * @param {number} [options.width] - Width of the resource
         * @param {number} [options.height] - Height of the resource
         */
        constructor(source: number | Array<any>, options?: ISize);
        /**
         * Set a baseTexture by ID,
         * ArrayResource just takes resource from it, nothing more
         *
         * @param {PIXI.BaseTexture} baseTexture
         * @param {number} index - Zero-based index of resource to set
         * @return {PIXI.ArrayResource} Instance for chaining
         */
        addBaseTextureAt(baseTexture: BaseTexture, index: number): this;
        /**
         * Add binding
         * @member {PIXI.BaseTexture}
         * @override
         */
        bind(baseTexture: BaseTexture): void;
        /**
         * Upload the resources to the GPU.
         * @param {PIXI.Renderer} renderer
         * @param {PIXI.BaseTexture} texture
         * @param {PIXI.GLTexture} glTexture
         * @returns {boolean} whether texture was uploaded
         */
        upload(renderer: Renderer, texture: BaseTexture, glTexture: GLTexture): boolean;
    }
}
declare namespace feng3d.pixi {
    type IResourcePluginOptions = {
        [key: string]: any;
    };
    type IAutoDetectOptions = ISize | ICubeResourceOptions | IImageResourceOptions | ISVGResourceOptions | IVideoResourceOptions | IResourcePluginOptions;
    /**
     * Shape of supported resource plugins
     *
     */
    interface IResourcePlugin<R, RO> {
        test(source: unknown, extension: string): boolean;
        new (source: any, options?: RO): R;
    }
    /**
     * Collection of installed resource types, class must extend {@link PIXI.Resource}.
     * @example
     * class CustomResource extends PIXI.Resource {
     *   // MUST have source, options constructor signature
     *   // for auto-detected resources to be created.
     *   constructor(source, options) {
     *     super();
     *   }
     *   upload(renderer, baseTexture, glTexture) {
     *     // upload with GL
     *     return true;
     *   }
     *   // used to auto-detect resource
     *   static test(source, extension) {
     *     return extension === 'xyz'|| source instanceof SomeClass;
     *   }
     * }
     * // Install the new resource type
     * PIXI.INSTALLED.push(CustomResource);
     *
     * @type {Array<PIXI.IResourcePlugin>}
     * @static
     * @readonly
     */
    const INSTALLED: Array<IResourcePlugin<any, any>>;
    /**
     * Create a resource element from a single source element. This
     * auto-detects which type of resource to create. All resources that
     * are auto-detectable must have a static `test` method and a constructor
     * with the arguments `(source, options?)`. Currently, the supported
     * resources for auto-detection include:
     *  - {@link PIXI.ImageResource}
     *  - {@link PIXI.CanvasResource}
     *  - {@link PIXI.VideoResource}
     *  - {@link PIXI.SVGResource}
     *  - {@link PIXI.BufferResource}
     * @static
     * @function autoDetectResource
     * @param {string|*} source - Resource source, this can be the URL to the resource,
     *        a typed-array (for BufferResource), HTMLVideoElement, SVG data-uri
     *        or any other resource that can be auto-detected. If not resource is
     *        detected, it's assumed to be an ImageResource.
     * @param {object} [options] - Pass-through options to use for Resource
     * @param {number} [options.width] - Width of BufferResource or SVG rasterization
     * @param {number} [options.height] - Height of BufferResource or SVG rasterization
     * @param {boolean} [options.autoLoad=true] - Image, SVG and Video flag to start loading
     * @param {number} [options.scale=1] - SVG source scale. Overridden by width, height
     * @param {boolean} [options.createBitmap=PIXI.settings.CREATE_IMAGE_BITMAP] - Image option to create Bitmap object
     * @param {boolean} [options.crossorigin=true] - Image and Video option to set crossOrigin
     * @param {boolean} [options.autoPlay=true] - Video option to start playing video immediately
     * @param {number} [options.updateFPS=0] - Video option to update how many times a second the
     *        texture should be updated from the video. Leave at 0 to update at every render
     * @return {PIXI.Resource} The created resource.
     */
    function autoDetectResource<R extends Resource, RO>(source: unknown, options?: RO): R;
}
declare namespace feng3d.pixi {
    /**
     * Base for all the image/canvas resources
     */
    class BaseImageResource extends Resource {
        source: ImageSource;
        noSubImage: boolean;
        /**
         * @param {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement|SVGElement} source
         */
        constructor(source: ImageSource);
        /**
         * Set cross origin based detecting the url and the crossorigin
         * @protected
         * @param {HTMLElement} element - Element to apply crossOrigin
         * @param {string} url - URL to check
         * @param {boolean|string} [crossorigin=true] - Cross origin value to use
         */
        static crossOrigin(element: HTMLImageElement | HTMLVideoElement, url: string, crossorigin?: boolean | string): void;
        /**
         * Upload the texture to the GPU.
         * @param {PIXI.Renderer} renderer - Upload to the renderer
         * @param {PIXI.BaseTexture} baseTexture - Reference to parent texture
         * @param {PIXI.GLTexture} glTexture
         * @param {HTMLImageElement|HTMLCanvasElement|HTMLVideoElement|SVGElement} [source] - (optional)
         * @returns {boolean} true is success
         */
        upload(renderer: Renderer, baseTexture: BaseTexture, glTexture: GLTexture, source?: ImageSource): boolean;
        /**
         * Checks if source width/height was changed, resize can cause extra baseTexture update.
         * Triggers one update in any case.
         */
        update(): void;
        /**
         * Destroy this BaseImageResource
         * @override
         */
        dispose(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * @interface SharedArrayBuffer
     */
    /**
     * Buffer resource with data of typed array.
     */
    class BufferResource extends Resource {
        data: Float32Array | Uint8Array | Uint16Array | Uint32Array;
        /**
         * @param {Float32Array|Uint8Array|Uint32Array} source - Source buffer
         * @param {object} options - Options
         * @param {number} options.width - Width of the texture
         * @param {number} options.height - Height of the texture
         */
        constructor(source: Float32Array | Uint8Array | Uint16Array | Uint32Array, options: ISize);
        /**
         * Upload the texture to the GPU.
         * @param {PIXI.Renderer} renderer - Upload to the renderer
         * @param {PIXI.BaseTexture} baseTexture - Reference to parent texture
         * @param {PIXI.GLTexture} glTexture - glTexture
         * @returns {boolean} true is success
         */
        upload(renderer: Renderer, baseTexture: BaseTexture, glTexture: GLTexture): boolean;
        /**
         * Destroy and don't use after this
         * @override
         */
        dispose(): void;
        /**
         * Used to auto-detect the type of resource.
         *
         * @static
         * @param {*} source - The source object
         * @return {boolean} `true` if <canvas>
         */
        static test(source: unknown): source is Float32Array | Uint8Array | Uint32Array;
    }
}
declare namespace feng3d.pixi {
    /**
     * @interface OffscreenCanvas
     */
    /**
     * Resource type for HTMLCanvasElement.
     */
    class CanvasResource extends BaseImageResource {
        /**
         * @param {HTMLCanvasElement} source - Canvas element to use
         */
        constructor(source: HTMLCanvasElement);
        /**
         * Used to auto-detect the type of resource.
         *
         * @static
         * @param {HTMLCanvasElement|OffscreenCanvas} source - The source object
         * @return {boolean} `true` if source is HTMLCanvasElement or OffscreenCanvas
         */
        static test(source: unknown): source is OffscreenCanvas | HTMLCanvasElement;
    }
}
declare namespace feng3d.pixi {
    /**
     * Constructor options for CubeResource
     */
    interface ICubeResourceOptions extends ISize {
        autoLoad?: boolean;
        linkBaseTexture?: boolean;
    }
    /**
     * Resource for a CubeTexture which contains six resources.
     *
     */
    class CubeResource extends AbstractMultiResource {
        items: ArrayFixed<BaseTexture, 6>;
        linkBaseTexture: boolean;
        /**
         * @param {Array<string|PIXI.Resource>} [source] - Collection of URLs or resources
         *        to use as the sides of the cube.
         * @param {object} [options] - ImageResource options
         * @param {number} [options.width] - Width of resource
         * @param {number} [options.height] - Height of resource
         * @param {number} [options.autoLoad=true] - Whether to auto-load resources
         * @param {number} [options.linkBaseTexture=true] - In case BaseTextures are supplied,
         *   whether to copy them or use
         */
        constructor(source?: ArrayFixed<string | Resource, 6>, options?: ICubeResourceOptions);
        /**
         * Add binding
         *
         * @override
         * @param {PIXI.BaseTexture} baseTexture - parent base texture
         */
        bind(baseTexture: BaseTexture): void;
        addBaseTextureAt(baseTexture: BaseTexture, index: number, linkBaseTexture?: boolean): this;
        /**
         * Upload the resource
         *
         * @returns {boolean} true is success
         */
        upload(renderer: Renderer, _baseTexture: BaseTexture, glTexture: GLTexture): boolean;
        /**
         * Number of texture sides to store for CubeResources
         *
         * @name PIXI.CubeResource.SIDES
         * @static
         * @member {number}
         * @default 6
         */
        static SIDES: number;
        /**
         * Used to auto-detect the type of resource.
         *
         * @static
         * @param {object} source - The source object
         * @return {boolean} `true` if source is an array of 6 elements
         */
        static test(source: unknown): source is ArrayFixed<string | Resource, 6>;
    }
}
declare namespace feng3d.pixi {
    /**
     * Resource type for DepthTexture.
     */
    class DepthResource extends BufferResource {
        /**
         * Upload the texture to the GPU.
         * @param {PIXI.Renderer} renderer - Upload to the renderer
         * @param {PIXI.BaseTexture} baseTexture - Reference to parent texture
         * @param {PIXI.GLTexture} glTexture - glTexture
         * @returns {boolean} true is success
         */
        upload(renderer: Renderer, baseTexture: BaseTexture, glTexture: GLTexture): boolean;
    }
}
declare namespace feng3d.pixi {
    /**
     * Resource type for ImageBitmap.
     */
    class ImageBitmapResource extends BaseImageResource {
        /**
         * @param {ImageBitmap} source - Image element to use
         */
        constructor(source: ImageBitmap);
        /**
         * Used to auto-detect the type of resource.
         *
         * @static
         * @param {ImageBitmap} source - The source object
         * @return {boolean} `true` if source is an ImageBitmap
         */
        static test(source: unknown): source is ImageBitmap;
    }
}
declare namespace feng3d.pixi {
    interface IImageResourceOptions {
        autoLoad?: boolean;
        createBitmap?: boolean;
        crossorigin?: boolean | string;
        alphaMode?: ALPHA_MODES;
    }
    /**
     * Resource type for HTMLImageElement.
     */
    class ImageResource extends BaseImageResource {
        url: string;
        private _load;
        private _process;
        preserveBitmap: boolean;
        createBitmap: boolean;
        alphaMode: ALPHA_MODES;
        bitmap: ImageBitmap;
        /**
         * @param {HTMLImageElement|string} source - image source or URL
         * @param {object} [options]
         * @param {boolean} [options.autoLoad=true] - start loading process
         * @param {boolean} [options.createBitmap=PIXI.settings.CREATE_IMAGE_BITMAP] - whether its required to create
         *        a bitmap before upload
         * @param {boolean} [options.crossorigin=true] - Load image using cross origin
         * @param {PIXI.ALPHA_MODES} [options.alphaMode=PIXI.ALPHA_MODES.UNPACK] - Premultiply image alpha in bitmap
         */
        constructor(source: HTMLImageElement | string, options?: IImageResourceOptions);
        /**
         * returns a promise when image will be loaded and processed
         *
         * @param {boolean} [createBitmap] - whether process image into bitmap
         * @returns {Promise<void>}
         */
        load(createBitmap?: boolean): Promise<ImageResource>;
        /**
         * Called when we need to convert image into BitmapImage.
         * Can be called multiple times, real promise is cached inside.
         *
         * @returns {Promise<void>} cached promise to fill that bitmap
         */
        process(): Promise<ImageResource>;
        /**
         * Upload the image resource to GPU.
         *
         * @param {PIXI.Renderer} renderer - Renderer to upload to
         * @param {PIXI.BaseTexture} baseTexture - BaseTexture for this resource
         * @param {PIXI.GLTexture} glTexture - GLTexture to use
         * @returns {boolean} true is success
         */
        upload(renderer: Renderer, baseTexture: BaseTexture, glTexture: GLTexture): boolean;
        /**
         * Destroys this texture
         * @override
         */
        dispose(): void;
        /**
         * Used to auto-detect the type of resource.
         *
         * @static
         * @param {string|HTMLImageElement} source - The source object
         * @return {boolean} `true` if source is string or HTMLImageElement
         */
        static test(source: unknown): source is string | HTMLImageElement;
    }
}
declare namespace feng3d.pixi {
    interface ISVGResourceOptions {
        source?: string;
        scale?: number;
        width?: number;
        height?: number;
        autoLoad?: boolean;
        crossorigin?: boolean | string;
    }
    /**
     * Resource type for SVG elements and graphics.
     */
    class SVGResource extends BaseImageResource {
        readonly svg: string;
        readonly scale: number;
        readonly _overrideWidth: number;
        readonly _overrideHeight: number;
        private _resolve;
        private _load;
        private _crossorigin?;
        /**
         * @param {string} source - Base64 encoded SVG element or URL for SVG file.
         * @param {object} [options] - Options to use
         * @param {number} [options.scale=1] - Scale to apply to SVG. Overridden by...
         * @param {number} [options.width] - Rasterize SVG this wide. Aspect ratio preserved if height not specified.
         * @param {number} [options.height] - Rasterize SVG this high. Aspect ratio preserved if width not specified.
         * @param {boolean} [options.autoLoad=true] - Start loading right away.
         */
        constructor(sourceBase64: string, options?: ISVGResourceOptions);
        load(): Promise<SVGResource>;
        /**
         * Loads an SVG image from `imageUrl` or `data URL`.
         *
         * @private
         */
        private _loadSvg;
        /**
         * Get size from an svg string using regexp.
         *
         * @method
         * @param {string} svgString - a serialized svg element
         * @return {PIXI.ISize} image extension
         */
        static getSize(svgString?: string): ISize;
        /**
         * Destroys this texture
         * @override
         */
        dispose(): void;
        /**
         * Used to auto-detect the type of resource.
         *
         * @static
         * @param {*} source - The source object
         * @param {string} extension - The extension of source, if set
         */
        static test(source: unknown, extension?: string): boolean;
        /**
         * RegExp for SVG size.
         *
         * @static
         * @constant {RegExp|string} SVG_SIZE
         * @memberof PIXI.SVGResource
         * @example &lt;svg width="100" height="100"&gt;&lt;/svg&gt;
         */
        static SVG_SIZE: RegExp;
    }
}
declare namespace feng3d.pixi {
    interface IVideoResourceOptions {
        autoLoad?: boolean;
        autoPlay?: boolean;
        updateFPS?: number;
        crossorigin?: boolean | string;
    }
    interface IVideoResourceOptionsElement {
        src: string;
        mime: string;
    }
    /**
     * Resource type for HTMLVideoElement.
     */
    class VideoResource extends BaseImageResource {
        protected _autoUpdate: boolean;
        protected _isConnectedToTicker: boolean;
        protected _updateFPS: number;
        protected _msToNextUpdate: number;
        protected autoPlay: boolean;
        private _load;
        private _resolve;
        /**
         * @param {HTMLVideoElement|object|string|Array<string|object>} source - Video element to use.
         * @param {object} [options] - Options to use
         * @param {boolean} [options.autoLoad=true] - Start loading the video immediately
         * @param {boolean} [options.autoPlay=true] - Start playing video immediately
         * @param {number} [options.updateFPS=0] - How many times a second to update the texture from the video.
         * Leave at 0 to update at every render.
         * @param {boolean} [options.crossorigin=true] - Load image using cross origin
         */
        constructor(source?: HTMLVideoElement | Array<string | IVideoResourceOptionsElement> | string, options?: IVideoResourceOptions);
        /**
         * Trigger updating of the texture
         *
         * @param {number} [deltaTime=0] - time delta since last tick
         */
        update(_deltaTime?: number): void;
        /**
         * Start preloading the video resource.
         *
         * @protected
         * @return {Promise<void>} Handle the validate event
         */
        load(): Promise<VideoResource>;
        /**
         * Handle video error events.
         *
         * @private
         */
        private _onError;
        /**
         * Returns true if the underlying source is playing.
         *
         * @private
         * @return {boolean} True if playing.
         */
        private _isSourcePlaying;
        /**
         * Returns true if the underlying source is ready for playing.
         *
         * @private
         * @return {boolean} True if ready.
         */
        private _isSourceReady;
        /**
         * Runs the update loop when the video is ready to play
         *
         * @private
         */
        private _onPlayStart;
        /**
         * Fired when a pause event is triggered, stops the update loop
         *
         * @private
         */
        private _onPlayStop;
        /**
         * Fired when the video is loaded and ready to play
         *
         * @private
         */
        private _onCanPlay;
        /**
         * Destroys this texture
         * @override
         */
        dispose(): void;
        /**
         * Should the base texture automatically update itself, set to true by default
         *
         * @member {boolean}
         */
        get autoUpdate(): boolean;
        set autoUpdate(value: boolean);
        /**
         * How many times a second to update the texture from the video. Leave at 0 to update at every render.
         * A lower fps can help performance, as updating the texture at 60fps on a 30ps video may not be efficient.
         *
         * @member {number}
         */
        get updateFPS(): number;
        set updateFPS(value: number);
        /**
         * Used to auto-detect the type of resource.
         *
         * @static
         * @param {*} source - The source object
         * @param {string} extension - The extension of source, if set
         * @return {boolean} `true` if video source
         */
        static test(source: unknown, extension?: string): source is HTMLVideoElement;
        /**
         * List of common video file extensions supported by VideoResource.
         * @constant
         * @member {Array<string>}
         * @static
         * @readonly
         */
        static TYPES: string[];
        /**
         * Map of video MIME types that can't be directly derived from file extensions.
         * @constant
         * @member {object}
         * @static
         * @readonly
         */
        static MIME_TYPES: Dict<string>;
    }
}
declare namespace feng3d.pixi {
    const resources: {
        Resource: typeof Resource;
    };
}
declare namespace feng3d.pixi {
    /**
     * Stores a texture's frame in UV coordinates, in
     * which everything lies in the rectangle `[(0,0), (1,0),
     * (1,1), (0,1)]`.
     *
     * | Corner       | Coordinates |
     * |--------------|-------------|
     * | Top-Left     | `(x0,y0)`   |
     * | Top-Right    | `(x1,y1)`   |
     * | Bottom-Right | `(x2,y2)`   |
     * | Bottom-Left  | `(x3,y3)`   |
     *
     * @protected
     */
    class TextureUvs {
        x0: number;
        y0: number;
        x1: number;
        y1: number;
        x2: number;
        y2: number;
        x3: number;
        y3: number;
        uvsFloat32: Float32Array;
        constructor();
        /**
         * Sets the texture Uvs based on the given frame information.
         *
         * @protected
         * @param {PIXI.Rectangle} frame - The frame of the texture
         * @param {PIXI.Rectangle} baseFrame - The base frame of the texture
         * @param {number} rotate - Rotation of frame, see {@link PIXI.groupD8}
         */
        set(frame: Rectangle, baseFrame: ISize, rotate: number): void;
        toString(): string;
    }
}
declare namespace feng3d.pixi {
    type ImageSource = HTMLImageElement | HTMLCanvasElement | HTMLVideoElement | ImageBitmap;
    interface IBaseTextureOptions<RO = any> {
        alphaMode?: ALPHA_MODES;
        mipmap?: MIPMAP_MODES;
        anisotropicLevel?: number;
        scaleMode?: SCALE_MODES;
        width?: number;
        height?: number;
        wrapMode?: WRAP_MODES;
        format?: FORMATS;
        type?: TYPES;
        target?: TARGETS;
        resolution?: number;
        resourceOptions?: RO;
        pixiIdPrefix?: string;
    }
    interface BaseTextureEventMap {
        loaded: BaseTexture;
        update: BaseTexture;
        dispose: {
            texture: BaseTexture;
            skipRemove?: boolean;
        };
        error: {
            texture: BaseTexture | Texture;
            skipRemove?: boolean;
            event?: ErrorEvent;
        };
    }
    /**
     * A Texture stores the information that represents an image.
     * All textures have a base texture, which contains information about the source.
     * Therefore you can have many textures all using a single BaseTexture
     *
     * @typeParam R - The BaseTexture's Resource type.
     * @typeParam RO - The options for constructing resource.
     */
    class BaseTexture<R extends Resource = Resource, RO = IAutoDetectOptions, T extends BaseTextureEventMap = BaseTextureEventMap> extends EventEmitter<T> {
        width: number;
        height: number;
        resolution: number;
        alphaMode?: ALPHA_MODES;
        mipmap?: MIPMAP_MODES;
        anisotropicLevel?: number;
        scaleMode?: SCALE_MODES;
        wrapMode?: WRAP_MODES;
        format?: FORMATS;
        type?: TYPES;
        target?: TARGETS;
        readonly uid: number;
        touched: number;
        isPowerOfTwo: boolean;
        _glTextures: {
            [key: number]: GLTexture;
        };
        dirtyId: number;
        dirtyStyleId: number;
        cacheId: string;
        valid: boolean;
        textureCacheIds: Array<string>;
        destroyed: boolean;
        resource: R;
        _batchEnabled: number;
        _batchLocation: number;
        parentTextureArray: BaseTexture;
        /**
         * @param {PIXI.Resource|string|HTMLImageElement|HTMLCanvasElement|HTMLVideoElement} [resource=null] -
         *        The current resource to use, for things that aren't Resource objects, will be converted
         *        into a Resource.
         * @param {Object} [options] - Collection of options
         * @param {PIXI.MIPMAP_MODES} [options.mipmap=PIXI.settings.MIPMAP_TEXTURES] - If mipmapping is enabled for texture
         * @param {number} [options.anisotropicLevel=PIXI.settings.ANISOTROPIC_LEVEL] - Anisotropic filtering level of texture
         * @param {PIXI.WRAP_MODES} [options.wrapMode=PIXI.settings.WRAP_MODE] - Wrap mode for textures
         * @param {PIXI.SCALE_MODES} [options.scaleMode=PIXI.settings.SCALE_MODE] - Default scale mode, linear, nearest
         * @param {PIXI.FORMATS} [options.format=PIXI.FORMATS.RGBA] - GL format type
         * @param {PIXI.TYPES} [options.type=PIXI.TYPES.UNSIGNED_BYTE] - GL data type
         * @param {PIXI.TARGETS} [options.target=PIXI.TARGETS.TEXTURE_2D] - GL texture target
         * @param {PIXI.ALPHA_MODES} [options.alphaMode=PIXI.ALPHA_MODES.UNPACK] - Pre multiply the image alpha
         * @param {number} [options.width=0] - Width of the texture
         * @param {number} [options.height=0] - Height of the texture
         * @param {number} [options.resolution] - Resolution of the base texture
         * @param {object} [options.resourceOptions] - Optional resource options,
         *        see {@link PIXI.autoDetectResource autoDetectResource}
         */
        constructor(resource?: R | ImageSource | string | any, options?: IBaseTextureOptions<RO>);
        /**
         * Pixel width of the source of this texture
         *
         * @readonly
         * @member {number}
         */
        get realWidth(): number;
        /**
         * Pixel height of the source of this texture
         *
         * @readonly
         * @member {number}
         */
        get realHeight(): number;
        /**
         * Changes style options of BaseTexture
         *
         * @param {PIXI.SCALE_MODES} [scaleMode] - Pixi scalemode
         * @param {PIXI.MIPMAP_MODES} [mipmap] - enable mipmaps
         * @returns {PIXI.BaseTexture} this
         */
        setStyle(scaleMode?: SCALE_MODES, mipmap?: MIPMAP_MODES): this;
        /**
         * Changes w/h/resolution. Texture becomes valid if width and height are greater than zero.
         *
         * @param {number} width - Visual width
         * @param {number} height - Visual height
         * @param {number} [resolution] - Optionally set resolution
         * @returns {PIXI.BaseTexture} this
         */
        setSize(width: number, height: number, resolution?: number): this;
        /**
         * Sets real size of baseTexture, preserves current resolution.
         *
         * @param {number} realWidth - Full rendered width
         * @param {number} realHeight - Full rendered height
         * @param {number} [resolution] - Optionally set resolution
         * @returns {PIXI.BaseTexture} this
         */
        setRealSize(realWidth: number, realHeight: number, resolution?: number): this;
        /**
         * Refresh check for isPowerOfTwo texture based on size
         *
         * @private
         */
        protected _refreshPOT(): void;
        /**
         * Changes resolution
         *
         * @param {number} resolution - res
         * @returns {PIXI.BaseTexture} this
         */
        setResolution(resolution: number): this;
        /**
         * Sets the resource if it wasn't set. Throws error if resource already present
         *
         * @param {PIXI.Resource} resource - that is managing this BaseTexture
         * @returns {PIXI.BaseTexture} this
         */
        setResource(resource: R): this;
        /**
         * Invalidates the object. Texture becomes valid if width and height are greater than zero.
         */
        update(): void;
        /**
         * Handle errors with resources.
         * @private
         * @param {ErrorEvent} event - Error event emitted.
         */
        onError(event: ErrorEvent): void;
        /**
         * Destroys this base texture.
         * The method stops if resource doesn't want this texture to be destroyed.
         * Removes texture from all caches.
         */
        destroy(): void;
        /**
         * Frees the texture from WebGL memory without destroying this texture object.
         * This means you can still use the texture later which will upload it to GPU
         * memory again.
         *
         * @fires PIXI.BaseTexture#dispose
         */
        dispose(): void;
        /**
         * Utility function for BaseTexture|Texture cast
         */
        castToBaseTexture(): BaseTexture;
        /**
         * Helper function that creates a base texture based on the source you provide.
         * The source can be - image url, image element, canvas element. If the
         * source is an image url or an image element and not in the base texture
         * cache, it will be created and loaded.
         *
         * @static
         * @param {string|HTMLImageElement|HTMLCanvasElement|SVGElement|HTMLVideoElement} source - The
         *        source to create base texture from.
         * @param {object} [options] - See {@link PIXI.BaseTexture}'s constructor for options.
         * @param {string} [options.pixiIdPrefix=pixiid] - If a source has no id, this is the prefix of the generated id
         * @param {boolean} [strict] - Enforce strict-mode, see {@link PIXI.settings.STRICT_TEXTURE_CACHE}.
         * @returns {PIXI.BaseTexture} The new base texture.
         */
        static from<R extends Resource = Resource, RO = IAutoDetectOptions>(source: ImageSource | string, options?: IBaseTextureOptions<RO>, strict?: boolean): BaseTexture<R>;
        /**
         * Create a new BaseTexture with a BufferResource from a Float32Array.
         * RGBA values are floats from 0 to 1.
         * @static
         * @param {Float32Array|Uint8Array} buffer - The optional array to use, if no data
         *        is provided, a new Float32Array is created.
         * @param {number} width - Width of the resource
         * @param {number} height - Height of the resource
         * @param {object} [options] - See {@link PIXI.BaseTexture}'s constructor for options.
         * @return {PIXI.BaseTexture} The resulting new BaseTexture
         */
        static fromBuffer(buffer: Float32Array | Uint8Array, width: number, height: number, options?: IBaseTextureOptions): BaseTexture<BufferResource>;
        /**
         * Adds a BaseTexture to the global BaseTextureCache. This cache is shared across the whole PIXI object.
         *
         * @static
         * @param {PIXI.BaseTexture} baseTexture - The BaseTexture to add to the cache.
         * @param {string} id - The id that the BaseTexture will be stored against.
         */
        static addToCache(baseTexture: BaseTexture, id: string): void;
        /**
         * Remove a BaseTexture from the global BaseTextureCache.
         *
         * @static
         * @param {string|PIXI.BaseTexture} baseTexture - id of a BaseTexture to be removed, or a BaseTexture instance itself.
         * @return {PIXI.BaseTexture|null} The BaseTexture that was removed.
         */
        static removeFromCache(baseTexture: string | BaseTexture): BaseTexture | null;
        /**
         * Global number of the texture batch, used by multi-texture renderers
         *
         * @static
         * @member {number}
         */
        static _globalBatch: number;
    }
}
declare namespace feng3d.pixi {
    /**
     * Internal texture for WebGL context
     */
    class GLTexture {
        texture: WebGLTexture;
        width: number;
        height: number;
        mipmap: boolean;
        wrapMode: number;
        type: number;
        internalFormat: number;
        dirtyId: number;
        dirtyStyleId: number;
        constructor(texture: WebGLTexture);
    }
}
declare namespace feng3d.pixi {
    type TextureSource = string | BaseTexture | ImageSource;
    /**
     * A texture stores the information that represents an image or part of an image.
     *
     * It cannot be added to the display list directly; instead use it as the texture for a Sprite.
     * If no frame is provided for a texture, then the whole image is used.
     *
     * You can directly create a texture from an image and then reuse it multiple times like this :
     *
     * ```js
     * let texture = PIXI.Texture.from('assets/image.png');
     * let sprite1 = new PIXI.Sprite(texture);
     * let sprite2 = new PIXI.Sprite(texture);
     * ```
     *
     * If you didnt pass the texture frame to constructor, it enables `noFrame` mode:
     * it subscribes on baseTexture events, it automatically resizes at the same time as baseTexture.
     *
     * Textures made from SVGs, loaded or not, cannot be used before the file finishes processing.
     * You can check for this by checking the sprite's _textureID property.
     * ```js
     * var texture = PIXI.Texture.from('assets/image.svg');
     * var sprite1 = new PIXI.Sprite(texture);
     * //sprite1._textureID should not be undefined if the texture has finished processing the SVG file
     * ```
     * You can use a ticker or rAF to ensure your sprites load the finished textures after processing. See issue #3068.
     *
     * @typeParam R - The BaseTexture's Resource type.
     */
    class Texture<R extends Resource = Resource> extends EventEmitter {
        baseTexture: BaseTexture<R>;
        orig: Rectangle;
        trim: Rectangle;
        valid: boolean;
        noFrame: boolean;
        defaultAnchor: Vector2;
        uvMatrix: TextureMatrix;
        protected _rotate: number;
        _updateID: number;
        _frame: Rectangle;
        _uvs: TextureUvs;
        textureCacheIds: Array<string>;
        /**
         * @param {PIXI.BaseTexture} baseTexture - The base texture source to create the texture from
         * @param {PIXI.Rectangle} [frame] - The rectangle frame of the texture to show
         * @param {PIXI.Rectangle} [orig] - The area of original texture
         * @param {PIXI.Rectangle} [trim] - Trimmed rectangle of original texture
         * @param {number} [rotate] - indicates how the texture was rotated by texture packer. See {@link PIXI.groupD8}
         * @param {PIXI.IPointData} [anchor] - Default anchor point used for sprite placement / rotation
         */
        constructor(baseTexture: BaseTexture<R>, frame?: Rectangle, orig?: Rectangle, trim?: Rectangle, rotate?: number, anchor?: Vector2);
        /**
         * Updates this texture on the gpu.
         *
         * Calls the TextureResource update.
         *
         * If you adjusted `frame` manually, please call `updateUvs()` instead.
         *
         */
        update(): void;
        /**
         * Called when the base texture is updated
         *
         * @protected
         * @param {PIXI.BaseTexture} baseTexture - The base texture.
         */
        onBaseTextureUpdated(e: Event<BaseTexture>): void;
        /**
         * Destroys this texture
         *
         * @param {boolean} [destroyBase=false] - Whether to destroy the base texture as well
         */
        destroy(destroyBase?: boolean): void;
        /**
         * Creates a new texture object that acts the same as this one.
         *
         * @return {PIXI.Texture} The new texture
         */
        clone(): Texture;
        /**
         * Updates the internal WebGL UV cache. Use it after you change `frame` or `trim` of the texture.
         * Call it after changing the frame
         */
        updateUvs(): void;
        /**
         * Helper function that creates a new Texture based on the source you provide.
         * The source can be - frame id, image url, video url, canvas element, video element, base texture
         *
         * @static
         * @param {string|HTMLImageElement|HTMLCanvasElement|HTMLVideoElement|PIXI.BaseTexture} source -
         *        Source to create texture from
         * @param {object} [options] - See {@link PIXI.BaseTexture}'s constructor for options.
         * @param {string} [options.pixiIdPrefix=pixiid] - If a source has no id, this is the prefix of the generated id
         * @param {boolean} [strict] - Enforce strict-mode, see {@link PIXI.settings.STRICT_TEXTURE_CACHE}.
         * @return {PIXI.Texture} The newly created texture
         */
        static from<R extends Resource = Resource, RO = any>(source: TextureSource, options?: IBaseTextureOptions<RO>, strict?: boolean): Texture<R>;
        /**
         * Useful for loading textures via URLs. Use instead of `Texture.from` because
         * it does a better job of handling failed URLs more effectively. This also ignores
         * `PIXI.settings.STRICT_TEXTURE_CACHE`. Works for Videos, SVGs, Images.
         * @param {string} url - The remote URL to load.
         * @param {object} [options] - Optional options to include
         * @return {Promise<PIXI.Texture>} A Promise that resolves to a Texture.
         */
        static fromURL<R extends Resource = Resource, RO = any>(url: string, options?: IBaseTextureOptions<RO>): Promise<Texture<R>>;
        /**
         * Create a new Texture with a BufferResource from a Float32Array.
         * RGBA values are floats from 0 to 1.
         * @static
         * @param {Float32Array|Uint8Array} buffer - The optional array to use, if no data
         *        is provided, a new Float32Array is created.
         * @param {number} width - Width of the resource
         * @param {number} height - Height of the resource
         * @param {object} [options] - See {@link PIXI.BaseTexture}'s constructor for options.
         * @return {PIXI.Texture} The resulting new BaseTexture
         */
        static fromBuffer(buffer: Float32Array | Uint8Array, width: number, height: number, options?: IBaseTextureOptions<ISize>): Texture<BufferResource>;
        /**
         * Create a texture from a source and add to the cache.
         *
         * @static
         * @param {HTMLImageElement|HTMLCanvasElement|string} source - The input source.
         * @param {String} imageUrl - File name of texture, for cache and resolving resolution.
         * @param {String} [name] - Human readable name for the texture cache. If no name is
         *        specified, only `imageUrl` will be used as the cache ID.
         * @return {PIXI.Texture} Output texture
         */
        static fromLoader<R extends Resource = Resource>(source: HTMLImageElement | HTMLCanvasElement | string, imageUrl: string, name?: string, options?: IBaseTextureOptions): Promise<Texture<R>>;
        /**
         * Adds a Texture to the global TextureCache. This cache is shared across the whole PIXI object.
         *
         * @static
         * @param {PIXI.Texture} texture - The Texture to add to the cache.
         * @param {string} id - The id that the Texture will be stored against.
         */
        static addToCache(texture: Texture, id: string): void;
        /**
         * Remove a Texture from the global TextureCache.
         *
         * @static
         * @param {string|PIXI.Texture} texture - id of a Texture to be removed, or a Texture instance itself
         * @return {PIXI.Texture|null} The Texture that was removed
         */
        static removeFromCache(texture: string | Texture): Texture | null;
        /**
         * Returns resolution of baseTexture
         *
         * @member {number}
         * @readonly
         */
        get resolution(): number;
        /**
         * The frame specifies the region of the base texture that this texture uses.
         * Please call `updateUvs()` after you change coordinates of `frame` manually.
         *
         * @member {PIXI.Rectangle}
         */
        get frame(): Rectangle;
        set frame(frame: Rectangle);
        /**
         * Indicates whether the texture is rotated inside the atlas
         * set to 2 to compensate for texture packer rotation
         * set to 6 to compensate for spine packer rotation
         * can be used to rotate or mirror sprites
         * See {@link PIXI.groupD8} for explanation
         *
         * @member {number}
         */
        get rotate(): number;
        set rotate(rotate: number);
        /**
         * The width of the Texture in pixels.
         *
         * @member {number}
         */
        get width(): number;
        /**
         * The height of the Texture in pixels.
         *
         * @member {number}
         */
        get height(): number;
        /**
         * Utility function for BaseTexture|Texture cast
         */
        castToBaseTexture(): BaseTexture;
        static readonly EMPTY: Texture<CanvasResource>;
        static readonly WHITE: Texture<CanvasResource>;
    }
}
declare namespace feng3d.pixi {
    interface IUnloadableTexture {
        _texture: Texture | RenderTexture;
        children: IUnloadableTexture[];
    }
    /**
     * System plugin to the renderer to manage texture garbage collection on the GPU,
     * ensuring that it does not get clogged up with textures that are no longer being used.
     *
     */
    class TextureGCSystem extends System {
        count: number;
        checkCount: number;
        maxIdle: number;
        checkCountMax: number;
        mode: number;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Checks to see when the last time a texture was used
         * if the texture has not been used for a specified amount of time it will be removed from the GPU
         */
        protected postrender(): void;
        /**
         * Checks to see when the last time a texture was used
         * if the texture has not been used for a specified amount of time it will be removed from the GPU
         */
        run(): void;
        /**
         * Removes all the textures within the specified displayObject and its children from the GPU
         *
         * @param {PIXI.DisplayObject} displayObject - the displayObject to remove the textures from.
         */
        unload(displayObject: IUnloadableTexture): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Class controls uv mapping from Texture normal space to BaseTexture normal space.
     *
     * Takes `trim` and `rotate` into account. May contain clamp settings for Meshes and TilingSprite.
     *
     * Can be used in Texture `uvMatrix` field, or separately, you can use different clamp settings on the same texture.
     * If you want to add support for texture region of certain feature or filter, that's what you're looking for.
     *
     * Takes track of Texture changes through `_lastTextureID` private field.
     * Use `update()` method call to track it from outside.
     *
     * @see PIXI.Texture
     * @see PIXI.Mesh
     * @see PIXI.TilingSprite
     */
    class TextureMatrix {
        mapCoord: Matrix;
        clampOffset: number;
        clampMargin: number;
        readonly uClampFrame: Float32Array;
        readonly uClampOffset: Float32Array;
        _textureID: number;
        _updateID: number;
        _texture: Texture;
        isSimple: boolean;
        /**
         *
         * @param {PIXI.Texture} texture - observed texture
         * @param {number} [clampMargin] - Changes frame clamping, 0.5 by default. Use -0.5 for extra border.
         * @constructor
         */
        constructor(texture: Texture, clampMargin?: number);
        /**
         * texture property
         * @member {PIXI.Texture}
         */
        get texture(): Texture;
        set texture(value: Texture);
        /**
         * Multiplies uvs array to transform
         * @param {Float32Array} uvs - mesh uvs
         * @param {Float32Array} [out=uvs] - output
         * @returns {Float32Array} output
         */
        multiplyUvs(uvs: Float32Array, out?: Float32Array): Float32Array;
        /**
         * updates matrices if texture was changed
         * @param {boolean} [forceUpdate=false] - if true, matrices will be updated any case
         * @returns {boolean} whether or not it was updated
         */
        update(forceUpdate?: boolean): boolean;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage textures.
     *
     */
    class TextureSystem extends System {
        boundTextures: BaseTexture[];
        managedTextures: Array<BaseTexture>;
        protected CONTEXT_UID: number;
        protected gl: IRenderingContext;
        protected webGLVersion: number;
        protected unknownTexture: BaseTexture;
        protected _unknownBoundTextures: boolean;
        currentLocation: number;
        emptyTextures: {
            [key: number]: GLTexture;
        };
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Sets up the renderer context and necessary buffers.
         */
        contextChange(): void;
        /**
         * Bind a texture to a specific location
         *
         * If you want to unbind something, please use `unbind(texture)` instead of `bind(null, textureLocation)`
         *
         * @param {PIXI.Texture|PIXI.BaseTexture} texture_ - Texture to bind
         * @param {number} [location=0] - Location to bind at
         */
        bind(texture: Texture | BaseTexture, location?: number): void;
        /**
         * Resets texture location and bound textures
         *
         * Actual `bind(null, i)` calls will be performed at next `unbind()` call
         */
        reset(): void;
        /**
         * Unbind a texture
         * @param {PIXI.BaseTexture} texture - Texture to bind
         */
        unbind(texture?: BaseTexture): void;
        /**
         * Initialize a texture
         *
         * @private
         * @param {PIXI.BaseTexture} texture - Texture to initialize
         */
        initTexture(texture: BaseTexture): GLTexture;
        initTextureType(texture: BaseTexture, glTexture: GLTexture): void;
        /**
         * Update a texture
         *
         * @private
         * @param {PIXI.BaseTexture} texture - Texture to initialize
         */
        updateTexture(texture: BaseTexture): void;
        private _onDestroyTexture;
        /**
         * Deletes the texture from WebGL
         *
         * @private
         * @param {PIXI.BaseTexture|PIXI.Texture} texture_ - the texture to destroy
         * @param {boolean} [skipRemove=false] - Whether to skip removing the texture from the TextureManager.
         */
        destroyTexture(texture: BaseTexture | Texture, skipRemove?: boolean): void;
        /**
         * Update texture style such as mipmap flag
         *
         * @private
         * @param {PIXI.BaseTexture} texture - Texture to update
         */
        updateTextureStyle(texture: BaseTexture): void;
        /**
         * Set style for texture
         *
         * @private
         * @param {PIXI.BaseTexture} texture - Texture to update
         * @param {PIXI.GLTexture} glTexture
         */
        setStyle(texture: BaseTexture, glTexture: GLTexture): void;
    }
    interface ISystems {
        TextureSystem: new (...args: any) => TextureSystem;
    }
}
declare namespace feng3d.pixi {
    interface BaseRenderTexture extends BaseTexture {
    }
    /**
     * A BaseRenderTexture is a special texture that allows any PixiJS display object to be rendered to it.
     *
     * __Hint__: All DisplayObjects (i.e. Sprites) that render to a BaseRenderTexture should be preloaded
     * otherwise black rectangles will be drawn instead.
     *
     * A BaseRenderTexture takes a snapshot of any Display Object given to its render method. The position
     * and rotation of the given Display Objects is ignored. For example:
     *
     * ```js
     * let renderer = PIXI.autoDetectRenderer();
     * let baseRenderTexture = new PIXI.BaseRenderTexture({ width: 800, height: 600 });
     * let renderTexture = new PIXI.RenderTexture(baseRenderTexture);
     * let sprite = PIXI.Sprite.from("spinObj_01.png");
     *
     * sprite.position.x = 800/2;
     * sprite.position.y = 600/2;
     * sprite.anchorX = 0.5;
     * sprite.anchorY = 0.5;
     *
     * renderer.render(sprite, {renderTexture});
     * ```
     *
     * The Sprite in this case will be rendered using its local transform. To render this sprite at 0,0
     * you can clear the transform
     *
     * ```js
     *
     * sprite.setTransform()
     *
     * let baseRenderTexture = new PIXI.BaseRenderTexture({ width: 100, height: 100 });
     * let renderTexture = new PIXI.RenderTexture(baseRenderTexture);
     *
     * renderer.render(sprite, {renderTexture});  // Renders to center of RenderTexture
     * ```
     *
     */
    class BaseRenderTexture extends BaseTexture {
        clearColor: number[];
        framebuffer: Framebuffer;
        maskStack: Array<MaskData>;
        filterStack: Array<any>;
        /**
         * @param {object} [options]
         * @param {number} [options.width=100] - The width of the base render texture.
         * @param {number} [options.height=100] - The height of the base render texture.
         * @param {PIXI.SCALE_MODES} [options.scaleMode] - See {@link PIXI.SCALE_MODES} for possible values.
         * @param {number} [options.resolution=1] - The resolution / device pixel ratio of the texture being generated.
         */
        constructor(options?: IBaseTextureOptions);
        /**
         * Resizes the BaseRenderTexture.
         *
         * @param {number} width - The width to resize to.
         * @param {number} height - The height to resize to.
         */
        resize(width: number, height: number): void;
        /**
         * Frees the texture and framebuffer from WebGL memory without destroying this texture object.
         * This means you can still use the texture later which will upload it to GPU
         * memory again.
         *
         * @fires PIXI.BaseTexture#dispose
         */
        dispose(): void;
        /**
         * Destroys this texture.
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * A RenderTexture is a special texture that allows any PixiJS display object to be rendered to it.
     *
     * __Hint__: All DisplayObjects (i.e. Sprites) that render to a RenderTexture should be preloaded
     * otherwise black rectangles will be drawn instead.
     *
     * __Hint-2__: The actual memory allocation will happen on first render.
     * You shouldn't create renderTextures each frame just to delete them after, try to reuse them.
     *
     * A RenderTexture takes a snapshot of any Display Object given to its render method. For example:
     *
     * ```js
     * let renderer = PIXI.autoDetectRenderer();
     * let renderTexture = PIXI.RenderTexture.create({ width: 800, height: 600 });
     * let sprite = PIXI.Sprite.from("spinObj_01.png");
     *
     * sprite.position.x = 800/2;
     * sprite.position.y = 600/2;
     * sprite.anchorX = 0.5;
     * sprite.anchorY = 0.5;
     *
     * renderer.render(sprite, {renderTexture});
     * ```
     * Note that you should not create a new renderer, but reuse the same one as the rest of the application.
     *
     * The Sprite in this case will be rendered using its local transform. To render this sprite at 0,0
     * you can clear the transform
     *
     * ```js
     *
     * sprite.setTransform()
     *
     * let renderTexture = new PIXI.RenderTexture.create({ width: 100, height: 100 });
     *
     * renderer.render(sprite, {renderTexture});  // Renders to center of RenderTexture
     * ```
     *
     */
    class RenderTexture extends Texture {
        baseTexture: BaseRenderTexture;
        filterFrame: Rectangle | null;
        filterPoolKey: string | number | null;
        /**
         * @param {PIXI.BaseRenderTexture} baseRenderTexture - The base texture object that this texture uses
         * @param {PIXI.Rectangle} [frame] - The rectangle frame of the texture to show
         */
        constructor(baseRenderTexture: BaseRenderTexture, frame?: Rectangle);
        /**
         * Shortcut to `this.baseTexture.framebuffer`, saves baseTexture cast.
         * @member {PIXI.Framebuffer}
         * @readonly
         */
        get framebuffer(): Framebuffer;
        /**
         * Resizes the RenderTexture.
         *
         * @param {number} width - The width to resize to.
         * @param {number} height - The height to resize to.
         * @param {boolean} [resizeBaseTexture=true] - Should the baseTexture.width and height values be resized as well?
         */
        resize(width: number, height: number, resizeBaseTexture?: boolean): void;
        /**
         * Changes the resolution of baseTexture, but does not change framebuffer size.
         *
         * @param {number} resolution - The new resolution to apply to RenderTexture
         */
        setResolution(resolution: number): void;
        /**
         * Use the object-based construction instead.
         *
         * @method
         * @deprecated since 6.0.0
         * @param {number} [width]
         * @param {number} [height]
         * @param {PIXI.SCALE_MODES} [scaleMode=PIXI.settings.SCALE_MODE]
         * @param {number} [resolution=1]
         */
        static create(width: number, height: number, scaleMode?: SCALE_MODES, resolution?: number): RenderTexture;
        /**
         * A short hand way of creating a render texture.
         *
         * @method
         * @param {object} [options] - Options
         * @param {number} [options.width=100] - The width of the render texture
         * @param {number} [options.height=100] - The height of the render texture
         * @param {number} [options.scaleMode=PIXI.settings.SCALE_MODE] - See {@link PIXI.SCALE_MODES} for possible values
         * @param {number} [options.resolution=1] - The resolution / device pixel ratio of the texture being generated
         * @return {PIXI.RenderTexture} The new render texture
         */
        static create(options?: IBaseTextureOptions): RenderTexture;
    }
}
declare namespace feng3d.pixi {
    /**
     * Experimental!
     *
     * Texture pool, used by FilterSystem and plugins
     * Stores collection of temporary pow2 or screen-sized renderTextures
     *
     * If you use custom RenderTexturePool for your filters, you can use methods
     * `getFilterTexture` and `returnFilterTexture` same as in
     *
     */
    class RenderTexturePool {
        textureOptions: IBaseTextureOptions;
        enableFullScreen: boolean;
        texturePool: {
            [x in string | number]: RenderTexture[];
        };
        private _pixelsWidth;
        private _pixelsHeight;
        /**
         * @param {object} [textureOptions] - options that will be passed to BaseRenderTexture constructor
         * @param {PIXI.SCALE_MODES} [textureOptions.scaleMode] - See {@link PIXI.SCALE_MODES} for possible values.
         */
        constructor(textureOptions?: IBaseTextureOptions);
        /**
         * creates of texture with params that were specified in pool constructor
         *
         * @param {number} realWidth - width of texture in pixels
         * @param {number} realHeight - height of texture in pixels
         * @returns {RenderTexture}
         */
        createTexture(realWidth: number, realHeight: number): RenderTexture;
        /**
         * Gets a Power-of-Two render texture or fullScreen texture
         *
         * @protected
         * @param {number} minWidth - The minimum width of the render texture in real pixels.
         * @param {number} minHeight - The minimum height of the render texture in real pixels.
         * @param {number} [resolution=1] - The resolution of the render texture.
         * @return {PIXI.RenderTexture} The new render texture.
         */
        getOptimalTexture(minWidth: number, minHeight: number, resolution?: number): RenderTexture;
        /**
         * Gets extra texture of the same size as input renderTexture
         *
         * `getFilterTexture(input, 0.5)` or `getFilterTexture(0.5, input)`
         *
         * @param {PIXI.RenderTexture} input - renderTexture from which size and resolution will be copied
         * @param {number} [resolution] - override resolution of the renderTexture
         *  It overrides, it does not multiply
         * @returns {PIXI.RenderTexture}
         */
        getFilterTexture(input: RenderTexture, resolution?: number): RenderTexture;
        /**
         * Place a render texture back into the pool.
         * @param {PIXI.RenderTexture} renderTexture - The renderTexture to free
         */
        returnTexture(renderTexture: RenderTexture): void;
        /**
         * Alias for returnTexture, to be compliant with FilterSystem interface
         * @param {PIXI.RenderTexture} renderTexture - The renderTexture to free
         */
        returnFilterTexture(renderTexture: RenderTexture): void;
        /**
         * Clears the pool
         *
         * @param {boolean} [destroyTextures=true] - destroy all stored textures
         */
        clear(destroyTextures?: boolean): void;
        /**
         * If screen size was changed, drops all screen-sized textures,
         * sets new screen size, sets `enableFullScreen` to true
         *
         * Size is measured in pixels, `renderer.view` can be passed here, not `renderer.screen`
         *
         * @param {PIXI.ISize} size - Initial size of screen
         */
        setScreenSize(size: ISize): void;
        /**
         * Key that is used to store fullscreen renderTextures in a pool
         *
         * @static
         * @const {string}
         */
        static SCREEN_KEY: string;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage render textures.
     *
     * Should be added after FramebufferSystem
     *
     * ### Frames
     *
     * The `RenderTextureSystem` holds a sourceFrame → destinationFrame projection. The following table explains the different
     * coordinate spaces used:
     *
     * | Frame                  | Description                                                      | Coordinate System                                       |
     * | ---------------------- | ---------------------------------------------------------------- | ------------------------------------------------------- |
     * | sourceFrame            | The rectangle inside of which display-objects are being rendered | **World Space**: The origin on the top-left             |
     * | destinationFrame       | The rectangle in the render-target (canvas or texture) into which contents should be rendered | If rendering to the canvas, this is in screen space and the origin is on the top-left. If rendering to a render-texture, this is in its base-texture's space with the origin on the bottom-left.  |
     * | viewportFrame          | The framebuffer viewport corresponding to the destination-frame  | **Window Coordinates**: The origin is always on the bottom-left. |
     *
     */
    class RenderTextureSystem extends System {
        clearColor: number[];
        defaultMaskStack: Array<MaskData>;
        current: RenderTexture;
        readonly sourceFrame: Rectangle;
        readonly destinationFrame: Rectangle;
        readonly viewportFrame: Rectangle;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Bind the current render texture
         *
         * @param {PIXI.RenderTexture} [renderTexture] - RenderTexture to bind, by default its `null`, the screen
         * @param {PIXI.Rectangle} [sourceFrame] - part of screen that is mapped to the renderTexture
         * @param {PIXI.Rectangle} [destinationFrame] - part of renderTexture, by default it has the same size as sourceFrame
         */
        bind(renderTexture?: RenderTexture, sourceFrame?: Rectangle, destinationFrame?: Rectangle): void;
        /**
         * Erases the render texture and fills the drawing area with a colour
         *
         * @param {number[]} [clearColor] - The color as rgba, default to use the renderer backgroundColor
         * @param {PIXI.BUFFER_BITS} [mask=BUFFER_BITS.COLOR | BUFFER_BITS.DEPTH] - Bitwise OR of masks
         *  that indicate the buffers to be cleared, by default COLOR and DEPTH buffers.
         * @return {PIXI.Renderer} Returns itself.
         */
        clear(clearColor?: number[], mask?: BUFFER_BITS): void;
        resize(): void;
        /**
         * Resets renderTexture state
         */
        reset(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Holds the information for a single attribute structure required to render geometry.
     *
     * This does not contain the actual data, but instead has a buffer id that maps to a {@link PIXI.Buffer}
     * This can include anything from positions, uvs, normals, colors etc.
     *
     */
    class Attribute {
        buffer: number;
        size: number;
        normalized: boolean;
        type: TYPES;
        stride: number;
        start: number;
        instance: boolean;
        /**
         * @param {string} buffer - the id of the buffer that this attribute will look for
         * @param {Number} [size=0] - the size of the attribute. If you have 2 floats per vertex (eg position x and y) this would be 2.
         * @param {Boolean} [normalized=false] - should the data be normalized.
         * @param {Number} [type=PIXI.TYPES.FLOAT] - what type of number is the attribute. Check {@link PIXI.TYPES} to see the ones available
         * @param {Number} [stride=0] - How far apart (in floats) the start of each value is. (used for interleaving data)
         * @param {Number} [start=0] - How far into the array to start reading values (used for interleaving data)
         */
        constructor(buffer: number, size?: number, normalized?: boolean, type?: number, stride?: number, start?: number, instance?: boolean);
        /**
         * Destroys the Attribute.
         */
        destroy(): void;
        /**
         * Helper function that creates an Attribute based on the information provided
         *
         * @static
         * @param {string} buffer - the id of the buffer that this attribute will look for
         * @param {Number} [size=0] - the size of the attribute. If you have 2 floats per vertex (eg position x and y) this would be 2
         * @param {Boolean} [normalized=false] - should the data be normalized.
         * @param {Number} [type=PIXI.TYPES.FLOAT] - what type of number is the attribute. Check {@link PIXI.TYPES} to see the ones available
         * @param {Number} [stride=0] - How far apart (in floats) the start of each value is. (used for interleaving data)
         *
         * @returns {PIXI.Attribute} A new {@link PIXI.Attribute} based on the information provided
         */
        static from(buffer: number, size?: number, normalized?: boolean, type?: TYPES, stride?: number): Attribute;
    }
}
declare namespace feng3d.pixi {
    /**
     * Marks places in PixiJS where you can pass Float32Array, UInt32Array, any typed arrays, and ArrayBuffer
     *
     * Same as ArrayBuffer in typescript lib, defined here just for documentation
     */
    interface IArrayBuffer extends ArrayBuffer {
    }
    /**
     * PixiJS classes use this type instead of ArrayBuffer and typed arrays
     * to support expressions like `geometry.buffers[0].data[0] = position.x`.
     *
     * Gives access to indexing and `length` field
     *
     * @popelyshev: If data is actually ArrayBuffer and throws Exception on indexing - its user problem :)
     */
    interface ITypedArray extends IArrayBuffer {
        readonly length: number;
        [index: number]: number;
        readonly BYTES_PER_ELEMENT: number;
    }
    /**
     * A wrapper for data so that it can be used and uploaded by WebGL
     *
     */
    class Buffer {
        data: ITypedArray;
        index: boolean;
        static: boolean;
        id: number;
        disposeRunner: Runner;
        _glBuffers: {
            [key: number]: GLBuffer;
        };
        _updateID: number;
        /**
         * @param {ArrayBuffer| SharedArrayBuffer|ArrayBufferView} data - the data to store in the buffer.
         * @param {boolean} [_static=true] - `true` for static buffer
         * @param {boolean} [index=false] - `true` for index buffer
         */
        constructor(data?: IArrayBuffer, _static?: boolean, index?: boolean);
        /**
         * flags this buffer as requiring an upload to the GPU
         * @param {ArrayBuffer|SharedArrayBuffer|ArrayBufferView} [data] - the data to update in the buffer.
         */
        update(data?: IArrayBuffer): void;
        /**
         * disposes WebGL resources that are connected to this geometry
         */
        dispose(): void;
        /**
         * Destroys the buffer
         */
        destroy(): void;
        /**
         * Helper function that creates a buffer based on an array or TypedArray
         *
         * @static
         * @param {ArrayBufferView | number[]} data - the TypedArray that the buffer will store. If this is a regular Array it will be converted to a Float32Array.
         * @return {PIXI.Buffer} A new Buffer based on the data provided.
         */
        static from(data: IArrayBuffer | number[]): Buffer;
    }
}
declare namespace feng3d.pixi {
    /**
     * The Geometry represents a model. It consists of two components:
     * - GeometryStyle - The structure of the model such as the attributes layout
     * - GeometryData - the data of the model - this consists of buffers.
     * This can include anything from positions, uvs, normals, colors etc.
     *
     * Geometry can be defined without passing in a style or data if required (thats how I prefer!)
     *
     * ```js
     * let geometry = new PIXI.Geometry();
     *
     * geometry.addAttribute('positions', [0, 0, 100, 0, 100, 100, 0, 100], 2);
     * geometry.addAttribute('uvs', [0,0,1,0,1,1,0,1],2)
     * geometry.addIndex([0,1,2,1,3,2])
     *
     * ```
     */
    class Geometry {
        buffers: Array<Buffer>;
        indexBuffer: Buffer;
        attributes: {
            [key: string]: Attribute;
        };
        id: number;
        instanced: boolean;
        instanceCount: number;
        glVertexArrayObjects: {
            [key: number]: {
                [key: string]: WebGLVertexArrayObject;
            };
        };
        disposeRunner: Runner;
        refCount: number;
        /**
         * @param {PIXI.Buffer[]} [buffers] - an array of buffers. optional.
         * @param {object} [attributes] - of the geometry, optional structure of the attributes layout
         */
        constructor(buffers?: Array<Buffer>, attributes?: {
            [key: string]: Attribute;
        });
        /**
        *
        * Adds an attribute to the geometry
        * Note: `stride` and `start` should be `undefined` if you dont know them, not 0!
        *
        * @param {String} id - the name of the attribute (matching up to a shader)
        * @param {PIXI.Buffer|number[]} buffer - the buffer that holds the data of the attribute . You can also provide an Array and a buffer will be created from it.
        * @param {Number} [size=0] - the size of the attribute. If you have 2 floats per vertex (eg position x and y) this would be 2
        * @param {Boolean} [normalized=false] - should the data be normalized.
        * @param {Number} [type=PIXI.TYPES.FLOAT] - what type of number is the attribute. Check {PIXI.TYPES} to see the ones available
        * @param {Number} [stride] - How far apart (in floats) the start of each value is. (used for interleaving data)
        * @param {Number} [start] - How far into the array to start reading values (used for interleaving data)
        * @param {boolean} [instance=false] - Instancing flag
        *
        * @return {PIXI.Geometry} returns self, useful for chaining.
        */
        addAttribute(id: string, buffer: Buffer | Float32Array | Uint32Array | Array<number>, size?: number, normalized?: boolean, type?: TYPES, stride?: number, start?: number, instance?: boolean): this;
        /**
         * returns the requested attribute
         *
         * @param {String} id - the name of the attribute required
         * @return {PIXI.Attribute} the attribute requested.
         */
        getAttribute(id: string): Attribute;
        /**
         * returns the requested buffer
         *
         * @param {String} id - the name of the buffer required
         * @return {PIXI.Buffer} the buffer requested.
         */
        getBuffer(id: string): Buffer;
        /**
        *
        * Adds an index buffer to the geometry
        * The index buffer contains integers, three for each triangle in the geometry, which reference the various attribute buffers (position, colour, UV coordinates, other UV coordinates, normal, …). There is only ONE index buffer.
        *
        * @param {PIXI.Buffer|number[]} [buffer] - the buffer that holds the data of the index buffer. You can also provide an Array and a buffer will be created from it.
        * @return {PIXI.Geometry} returns self, useful for chaining.
        */
        addIndex(buffer?: Buffer | IArrayBuffer | number[]): Geometry;
        /**
         * returns the index buffer
         *
         * @return {PIXI.Buffer} the index buffer.
         */
        getIndex(): Buffer;
        /**
         * this function modifies the structure so that all current attributes become interleaved into a single buffer
         * This can be useful if your model remains static as it offers a little performance boost
         *
         * @return {PIXI.Geometry} returns self, useful for chaining.
         */
        interleave(): Geometry;
        getSize(): number;
        /**
         * disposes WebGL resources that are connected to this geometry
         */
        dispose(): void;
        /**
         * Destroys the geometry.
         */
        destroy(): void;
        /**
         * returns a clone of the geometry
         *
         * @returns {PIXI.Geometry} a new clone of this geometry
         */
        clone(): Geometry;
        /**
         * merges an array of geometries into a new single one
         * geometry attribute styles must match for this operation to work
         *
         * @param {PIXI.Geometry[]} geometries - array of geometries to merge
         * @returns {PIXI.Geometry} shiny new geometry!
         */
        static merge(geometries: Array<Geometry>): Geometry;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage geometry.
     *
     */
    class GeometrySystem extends System {
        hasVao: boolean;
        hasInstance: boolean;
        canUseUInt32ElementIndex: boolean;
        protected CONTEXT_UID: number;
        protected gl: IRenderingContext;
        protected _activeGeometry: Geometry;
        protected _activeVao: WebGLVertexArrayObject;
        protected _boundBuffer: GLBuffer;
        readonly managedGeometries: {
            [key: number]: Geometry;
        };
        readonly managedBuffers: {
            [key: number]: Buffer;
        };
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Sets up the renderer context and necessary buffers.
         */
        protected contextChange(): void;
        /**
         * Binds geometry so that is can be drawn. Creating a Vao if required
         *
         * @param {PIXI.Geometry} geometry - instance of geometry to bind
         * @param {PIXI.Shader} [shader] - instance of shader to use vao for
         */
        bind(geometry?: Geometry, shader?: Shader): void;
        /**
         * Reset and unbind any active VAO and geometry
         */
        reset(): void;
        /**
         * Update buffers
         * @protected
         */
        updateBuffers(): void;
        /**
         * Check compatibility between a geometry and a program
         * @protected
         * @param {PIXI.Geometry} geometry - Geometry instance
         * @param {PIXI.Program} program - Program instance
         */
        protected checkCompatibility(geometry: Geometry, program: Program): void;
        /**
         * Takes a geometry and program and generates a unique signature for them.
         *
         * @param {PIXI.Geometry} geometry - to get signature from
         * @param {PIXI.Program} program - to test geometry against
         * @returns {String} Unique signature of the geometry and program
         * @protected
         */
        protected getSignature(geometry: Geometry, program: Program): string;
        /**
         * Creates or gets Vao with the same structure as the geometry and stores it on the geometry.
         * If vao is created, it is bound automatically.
         *
         * @protected
         * @param {PIXI.Geometry} geometry - Instance of geometry to to generate Vao for
         * @param {PIXI.Program} program - Instance of program
         * @param {boolean} [incRefCount=false] - Increment refCount of all geometry buffers
         */
        protected initGeometryVao(geometry: Geometry, program: Program, incRefCount?: boolean): WebGLVertexArrayObject;
        /**
         * Disposes buffer
         * @param {PIXI.Buffer} buffer - buffer with data
         * @param {boolean} [contextLost=false] - If context was lost, we suppress deleteVertexArray
         */
        disposeBuffer(buffer: Buffer, contextLost?: boolean): void;
        /**
         * Disposes geometry
         * @param {PIXI.Geometry} geometry - Geometry with buffers. Only VAO will be disposed
         * @param {boolean} [contextLost=false] - If context was lost, we suppress deleteVertexArray
         */
        disposeGeometry(geometry: Geometry, contextLost?: boolean): void;
        /**
         * dispose all WebGL resources of all managed geometries and buffers
         * @param {boolean} [contextLost=false] - If context was lost, we suppress `gl.delete` calls
         */
        disposeAll(contextLost?: boolean): void;
        /**
         * Activate vertex array object
         *
         * @protected
         * @param {PIXI.Geometry} geometry - Geometry instance
         * @param {PIXI.Program} program - Shader program instance
         */
        protected activateVao(geometry: Geometry, program: Program): void;
        /**
         * Draw the geometry
         *
         * @param {Number} type - the type primitive to render
         * @param {Number} [size] - the number of elements to be rendered
         * @param {Number} [start] - Starting index
         * @param {Number} [instanceCount] - the number of instances of the set of elements to execute
         */
        draw(type: DRAW_MODES, size?: number, start?: number, instanceCount?: number): this;
        /**
         * Unbind/reset everything
         * @protected
         */
        protected unbind(): void;
    }
}
declare namespace feng3d.pixi {
    class GLBuffer {
        buffer: WebGLBuffer;
        updateID: number;
        byteLength: number;
        refCount: number;
        constructor(buffer?: WebGLBuffer);
    }
}
declare namespace feng3d.pixi {
    /**
     * Flexible wrapper around `ArrayBuffer` that also provides typed array views on demand.
     *
     */
    class ViewableBuffer {
        size: number;
        rawBinaryData: ArrayBuffer;
        uint32View: Uint32Array;
        float32View: Float32Array;
        private _int8View;
        private _uint8View;
        private _int16View;
        private _uint16View;
        private _int32View;
        /**
         * @param {number} length - The size of the buffer in bytes.
         */
        constructor(length: number);
        /**
         * @param {ArrayBuffer} arrayBuffer - The source array buffer.
         */
        constructor(arrayBuffer: ArrayBuffer);
        /**
         * View on the raw binary data as a `Int8Array`.
         *
         * @member {Int8Array}
         */
        get int8View(): Int8Array;
        /**
         * View on the raw binary data as a `Uint8Array`.
         *
         * @member {Uint8Array}
         */
        get uint8View(): Uint8Array;
        /**
         * View on the raw binary data as a `Int16Array`.
         *
         * @member {Int16Array}
         */
        get int16View(): Int16Array;
        /**
         * View on the raw binary data as a `Uint16Array`.
         *
         * @member {Uint16Array}
         */
        get uint16View(): Uint16Array;
        /**
         * View on the raw binary data as a `Int32Array`.
         *
         * @member {Int32Array}
         */
        get int32View(): Int32Array;
        /**
         * Returns the view of the given type.
         *
         * @param {string} type - One of `int8`, `uint8`, `int16`,
         *    `uint16`, `int32`, `uint32`, and `float32`.
         * @return {object} typed array of given type
         */
        view(type: string): ITypedArray;
        /**
         * Destroys all buffer references. Do not use after calling
         * this.
         */
        destroy(): void;
        static sizeOf(type: string): number;
    }
}
declare namespace feng3d.pixi {
    function checkMaxIfStatementsInShader(maxIfs: number, gl: IRenderingContext): number;
}
declare namespace feng3d.pixi {
    /**
     * @function compileProgram
     * @private.glCore.shader
     * @param {WebGLRenderingContext} gl - The current WebGL context {WebGLProgram}
     * @param {string|string[]} vertexSrc - The vertex shader source as an array of strings.
     * @param {string|string[]} fragmentSrc - fragment shader source as an array of strings.
     * @param {Object} attributeLocations - An attribute location map that lets you manually set the attribute locations
     * @return {WebGLProgram} the shader program
     */
    function compileProgram(gl: WebGLRenderingContextBase, vertexSrc: string, fragmentSrc: string, attributeLocations?: Dict<number>): WebGLProgram;
}
declare namespace feng3d.pixi {
    /**
     * @method defaultValue.glCore.shader
     * @param {string} type - Type of value
     * @param {number} size
     * @private
     */
    function defaultValue(type: string, size: number): number | Float32Array | Int32Array | Uint32Array | boolean | boolean[];
}
declare namespace feng3d.pixi {
    type UniformsSyncCallback = (...args: any[]) => void;
    function generateUniformsSync(group: UniformGroup, uniformData: Dict<any>): UniformsSyncCallback;
}
declare namespace feng3d.pixi {
    function getMaxFragmentPrecision(): string;
}
declare namespace feng3d.pixi {
    /**
     * returns a little WebGL context to use for program inspection.
     *
     * @static
     * @private
     * @returns {WebGLRenderingContext} a gl context to test with
     */
    function getTestContext(): WebGLRenderingContext | WebGL2RenderingContext;
}
declare namespace feng3d.pixi {
    /**
     * @private
     * @method mapSize.glCore.shader
     * @param {String} type
     * @return {Number}
     */
    function mapSize(type: string): number;
}
declare namespace feng3d.pixi {
    function mapType(gl: any, type: number): string;
}
declare namespace feng3d.pixi {
    /**
     * Sets the float precision on the shader, ensuring the device supports the request precision.
     * If the precision is already present, it just ensures that the device is able to handle it.
     *
     * @private
     * @param {string} src - The shader source
     * @param {string} requestedPrecision - The request float precision of the shader. Options are 'lowp', 'mediump' or 'highp'.
     * @param {string} maxSupportedPrecision - The maximum precision the shader supports.
     *
     * @return {string} modified shader source
     */
    function setPrecision(src: string, requestedPrecision: string, maxSupportedPrecision: string): string;
}
declare namespace feng3d.pixi {
    interface IUniformParser {
        test(data: unknown, uniform: any): boolean;
        code(name: string, uniform: any): string;
    }
    const uniformParsers: IUniformParser[];
}
declare namespace feng3d.pixi {
    /**
     * Not all platforms allow to generate function code (e.g., `new Function`).
     * this provides the platform-level detection.
     *
     * @private
     * @returns {boolean}
     */
    function unsafeEvalSupported(): boolean;
}
declare namespace feng3d.pixi {
    const defaultProgram_frag = "\nvarying vec2 vTextureCoord;\n\nuniform sampler2D uSampler;\n\nvoid main(void){\n   gl_FragColor *= texture2D(uSampler, vTextureCoord);\n}";
}
declare namespace feng3d.pixi {
    const defaultProgram_vert = "\nattribute vec2 aVertexPosition;\nattribute vec2 aTextureCoord;\n\nuniform mat3 projectionMatrix;\n\nvarying vec2 vTextureCoord;\n\nvoid main(void){\n   gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);\n   vTextureCoord = aTextureCoord;\n}\n";
}
declare namespace feng3d.pixi {
    class IGLUniformData {
        location: WebGLUniformLocation;
        value: number | boolean | Float32Array | Int32Array | Uint32Array | boolean[];
    }
    /**
     * Helper class to create a WebGL Program
     *
     */
    class GLProgram {
        program: WebGLProgram;
        uniformData: Dict<any>;
        uniformGroups: Dict<any>;
        /**
         * Makes a new Pixi program
         *
         * @param {WebGLProgram} program - webgl program
         * @param {Object} uniformData - uniforms
         */
        constructor(program: WebGLProgram, uniformData: {
            [key: string]: IGLUniformData;
        });
        /**
         * Destroys this program
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    interface IAttributeData {
        type: string;
        size: number;
        location: number;
        name: string;
    }
    interface IUniformData {
        type: string;
        size: number;
        isArray: RegExpMatchArray;
        value: any;
    }
    /**
     * Helper class to create a shader program.
     *
     */
    class Program {
        id: number;
        vertexSrc: string;
        fragmentSrc: string;
        nameCache: any;
        glPrograms: {
            [key: number]: GLProgram;
        };
        syncUniforms: any;
        attributeData: {
            [key: string]: IAttributeData;
        };
        uniformData: {
            [key: string]: IUniformData;
        };
        /**
         * @param {string} [vertexSrc] - The source of the vertex shader.
         * @param {string} [fragmentSrc] - The source of the fragment shader.
         * @param {string} [name] - Name for shader
         */
        constructor(vertexSrc?: string, fragmentSrc?: string, name?: string);
        /**
         * Extracts the data for a buy creating a small test program
         * or reading the src directly.
         * @protected
         *
         * @param {string} [vertexSrc] - The source of the vertex shader.
         * @param {string} [fragmentSrc] - The source of the fragment shader.
         */
        protected extractData(vertexSrc: string, fragmentSrc: string): void;
        /**
         * returns the attribute data from the program
         * @private
         *
         * @param {WebGLProgram} [program] - the WebGL program
         * @param {WebGLRenderingContext} [gl] - the WebGL context
         *
         * @returns {object} the attribute data for this program
         */
        protected getAttributeData(program: WebGLProgram, gl: WebGLRenderingContextBase): {
            [key: string]: IAttributeData;
        };
        /**
         * returns the uniform data from the program
         * @private
         *
         * @param {webGL-program} [program] - the webgl program
         * @param {context} [gl] - the WebGL context
         *
         * @returns {object} the uniform data for this program
         */
        private getUniformData;
        /**
         * The default vertex shader source
         *
         * @static
         * @constant
         * @member {string}
         */
        static get defaultVertexSrc(): string;
        /**
         * The default fragment shader source
         *
         * @static
         * @constant
         * @member {string}
         */
        static get defaultFragmentSrc(): string;
        /**
         * A short hand function to create a program based of a vertex and fragment shader
         * this method will also check to see if there is a cached program.
         *
         * @param {string} [vertexSrc] - The source of the vertex shader.
         * @param {string} [fragmentSrc] - The source of the fragment shader.
         * @param {string} [name=pixi-shader] - Name for shader
         *
         * @returns {PIXI.Program} an shiny new Pixi shader!
         */
        static from(vertexSrc?: string, fragmentSrc?: string, name?: string): Program;
    }
}
declare namespace feng3d.pixi {
    /**
     * A helper class for shaders
     *
     */
    class Shader {
        program: Program;
        uniformGroup: UniformGroup;
        /**
         * @param {PIXI.Program} [program] - The program the shader will use.
         * @param {object} [uniforms] - Custom uniforms to use to augment the built-in ones.
         */
        constructor(program: Program, uniforms?: Dict<any>);
        checkUniformExists(name: string, group: UniformGroup): boolean;
        destroy(): void;
        /**
         * Shader uniform values, shortcut for `uniformGroup.uniforms`
         * @readonly
         * @member {object}
         */
        get uniforms(): Dict<any>;
        /**
         * A short hand function to create a shader based of a vertex and fragment shader
         *
         * @param {string} [vertexSrc] - The source of the vertex shader.
         * @param {string} [fragmentSrc] - The source of the fragment shader.
         * @param {object} [uniforms] - Custom uniforms to use to augment the built-in ones.
         *
         * @returns {PIXI.Shader} an shiny new Pixi shader!
         */
        static from(vertexSrc?: string, fragmentSrc?: string, uniforms?: Dict<any>): Shader;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage shaders.
     *
     */
    class ShaderSystem extends System {
        protected gl: IRenderingContext;
        shader: Shader;
        program: Program;
        id: number;
        destroyed: boolean;
        private cache;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Overrideable function by `@pixi/unsafe-eval` to silence
         * throwing an error if platform doesn't support unsafe-evals.
         *
         * @private
         */
        systemCheck(): void;
        protected contextChange(gl: IRenderingContext): void;
        /**
         * Changes the current shader to the one given in parameter
         *
         * @param {PIXI.Shader} shader - the new shader
         * @param {boolean} [dontSync] - false if the shader should automatically sync its uniforms.
         * @returns {PIXI.GLProgram} the glProgram that belongs to the shader.
         */
        bind(shader: Shader, dontSync?: boolean): GLProgram;
        /**
         * Uploads the uniforms values to the currently bound shader.
         *
         * @param {object} uniforms - the uniforms values that be applied to the current shader
         */
        setUniforms(uniforms: Dict<any>): void;
        /**
         *
         * syncs uniforms on the group
         * @param {*} group - the uniform group to sync
         * @param {*} [syncData] - this is data that is passed to the sync function and any nested sync functions
         */
        syncUniformGroup(group: UniformGroup, syncData?: any): void;
        /**
         * Overrideable by the @pixi/unsafe-eval package to use static
         * syncUnforms instead.
         *
         * @private
         */
        syncUniforms(group: UniformGroup, glProgram: GLProgram, syncData: any): void;
        createSyncGroups(group: UniformGroup): UniformsSyncCallback;
        /**
         * Takes a uniform group and data and generates a unique signature for them.
         *
         * @param {PIXI.UniformGroup} group - the uniform group to get signature of
         * @param {Object} uniformData - uniform information generated by the shader
         * @returns {String} Unique signature of the uniform group
         * @private
         */
        private getSignature;
        /**
         * Returns the underlying GLShade rof the currently bound shader.
         * This can be handy for when you to have a little more control over the setting of your uniforms.
         *
         * @return {PIXI.GLProgram} the glProgram for the currently bound Shader for this context
         */
        getglProgram(): GLProgram;
        /**
         * Generates a glProgram version of the Shader provided.
         *
         * @private
         * @param {PIXI.Shader} shader - the shader that the glProgram will be based on.
         * @return {PIXI.GLProgram} A shiny new glProgram!
         */
        generateShader(shader: Shader): GLProgram;
        /**
         * Resets ShaderSystem state, does not affect WebGL state
         */
        reset(): void;
        /**
         * Destroys this System and removes all its textures
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Uniform group holds uniform map and some ID's for work
     *
     */
    class UniformGroup {
        readonly uniforms: Dict<any>;
        readonly group: boolean;
        id: number;
        syncUniforms: Dict<UniformsSyncCallback>;
        dirtyId: number;
        static: boolean;
        /**
         * @param {object} [uniforms] - Custom uniforms to use to augment the built-in ones.
         * @param {boolean} [_static] - Uniforms wont be changed after creation
         */
        constructor(uniforms: Dict<any>, _static?: boolean);
        update(): void;
        add(name: string, uniforms: Dict<any>, _static?: boolean): void;
        static from(uniforms: Dict<any>, _static?: boolean): UniformGroup;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage the projection matrix.
     *
     * The `projectionMatrix` is a global uniform provided to all shaders. It is used to transform points in world space to
     * normalized device coordinates.
     *
     */
    class ProjectionSystem extends System {
        destinationFrame: Rectangle;
        sourceFrame: Rectangle;
        defaultFrame: Rectangle;
        projectionMatrix: Matrix;
        transform: Matrix;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Updates the projection-matrix based on the sourceFrame → destinationFrame mapping provided.
         *
         * NOTE: It is expected you call `renderer.framebuffer.setViewport(destinationFrame)` after this. This is because
         * the framebuffer viewport converts shader vertex output in normalized device coordinates to window coordinates.
         *
         * NOTE-2: {@link RenderTextureSystem#bind} updates the projection-matrix when you bind a render-texture. It is expected
         * that you dirty the current bindings when calling this manually.
         *
         * @param {PIXI.Rectangle} destinationFrame - The rectangle in the render-target to render the contents
         *  into. If rendering to the canvas, the origin is on the top-left; if rendering to a render-texture, the origin
         *  is on the bottom-left.
         * @param {PIXI.Rectangle} sourceFrame - The rectangle in world space that contains the contents being rendered.
         * @param {Number} resolution - The resolution of the render-target, which is the ratio of world-space (or CSS) pixels
         *  to physical pixels.
         * @param {boolean} root - Whether the render-target is the screen. This is required because rendering to textures
         *  is y-flipped (i.e. upside down relative to the screen).
         */
        update(destinationFrame: Rectangle, sourceFrame: Rectangle, resolution: number, root: boolean): void;
        /**
         * Calculates the `projectionMatrix` to map points inside `sourceFrame` to inside `destinationFrame`.
         *
         * @param {PIXI.Rectangle} destinationFrame - The destination frame in the render-target.
         * @param {PIXI.Rectangle} sourceFrame - The source frame in world space.
         * @param {Number} resolution - The render-target's resolution, i.e. ratio of CSS to physical pixels.
         * @param {boolean} root - Whether rendering into the screen. Otherwise, if rendering to a framebuffer, the projection
         *  is y-flipped.
         */
        calculateProjection(_destinationFrame: Rectangle, sourceFrame: Rectangle, _resolution: number, root: boolean): void;
        /**
         * Sets the transform of the active render target to the given matrix
         *
         * @param {PIXI.Matrix} matrix - The transformation matrix
         */
        setTransform(_matrix: Matrix): void;
    }
}
declare namespace feng3d.pixi {
    interface ISupportDict {
        uint32Indices: boolean;
    }
    /**
     * System plugin to the renderer to manage the context.
     *
     */
    class ContextSystem extends System {
        webGLVersion: number;
        readonly supports: ISupportDict;
        protected CONTEXT_UID: number;
        protected gl: IRenderingContext;
        extensions: WebGLExtensions;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * `true` if the context is lost
         * @member {boolean}
         * @readonly
         */
        get isLost(): boolean;
        /**
         * Handle the context change event
         * @param {WebGLRenderingContext} gl - new webgl context
         */
        protected contextChange(gl: IRenderingContext): void;
        /**
         * Initialize the context
         *
         * @protected
         * @param {WebGLRenderingContext} gl - WebGL context
         */
        initFromContext(gl: IRenderingContext): void;
        /**
         * Initialize from context options
         *
         * @protected
         * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/getContext
         * @param {object} options - context attributes
         */
        initFromOptions(options: WebGLContextAttributes): void;
        /**
         * Helper class to create a WebGL Context
         *
         * @param {HTMLCanvasElement} canvas - the canvas element that we will get the context from
         * @param {object} options - An options object that gets passed in to the canvas element containing the
         *    context attributes
         * @see https://developer.mozilla.org/en/docs/Web/API/HTMLCanvasElement/getContext
         * @return {WebGLRenderingContext} the WebGL context
         */
        createContext(canvas: HTMLCanvasElement, options: WebGLContextAttributes): IRenderingContext;
        /**
         * Auto-populate the extensions
         *
         * @protected
         */
        protected getExtensions(): void;
        /**
         * Handles a lost webgl context
         *
         * @protected
         * @param {WebGLContextEvent} event - The context lost event.
         */
        protected handleContextLost(event: WebGLContextEvent): void;
        /**
         * Handles a restored webgl context
         *
         * @protected
         */
        protected handleContextRestored(): void;
        destroy(): void;
        /**
         * Handle the post-render runner event
         *
         * @protected
         */
        protected postrender(): void;
        /**
         * Validate context
         *
         * @protected
         * @param {WebGLRenderingContext} gl - Render context
         */
        protected validateContext(gl: IRenderingContext): void;
    }
}
declare namespace feng3d.pixi {
    interface WEBGL_compressed_texture_pvrtc {
        COMPRESSED_RGB_PVRTC_4BPPV1_IMG: number;
        COMPRESSED_RGBA_PVRTC_4BPPV1_IMG: number;
        COMPRESSED_RGB_PVRTC_2BPPV1_IMG: number;
        COMPRESSED_RGBA_PVRTC_2BPPV1_IMG: number;
    }
    interface WEBGL_compressed_texture_etc {
        COMPRESSED_R11_EAC: number;
        COMPRESSED_SIGNED_R11_EAC: number;
        COMPRESSED_RG11_EAC: number;
        COMPRESSED_SIGNED_RG11_EAC: number;
        COMPRESSED_RGB8_ETC2: number;
        COMPRESSED_RGBA8_ETC2_EAC: number;
        COMPRESSED_SRGB8_ETC2: number;
        COMPRESSED_SRGB8_ALPHA8_ETC2_EAC: number;
        COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2: number;
        COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2: number;
    }
    interface WEBGL_compressed_texture_etc1 {
        COMPRESSED_RGB_ETC1_WEBGL: number;
    }
    interface WEBGL_compressed_texture_atc {
        COMPRESSED_RGB_ATC_WEBGL: number;
        COMPRESSED_RGBA_ATC_EXPLICIT_ALPHA_WEBGL: number;
        COMPRESSED_RGBA_ATC_INTERPOLATED_ALPHA_WEBGL: number;
    }
    interface WebGLExtensions {
        drawBuffers?: WEBGL_draw_buffers;
        depthTexture?: OES_texture_float;
        loseContext?: WEBGL_lose_context;
        vertexArrayObject?: OES_vertex_array_object;
        anisotropicFiltering?: EXT_texture_filter_anisotropic;
        uint32ElementIndex?: OES_element_index_uint;
        floatTexture?: OES_texture_float;
        floatTextureLinear?: OES_texture_float_linear;
        textureHalfFloat?: OES_texture_half_float;
        textureHalfFloatLinear?: OES_texture_half_float_linear;
        colorBufferFloat?: WEBGL_color_buffer_float;
        s3tc?: WEBGL_compressed_texture_s3tc;
        s3tc_sRGB?: WEBGL_compressed_texture_s3tc_srgb;
        etc?: WEBGL_compressed_texture_etc;
        etc1?: WEBGL_compressed_texture_etc1;
        pvrtc?: WEBGL_compressed_texture_pvrtc;
        atc?: WEBGL_compressed_texture_atc;
        astc?: WEBGL_compressed_texture_astc;
    }
}
declare namespace feng3d.pixi {
    /**
     * Maps gl blend combinations to WebGL.
     *
     * @function mapWebGLBlendModesToPixi
     * @private
     * @param {WebGLRenderingContext} gl - The rendering context.
     * @param {number[][]} [array=[]] - The array to output into.
     * @return {number[][]} Mapped modes.
     */
    function mapWebGLBlendModesToPixi(gl: WebGLRenderingContextBase, array?: number[][]): number[][];
}
declare namespace feng3d.pixi {
    /**
     * This is a WebGL state, and is is passed The WebGL StateManager.
     *
     * Each mesh rendered may require WebGL to be in a different state.
     * For example you may want different blend mode or to enable polygon offsets
     *
     */
    class State {
        data: number;
        _blendMode: BLEND_MODES;
        _polygonOffset: number;
        constructor();
        /**
         * Activates blending of the computed fragment color values
         *
         * @member {boolean}
         */
        get blend(): boolean;
        set blend(value: boolean);
        /**
         * Activates adding an offset to depth values of polygon's fragments
         *
         * @member {boolean}
         * @default false
         */
        get offsets(): boolean;
        set offsets(value: boolean);
        /**
         * Activates culling of polygons.
         *
         * @member {boolean}
         * @default false
         */
        get culling(): boolean;
        set culling(value: boolean);
        /**
         * Activates depth comparisons and updates to the depth buffer.
         *
         * @member {boolean}
         * @default false
         */
        get depthTest(): boolean;
        set depthTest(value: boolean);
        /**
         * Enables or disables writing to the depth buffer.
         *
         * @member {boolean}
         * @default true
         */
        get depthMask(): boolean;
        set depthMask(value: boolean);
        /**
         * Specifies whether or not front or back-facing polygons can be culled.
         * @member {boolean}
         * @default false
         */
        get clockwiseFrontFace(): boolean;
        set clockwiseFrontFace(value: boolean);
        /**
         * The blend mode to be applied when this state is set. Apply a value of `PIXI.BLEND_MODES.NORMAL` to reset the blend mode.
         * Setting this mode to anything other than NO_BLEND will automatically switch blending on.
         *
         * @member {number}
         * @default PIXI.BLEND_MODES.NORMAL
         * @see PIXI.BLEND_MODES
         */
        get blendMode(): BLEND_MODES;
        set blendMode(value: BLEND_MODES);
        /**
         * The polygon offset. Setting this property to anything other than 0 will automatically enable polygon offset fill.
         *
         * @member {number}
         * @default 0
         */
        get polygonOffset(): number;
        set polygonOffset(value: number);
        toString(): string;
        static for2d(): State;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage WebGL state machines.
     *
     */
    class StateSystem extends System {
        stateId: number;
        polygonOffset: number;
        blendMode: BLEND_MODES;
        protected _blendEq: boolean;
        protected gl: IRenderingContext;
        protected blendModes: number[][];
        protected readonly map: Array<(value: boolean) => void>;
        protected readonly checks: Array<(system: this, state: State) => void>;
        protected defaultState: State;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        contextChange(gl: IRenderingContext): void;
        /**
         * Sets the current state
         *
         * @param {*} state - The state to set.
         */
        set(state: State): void;
        /**
         * Sets the state, when previous state is unknown
         *
         * @param {*} state - The state to set
         */
        forceState(state: State): void;
        /**
         * Enables or disabled blending.
         *
         * @param {boolean} value - Turn on or off webgl blending.
         */
        setBlend(value: boolean): void;
        /**
         * Enables or disable polygon offset fill
         *
         * @param {boolean} value - Turn on or off webgl polygon offset testing.
         */
        setOffset(value: boolean): void;
        /**
         * Sets whether to enable or disable depth test.
         *
         * @param {boolean} value - Turn on or off webgl depth testing.
         */
        setDepthTest(value: boolean): void;
        /**
         * Sets whether to enable or disable depth mask.
         *
         * @param {boolean} value - Turn on or off webgl depth mask.
         */
        setDepthMask(value: boolean): void;
        /**
         * Sets whether to enable or disable cull face.
         *
         * @param {boolean} value - Turn on or off webgl cull face.
         */
        setCullFace(value: boolean): void;
        /**
         * Sets the gl front face.
         *
         * @param {boolean} value - true is clockwise and false is counter-clockwise
         */
        setFrontFace(value: boolean): void;
        /**
         * Sets the blend mode.
         *
         * @param {number} value - The blend mode to set to.
         */
        setBlendMode(value: number): void;
        /**
         * Sets the polygon offset.
         *
         * @param {number} value - the polygon offset
         * @param {number} scale - the polygon offset scale
         */
        setPolygonOffset(value: number, scale: number): void;
        /**
         * Resets all the logic and disables the vaos
         */
        reset(): void;
        /**
         * checks to see which updates should be checked based on which settings have been activated.
         * For example, if blend is enabled then we should check the blend modes each time the state is changed
         * or if polygon fill is activated then we need to check if the polygon offset changes.
         * The idea is that we only check what we have too.
         *
         * @param {Function} func - the checking function to add or remove
         * @param {boolean} value - should the check function be added or removed.
         */
        updateCheck(func: (system: this, state: State) => void, value: boolean): void;
        /**
         * A private little wrapper function that we call to check the blend mode.
         *
         * @static
         * @private
         * @param {PIXI.StateSystem} System - the System to perform the state check on
         * @param {PIXI.State} state - the state that the blendMode will pulled from
         */
        static checkBlendMode(system: StateSystem, state: State): void;
        /**
         * A private little wrapper function that we call to check the polygon offset.
         *
         * @static
         * @private
         * @param {PIXI.StateSystem} System - the System to perform the state check on
         * @param {PIXI.State} state - the state that the blendMode will pulled from
         */
        static checkPolygonOffset(system: StateSystem, state: State): void;
    }
}
declare namespace feng3d.pixi {
    const defaultFilter_frag = "\nvarying vec2 vTextureCoord;\n\nuniform sampler2D uSampler;\n\nvoid main(void){\n   gl_FragColor = texture2D(uSampler, vTextureCoord);\n}\n";
}
declare namespace feng3d.pixi {
    const defaultFilter_vert = "\nattribute vec2 aVertexPosition;\n\nuniform mat3 projectionMatrix;\n\nvarying vec2 vTextureCoord;\n\nuniform vec4 inputSize;\nuniform vec4 outputFrame;\n\nvec4 filterVertexPosition( void )\n{\n    vec2 position = aVertexPosition * max(outputFrame.zw, vec2(0.)) + outputFrame.xy;\n\n    return vec4((projectionMatrix * vec3(position, 1.0)).xy, 0.0, 1.0);\n}\n\nvec2 filterTextureCoord( void )\n{\n    return aVertexPosition * (outputFrame.zw * inputSize.zw);\n}\n\nvoid main(void)\n{\n    gl_Position = filterVertexPosition();\n    vTextureCoord = filterTextureCoord();\n}\n";
}
declare namespace feng3d.pixi {
    /**
     * A filter is a special shader that applies post-processing effects to an input texture and writes into an output
     * render-target.
     *
     * {@link http://pixijs.io/examples/#/filters/blur-filter.js Example} of the
     * {@link PIXI.filters.BlurFilter BlurFilter}.
     *
     * ### Usage
     * Filters can be applied to any DisplayObject or Container.
     * PixiJS' `FilterSystem` renders the container into temporary Framebuffer,
     * then filter renders it to the screen.
     * Multiple filters can be added to the `filters` array property and stacked on each other.
     *
     * ```
     * const filter = new PIXI.Filter(myShaderVert, myShaderFrag, { myUniform: 0.5 });
     * const container = new PIXI.Container();
     * container.filters = [filter];
     * ```
     *
     * ### Previous Version Differences
     *
     * In PixiJS **v3**, a filter was always applied to _whole screen_.
     *
     * In PixiJS **v4**, a filter can be applied _only part of the screen_.
     * Developers had to create a set of uniforms to deal with coordinates.
     *
     * In PixiJS **v5** combines _both approaches_.
     * Developers can use normal coordinates of v3 and then allow filter to use partial Framebuffers,
     * bringing those extra uniforms into account.
     *
     * Also be aware that we have changed default vertex shader, please consult
     * {@link https://github.com/pixijs/pixi.js/wiki/v5-Creating-filters Wiki}.
     *
     * ### Frames
     *
     * The following table summarizes the coordinate spaces used in the filtering pipeline:
     *
     * <table>
     * <thead>
     *   <tr>
     *     <th>Coordinate Space</th>
     *     <th>Description</th>
     *   </tr>
     * </thead>
     * <tbody>
     *   <tr>
     *     <td>Texture Coordinates</td>
     *     <td>
     *         The texture (or UV) coordinates in the input base-texture's space. These are normalized into the (0,1) range along
     *         both axes.
     *     </td>
     *   </tr>
     *   <tr>
     *     <td>World Space</td>
     *     <td>
     *         A point in the same space as the world bounds of any display-object (i.e. in the scene graph's space).
     *     </td>
     *   </tr>
     *   <tr>
     *     <td>Physical Pixels</td>
     *     <td>
     *         This is base-texture's space with the origin on the top-left. You can calculate these by multiplying the texture
     *         coordinates by the dimensions of the texture.
     *     </td>
     *   </tr>
     * </tbody>
     * </table>
     *
     * ### Built-in Uniforms
     *
     * PixiJS viewport uses screen (CSS) coordinates, `(0, 0, renderer.screen.width, renderer.screen.height)`,
     * and `projectionMatrix` uniform maps it to the gl viewport.
     *
     * **uSampler**
     *
     * The most important uniform is the input texture that container was rendered into.
     * _Important note: as with all Framebuffers in PixiJS, both input and output are
     * premultiplied by alpha._
     *
     * By default, input normalized coordinates are passed to fragment shader with `vTextureCoord`.
     * Use it to sample the input.
     *
     * ```
     * const fragment = `
     * varying vec2 vTextureCoord;
     * uniform sampler2D uSampler;
     * void main(void)
     * {
     *    gl_FragColor = texture2D(uSampler, vTextureCoord);
     * }
     * `;
     *
     * const myFilter = new PIXI.Filter(null, fragment);
     * ```
     *
     * This filter is just one uniform less than {@link PIXI.filters.AlphaFilter AlphaFilter}.
     *
     * **outputFrame**
     *
     * The `outputFrame` holds the rectangle where filter is applied in screen (CSS) coordinates.
     * It's the same as `renderer.screen` for a fullscreen filter.
     * Only a part of  `outputFrame.zw` size of temporary Framebuffer is used,
     * `(0, 0, outputFrame.width, outputFrame.height)`,
     *
     * Filters uses this quad to normalized (0-1) space, its passed into `aVertexPosition` attribute.
     * To calculate vertex position in screen space using normalized (0-1) space:
     *
     * ```
     * vec4 filterVertexPosition( void )
     * {
     *     vec2 position = aVertexPosition * max(outputFrame.zw, vec2(0.)) + outputFrame.xy;
     *     return vec4((projectionMatrix * vec3(position, 1.0)).xy, 0.0, 1.0);
     * }
     * ```
     *
     * **inputSize**
     *
     * Temporary framebuffer is different, it can be either the size of screen, either power-of-two.
     * The `inputSize.xy` are size of temporary framebuffer that holds input.
     * The `inputSize.zw` is inverted, it's a shortcut to evade division inside the shader.
     *
     * Set `inputSize.xy = outputFrame.zw` for a fullscreen filter.
     *
     * To calculate input normalized coordinate, you have to map it to filter normalized space.
     * Multiply by `outputFrame.zw` to get input coordinate.
     * Divide by `inputSize.xy` to get input normalized coordinate.
     *
     * ```
     * vec2 filterTextureCoord( void )
     * {
     *     return aVertexPosition * (outputFrame.zw * inputSize.zw); // same as /inputSize.xy
     * }
     * ```
     * **resolution**
     *
     * The `resolution` is the ratio of screen (CSS) pixels to real pixels.
     *
     * **inputPixel**
     *
     * `inputPixel.xy` is the size of framebuffer in real pixels, same as `inputSize.xy * resolution`
     * `inputPixel.zw` is inverted `inputPixel.xy`.
     *
     * It's handy for filters that use neighbour pixels, like {@link PIXI.filters.FXAAFilter FXAAFilter}.
     *
     * **inputClamp**
     *
     * If you try to get info from outside of used part of Framebuffer - you'll get undefined behaviour.
     * For displacements, coordinates has to be clamped.
     *
     * The `inputClamp.xy` is left-top pixel center, you may ignore it, because we use left-top part of Framebuffer
     * `inputClamp.zw` is bottom-right pixel center.
     *
     * ```
     * vec4 color = texture2D(uSampler, clamp(modifiedTextureCoord, inputClamp.xy, inputClamp.zw))
     * ```
     * OR
     * ```
     * vec4 color = texture2D(uSampler, min(modifigedTextureCoord, inputClamp.zw))
     * ```
     *
     * ### Additional Information
     *
     * Complete documentation on Filter usage is located in the
     * {@link https://github.com/pixijs/pixi.js/wiki/v5-Creating-filters Wiki}.
     *
     * Since PixiJS only had a handful of built-in filters, additional filters can be downloaded
     * {@link https://github.com/pixijs/pixi-filters here} from the PixiJS Filters repository.
     *
     */
    class Filter extends Shader {
        padding: number;
        resolution: number;
        enabled: boolean;
        autoFit: boolean;
        legacy: boolean;
        state: State;
        /**
         * @param {string} [vertexSrc] - The source of the vertex shader.
         * @param {string} [fragmentSrc] - The source of the fragment shader.
         * @param {object} [uniforms] - Custom uniforms to use to augment the built-in ones.
         */
        constructor(vertexSrc?: string, fragmentSrc?: string, uniforms?: Dict<any>);
        /**
         * Applies the filter
         *
         * @param {PIXI.FilterSystem} filterManager - The renderer to retrieve the filter from
         * @param {PIXI.RenderTexture} input - The input render target.
         * @param {PIXI.RenderTexture} output - The target to output to.
         * @param {PIXI.CLEAR_MODES} [clearMode] - Should the output be cleared before rendering to it.
         * @param {object} [currentState] - It's current state of filter.
         *        There are some useful properties in the currentState :
         *        target, filters, sourceFrame, destinationFrame, renderTarget, resolution
         */
        apply(filterManager: FilterSystem, input: RenderTexture, output: RenderTexture, clearMode?: CLEAR_MODES, _currentState?: FilterState): void;
        /**
         * Sets the blendmode of the filter
         *
         * @member {number}
         * @default PIXI.BLEND_MODES.NORMAL
         */
        get blendMode(): BLEND_MODES;
        set blendMode(value: BLEND_MODES);
        /**
         * The default vertex shader source
         *
         * @static
         * @type {string}
         * @constant
         */
        static get defaultVertexSrc(): string;
        /**
         * The default fragment shader source
         *
         * @static
         * @type {string}
         * @constant
         */
        static get defaultFragmentSrc(): string;
        /**
         * Used for caching shader IDs
         *
         * @static
         * @type {object}
         * @protected
         */
        static SOURCE_KEY_MAP: Dict<string>;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage filter states.
     *
     * @private
     */
    class FilterState {
        renderTexture: RenderTexture;
        target: IFilterTarget;
        legacy: boolean;
        resolution: number;
        sourceFrame: Rectangle;
        destinationFrame: Rectangle;
        bindingSourceFrame: Rectangle;
        bindingDestinationFrame: Rectangle;
        filters: Array<Filter>;
        transform: Matrix;
        constructor();
        /**
         * clears the state
         * @private
         */
        clear(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage filters.
     *
     * ## Pipeline
     *
     * The FilterSystem executes the filtering pipeline by rendering the display-object into a texture, applying its
     * [filters]{@link PIXI.Filter} in series, and the last filter outputs into the final render-target.
     *
     * The filter-frame is the rectangle in world space being filtered, and those contents are mapped into
     * `(0, 0, filterFrame.width, filterFrame.height)` into the filter render-texture. The filter-frame is also called
     * the source-frame, as it is used to bind the filter render-textures. The last filter outputs to the `filterFrame`
     * in the final render-target.
     *
     * ## Usage
     *
     * {@link PIXI.Container#renderAdvanced} is an example of how to use the filter system. It is a 3 step process:
     *
     * * **push**: Use {@link PIXI.FilterSystem#push} to push the set of filters to be applied on a filter-target.
     * * **render**: Render the contents to be filtered using the renderer. The filter-system will only capture the contents
     *      inside the bounds of the filter-target. NOTE: Using {@link PIXI.Renderer#render} is
     *      illegal during an existing render cycle, and it may reset the filter system.
     * * **pop**: Use {@link PIXI.FilterSystem#pop} to pop & execute the filters you initially pushed. It will apply them
     *      serially and output to the bounds of the filter-target.
     *
     */
    class FilterSystem extends System {
        readonly defaultFilterStack: Array<FilterState>;
        statePool: Array<FilterState>;
        texturePool: RenderTexturePool;
        forceClear: boolean;
        useMaxPadding: boolean;
        protected quad: Quad;
        protected quadUv: QuadUv;
        protected activeState: FilterState;
        protected globalUniforms: UniformGroup;
        private tempRect;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Pushes a set of filters to be applied later to the system. This will redirect further rendering into an
         * input render-texture for the rest of the filtering pipeline.
         *
         * @param {PIXI.DisplayObject} target - The target of the filter to render.
         * @param {PIXI.Filter[]} filters - The filters to apply.
         */
        push(target: IFilterTarget, filters: Array<Filter>): void;
        /**
         * Pops off the filter and applies it.
         */
        pop(): void;
        /**
         * Binds a renderTexture with corresponding `filterFrame`, clears it if mode corresponds.
         *
         * @param {PIXI.RenderTexture} filterTexture - renderTexture to bind, should belong to filter pool or filter stack
         * @param {PIXI.CLEAR_MODES} [clearMode] - clearMode, by default its CLEAR/YES. See {@link PIXI.CLEAR_MODES}
         */
        bindAndClear(filterTexture: RenderTexture, clearMode?: CLEAR_MODES): void;
        /**
         * Draws a filter.
         *
         * @param {PIXI.Filter} filter - The filter to draw.
         * @param {PIXI.RenderTexture} input - The input render target.
         * @param {PIXI.RenderTexture} output - The target to output to.
         * @param {PIXI.CLEAR_MODES} [clearMode] - Should the output be cleared before rendering to it
         */
        applyFilter(filter: Filter, input: RenderTexture, output: RenderTexture, clearMode?: CLEAR_MODES): void;
        /**
         * Multiply _input normalized coordinates_ to this matrix to get _sprite texture normalized coordinates_.
         *
         * Use `outputMatrix * vTextureCoord` in the shader.
         *
         * @param {PIXI.Matrix} outputMatrix - The matrix to output to.
         * @param {PIXI.Sprite} sprite - The sprite to map to.
         * @return {PIXI.Matrix} The mapped matrix.
         */
        calculateSpriteMatrix(outputMatrix: Matrix, sprite: ISpriteMaskTarget): Matrix;
        /**
         * Destroys this Filter System.
         */
        destroy(): void;
        /**
         * Gets a Power-of-Two render texture or fullScreen texture
         *
         * @protected
         * @param {number} minWidth - The minimum width of the render texture in real pixels.
         * @param {number} minHeight - The minimum height of the render texture in real pixels.
         * @param {number} [resolution=1] - The resolution of the render texture.
         * @return {PIXI.RenderTexture} The new render texture.
         */
        protected getOptimalFilterTexture(minWidth: number, minHeight: number, resolution?: number): RenderTexture;
        /**
         * Gets extra render texture to use inside current filter
         * To be compliant with older filters, you can use params in any order
         *
         * @param {PIXI.RenderTexture} [input] - renderTexture from which size and resolution will be copied
         * @param {number} [resolution] - override resolution of the renderTexture
         * @returns {PIXI.RenderTexture}
         */
        getFilterTexture(input?: RenderTexture, resolution?: number): RenderTexture;
        /**
         * Frees a render texture back into the pool.
         *
         * @param {PIXI.RenderTexture} renderTexture - The renderTarget to free
         */
        returnFilterTexture(renderTexture: RenderTexture): void;
        /**
         * Empties the texture pool.
         */
        emptyPool(): void;
        /**
         * calls `texturePool.resize()`, affects fullScreen renderTextures
         */
        resize(): void;
        /**
         * @param {PIXI.Matrix} matrix - first param
         * @param {PIXI.Rectangle} rect - second param
         */
        private transformAABB;
        private roundFrame;
    }
    interface ISystems {
        FilterSystem: new (...args: any) => FilterSystem;
    }
}
declare namespace feng3d.pixi {
    interface IFilterTarget {
        filterArea: Rectangle;
        getBounds(skipUpdate?: boolean): Rectangle;
    }
}
declare namespace feng3d.pixi {
    const spriteMaskFilter_frag = "\nvarying vec2 vMaskCoord;\nvarying vec2 vTextureCoord;\n\nuniform sampler2D uSampler;\nuniform sampler2D mask;\nuniform float alpha;\nuniform float npmAlpha;\nuniform vec4 maskClamp;\n\nvoid main(void)\n{\n    float clip = step(3.5,\n        step(maskClamp.x, vMaskCoord.x) +\n        step(maskClamp.y, vMaskCoord.y) +\n        step(vMaskCoord.x, maskClamp.z) +\n        step(vMaskCoord.y, maskClamp.w));\n\n    vec4 original = texture2D(uSampler, vTextureCoord);\n    vec4 masky = texture2D(mask, vMaskCoord);\n    float alphaMul = 1.0 - npmAlpha * (1.0 - masky.a);\n\n    original *= (alphaMul * masky.r * alpha * clip);\n\n    gl_FragColor = original;\n}\n";
}
declare namespace feng3d.pixi {
    const spriteMaskFilter_vert = "\nattribute vec2 aVertexPosition;\nattribute vec2 aTextureCoord;\n\nuniform mat3 projectionMatrix;\nuniform mat3 otherMatrix;\n\nvarying vec2 vMaskCoord;\nvarying vec2 vTextureCoord;\n\nvoid main(void)\n{\n    gl_Position = vec4((projectionMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);\n\n    vTextureCoord = aTextureCoord;\n    vMaskCoord = ( otherMatrix * vec3( aTextureCoord, 1.0)  ).xy;\n}\n";
}
declare namespace feng3d.pixi {
    interface ISpriteMaskTarget extends IMaskTarget {
        _texture: Texture;
        worldAlpha: number;
        anchorX: number;
        anchorY: number;
    }
    /**
     * This handles a Sprite acting as a mask, as opposed to a Graphic.
     *
     * WebGL only.
     *
     */
    class SpriteMaskFilter extends Filter {
        maskSprite: IMaskTarget;
        maskMatrix: Matrix;
        /**
         * @param {PIXI.Sprite} sprite - the target sprite
         */
        constructor(sprite: IMaskTarget);
        /**
         * Applies the filter
         *
         * @param {PIXI.FilterSystem} filterManager - The renderer to retrieve the filter from
         * @param {PIXI.RenderTexture} input - The input render target.
         * @param {PIXI.RenderTexture} output - The target to output to.
         * @param {PIXI.CLEAR_MODES} clearMode - Should the output be cleared before rendering to it.
         */
        apply(filterManager: FilterSystem, input: RenderTexture, output: RenderTexture, clearMode: CLEAR_MODES): void;
    }
}
declare namespace feng3d.pixi {
    const texture_frag = "\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvarying float vTextureId;\nuniform sampler2D uSamplers[%count%];\n\nvoid main(void){\n    vec4 color;\n    %forloop%\n    gl_FragColor = color * vColor;\n}\n";
}
declare namespace feng3d.pixi {
    const texture_vert = "\nprecision highp float;\nattribute vec2 aVertexPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\nattribute float aTextureId;\n\nuniform mat3 projectionMatrix;\nuniform mat3 translationMatrix;\nuniform vec4 tint;\n\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvarying float vTextureId;\n\nvoid main(void){\n    gl_Position = vec4((projectionMatrix * translationMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);\n\n    vTextureCoord = aTextureCoord;\n    vTextureId = aTextureId;\n    vColor = aColor * tint;\n}\n";
}
declare namespace feng3d.pixi {
    /**
     * Base for a common object renderer that can be used as a
     * system renderer plugin.
     *
     */
    class ObjectRenderer {
        protected renderer: Renderer;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this manager works for.
         */
        constructor(renderer: Renderer);
        /**
         * Stub method that should be used to empty the current
         * batch by rendering objects now.
         */
        flush(): void;
        /**
         * Generic destruction method that frees all resources. This
         * should be called by subclasses.
         */
        destroy(): void;
        /**
         * Stub method that initializes any state required before
         * rendering starts. It is different from the `prerender`
         * signal, which occurs every frame, in that it is called
         * whenever an object requests _this_ renderer specifically.
         */
        start(): void;
        /**
         * Stops the renderer. It should free up any state and
         * become dormant.
         */
        stop(): void;
        /**
         * Keeps the object to render. It doesn't have to be
         * rendered immediately.
         *
         * @param {PIXI.DisplayObject} object - The object to render.
         */
        render(_object: any): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Interface for elements like Sprite, Mesh etc. for batching.
     */
    interface IBatchableElement {
        _texture: Texture;
        vertexData: Float32Array;
        indices: Uint16Array | Uint32Array | Array<number>;
        uvs: Float32Array;
        worldAlpha: number;
        _tintRGB: number;
        blendMode: BLEND_MODES;
    }
    /**
     * Renderer dedicated to drawing and batching sprites.
     *
     * This is the default batch renderer. It buffers objects
     * with texture-based geometries and renders them in
     * batches. It uploads multiple textures to the GPU to
     * reduce to the number of draw calls.
     *
     * @protected
     */
    class AbstractBatchRenderer extends ObjectRenderer {
        readonly state: State;
        size: number;
        MAX_TEXTURES: number;
        protected shaderGenerator: BatchShaderGenerator;
        protected geometryClass: typeof BatchGeometry;
        protected vertexSize: number;
        protected _vertexCount: number;
        protected _indexCount: number;
        protected _bufferedElements: Array<IBatchableElement>;
        protected _bufferedTextures: Array<BaseTexture>;
        protected _bufferSize: number;
        protected _shader: Shader;
        protected _flushId: number;
        protected _aBuffers: Array<ViewableBuffer>;
        protected _iBuffers: Array<Uint16Array>;
        protected _dcIndex: number;
        protected _aIndex: number;
        protected _iIndex: number;
        protected _attributeBuffer: ViewableBuffer;
        protected _indexBuffer: Uint16Array;
        protected _tempBoundTextures: BaseTexture[];
        private _packedGeometries;
        private _packedGeometryPoolSize;
        /**
         * This will hook onto the renderer's `contextChange`
         * and `prerender` signals.
         *
         * @param {PIXI.Renderer} renderer - The renderer this works for.
         */
        constructor(renderer: Renderer);
        /**
         * Handles the `contextChange` signal.
         *
         * It calculates `this.MAX_TEXTURES` and allocating the
         * packed-geometry object pool.
         */
        contextChange(): void;
        /**
         * Makes sure that static and dynamic flush pooled objects have correct dimensions
         */
        initFlushBuffers(): void;
        /**
         * Handles the `prerender` signal.
         *
         * It ensures that flushes start from the first geometry
         * object again.
         */
        onPrerender(): void;
        /**
         * Buffers the "batchable" object. It need not be rendered
         * immediately.
         *
         * @param {PIXI.DisplayObject} element - the element to render when
         *    using this renderer
         */
        render(element: IBatchableElement): void;
        buildTexturesAndDrawCalls(): void;
        /**
         * Populating drawcalls for rendering
         *
         * @param {PIXI.BatchTextureArray} texArray
         * @param {number} start
         * @param {number} finish
         */
        buildDrawCalls(texArray: BatchTextureArray, start: number, finish: number): void;
        /**
         * Bind textures for current rendering
         *
         * @param {PIXI.BatchTextureArray} texArray
         */
        bindAndClearTexArray(texArray: BatchTextureArray): void;
        updateGeometry(): void;
        drawBatches(): void;
        /**
         * Renders the content _now_ and empties the current batch.
         */
        flush(): void;
        /**
         * Starts a new sprite batch.
         */
        start(): void;
        /**
         * Stops and flushes the current batch.
         */
        stop(): void;
        /**
         * Destroys this `AbstractBatchRenderer`. It cannot be used again.
         */
        destroy(): void;
        /**
         * Fetches an attribute buffer from `this._aBuffers` that
         * can hold atleast `size` floats.
         *
         * @param {number} size - minimum capacity required
         * @return {ViewableBuffer} - buffer than can hold atleast `size` floats
         * @private
         */
        getAttributeBuffer(size: number): ViewableBuffer;
        /**
         * Fetches an index buffer from `this._iBuffers` that can
         * have at least `size` capacity.
         *
         * @param {number} size - minimum required capacity
         * @return {Uint16Array} - buffer that can fit `size`
         *    indices.
         * @private
         */
        getIndexBuffer(size: number): Uint16Array;
        /**
         * Takes the four batching parameters of `element`, interleaves
         * and pushes them into the batching attribute/index buffers given.
         *
         * It uses these properties: `vertexData` `uvs`, `textureId` and
         * `indicies`. It also uses the "tint" of the base-texture, if
         * present.
         *
         * @param {PIXI.Sprite} element - element being rendered
         * @param {PIXI.ViewableBuffer} attributeBuffer - attribute buffer.
         * @param {Uint16Array} indexBuffer - index buffer
         * @param {number} aIndex - number of floats already in the attribute buffer
         * @param {number} iIndex - number of indices already in `indexBuffer`
         */
        packInterleavedGeometry(element: IBatchableElement, attributeBuffer: ViewableBuffer, indexBuffer: Uint16Array, aIndex: number, iIndex: number): void;
        /**
         * Pool of `BatchDrawCall` objects that `flush` used
         * to create "batches" of the objects being rendered.
         *
         * These are never re-allocated again.
         * Shared between all batch renderers because it can be only one "flush" working at the moment.
         *
         * @static
         * @member {PIXI.BatchDrawCall[]}
         */
        static _drawCallPool: Array<BatchDrawCall>;
        /**
         * Pool of `BatchDrawCall` objects that `flush` used
         * to create "batches" of the objects being rendered.
         *
         * These are never re-allocated again.
         * Shared between all batch renderers because it can be only one "flush" working at the moment.
         *
         * @static
         * @member {PIXI.BatchTextureArray[]}
         */
        static _textureArrayPool: Array<BatchTextureArray>;
    }
}
declare namespace feng3d.pixi {
    /**
     * Used by the batcher to draw batches.
     * Each one of these contains all information required to draw a bound geometry.
     *
     */
    class BatchDrawCall {
        texArray: BatchTextureArray;
        type: DRAW_MODES;
        blend: BLEND_MODES;
        start: number;
        size: number;
        data: any;
        constructor();
    }
}
declare namespace feng3d.pixi {
    /**
     * Geometry used to batch standard PIXI content (e.g. Mesh, Sprite, Graphics objects).
     *
     */
    class BatchGeometry extends Geometry {
        _buffer: Buffer;
        _indexBuffer: Buffer;
        /**
         * @param {boolean} [_static=false] - Optimization flag, where `false`
         *        is updated every frame, `true` doesn't change frame-to-frame.
         */
        constructor(_static?: boolean);
    }
}
declare namespace feng3d.pixi {
    interface IBatchFactoryOptions {
        vertex?: string;
        fragment?: string;
        geometryClass?: typeof BatchGeometry;
        vertexSize?: number;
    }
    /**
     * @hideconstructor
     */
    class BatchPluginFactory {
        /**
         * Create a new BatchRenderer plugin for Renderer. this convenience can provide an easy way
         * to extend BatchRenderer with all the necessary pieces.
         * @example
         * const fragment = `
         * varying vec2 vTextureCoord;
         * varying vec4 vColor;
         * varying float vTextureId;
         * uniform sampler2D uSamplers[%count%];
         *
         * void main(void){
         *     vec4 color;
         *     %forloop%
         *     gl_FragColor = vColor * vec4(color.a - color.rgb, color.a);
         * }
         * `;
         * const InvertBatchRenderer = PIXI.BatchPluginFactory.create({ fragment });
         * PIXI.Renderer.registerPlugin('invert', InvertBatchRenderer);
         * const sprite = new PIXI.Sprite();
         * sprite.pluginName = 'invert';
         *
         * @static
         * @param {object} [options]
         * @param {string} [options.vertex=PIXI.BatchPluginFactory.defaultVertexSrc] - Vertex shader source
         * @param {string} [options.fragment=PIXI.BatchPluginFactory.defaultFragmentTemplate] - Fragment shader template
         * @param {number} [options.vertexSize=6] - Vertex size
         * @param {object} [options.geometryClass=PIXI.BatchGeometry]
         * @return {*} New batch renderer plugin
         */
        static create(options?: IBatchFactoryOptions): typeof AbstractBatchRenderer;
        /**
         * The default vertex shader source
         *
         * @static
         * @type {string}
         * @constant
         */
        static get defaultVertexSrc(): string;
        /**
         * The default fragment shader source
         *
         * @static
         * @type {string}
         * @constant
         */
        static get defaultFragmentTemplate(): string;
    }
    const BatchRenderer: typeof AbstractBatchRenderer;
}
declare namespace feng3d.pixi {
    /**
     * Helper that generates batching multi-texture shader. Use it with your new BatchRenderer
     *
     */
    class BatchShaderGenerator {
        vertexSrc: string;
        fragTemplate: string;
        programCache: {
            [key: number]: Program;
        };
        defaultGroupCache: {
            [key: number]: UniformGroup;
        };
        /**
         * @param {string} vertexSrc - Vertex shader
         * @param {string} fragTemplate - Fragment shader template
         */
        constructor(vertexSrc: string, fragTemplate: string);
        generateShader(maxTextures: number): Shader;
        generateSampleSrc(maxTextures: number): string;
    }
}
declare namespace feng3d.pixi {
    /**
     * System plugin to the renderer to manage batching.
     *
     */
    class BatchSystem extends System {
        readonly emptyRenderer: ObjectRenderer;
        currentRenderer: ObjectRenderer;
        /**
         * @param {PIXI.Renderer} renderer - The renderer this System works for.
         */
        constructor(renderer: Renderer);
        /**
         * Changes the current renderer to the one given in parameter
         *
         * @param {PIXI.ObjectRenderer} objectRenderer - The object renderer to use.
         */
        setObjectRenderer(objectRenderer: ObjectRenderer): void;
        /**
         * This should be called if you wish to do some custom rendering
         * It will basically render anything that may be batched up such as sprites
         */
        flush(): void;
        /**
         * Reset the system to an empty renderer
         */
        reset(): void;
        /**
         * Handy function for batch renderers: copies bound textures in first maxTextures locations to array
         * sets actual _batchLocation for them
         *
         * @param {PIXI.BaseTexture[]} arr - arr copy destination
         * @param {number} maxTextures - number of copied elements
         */
        copyBoundTextures(arr: BaseTexture[], maxTextures: number): void;
        /**
         * Assigns batch locations to textures in array based on boundTextures state.
         * All textures in texArray should have `_batchEnabled = _batchId`,
         * and their count should be less than `maxTextures`.
         *
         * @param {PIXI.BatchTextureArray} texArray - textures to bound
         * @param {PIXI.BaseTexture[]} boundTextures - current state of bound textures
         * @param {number} batchId - marker for _batchEnabled param of textures in texArray
         * @param {number} maxTextures - number of texture locations to manipulate
         */
        boundArray(texArray: BatchTextureArray, boundTextures: Array<BaseTexture>, batchId: number, maxTextures: number): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Used by the batcher to build texture batches.
     * Holds list of textures and their respective locations.
     *
     */
    class BatchTextureArray {
        elements: BaseTexture[];
        ids: number[];
        count: number;
        constructor();
        clear(): void;
    }
}
declare namespace feng3d.pixi {
    const alpha_frag = "\nvarying vec2 vTextureCoord;\n\nuniform sampler2D uSampler;\nuniform float uAlpha;\n\nvoid main(void)\n{\n   gl_FragColor = texture2D(uSampler, vTextureCoord) * uAlpha;\n}\n\n";
}
declare namespace feng3d.pixi {
    /**
     * Simplest filter - applies alpha.
     *
     * Use this instead of Container's alpha property to avoid visual layering of individual elements.
     * AlphaFilter applies alpha evenly across the entire display object and any opaque elements it contains.
     * If elements are not opaque, they will blend with each other anyway.
     *
     * Very handy if you want to use common features of all filters:
     *
     * 1. Assign a blendMode to this filter, blend all elements inside display object with background.
     *
     * 2. To use clipping in display coordinates, assign a filterArea to the same container that has this filter.
     *
     */
    class AlphaFilter extends Filter {
        /**
         * @param {number} [alpha=1] - Amount of alpha from 0 to 1, where 0 is transparent
         */
        constructor(alpha?: number);
        /**
         * Coefficient for alpha multiplication
         *
         * @member {number}
         * @default 1
         */
        get alpha(): number;
        set alpha(value: number);
    }
}
declare namespace feng3d.pixi {
    /**
     * The BlurFilter applies a Gaussian blur to an object.
     *
     * The strength of the blur can be set for the x-axis and y-axis separately.
     *
     */
    class BlurFilter extends Filter {
        blurXFilter: BlurFilterPass;
        blurYFilter: BlurFilterPass;
        private _repeatEdgePixels;
        /**
         * @param {number} [strength=8] - The strength of the blur filter.
         * @param {number} [quality=4] - The quality of the blur filter.
         * @param {number} [resolution=PIXI.settings.FILTER_RESOLUTION] - The resolution of the blur filter.
         * @param {number} [kernelSize=5] - The kernelSize of the blur filter.Options: 5, 7, 9, 11, 13, 15.
         */
        constructor(strength?: number, quality?: number, resolution?: number, kernelSize?: number);
        /**
         * Applies the filter.
         *
         * @param {PIXI.FilterSystem} filterManager - The manager.
         * @param {PIXI.RenderTexture} input - The input target.
         * @param {PIXI.RenderTexture} output - The output target.
         * @param {PIXI.CLEAR_MODES} clearMode - How to clear
         */
        apply(filterManager: FilterSystem, input: RenderTexture, output: RenderTexture, clearMode: CLEAR_MODES): void;
        protected updatePadding(): void;
        /**
         * Sets the strength of both the blurX and blurY properties simultaneously
         *
         * @member {number}
         * @default 2
         */
        get blur(): number;
        set blur(value: number);
        /**
         * Sets the number of passes for blur. More passes means higher quality bluring.
         *
         * @member {number}
         * @default 1
         */
        get quality(): number;
        set quality(value: number);
        /**
         * Sets the strength of the blurX property
         *
         * @member {number}
         * @default 2
         */
        get blurX(): number;
        set blurX(value: number);
        /**
         * Sets the strength of the blurY property
         *
         * @member {number}
         * @default 2
         */
        get blurY(): number;
        set blurY(value: number);
        /**
         * Sets the blendmode of the filter
         *
         * @member {number}
         * @default PIXI.BLEND_MODES.NORMAL
         */
        get blendMode(): BLEND_MODES;
        set blendMode(value: BLEND_MODES);
        /**
         * If set to true the edge of the target will be clamped
         *
         * @member {boolean}
         * @default false
         */
        get repeatEdgePixels(): boolean;
        set repeatEdgePixels(value: boolean);
    }
}
declare namespace feng3d.pixi {
    /**
     * The BlurFilterPass applies a horizontal or vertical Gaussian blur to an object.
     *
     */
    class BlurFilterPass extends Filter {
        horizontal: boolean;
        strength: number;
        passes: number;
        private _quality;
        /**
         * @param {boolean} horizontal - Do pass along the x-axis (`true`) or y-axis (`false`).
         * @param {number} [strength=8] - The strength of the blur filter.
         * @param {number} [quality=4] - The quality of the blur filter.
         * @param {number} [resolution=PIXI.settings.FILTER_RESOLUTION] - The resolution of the blur filter.
         * @param {number} [kernelSize=5] - The kernelSize of the blur filter.Options: 5, 7, 9, 11, 13, 15.
         */
        constructor(horizontal: boolean, strength?: number, quality?: number, resolution?: number, kernelSize?: number);
        /**
         * Applies the filter.
         *
         * @param {PIXI.FilterSystem} filterManager - The manager.
         * @param {PIXI.RenderTexture} input - The input target.
         * @param {PIXI.RenderTexture} output - The output target.
         * @param {PIXI.CLEAR_MODES} clearMode - How to clear
         */
        apply(filterManager: FilterSystem, input: RenderTexture, output: RenderTexture, clearMode: CLEAR_MODES): void;
        /**
         * Sets the strength of both the blur.
         *
         * @member {number}
         * @default 16
         */
        get blur(): number;
        set blur(value: number);
        /**
         * Sets the quality of the blur by modifying the number of passes. More passes means higher
         * quality bluring but the lower the performance.
         *
         * @member {number}
         * @default 4
         */
        get quality(): number;
        set quality(value: number);
    }
}
declare namespace feng3d.pixi {
    function generateBlurFragSource(kernelSize: number): string;
}
declare namespace feng3d.pixi {
    function generateBlurVertSource(kernelSize: number, x: boolean): string;
}
declare namespace feng3d.pixi {
    function getMaxKernelSize(gl: IRenderingContext): number;
}
declare namespace feng3d.pixi {
    const colorMatrix_frag = "\nvarying vec2 vTextureCoord;\nuniform sampler2D uSampler;\nuniform float m[20];\nuniform float uAlpha;\n\nvoid main(void)\n{\n    vec4 c = texture2D(uSampler, vTextureCoord);\n\n    if (uAlpha == 0.0) {\n        gl_FragColor = c;\n        return;\n    }\n\n    // Un-premultiply alpha before applying the color matrix. See issue #3539.\n    if (c.a > 0.0) {\n      c.rgb /= c.a;\n    }\n\n    vec4 result;\n\n    result.r = (m[0] * c.r);\n        result.r += (m[1] * c.g);\n        result.r += (m[2] * c.b);\n        result.r += (m[3] * c.a);\n        result.r += m[4];\n\n    result.g = (m[5] * c.r);\n        result.g += (m[6] * c.g);\n        result.g += (m[7] * c.b);\n        result.g += (m[8] * c.a);\n        result.g += m[9];\n\n    result.b = (m[10] * c.r);\n       result.b += (m[11] * c.g);\n       result.b += (m[12] * c.b);\n       result.b += (m[13] * c.a);\n       result.b += m[14];\n\n    result.a = (m[15] * c.r);\n       result.a += (m[16] * c.g);\n       result.a += (m[17] * c.b);\n       result.a += (m[18] * c.a);\n       result.a += m[19];\n\n    vec3 rgb = mix(c.rgb, result.rgb, uAlpha);\n\n    // Premultiply alpha again.\n    rgb *= result.a;\n\n    gl_FragColor = vec4(rgb, result.a);\n}\n\n";
}
declare namespace feng3d.pixi {
    type ColorMatrix = ArrayFixed<number, 20>;
    /**
     * The ColorMatrixFilter class lets you apply a 5x4 matrix transformation on the RGBA
     * color and alpha values of every pixel on your displayObject to produce a result
     * with a new set of RGBA color and alpha values. It's pretty powerful!
     *
     * ```js
     *  let colorMatrix = new PIXI.filters.ColorMatrixFilter();
     *  container.filters = [colorMatrix];
     *  colorMatrix.contrast(2);
     * ```
     * @author Clément Chenebault <clement@goodboydigital.com>
     */
    class ColorMatrixFilter extends Filter {
        grayscale: (scale: number, multiply: boolean) => void;
        constructor();
        /**
         * Transforms current matrix and set the new one
         *
         * @param {number[]} matrix - 5x4 matrix
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        private _loadMatrix;
        /**
         * Multiplies two mat5's
         *
         * @private
         * @param {number[]} out - 5x4 matrix the receiving matrix
         * @param {number[]} a - 5x4 matrix the first operand
         * @param {number[]} b - 5x4 matrix the second operand
         * @returns {number[]} 5x4 matrix
         */
        private _multiply;
        /**
         * Create a Float32 Array and normalize the offset component to 0-1
         *
         * @private
         * @param {number[]} matrix - 5x4 matrix
         * @return {number[]} 5x4 matrix with all values between 0-1
         */
        private _colorMatrix;
        /**
         * Adjusts brightness
         *
         * @param {number} b - value of the brigthness (0-1, where 0 is black)
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        brightness(b: number, multiply: boolean): void;
        /**
         * Set the matrices in grey scales
         *
         * @param {number} scale - value of the grey (0-1, where 0 is black)
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        greyscale(scale: number, multiply: boolean): void;
        /**
         * Set the black and white matrice.
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        blackAndWhite(multiply: boolean): void;
        /**
         * Set the hue property of the color
         *
         * @param {number} rotation - in degrees
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        hue(rotation: number, multiply: boolean): void;
        /**
         * Set the contrast matrix, increase the separation between dark and bright
         * Increase contrast : shadows darker and highlights brighter
         * Decrease contrast : bring the shadows up and the highlights down
         *
         * @param {number} amount - value of the contrast (0-1)
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        contrast(amount: number, multiply: boolean): void;
        /**
         * Set the saturation matrix, increase the separation between colors
         * Increase saturation : increase contrast, brightness, and sharpness
         *
         * @param {number} amount - The saturation amount (0-1)
         * @param {boolean} [multiply] - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        saturate(amount?: number, multiply?: boolean): void;
        /**
         * Desaturate image (remove color)
         *
         * Call the saturate function
         *
         */
        desaturate(): void;
        /**
         * Negative image (inverse of classic rgb matrix)
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        negative(multiply: boolean): void;
        /**
         * Sepia image
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        sepia(multiply: boolean): void;
        /**
         * Color motion picture process invented in 1916 (thanks Dominic Szablewski)
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        technicolor(multiply: boolean): void;
        /**
         * Polaroid filter
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        polaroid(multiply: boolean): void;
        /**
         * Filter who transforms : Red -> Blue and Blue -> Red
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        toBGR(multiply: boolean): void;
        /**
         * Color reversal film introduced by Eastman Kodak in 1935. (thanks Dominic Szablewski)
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        kodachrome(multiply: boolean): void;
        /**
         * Brown delicious browni filter (thanks Dominic Szablewski)
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        browni(multiply: boolean): void;
        /**
         * Vintage filter (thanks Dominic Szablewski)
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        vintage(multiply: boolean): void;
        /**
         * We don't know exactly what it does, kind of gradient map, but funny to play with!
         *
         * @param {number} desaturation - Tone values.
         * @param {number} toned - Tone values.
         * @param {number} lightColor - Tone values, example: `0xFFE580`
         * @param {number} darkColor - Tone values, example: `0xFFE580`
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        colorTone(desaturation: number, toned: number, lightColor: number, darkColor: number, multiply: boolean): void;
        /**
         * Night effect
         *
         * @param {number} intensity - The intensity of the night effect.
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        night(intensity: number, multiply: boolean): void;
        /**
         * Predator effect
         *
         * Erase the current matrix by setting a new indepent one
         *
         * @param {number} amount - how much the predator feels his future victim
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        predator(amount: number, multiply: boolean): void;
        /**
         * LSD effect
         *
         * Multiply the current matrix
         *
         * @param {boolean} multiply - if true, current matrix and matrix are multiplied. If false,
         *  just set the current matrix with @param matrix
         */
        lsd(multiply: boolean): void;
        /**
         * Erase the current matrix by setting the default one
         *
         */
        reset(): void;
        /**
         * The matrix of the color matrix filter
         *
         * @member {number[]}
         * @default [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0]
         */
        get matrix(): ColorMatrix;
        set matrix(value: ColorMatrix);
        /**
         * The opacity value to use when mixing the original and resultant colors.
         *
         * When the value is 0, the original color is used without modification.
         * When the value is 1, the result color is used.
         * When in the range (0, 1) the color is interpolated between the original and result by this amount.
         *
         * @member {number}
         * @default 1
         */
        get alpha(): number;
        set alpha(value: number);
    }
}
declare namespace feng3d.pixi {
    const displacement_frag = "\nvarying vec2 vFilterCoord;\nvarying vec2 vTextureCoord;\n\nuniform vec2 scale;\nuniform mat2 rotation;\nuniform sampler2D uSampler;\nuniform sampler2D mapSampler;\n\nuniform highp vec4 inputSize;\nuniform vec4 inputClamp;\n\nvoid main(void)\n{\n  vec4 map =  texture2D(mapSampler, vFilterCoord);\n\n  map -= 0.5;\n  map.xy = scale * inputSize.zw * (rotation * map.xy);\n\n  gl_FragColor = texture2D(uSampler, clamp(vec2(vTextureCoord.x + map.x, vTextureCoord.y + map.y), inputClamp.xy, inputClamp.zw));\n}\n\n";
}
declare namespace feng3d.pixi {
    const displacement_vert = "\nattribute vec2 aVertexPosition;\n\nuniform mat3 projectionMatrix;\nuniform mat3 filterMatrix;\n\nvarying vec2 vTextureCoord;\nvarying vec2 vFilterCoord;\n\nuniform vec4 inputSize;\nuniform vec4 outputFrame;\n\nvec4 filterVertexPosition( void )\n{\n    vec2 position = aVertexPosition * max(outputFrame.zw, vec2(0.)) + outputFrame.xy;\n\n    return vec4((projectionMatrix * vec3(position, 1.0)).xy, 0.0, 1.0);\n}\n\nvec2 filterTextureCoord( void )\n{\n    return aVertexPosition * (outputFrame.zw * inputSize.zw);\n}\n\nvoid main(void)\n{\n\tgl_Position = filterVertexPosition();\n\tvTextureCoord = filterTextureCoord();\n\tvFilterCoord = ( filterMatrix * vec3( vTextureCoord, 1.0)  ).xy;\n}\n\n";
}
declare namespace feng3d.pixi {
    /**
     * The DisplacementFilter class uses the pixel values from the specified texture
     * (called the displacement map) to perform a displacement of an object.
     *
     * You can use this filter to apply all manor of crazy warping effects.
     * Currently the `r` property of the texture is used to offset the `x`
     * and the `g` property of the texture is used to offset the `y`.
     *
     * The way it works is it uses the values of the displacement map to look up the
     * correct pixels to output. This means it's not technically moving the original.
     * Instead, it's starting at the output and asking "which pixel from the original goes here".
     * For example, if a displacement map pixel has `red = 1` and the filter scale is `20`,
     * this filter will output the pixel approximately 20 pixels to the right of the original.
     *
     */
    class DisplacementFilter extends Filter {
        maskSprite: ISpriteMaskTarget;
        maskMatrix: Matrix;
        scale: Vector2;
        /**
         * @param {PIXI.Sprite} sprite - The sprite used for the displacement map. (make sure its added to the scene!)
         * @param {number} [scale] - The scale of the displacement
         */
        constructor(sprite: ISpriteMaskTarget, scale?: number);
        /**
         * Applies the filter.
         *
         * @param {PIXI.FilterSystem} filterManager - The manager.
         * @param {PIXI.RenderTexture} input - The input target.
         * @param {PIXI.RenderTexture} output - The output target.
         * @param {PIXI.CLEAR_MODES} clearMode - clearMode.
         */
        apply(filterManager: FilterSystem, input: RenderTexture, output: RenderTexture, clearMode: CLEAR_MODES): void;
        /**
         * The texture used for the displacement map. Must be power of 2 sized texture.
         *
         * @member {PIXI.Texture}
         */
        get map(): Texture;
        set map(value: Texture);
    }
}
declare namespace feng3d.pixi {
    const fxaa_frag = "\nvarying vec2 v_rgbNW;\nvarying vec2 v_rgbNE;\nvarying vec2 v_rgbSW;\nvarying vec2 v_rgbSE;\nvarying vec2 v_rgbM;\n\nvarying vec2 vFragCoord;\nuniform sampler2D uSampler;\nuniform highp vec4 inputPixel;\n\n\n/**\n Basic FXAA implementation based on the code on geeks3d.com with the\n modification that the texture2DLod stuff was removed since it's\n unsupported by WebGL.\n\n --\n\n From:\n https://github.com/mitsuhiko/webgl-meincraft\n\n Copyright (c) 2011 by Armin Ronacher.\n\n Some rights reserved.\n\n Redistribution and use in source and binary forms, with or without\n modification, are permitted provided that the following conditions are\n met:\n\n * Redistributions of source code must retain the above copyright\n notice, this list of conditions and the following disclaimer.\n\n * Redistributions in binary form must reproduce the above\n copyright notice, this list of conditions and the following\n disclaimer in the documentation and/or other materials provided\n with the distribution.\n\n * The names of the contributors may not be used to endorse or\n promote products derived from this software without specific\n prior written permission.\n\n THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS\n \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT\n LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR\n A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT\n OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,\n SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT\n LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,\n DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY\n THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE\n OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n */\n\n#ifndef FXAA_REDUCE_MIN\n#define FXAA_REDUCE_MIN   (1.0/ 128.0)\n#endif\n#ifndef FXAA_REDUCE_MUL\n#define FXAA_REDUCE_MUL   (1.0 / 8.0)\n#endif\n#ifndef FXAA_SPAN_MAX\n#define FXAA_SPAN_MAX     8.0\n#endif\n\n//optimized version for mobile, where dependent\n//texture reads can be a bottleneck\nvec4 fxaa(sampler2D tex, vec2 fragCoord, vec2 inverseVP,\n          vec2 v_rgbNW, vec2 v_rgbNE,\n          vec2 v_rgbSW, vec2 v_rgbSE,\n          vec2 v_rgbM) {\n    vec4 color;\n    vec3 rgbNW = texture2D(tex, v_rgbNW).xyz;\n    vec3 rgbNE = texture2D(tex, v_rgbNE).xyz;\n    vec3 rgbSW = texture2D(tex, v_rgbSW).xyz;\n    vec3 rgbSE = texture2D(tex, v_rgbSE).xyz;\n    vec4 texColor = texture2D(tex, v_rgbM);\n    vec3 rgbM  = texColor.xyz;\n    vec3 luma = vec3(0.299, 0.587, 0.114);\n    float lumaNW = dot(rgbNW, luma);\n    float lumaNE = dot(rgbNE, luma);\n    float lumaSW = dot(rgbSW, luma);\n    float lumaSE = dot(rgbSE, luma);\n    float lumaM  = dot(rgbM,  luma);\n    float lumaMin = min(lumaM, min(min(lumaNW, lumaNE), min(lumaSW, lumaSE)));\n    float lumaMax = max(lumaM, max(max(lumaNW, lumaNE), max(lumaSW, lumaSE)));\n\n    mediump vec2 dir;\n    dir.x = -((lumaNW + lumaNE) - (lumaSW + lumaSE));\n    dir.y =  ((lumaNW + lumaSW) - (lumaNE + lumaSE));\n\n    float dirReduce = max((lumaNW + lumaNE + lumaSW + lumaSE) *\n                          (0.25 * FXAA_REDUCE_MUL), FXAA_REDUCE_MIN);\n\n    float rcpDirMin = 1.0 / (min(abs(dir.x), abs(dir.y)) + dirReduce);\n    dir = min(vec2(FXAA_SPAN_MAX, FXAA_SPAN_MAX),\n              max(vec2(-FXAA_SPAN_MAX, -FXAA_SPAN_MAX),\n                  dir * rcpDirMin)) * inverseVP;\n\n    vec3 rgbA = 0.5 * (\n                       texture2D(tex, fragCoord * inverseVP + dir * (1.0 / 3.0 - 0.5)).xyz +\n                       texture2D(tex, fragCoord * inverseVP + dir * (2.0 / 3.0 - 0.5)).xyz);\n    vec3 rgbB = rgbA * 0.5 + 0.25 * (\n                                     texture2D(tex, fragCoord * inverseVP + dir * -0.5).xyz +\n                                     texture2D(tex, fragCoord * inverseVP + dir * 0.5).xyz);\n\n    float lumaB = dot(rgbB, luma);\n    if ((lumaB < lumaMin) || (lumaB > lumaMax))\n        color = vec4(rgbA, texColor.a);\n    else\n        color = vec4(rgbB, texColor.a);\n    return color;\n}\n\nvoid main() {\n\n      vec4 color;\n\n      color = fxaa(uSampler, vFragCoord, inputPixel.zw, v_rgbNW, v_rgbNE, v_rgbSW, v_rgbSE, v_rgbM);\n\n      gl_FragColor = color;\n}\n";
}
declare namespace feng3d.pixi {
    const fxaa_vert = "\n\nattribute vec2 aVertexPosition;\n\nuniform mat3 projectionMatrix;\n\nvarying vec2 v_rgbNW;\nvarying vec2 v_rgbNE;\nvarying vec2 v_rgbSW;\nvarying vec2 v_rgbSE;\nvarying vec2 v_rgbM;\n\nvarying vec2 vFragCoord;\n\nuniform vec4 inputPixel;\nuniform vec4 outputFrame;\n\nvec4 filterVertexPosition( void )\n{\n    vec2 position = aVertexPosition * max(outputFrame.zw, vec2(0.)) + outputFrame.xy;\n\n    return vec4((projectionMatrix * vec3(position, 1.0)).xy, 0.0, 1.0);\n}\n\nvoid texcoords(vec2 fragCoord, vec2 inverseVP,\n               out vec2 v_rgbNW, out vec2 v_rgbNE,\n               out vec2 v_rgbSW, out vec2 v_rgbSE,\n               out vec2 v_rgbM) {\n    v_rgbNW = (fragCoord + vec2(-1.0, -1.0)) * inverseVP;\n    v_rgbNE = (fragCoord + vec2(1.0, -1.0)) * inverseVP;\n    v_rgbSW = (fragCoord + vec2(-1.0, 1.0)) * inverseVP;\n    v_rgbSE = (fragCoord + vec2(1.0, 1.0)) * inverseVP;\n    v_rgbM = vec2(fragCoord * inverseVP);\n}\n\nvoid main(void) {\n\n   gl_Position = filterVertexPosition();\n\n   vFragCoord = aVertexPosition * outputFrame.zw;\n\n   texcoords(vFragCoord, inputPixel.zw, v_rgbNW, v_rgbNE, v_rgbSW, v_rgbSE, v_rgbM);\n}\n";
}
declare namespace feng3d.pixi {
    /**
     * Basic FXAA (Fast Approximate Anti-Aliasing) implementation based on the code on geeks3d.com
     * with the modification that the texture2DLod stuff was removed since it is unsupported by WebGL.
     *
     * @see https://github.com/mitsuhiko/webgl-meincraft
     *
     *
     */
    class FXAAFilter extends Filter {
        constructor();
    }
}
declare namespace feng3d.pixi {
    const noise_frag = "\nprecision highp float;\n\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\n\nuniform float uNoise;\nuniform float uSeed;\nuniform sampler2D uSampler;\n\nfloat rand(vec2 co)\n{\n    return fract(sin(dot(co.xy, vec2(12.9898, 78.233))) * 43758.5453);\n}\n\nvoid main()\n{\n    vec4 color = texture2D(uSampler, vTextureCoord);\n    float randomValue = rand(gl_FragCoord.xy * uSeed);\n    float diff = (randomValue - 0.5) * uNoise;\n\n    // Un-premultiply alpha before applying the color matrix. See issue #3539.\n    if (color.a > 0.0) {\n        color.rgb /= color.a;\n    }\n\n    color.r += diff;\n    color.g += diff;\n    color.b += diff;\n\n    // Premultiply alpha again.\n    color.rgb *= color.a;\n\n    gl_FragColor = color;\n}\n";
}
declare namespace feng3d.pixi {
    /**
     * A Noise effect filter.
     *
     * original filter: https://github.com/evanw/glfx.js/blob/master/src/filters/adjust/noise.js
     *
     * @author Vico @vicocotea
     */
    class NoiseFilter extends Filter {
        /**
         * @param {number} [noise=0.5] - The noise intensity, should be a normalized value in the range [0, 1].
         * @param {number} [seed] - A random seed for the noise generation. Default is `Math.random()`.
         */
        constructor(noise?: number, seed?: number);
        /**
         * The amount of noise to apply, this value should be in the range (0, 1].
         *
         * @member {number}
         * @default 0.5
         */
        get noise(): number;
        set noise(value: number);
        /**
         * A seed value to apply to the random noise generation. `Math.random()` is a good value to use.
         *
         * @member {number}
         */
        get seed(): number;
        set seed(value: number);
    }
}
declare namespace feng3d.pixi {
    /**
     * Helper class to create a quad
     *
     */
    class Quad extends Geometry {
        constructor();
    }
}
declare namespace feng3d.pixi {
    /**
     * Helper class to create a quad with uvs like in v4
     *
     */
    class QuadUv extends Geometry {
        vertexBuffer: Buffer;
        uvBuffer: Buffer;
        vertices: Float32Array;
        uvs: Float32Array;
        constructor();
        /**
         * Maps two Rectangle to the quad.
         *
         * @param {PIXI.Rectangle} targetTextureFrame - the first rectangle
         * @param {PIXI.Rectangle} destinationFrame - the second rectangle
         * @return {PIXI.Quad} Returns itself.
         */
        map(targetTextureFrame: Rectangle, destinationFrame: Rectangle): this;
        /**
         * legacy upload method, just marks buffers dirty
         * @returns {PIXI.QuadUv} Returns itself.
         */
        invalidate(): this;
    }
}
declare namespace feng3d.pixi {
    /**
     * Any plugin that's usable for Application should contain these methods.
     * @see {@link PIXI.Application.registerPlugin}
     */
    interface IApplicationPlugin {
        /**
         * Called when Application is constructed, scoped to Application instance.
         * Passes in `options` as the only argument, which are Application constructor options.
         * @param {object} options - Application options.
         */
        init(options: IApplicationOptions): void;
        /**
         * Called when destroying Application, scoped to Application instance.
         */
        destroy(): void;
    }
    interface IApplicationOptions extends IRendererOptionsAuto {
    }
    interface Application {
    }
    /**
     * Convenience class to create a new PIXI application.
     *
     * This class automatically creates the renderer, ticker and root container.
     *
     * @example
     * // Create the application
     * const app = new PIXI.Application();
     *
     * // Add the view to the DOM
     * document.body.appendChild(app.view);
     *
     * // ex, add display objects
     * app.stage.addChild(PIXI.Sprite.from('something.png'));
     *
     */
    class Application {
        /** Collection of installed plugins. */
        private static _plugins;
        /**
         * The root display container that's rendered.
         * @member {PIXI.Container}
         */
        stage: Container;
        /**
         * WebGL renderer if available, otherwise CanvasRenderer.
         * @member {PIXI.Renderer|PIXI.CanvasRenderer}
         */
        renderer: Renderer | AbstractRenderer;
        /**
         * @param {object} [options] - The optional renderer parameters.
         * @param {boolean} [options.autoStart=true] - Automatically starts the rendering after the construction.
         *     **Note**: Setting this parameter to false does NOT stop the shared ticker even if you set
         *     options.sharedTicker to true in case that it is already started. Stop it by your own.
         * @param {number} [options.width=800] - The width of the renderers view.
         * @param {number} [options.height=600] - The height of the renderers view.
         * @param {HTMLCanvasElement} [options.view] - The canvas to use as a view, optional.
         * @param {boolean} [options.useContextAlpha=true] - Pass-through value for canvas' context `alpha` property.
         *   If you want to set transparency, please use `backgroundAlpha`. This option is for cases where the
         *   canvas needs to be opaque, possibly for performance reasons on some older devices.
         * @param {boolean} [options.autoDensity=false] - Resizes renderer view in CSS pixels to allow for
         *   resolutions other than 1.
         * @param {boolean} [options.antialias=false] - Sets antialias
         * @param {boolean} [options.preserveDrawingBuffer=false] - Enables drawing buffer preservation, enable this if you
         *  need to call toDataUrl on the WebGL context.
         * @param {number} [options.resolution=1] - The resolution / device pixel ratio of the renderer, retina would be 2.
         * @param {boolean} [options.forceCanvas=false] - prevents selection of WebGL renderer, even if such is present, this
         *   option only is available when using **pixi.js-legacy** or **@pixi/canvas-renderer** modules, otherwise
         *   it is ignored.
         * @param {number} [options.backgroundColor=0x000000] - The background color of the rendered area
         *  (shown if not transparent).
         * @param {number} [options.backgroundAlpha=1] - Value from 0 (fully transparent) to 1 (fully opaque).
         * @param {boolean} [options.clearBeforeRender=true] - This sets if the renderer will clear the canvas or
         *   not before the new render pass.
         * @param {string} [options.powerPreference] - Parameter passed to webgl context, set to "high-performance"
         *  for devices with dual graphics card. **(WebGL only)**.
         * @param {boolean} [options.sharedTicker=false] - `true` to use PIXI.Ticker.shared, `false` to create new ticker.
         *  If set to false, you cannot register a handler to occur before anything that runs on the shared ticker.
         *  The system ticker will always run before both the shared ticker and the app ticker.
         * @param {boolean} [options.sharedLoader=false] - `true` to use PIXI.Loader.shared, `false` to create new Loader.
         * @param {Window|HTMLElement} [options.resizeTo] - Element to automatically resize stage to.
         */
        constructor(options?: IApplicationOptions);
        /**
         * Register a middleware plugin for the application
         * @static
         * @param {PIXI.IApplicationPlugin} plugin - Plugin being installed
         */
        static registerPlugin(plugin: IApplicationPlugin): void;
        /**
         * Render the current stage.
         */
        render(): void;
        /**
         * Reference to the renderer's canvas element.
         * @member {HTMLCanvasElement}
         * @readonly
         */
        get view(): HTMLCanvasElement;
        /**
         * Reference to the renderer's screen rectangle. Its safe to use as `filterArea` or `hitArea` for the whole screen.
         * @member {PIXI.Rectangle}
         * @readonly
         */
        get screen(): Rectangle;
        /**
         * Destroy and don't use after this.
         * @param {Boolean} [removeView=false] - Automatically remove canvas from DOM.
         * @param {object|boolean} [stageOptions] - Options parameter. A boolean will act as if all options
         *  have been set to that value
         * @param {boolean} [stageOptions.children=false] - if set to true, all the children will have their destroy
         *  method called as well. 'stageOptions' will be passed on to those calls.
         * @param {boolean} [stageOptions.texture=false] - Only used for child Sprites if stageOptions.children is set
         *  to true. Should it destroy the texture of the child sprite
         * @param {boolean} [stageOptions.baseTexture=false] - Only used for child Sprites if stageOptions.children is set
         *  to true. Should it destroy the base texture of the child sprite
         */
        destroy(removeView?: boolean, stageOptions?: IDestroyOptions | boolean): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Middleware for for Application's resize functionality
     * @private
     */
    class ResizePlugin {
        static resizeTo: Window | HTMLElement;
        static resize: () => void;
        static renderer: Renderer;
        static queueResize: () => void;
        private static _resizeId;
        private static _resizeTo;
        private static cancelResize;
        /**
         * Initialize the plugin with scope of application instance
         * @static
         * @private
         * @param {object} [options] - See application options
         */
        static init(options?: IApplicationOptions): void;
        /**
         * Clean up the ticker, scoped to application
         *
         * @static
         * @private
         */
        static destroy(): void;
    }
}
declare namespace feng3d.pixi {
    interface Application {
        resizeTo: Window | HTMLElement;
        resize(): void;
    }
    interface IApplicationOptions {
        resizeTo?: Window | HTMLElement;
    }
}
declare namespace feng3d.pixi {
    /** Helper to deduce the argument types of a function. */
    type ArgumentTypes<T> = T extends (...args: infer U) => infer R ? U : never;
    /** Helper to replace the return type of a function with a different value. */
    type ReplaceReturnType<T, TNewReturn> = (...a: ArgumentTypes<T>) => TNewReturn;
    /** Helper to replace the return type of a function with `boolean`. */
    type WithBoolReturn<T> = ReplaceReturnType<T, boolean>;
    /** Helper to replace the return type of a function with `void`. */
    type WithVoidReturn<T> = ReplaceReturnType<T, void>;
    /**
     * Interface representing a single binding to the signal.
     * This can be used to detach the handler function from the owning signal
     * so that it will no longer receive events.
     */
    interface SignalBinding {
        /** Detaches this binding from the owning signal. */
        detach(): boolean;
        /** Detaches this binding from the owning signal. */
        dispose(): void;
    }
    /**
     * A signal is a dispatcher that can bind functions (handlers) to dispatched events.
     */
    class Signal<T extends Function = (() => void)> {
        private _head;
        private _tail;
        private _filter;
        /**
         * Gathers a list of all the handlers currently bound to this signal.
         */
        handlers(): SignalBinding[];
        /**
         * Returns true if this signal has any bound handlers.
         */
        hasAny(): boolean;
        /**
         * Returns true if the given binding is owned by this signal.
         *
         * @param node The binding to check.
         */
        has(node: SignalBinding): boolean;
        /**
         * Dispatch an event to all handlers.
         * If a filter was set, only if it returns `true` will the event get dispatched.
         *
         * @param args The arguments to pass to the filter and handlers.
         * @returns True if the event was dispatched, false otherwise.
         */
        dispatch(...args: ArgumentTypes<T>): boolean;
        /**
         * Binds a new handler function to this signal that will be called for each dispatch.
         *
         * @param fn The handler function to bind.
         * @param thisArg Optional `this` argument to use when calling this handler
         */
        add(fn: WithVoidReturn<T>, thisArg?: any): SignalBinding;
        /**
         * Binds a new handler function to this signal that will only be called once on the next dispatch.
         *
         * @param fn The handler function to bind.
         * @param thisArg Optional `this` argument to use when calling this handler.
         */
        once(fn: WithVoidReturn<T>, thisArg?: any): SignalBinding;
        /**
         * Detaches a binding from this signal so that it is no longer called.
         *
         * @param node_ The binding to detach.
         */
        detach(node_: SignalBinding): this;
        /**
         * Detaches all bindings.
         */
        detachAll(): this;
        /**
         * Sets the filter function to be called on each dispatch. This function takes the same
         * parameters as a handler, but must return a boolean. Only when this function returns
         * `true` will an event dispatch actually call bound handlers.
         *
         * @param filter The function to use as the filter.
         */
        filter(filter: WithBoolReturn<T>): void;
        /**
         * Sets up a link between the passed signals and this one such that when the passed
         * signal is dispatched, this signal is also dispatched.
         *
         * @param signals The signals to proxy.
         */
        proxy(...signals: Signal<T>[]): this;
        private _addSignalBinding;
    }
}
declare namespace feng3d.pixi.resource_loader {
    /**
     * A simple in-memory cache for resource.
     *
     * @memberof middleware
     * @function caching
     * @example
     * import { Loader, middleware } from 'resource-loader';
     * const loader = new Loader();
     * loader.use(middleware.caching);
     * @param {Resource} resource - Current Resource
     * @param {function} next - Callback when complete
     */
    function caching(resource: any, next: any): void;
    /**
     * A middleware for transforming XHR loaded Blobs into more useful objects
     *
     * @memberof middleware
     * @function parsing
     * @example
     * import { Loader, middleware } from 'resource-loader';
     * const loader = new Loader();
     * loader.use(middleware.parsing);
     * @param {Resource} resource - Current Resource
     * @param {function} next - Callback when complete
     */
    function parsing(resource: any, next: any): void;
    export const middleware: {
        caching: typeof caching;
        parsing: typeof parsing;
    };
    export {};
}
declare namespace feng3d.pixi.resource_loader {
    /**
     * Smaller version of the async library constructs.
     *
     * @namespace async
     */
    /**
     * Noop function
     *
     * @ignore
     * @function
     * @memberof async
     */
    function _noop(): void;
    /**
     * Iterates an array in series.
     *
     * @memberof async
     * @function eachSeries
     * @param {Array.<*>} array - Array to iterate.
     * @param {function} iterator - Function to call for each element.
     * @param {function} callback - Function to call when done, or on error.
     * @param {boolean} [deferNext=false] - Break synchronous each loop by calling next with a setTimeout of 1.
     */
    function eachSeries(array: any, iterator: any, callback: any, deferNext: any): void;
    /**
     * Async queue implementation,
     *
     * @memberof async
     * @function queue
     * @param {function} worker - The worker function to call for each task.
     * @param {number} concurrency - How many workers to run in parrallel.
     * @return {*} The async queue object.
     */
    function queue(worker: any, concurrency: any): {
        _tasks: any[];
        concurrency: any;
        saturated: typeof _noop;
        unsaturated: typeof _noop;
        buffer: number;
        empty: typeof _noop;
        drain: typeof _noop;
        error: typeof _noop;
        started: boolean;
        paused: boolean;
        push(data: any, callback: any): void;
        kill(): void;
        unshift(data: any, callback: any): void;
        process(): void;
        length(): number;
        running(): number;
        idle(): boolean;
        pause(): void;
        resume(): void;
    };
    export const async: {
        queue: typeof queue;
        eachSeries: typeof eachSeries;
    };
    export {};
}
declare namespace feng3d.pixi.resource_loader {
    /**
     * Encodes binary into base64.
     *
     * @function encodeBinary
     * @param {string} input The input data to encode.
     * @returns {string} The encoded base64 string
     */
    function encodeBinary(input: any): string;
}
declare namespace feng3d.pixi.resource_loader {
    /**
     * Manages the state and loading of multiple resources to load.
     *
     */
    class Loader {
        baseUrl: string;
        progress: number;
        loading: boolean;
        defaultQueryString: string;
        _beforeMiddleware: any[];
        _afterMiddleware: any[];
        _resourcesParsing: any[];
        _boundLoadResource: (r: any, d: any) => void;
        _queue: any;
        resources: {};
        onProgress: any;
        onError: any;
        onLoad: any;
        onStart: any;
        onComplete: any;
        static _defaultBeforeMiddleware: any;
        static _defaultAfterMiddleware: any;
        static pre: (fn: any) => typeof Loader;
        static use: (fn: any) => typeof Loader;
        /**
         * @param {string} [baseUrl=''] - The base url for all resources loaded by this loader.
         * @param {number} [concurrency=10] - The number of resources to load concurrently.
         */
        constructor(baseUrl?: string, concurrency?: number);
        /**
         * When the progress changes the loader and resource are disaptched.
         *
         * @memberof Loader
         * @callback OnProgressSignal
         * @param {Loader} loader - The loader the progress is advancing on.
         * @param {Resource} resource - The resource that has completed or failed to cause the progress to advance.
         */
        /**
         * When an error occurrs the loader and resource are disaptched.
         *
         * @memberof Loader
         * @callback OnErrorSignal
         * @param {Loader} loader - The loader the error happened in.
         * @param {Resource} resource - The resource that caused the error.
         */
        /**
         * When a load completes the loader and resource are disaptched.
         *
         * @memberof Loader
         * @callback OnLoadSignal
         * @param {Loader} loader - The loader that laoded the resource.
         * @param {Resource} resource - The resource that has completed loading.
         */
        /**
         * When the loader starts loading resources it dispatches this callback.
         *
         * @memberof Loader
         * @callback OnStartSignal
         * @param {Loader} loader - The loader that has started loading resources.
         */
        /**
         * When the loader completes loading resources it dispatches this callback.
         *
         * @memberof Loader
         * @callback OnCompleteSignal
         * @param {Loader} loader - The loader that has finished loading resources.
         */
        /**
         * Options for a call to `.add()`.
         *
         * @see Loader#add
         *
         * @typedef {object} IAddOptions
         * @property {string} [name] - The name of the resource to load, if not passed the url is used.
         * @property {string} [key] - Alias for `name`.
         * @property {string} [url] - The url for this resource, relative to the baseUrl of this loader.
         * @property {string|boolean} [crossOrigin] - Is this request cross-origin? Default is to
         *      determine automatically.
         * @property {number} [timeout=0] - A timeout in milliseconds for the load. If the load takes
         *      longer than this time it is cancelled and the load is considered a failure. If this value is
         *      set to `0` then there is no explicit timeout.
         * @property {Resource.LOAD_TYPE} [loadType=Resource.LOAD_TYPE.XHR] - How should this resource
         *      be loaded?
         * @property {Resource.XHR_RESPONSE_TYPE} [xhrType=Resource.XHR_RESPONSE_TYPE.DEFAULT] - How
         *      should the data being loaded be interpreted when using XHR?
         * @property {Resource.OnCompleteSignal} [onComplete] - Callback to add an an onComplete signal istener.
         * @property {Resource.OnCompleteSignal} [callback] - Alias for `onComplete`.
         * @property {Resource.IMetadata} [metadata] - Extra configuration for middleware and the Resource object.
         */
        /**
         * Adds a resource (or multiple resources) to the loader queue.
         *
         * This function can take a wide variety of different parameters. The only thing that is always
         * required the url to load. All the following will work:
         *
         * ```js
         * loader
         *     // normal param syntax
         *     .add('key', 'http://...', function () {})
         *     .add('http://...', function () {})
         *     .add('http://...')
         *
         *     // object syntax
         *     .add({
         *         name: 'key2',
         *         url: 'http://...'
         *     }, function () {})
         *     .add({
         *         url: 'http://...'
         *     }, function () {})
         *     .add({
         *         name: 'key3',
         *         url: 'http://...'
         *         onComplete: function () {}
         *     })
         *     .add({
         *         url: 'https://...',
         *         onComplete: function () {},
         *         crossOrigin: true
         *     })
         *
         *     // you can also pass an array of objects or urls or both
         *     .add([
         *         { name: 'key4', url: 'http://...', onComplete: function () {} },
         *         { url: 'http://...', onComplete: function () {} },
         *         'http://...'
         *     ])
         *
         *     // and you can use both params and options
         *     .add('key', 'http://...', { crossOrigin: true }, function () {})
         *     .add('http://...', { crossOrigin: true }, function () {});
         * ```
         *
         * @function
         * @variation 1
         * @param {string} name - The name of the resource to load.
         * @param {string} url - The url for this resource, relative to the baseUrl of this loader.
         * @param {Resource.OnCompleteSignal} [callback] - Function to call when this specific resource completes loading.
         * @return {this} Returns itself.
         */ /**
    * @function
    * @variation 2
    * @param {string} name - The name of the resource to load.
    * @param {string} url - The url for this resource, relative to the baseUrl of this loader.
    * @param {IAddOptions} [options] - The options for the load.
    * @param {Resource.OnCompleteSignal} [callback] - Function to call when this specific resource completes loading.
    * @return {this} Returns itself.
    */ /**
              * @function
              * @variation 3
              * @param {string} url - The url for this resource, relative to the baseUrl of this loader.
              * @param {Resource.OnCompleteSignal} [callback] - Function to call when this specific resource completes loading.
              * @return {this} Returns itself.
              */ /**
    * @function
    * @variation 4
    * @param {string} url - The url for this resource, relative to the baseUrl of this loader.
    * @param {IAddOptions} [options] - The options for the load.
    * @param {Resource.OnCompleteSignal} [callback] - Function to call when this specific resource completes loading.
    * @return {this} Returns itself.
    */ /**
         * @function
         * @variation 5
         * @param {IAddOptions} options - The options for the load. This object must contain a `url` property.
         * @param {Resource.OnCompleteSignal} [callback] - Function to call when this specific resource completes loading.
         * @return {this} Returns itself.
         */ /**
        * @function
        * @variation 6
        * @param {Array<IAddOptions|string>} resources - An array of resources to load, where each is
        *      either an object with the options or a string url. If you pass an object, it must contain a `url` property.
        * @param {Resource.OnCompleteSignal} [callback] - Function to call when this specific resource completes loading.
        * @return {this} Returns itself.
        */
        add(name: any, url?: any, options?: any, cb?: any): this;
        /**
         * Sets up a middleware function that will run *before* the
         * resource is loaded.
         *
         * @param {function} fn - The middleware function to register.
         * @return {this} Returns itself.
         */
        pre(fn: any): this;
        /**
         * Sets up a middleware function that will run *after* the
         * resource is loaded.
         *
         * @param {function} fn - The middleware function to register.
         * @return {this} Returns itself.
         */
        use(fn: any): this;
        /**
         * Resets the queue of the loader to prepare for a new load.
         *
         * @return {this} Returns itself.
         */
        reset(): this;
        /**
         * Starts loading the queued resources.
         *
         * @param {function} [cb] - Optional callback that will be bound to the `complete` event.
         * @return {this} Returns itself.
         */
        load(cb: any): this;
        /**
         * The number of resources to load concurrently.
         *
         * @member {number}
         * @default 10
         */
        get concurrency(): any;
        set concurrency(concurrency: any);
        /**
         * Prepares a url for usage based on the configuration of this object
         *
         * @private
         * @param {string} url - The url to prepare.
         * @return {string} The prepared url.
         */
        _prepareUrl(url: any): any;
        /**
         * Loads a single resource.
         *
         * @private
         * @param {Resource} resource - The resource to load.
         * @param {function} dequeue - The function to call when we need to dequeue this item.
         */
        _loadResource(resource: any, dequeue: any): void;
        /**
         * Called once loading has started.
         *
         * @private
         */
        _onStart(): void;
        /**
         * Called once each resource has loaded.
         *
         * @private
         */
        _onComplete(): void;
        /**
         * Called each time a resources is loaded.
         *
         * @private
         * @param {Resource} resource - The resource that was loaded
         */
        _onLoad(resource: any): void;
    }
}
declare namespace feng3d.pixi.resource_loader {
    /**
     * Manages the state and loading of a resource and all child resources.
     *
     */
    class Resource {
        _flags: number;
        name: string;
        url: string;
        extension: string;
        data: any;
        crossOrigin: any;
        timeout: any;
        loadType: any;
        xhrType: any;
        metadata: any;
        error: any;
        xhr: any;
        children: any[];
        type: number;
        progressChunk: number;
        _dequeue: () => void;
        _onLoadBinding: any;
        _elementTimer: number;
        _boundComplete: any;
        _boundOnError: any;
        _boundOnProgress: any;
        _boundOnTimeout: any;
        _boundXhrOnError: any;
        _boundXhrOnTimeout: any;
        _boundXhrOnAbort: any;
        _boundXhrOnLoad: any;
        onStart: any;
        onProgress: any;
        onComplete: any;
        onAfterMiddleware: any;
        xdr: any;
        /**
         * Sets the load type to be used for a specific extension.
         *
         * @static
         * @param {string} extname - The extension to set the type for, e.g. "png" or "fnt"
         * @param {Resource.LOAD_TYPE} loadType - The load type to set it to.
         */
        static setExtensionLoadType(extname: any, loadType: any): void;
        /**
         * Sets the load type to be used for a specific extension.
         *
         * @static
         * @param {string} extname - The extension to set the type for, e.g. "png" or "fnt"
         * @param {Resource.XHR_RESPONSE_TYPE} xhrType - The xhr type to set it to.
         */
        static setExtensionXhrType(extname: any, xhrType: any): void;
        /**
         * @param {string} name - The name of the resource to load.
         * @param {string|string[]} url - The url for this resource, for audio/video loads you can pass
         *      an array of sources.
         * @param {object} [options] - The options for the load.
         * @param {string|boolean} [options.crossOrigin] - Is this request cross-origin? Default is to
         *      determine automatically.
         * @param {number} [options.timeout=0] - A timeout in milliseconds for the load. If the load takes
         *      longer than this time it is cancelled and the load is considered a failure. If this value is
         *      set to `0` then there is no explicit timeout.
         * @param {Resource.LOAD_TYPE} [options.loadType=Resource.LOAD_TYPE.XHR] - How should this resource
         *      be loaded?
         * @param {Resource.XHR_RESPONSE_TYPE} [options.xhrType=Resource.XHR_RESPONSE_TYPE.DEFAULT] - How
         *      should the data being loaded be interpreted when using XHR?
         * @param {Resource.IMetadata} [options.metadata] - Extra configuration for middleware and the Resource object.
         */
        constructor(name: any, url: any, options: any);
        /**
         * When the resource starts to load.
         *
         * @memberof Resource
         * @callback OnStartSignal
         * @param {Resource} resource - The resource that the event happened on.
         */
        /**
         * When the resource reports loading progress.
         *
         * @memberof Resource
         * @callback OnProgressSignal
         * @param {Resource} resource - The resource that the event happened on.
         * @param {number} percentage - The progress of the load in the range [0, 1].
         */
        /**
         * When the resource finishes loading.
         *
         * @memberof Resource
         * @callback OnCompleteSignal
         * @param {Resource} resource - The resource that the event happened on.
         */
        /**
         * @memberof Resource
         * @typedef {object} IMetadata
         * @property {HTMLImageElement|HTMLAudioElement|HTMLVideoElement} [loadElement=null] - The
         *      element to use for loading, instead of creating one.
         * @property {boolean} [skipSource=false] - Skips adding source(s) to the load element. This
         *      is useful if you want to pass in a `loadElement` that you already added load sources to.
         * @property {string|string[]} [mimeType] - The mime type to use for the source element
         *      of a video/audio elment. If the urls are an array, you can pass this as an array as well
         *      where each index is the mime type to use for the corresponding url index.
         */
        /**
         * Stores whether or not this url is a data url.
         *
         * @readonly
         * @member {boolean}
         */
        get isDataUrl(): boolean;
        /**
         * Describes if this resource has finished loading. Is true when the resource has completely
         * loaded.
         *
         * @readonly
         * @member {boolean}
         */
        get isComplete(): boolean;
        /**
         * Describes if this resource is currently loading. Is true when the resource starts loading,
         * and is false again when complete.
         *
         * @readonly
         * @member {boolean}
         */
        get isLoading(): boolean;
        /**
         * Marks the resource as complete.
         *
         */
        complete(): void;
        /**
         * Aborts the loading of this resource, with an optional message.
         *
         * @param {string} message - The message to use for the error
         */
        abort(message: any): void;
        /**
         * Kicks off loading of this resource. This method is asynchronous.
         *
         * @param {Resource.OnCompleteSignal} [cb] - Optional callback to call once the resource is loaded.
         */
        load(cb: any): void;
        /**
         * Checks if the flag is set.
         *
         * @private
         * @param {number} flag - The flag to check.
         * @return {boolean} True if the flag is set.
         */
        _hasFlag(flag: any): boolean;
        /**
         * (Un)Sets the flag.
         *
         * @private
         * @param {number} flag - The flag to (un)set.
         * @param {boolean} value - Whether to set or (un)set the flag.
         */
        _setFlag(flag: any, value: any): void;
        /**
         * Clears all the events from the underlying loading source.
         *
         * @private
         */
        _clearEvents(): void;
        /**
         * Finalizes the load.
         *
         * @private
         */
        _finish(): void;
        /**
         * Loads this resources using an element that has a single source,
         * like an HTMLImageElement.
         *
         * @private
         * @param {string} type - The type of element to use.
         */
        _loadElement(type: any): void;
        /**
         * Loads this resources using an element that has multiple sources,
         * like an HTMLAudioElement or HTMLVideoElement.
         *
         * @private
         * @param {string} type - The type of element to use.
         */
        _loadSourceElement(type: any): void;
        /**
         * Loads this resources using an XMLHttpRequest.
         *
         * @private
         */
        _loadXhr(): void;
        /**
         * Loads this resources using an XDomainRequest. This is here because we need to support IE9 (gross).
         *
         * @private
         */
        _loadXdr(): void;
        /**
         * Creates a source used in loading via an element.
         *
         * @private
         * @param {string} type - The element type (video or audio).
         * @param {string} url - The source URL to load from.
         * @param {string} [mime] - The mime type of the video
         * @return {HTMLSourceElement} The source element.
         */
        _createSource(type: any, url: any, mime: any): HTMLSourceElement;
        /**
         * Called if a load errors out.
         *
         * @param {Event} event - The error event from the element that emits it.
         * @private
         */
        _onError(event: any): void;
        /**
         * Called if a load progress event fires for an element or xhr/xdr.
         *
         * @private
         * @param {XMLHttpRequestProgressEvent|Event} event - Progress event.
         */
        _onProgress(event: any): void;
        /**
         * Called if a timeout event fires for an element.
         *
         * @private
         */
        _onTimeout(): void;
        /**
         * Called if an error event fires for xhr/xdr.
         *
         * @private
         */
        _xhrOnError(): void;
        /**
         * Called if an error event fires for xhr/xdr.
         *
         * @private
         */
        _xhrOnTimeout(): void;
        /**
         * Called if an abort event fires for xhr/xdr.
         *
         * @private
         */
        _xhrOnAbort(): void;
        /**
         * Called when data successfully loads from an xhr/xdr request.
         *
         * @private
         * @param {XMLHttpRequestLoadEvent|Event} event - Load event
         */
        _xhrOnLoad(): void;
        /**
         * Sets the `crossOrigin` property for this resource based on if the url
         * for this resource is cross-origin. If crossOrigin was manually set, this
         * function does nothing.
         *
         * @private
         * @param {string} url - The url to test.
         * @param {object} [loc=window.location] - The location object to test against.
         * @return {string} The crossOrigin value to use (or empty string for none).
         */
        _determineCrossOrigin(url: any, loc?: any): "" | "anonymous";
        /**
         * Determines the responseType of an XHR request based on the extension of the
         * resource being loaded.
         *
         * @private
         * @return {Resource.XHR_RESPONSE_TYPE} The responseType to use.
         */
        _determineXhrType(): any;
        /**
         * Determines the loadType of a resource based on the extension of the
         * resource being loaded.
         *
         * @private
         * @return {Resource.LOAD_TYPE} The loadType to use.
         */
        _determineLoadType(): any;
        /**
         * Extracts the extension (sans '.') of the file being loaded by the resource.
         *
         * @private
         * @return {string} The extension.
         */
        _getExtension(): string;
        /**
         * Determines the mime type of an XHR request based on the responseType of
         * resource being loaded.
         *
         * @private
         * @param {Resource.XHR_RESPONSE_TYPE} type - The type to get a mime type for.
         * @return {string} The mime type to use.
         */
        _getMimeFromXhrType(type: any): "application/xml" | "application/octet-binary" | "application/blob" | "application/json" | "text/plain";
        /**
         * The types of resources a resource could represent.
         *
         * @static
         * @readonly
         * @enum {number}
         */
        static STATUS_FLAGS: {
            NONE: number;
            DATA_URL: number;
            COMPLETE: number;
            LOADING: number;
        };
        /**
         * The types of resources a resource could represent.
         *
         * @static
         * @readonly
         * @enum {number}
         */
        static TYPE: {
            UNKNOWN: number;
            JSON: number;
            XML: number;
            IMAGE: number;
            AUDIO: number;
            VIDEO: number;
            TEXT: number;
        };
        /**
         * The types of loading a resource can use.
         *
         * @static
         * @readonly
         * @enum {number}
         */
        static LOAD_TYPE: {
            /** Uses XMLHttpRequest to load the resource. */
            XHR: number;
            /** Uses an `Image` object to load the resource. */
            IMAGE: number;
            /** Uses an `Audio` object to load the resource. */
            AUDIO: number;
            /** Uses a `Video` object to load the resource. */
            VIDEO: number;
        };
        /**
         * The XHR ready states, used internally.
         *
         * @static
         * @readonly
         * @enum {string}
         */
        static XHR_RESPONSE_TYPE: {
            /** string */
            DEFAULT: string;
            /** ArrayBuffer */
            BUFFER: string;
            /** Blob */
            BLOB: string;
            /** Document */
            DOCUMENT: string;
            /** Object */
            JSON: string;
            /** String */
            TEXT: string;
        };
        static _loadTypeMap: {
            gif: number;
            png: number;
            bmp: number;
            jpg: number;
            jpeg: number;
            tif: number;
            tiff: number;
            webp: number;
            tga: number;
            svg: number;
            'svg+xml': number;
            mp3: number;
            ogg: number;
            wav: number;
            mp4: number;
            webm: number;
        };
        static _xhrTypeMap: {
            xhtml: string;
            html: string;
            htm: string;
            xml: string;
            tmx: string;
            svg: string;
            tsx: string;
            gif: string;
            png: string;
            bmp: string;
            jpg: string;
            jpeg: string;
            tif: string;
            tiff: string;
            webp: string;
            tga: string;
            json: string;
            text: string;
            txt: string;
            ttf: string;
            otf: string;
        };
        static EMPTY_GIF: string;
    }
    /**
     * @memberof Resource
     * @typedef {object} IMetadata
     * @property {HTMLImageElement|HTMLAudioElement|HTMLVideoElement} [loadElement=null] - The
     *      element to use for loading, instead of creating one.
     * @property {boolean} [skipSource=false] - Skips adding source(s) to the load element. This
     *      is useful if you want to pass in a `loadElement` that you already added load sources to.
     * @property {string|string[]} [mimeType] - The mime type to use for the source element
     *      of a video/audio elment. If the urls are an array, you can pass this as an array as well
     *      where each index is the mime type to use for the corresponding url index.
     */
    interface IMetadata {
        loadElement?: HTMLImageElement | HTMLAudioElement | HTMLVideoElement;
        skipSource?: boolean;
        mimeType?: string | string[];
    }
}
declare namespace feng3d.pixi {
    /**
     * Application plugin for supporting loader option. Installing the LoaderPlugin
     * is not necessary if using **pixi.js** or **pixi.js-legacy**.
     * @example
     * import {AppLoaderPlugin} from '@pixi/loaders';
     * import {Application} from '@pixi/app';
     * Application.registerPlugin(AppLoaderPlugin);
     */
    class AppLoaderPlugin {
        static loader: Loader;
        /**
         * Called on application constructor
         * @param {object} options
         * @private
         */
        static init(options?: IApplicationOptions): void;
        /**
         * Called when application destroyed
         *
         * @private
         */
        static destroy(): void;
    }
}
declare namespace feng3d.pixi {
    const Resource: typeof resource_loader.Resource;
    type Resource = resource_loader.Resource;
    export interface IResourceMetadata extends resource_loader.IMetadata, IBaseTextureOptions {
        imageMetadata?: any;
    }
    export interface ILoaderResource extends Resource {
        texture?: Texture;
        spritesheet?: Spritesheet;
        textures?: Dict<Texture>;
        metadata: IResourceMetadata;
    }
    export type TLoaderResource = {
        new (...args: any[]): ILoaderResource;
    } & typeof Resource;
    /**
    * Reference to **{@link https://github.com/englercj/resource-loader}**'s Resource class.
    * @see https://englercj.github.io/resource-loader/classes/resource.html
    * @class LoaderResource
    * @memberof PIXI
    */
    export const LoaderResource: TLoaderResource;
    export {};
}
declare namespace feng3d.pixi {
    /**
     * Loader plugin for handling Texture resources.
     * @implements PIXI.ILoaderPlugin
     */
    class TextureLoader {
        /**
         * Handle SVG elements a text, render with SVGResource.
         */
        static add(): void;
        /**
         * Called after a resource is loaded.
         * @see PIXI.Loader.loaderMiddleware
         * @param {PIXI.LoaderResource} resource
         * @param {function} next
         */
        static use(resource: ILoaderResource, next: (...args: any[]) => void): void;
    }
}
declare namespace feng3d.pixi {
    const ResourceLoader: typeof resource_loader.Loader;
    type Resource = resource_loader.Resource;
    export interface Application {
        loader: Loader;
    }
    export interface IApplicationOptions {
        sharedLoader?: boolean;
    }
    /**
     * The new loader, extends Resource Loader by Chad Engler: https://github.com/englercj/resource-loader
     *
     * ```js
     * const loader = PIXI.Loader.shared; // PixiJS exposes a premade instance for you to use.
     * //or
     * const loader = new PIXI.Loader(); // you can also create your own if you want
     *
     * const sprites = {};
     *
     * // Chainable `add` to enqueue a resource
     * loader.add('bunny', 'data/bunny.png')
     *       .add('spaceship', 'assets/spritesheet.json');
     * loader.add('scoreFont', 'assets/score.fnt');
     *
     * // Chainable `pre` to add a middleware that runs for each resource, *before* loading that resource.
     * // This is useful to implement custom caching modules (using filesystem, indexeddb, memory, etc).
     * loader.pre(cachingMiddleware);
     *
     * // Chainable `use` to add a middleware that runs for each resource, *after* loading that resource.
     * // This is useful to implement custom parsing modules (like spritesheet parsers, spine parser, etc).
     * loader.use(parsingMiddleware);
     *
     * // The `load` method loads the queue of resources, and calls the passed in callback called once all
     * // resources have loaded.
     * loader.load((loader, resources) => {
     *     // resources is an object where the key is the name of the resource loaded and the value is the resource object.
     *     // They have a couple default properties:
     *     // - `url`: The URL that the resource was loaded from
     *     // - `error`: The error that happened when trying to load (if any)
     *     // - `data`: The raw data that was loaded
     *     // also may contain other properties based on the middleware that runs.
     *     sprites.bunny = new PIXI.TilingSprite(resources.bunny.texture);
     *     sprites.spaceship = new PIXI.TilingSprite(resources.spaceship.texture);
     *     sprites.scoreFont = new PIXI.TilingSprite(resources.scoreFont.texture);
     * });
     *
     * // throughout the process multiple signals can be dispatched.
     * loader.onProgress.add(() => {}); // called once per loaded/errored file
     * loader.onError.add(() => {}); // called once per errored file
     * loader.onLoad.add(() => {}); // called once per loaded file
     * loader.onComplete.add(() => {}); // called once when the queued resources all load.
     * ```
     *
     * @see https://github.com/englercj/resource-loader
     * Loader
     */
    export class Loader extends ResourceLoader {
        /**
         * Collection of all installed `use` middleware for Loader.
         *
         * @static
         * @member {Array<PIXI.ILoaderPlugin>} _plugins
         * @memberof PIXI.Loader
         * @private
         */
        private static _plugins;
        private static _shared;
        private _protected;
        /**
         * @param {string} [baseUrl=''] - The base url for all resources loaded by this loader.
         * @param {number} [concurrency=10] - The number of resources to load concurrently.
         */
        constructor(baseUrl?: string, concurrency?: number);
        /**
         * Destroy the loader, removes references.
         * @memberof PIXI.Loader#
         * @method destroy
         * @public
         */
        destroy(): void;
        /**
         * A premade instance of the loader that can be used to load resources.
         * @name shared
         * @type {PIXI.Loader}
         * @static
         * @memberof PIXI.Loader
         */
        static get shared(): Loader;
        /**
         * Adds a Loader plugin for the global shared loader and all
         * new Loader instances created.
         *
         * @static
         * @method registerPlugin
         * @memberof PIXI.Loader
         * @param {PIXI.ILoaderPlugin} plugin - The plugin to add
         * @return {PIXI.Loader} Reference to PIXI.Loader for chaining
         */
        static registerPlugin(plugin: ILoaderPlugin): typeof Loader;
    }
    export interface ILoaderPlugin {
        add?(): void;
        pre?(resource: Resource, next?: (...args: any[]) => void): void;
        use?(resource: Resource, next?: (...args: any[]) => void): void;
    }
    export {};
}
declare namespace feng3d.pixi {
    type SpriteSource = TextureSource | Texture;
    /**
     * The Sprite object is the base for all textured objects that are rendered to the screen
     *
     * A sprite can be created directly from an image like this:
     *
     * ```js
     * let sprite = PIXI.Sprite.from('assets/image.png');
     * ```
     *
     * The more efficient way to create sprites is using a {@link PIXI.Spritesheet},
     * as swapping base textures when rendering to the screen is inefficient.
     *
     * ```js
     * PIXI.Loader.shared.add("assets/spritesheet.json").load(setup);
     *
     * function setup() {
     *   let sheet = PIXI.Loader.shared.resources["assets/spritesheet.json"].spritesheet;
     *   let sprite = new PIXI.Sprite(sheet.textures["image.png"]);
     *   ...
     * }
     * ```
     *
     */
    class Sprite extends Container implements IBatchableElement {
        static create(name?: string): Sprite;
        blendMode: BLEND_MODES;
        indices: Uint16Array;
        pluginName: string;
        _texture: Texture;
        _textureID: number;
        _cachedTint: number;
        protected _textureTrimmedID: number;
        uvs: Float32Array;
        /**
         * The anchor sets the origin point of the sprite. The default value is taken from the {@link PIXI.Texture|Texture}
         * and passed to the constructor.
         *
         * The default is `(0,0)`, this means the sprite's origin is the top left.
         *
         * Setting the anchor to `(0.5,0.5)` means the sprite's origin is centered.
         *
         * Setting the anchor to `(1,1)` would mean the sprite's origin point will be the bottom right corner.
         *
         * If you pass only single parameter, it will set both x and y to the same value as shown in the example below.
         *
         * @example
         * const sprite = new PIXI.Sprite(texture);
         * sprite.anchorX = 0.5; // This will set the origin to center.
         * sprite.anchorY = 0.5; // This will set the origin to center.
         */
        get anchorX(): number;
        set anchorX(v: number);
        protected _anchorX: number;
        get anchorY(): number;
        set anchorY(v: number);
        protected _anchorY: number;
        vertexData: Float32Array;
        private vertexTrimmedData;
        private _roundPixels;
        private _transformID;
        private _transformTrimmedID;
        private _tint;
        _tintRGB: number;
        /**
         * @param {PIXI.Texture} [texture] - The texture for this sprite.
         */
        constructor(texture?: Texture);
        /**
         * When the texture is updated, this event will fire to update the scale and frame
         *
         * @protected
         */
        protected _onTextureUpdate(): void;
        /**
         * Called when the anchor position updates.
         *
         * @private
         */
        private _onAnchorUpdate;
        /**
         * calculates worldTransform * vertices, store it in vertexData
         */
        calculateVertices(): void;
        /**
         * calculates worldTransform * vertices for a non texture with a trim. store it in vertexTrimmedData
         * This is used to ensure that the true width and height of a trimmed texture is respected
         */
        calculateTrimmedVertices(): void;
        /**
         *
         * Renders the object using the WebGL renderer
         *
         * @param {PIXI.Renderer} renderer - The webgl renderer to use.
         */
        _render(renderer: Renderer): void;
        /**
         * Updates the bounds of the sprite.
         *
         * @protected
         */
        _calculateBounds(): void;
        /**
         * Gets the local bounds of the sprite object.
         *
         * @param {PIXI.Rectangle} [rect] - Optional output rectangle.
         * @return {PIXI.Rectangle} The bounds.
         */
        getLocalBounds(rect?: Rectangle): Rectangle;
        /**
         * Tests if a point is inside this sprite
         *
         * @param {PIXI.IPointData} point - the point to test
         * @return {boolean} the result of the test
         */
        containsPoint(point: Vector2): boolean;
        /**
         * Destroys this sprite and optionally its texture and children
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
         *  have been set to that value
         * @param {boolean} [options.children=false] - if set to true, all the children will have their destroy
         *      method called as well. 'options' will be passed on to those calls.
         * @param {boolean} [options.texture=false] - Should it destroy the current texture of the sprite as well
         * @param {boolean} [options.baseTexture=false] - Should it destroy the base texture of the sprite as well
         */
        destroy(options?: IDestroyOptions | boolean): void;
        /**
         * Helper function that creates a new sprite based on the source you provide.
         * The source can be - frame id, image url, video url, canvas element, video element, base texture
         *
         * @static
         * @param {string|PIXI.Texture|HTMLCanvasElement|HTMLVideoElement} source - Source to create texture from
         * @param {object} [options] - See {@link PIXI.BaseTexture}'s constructor for options.
         * @return {PIXI.Sprite} The newly created sprite
         */
        static from(source: SpriteSource, options?: IBaseTextureOptions): Sprite;
        /**
         * If true PixiJS will Math.floor() x/y values when rendering, stopping pixel interpolation.
         * Advantages can include sharper image quality (like text) and faster rendering on canvas.
         * The main disadvantage is movement of objects may appear less smooth.
         * To set the global default, change {@link PIXI.settings.ROUND_PIXELS}
         *
         * @member {boolean}
         * @default false
         */
        set roundPixels(value: boolean);
        get roundPixels(): boolean;
        /**
         * The width of the sprite, setting this will actually modify the scale to achieve the value set
         *
         * @member {number}
         */
        get width(): number;
        set width(value: number);
        /**
         * The height of the sprite, setting this will actually modify the scale to achieve the value set
         *
         * @member {number}
         */
        get height(): number;
        set height(value: number);
        /**
         * The tint applied to the sprite. This is a hex value.
         * A value of 0xFFFFFF will remove any tint effect.
         *
         * @member {number}
         * @default 0xFFFFFF
         */
        get tint(): number;
        set tint(value: number);
        /**
         * The texture that the sprite is using
         *
         * @member {PIXI.Texture}
         */
        get texture(): Texture;
        set texture(value: Texture);
    }
}
declare namespace feng3d.pixi {
    /**
     * Represents the JSON data for a spritesheet atlas.
     */
    interface ISpritesheetFrameData {
        frame: {
            x: number;
            y: number;
            w: number;
            h: number;
        };
        trimmed?: boolean;
        rotated?: boolean;
        sourceSize?: {
            w: number;
            h: number;
        };
        spriteSourceSize?: {
            x: number;
            y: number;
        };
        anchor?: Vector2;
    }
    /**
     * Atlas format.
     */
    interface ISpritesheetData {
        frames: Dict<ISpritesheetFrameData>;
        animations?: Dict<string[]>;
        meta: {
            scale: string;
        };
    }
    /**
     * Utility class for maintaining reference to a collection
     * of Textures on a single Spritesheet.
     *
     * To access a sprite sheet from your code pass its JSON data file to Pixi's loader:
     *
     * ```js
     * PIXI.Loader.shared.add("images/spritesheet.json").load(setup);
     *
     * function setup() {
     *   let sheet = PIXI.Loader.shared.resources["images/spritesheet.json"].spritesheet;
     *   ...
     * }
     * ```
     * With the `sheet.textures` you can create Sprite objects,`sheet.animations` can be used to create an AnimatedSprite.
     *
     * Sprite sheets can be packed using tools like {@link https://codeandweb.com/texturepacker|TexturePacker},
     * {@link https://renderhjs.net/shoebox/|Shoebox} or {@link https://github.com/krzysztof-o/spritesheet.js|Spritesheet.js}.
     * Default anchor points (see {@link PIXI.Texture#defaultAnchor}) and grouping of animation sprites are currently only
     * supported by TexturePacker.
     *
     */
    class Spritesheet {
        /**
         * The maximum number of Textures to build per process.
         *
         * @type {number}
         * @default 1000
         */
        static readonly BATCH_SIZE = 1000;
        baseTexture: BaseTexture;
        textures: Dict<Texture>;
        animations: Dict<Texture[]>;
        data: ISpritesheetData;
        resolution: number;
        private _texture;
        private _frames;
        private _frameKeys;
        private _batchIndex;
        private _callback;
        /**
         * @param {PIXI.BaseTexture|PIXI.Texture} baseTexture - Reference to the source BaseTexture object.
         * @param {Object} data - Spritesheet image data.
         * @param {string} [resolutionFilename] - The filename to consider when determining
         *        the resolution of the spritesheet. If not provided, the imageUrl will
         *        be used on the BaseTexture.
         */
        constructor(texture: BaseTexture | Texture, data: ISpritesheetData, resolutionFilename?: string);
        /**
         * Generate the resolution from the filename or fallback
         * to the meta.scale field of the JSON data.
         *
         * @private
         * @param {string} resolutionFilename - The filename to use for resolving
         *        the default resolution.
         * @return {number} Resolution to use for spritesheet.
         */
        private _updateResolution;
        /**
         * Parser spritesheet from loaded data. This is done asynchronously
         * to prevent creating too many Texture within a single process.
         *
         * @param {Function} callback - Callback when complete returns
         *        a map of the Textures for this spritesheet.
         */
        parse(callback: () => void): void;
        /**
         * Process a batch of frames
         *
         * @private
         * @param {number} initialFrameIndex - The index of frame to start.
         */
        private _processFrames;
        /**
         * Parse animations config
         *
         * @private
         */
        private _processAnimations;
        /**
         * The parse has completed.
         *
         * @private
         */
        private _parseComplete;
        /**
         * Begin the next batch of textures.
         *
         * @private
         */
        private _nextBatch;
        /**
         * Destroy Spritesheet and don't use after this.
         *
         * @param {boolean} [destroyBase=false] - Whether to destroy the base texture as well
         */
        destroy(destroyBase?: boolean): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * {@link PIXI.Loader} middleware for loading texture atlases that have been created with
     * TexturePacker or similar JSON-based spritesheet.
     *
     * This middleware automatically generates Texture resources.
     *
     * If you're using Webpack or other bundlers and plan on bundling the atlas' JSON,
     * use the {@link PIXI.Spritesheet} class to directly parse the JSON.
     *
     * The Loader's image Resource name is automatically appended with `"_image"`.
     * If a Resource with this name is already loaded, the Loader will skip parsing the
     * Spritesheet. The code below will generate an internal Loader Resource called `"myatlas_image"`.
     *
     * @example
     * loader.add('myatlas', 'path/to/myatlas.json');
     * loader.load(() => {
     *   loader.resources.myatlas; // atlas JSON resource
     *   loader.resources.myatlas_image; // atlas Image resource
     * });
     *
     * @implements PIXI.ILoaderPlugin
     */
    class SpritesheetLoader {
        /**
         * Called after a resource is loaded.
         * @see PIXI.Loader.loaderMiddleware
         * @param {PIXI.LoaderResource} resource
         * @param {function} next
         */
        static use(resource: ILoaderResource, next: (...args: unknown[]) => void): void;
        /**
         * Get the spritesheets root path
         * @param {PIXI.LoaderResource} resource - Resource to check path
         * @param {string} baseUrl - Base root url
         */
        static getResourcePath(resource: ILoaderResource, baseUrl: string): string;
    }
}
declare namespace feng3d.pixi {
    const tilingSprite_simple_frag = "\nvarying vec2 vTextureCoord;\n\nuniform sampler2D uSampler;\nuniform vec4 uColor;\n\nvoid main(void)\n{\n    vec4 sample = texture2D(uSampler, vTextureCoord);\n    gl_FragColor = sample * uColor;\n}\n";
}
declare namespace feng3d.pixi {
    const tilingSprite_frag = "\nvarying vec2 vTextureCoord;\n\nuniform sampler2D uSampler;\nuniform vec4 uColor;\nuniform mat3 uMapCoord;\nuniform vec4 uClampFrame;\nuniform vec2 uClampOffset;\n\nvoid main(void)\n{\n    vec2 coord = vTextureCoord + ceil(uClampOffset - vTextureCoord);\n    coord = (uMapCoord * vec3(coord, 1.0)).xy;\n    coord = clamp(coord, uClampFrame.xy, uClampFrame.zw);\n\n    vec4 texSample = texture2D(uSampler, coord);\n    gl_FragColor = texSample * uColor;\n}\n";
}
declare namespace feng3d.pixi {
    /**
     * A tiling sprite is a fast way of rendering a tiling image.
     *
     */
    class TilingSprite extends Sprite {
        tileTransform: Transform;
        uvMatrix: TextureMatrix;
        uvRespectAnchor: boolean;
        /**
         * @param {PIXI.Texture} texture - the texture of the tiling sprite
         * @param {number} [width=100] - the width of the tiling sprite
         * @param {number} [height=100] - the height of the tiling sprite
         */
        constructor(texture?: Texture, width?: number, height?: number);
        /**
         * Changes frame clamping in corresponding textureTransform, shortcut
         * Change to -0.5 to add a pixel to the edge, recommended for transparent trimmed textures in atlas
         *
         * @default 0.5
         * @member {number}
         */
        get clampMargin(): number;
        set clampMargin(value: number);
        /**
         * The scaling of the image that is being tiled
         */
        get tileScaleX(): number;
        set tileScaleX(value: number);
        get tileScaleY(): number;
        set tileScaleY(value: number);
        /**
         * The offset of the image that is being tiled
         */
        get tileX(): number;
        set tileX(value: number);
        get tileY(): number;
        set tileY(value: number);
        /**
         * @protected
         */
        protected _onTextureUpdate(): void;
        /**
         * Renders the object using the WebGL renderer
         *
         * @param {PIXI.Renderer} renderer - The renderer
         */
        _render(renderer: Renderer): void;
        /**
         * Updates the bounds of the tiling sprite.
         *
         * @protected
         */
        _calculateBounds(): void;
        /**
         * Gets the local bounds of the sprite object.
         *
         * @param {PIXI.Rectangle} [rect] - Optional output rectangle.
         * @return {PIXI.Rectangle} The bounds.
         */
        getLocalBounds(rect?: Rectangle): Rectangle;
        /**
         * Checks if a point is inside this tiling sprite.
         *
         * @param {PIXI.IPointData} point - the point to check
         * @return {boolean} Whether or not the sprite contains the point.
         */
        containsPoint(point: Vector2): boolean;
        /**
         * Destroys this sprite and optionally its texture and children
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
         *  have been set to that value
         * @param {boolean} [options.children=false] - if set to true, all the children will have their destroy
         *      method called as well. 'options' will be passed on to those calls.
         * @param {boolean} [options.texture=false] - Should it destroy the current texture of the sprite as well
         * @param {boolean} [options.baseTexture=false] - Should it destroy the base texture of the sprite as well
         */
        destroy(options?: IDestroyOptions | boolean): void;
        /**
         * Helper function that creates a new tiling sprite based on the source you provide.
         * The source can be - frame id, image url, video url, canvas element, video element, base texture
         *
         * @static
         * @param {string|PIXI.Texture|HTMLCanvasElement|HTMLVideoElement} source - Source to create texture from
         * @param {Object} options - See {@link PIXI.BaseTexture}'s constructor for options.
         * @param {number} options.width - required width of the tiling sprite
         * @param {number} options.height - required height of the tiling sprite
         * @return {PIXI.TilingSprite} The newly created texture
         */
        static from(source: TextureSource, options: ISize & IBaseTextureOptions): TilingSprite;
        /**
         * The width of the sprite, setting this will actually modify the scale to achieve the value set
         *
         * @member {number}
         */
        get width(): number;
        set width(value: number);
        /**
         * The height of the TilingSprite, setting this will actually modify the scale to achieve the value set
         *
         * @member {number}
         */
        get height(): number;
        set height(value: number);
    }
}
declare namespace feng3d.pixi {
    const tilingSprite_vert = "\nattribute vec2 aVertexPosition;\nattribute vec2 aTextureCoord;\n\nuniform mat3 projectionMatrix;\nuniform mat3 translationMatrix;\nuniform mat3 uTransform;\n\nvarying vec2 vTextureCoord;\n\nvoid main(void)\n{\n    gl_Position = vec4((projectionMatrix * translationMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);\n\n    vTextureCoord = (uTransform * vec3(aTextureCoord, 1.0)).xy;\n}\n";
}
declare namespace feng3d.pixi {
    /**
     * WebGL renderer plugin for tiling sprites
     *
     */
    class TilingSpriteRenderer extends ObjectRenderer {
        shader: Shader;
        simpleShader: Shader;
        quad: QuadUv;
        readonly state: State;
        /**
         * constructor for renderer
         *
         * @param {PIXI.Renderer} renderer - The renderer this tiling awesomeness works for.
         */
        constructor(renderer: Renderer);
        /**
         *
         * @param {PIXI.TilingSprite} ts - tilingSprite to be rendered
         */
        render(ts: TilingSprite): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * An AnimatedSprite is a simple way to display an animation depicted by a list of textures.
     *
     * ```js
     * let alienImages = ["image_sequence_01.png","image_sequence_02.png","image_sequence_03.png","image_sequence_04.png"];
     * let textureArray = [];
     *
     * for (let i=0; i < 4; i++)
     * {
     *      let texture = PIXI.Texture.from(alienImages[i]);
     *      textureArray.push(texture);
     * };
     *
     * let animatedSprite = new PIXI.AnimatedSprite(textureArray);
     * ```
     *
     * The more efficient and simpler way to create an animated sprite is using a {@link PIXI.Spritesheet}
     * containing the animation definitions:
     *
     * ```js
     * PIXI.Loader.shared.add("assets/spritesheet.json").load(setup);
     *
     * function setup() {
     *   let sheet = PIXI.Loader.shared.resources["assets/spritesheet.json"].spritesheet;
     *   animatedSprite = new PIXI.AnimatedSprite(sheet.animations["image_sequence"]);
     *   ...
     * }
     * ```
     *
     */
    class AnimatedSprite extends Sprite {
        animationSpeed: number;
        loop: boolean;
        updateAnchor: boolean;
        onComplete?: () => void;
        onFrameChange?: (currentFrame: number) => void;
        onLoop?: () => void;
        private _playing;
        private _textures;
        private _durations;
        private _autoUpdate;
        private _isConnectedToTicker;
        private _currentTime;
        private _previousFrame;
        /**
         * @param {PIXI.Texture[]|PIXI.AnimatedSprite.FrameObject[]} textures - An array of {@link PIXI.Texture} or frame
         *  objects that make up the animation.
         * @param {boolean} [autoUpdate=true] - Whether to use PIXI.Ticker.shared to auto update animation time.
         */
        constructor(textures: Texture[] | FrameObject[], autoUpdate?: boolean);
        /**
         * Stops the AnimatedSprite.
         *
         */
        stop(): void;
        /**
         * Plays the AnimatedSprite.
         *
         */
        play(): void;
        /**
         * Stops the AnimatedSprite and goes to a specific frame.
         *
         * @param {number} frameNumber - Frame index to stop at.
         */
        gotoAndStop(frameNumber: number): void;
        /**
         * Goes to a specific frame and begins playing the AnimatedSprite.
         *
         * @param {number} frameNumber - Frame index to start at.
         */
        gotoAndPlay(frameNumber: number): void;
        /**
         * Updates the object transform for rendering.
         *
         * @param {number} deltaTime - Time since last tick.
         */
        update(deltaTime: number): void;
        /**
         * Updates the displayed texture to match the current frame index.
         *
         * @private
         */
        private updateTexture;
        /**
         * Stops the AnimatedSprite and destroys it.
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
         *  have been set to that value.
         * @param {boolean} [options.children=false] - If set to true, all the children will have their destroy
         *      method called as well. 'options' will be passed on to those calls.
         * @param {boolean} [options.texture=false] - Should it destroy the current texture of the sprite as well.
         * @param {boolean} [options.baseTexture=false] - Should it destroy the base texture of the sprite as well.
         */
        destroy(options?: IDestroyOptions | boolean): void;
        /**
         * A short hand way of creating an AnimatedSprite from an array of frame ids.
         *
         * @static
         * @param {string[]} frames - The array of frames ids the AnimatedSprite will use as its texture frames.
         * @return {PIXI.AnimatedSprite} The new animated sprite with the specified frames.
         */
        static fromFrames(frames: string[]): AnimatedSprite;
        /**
         * A short hand way of creating an AnimatedSprite from an array of image ids.
         *
         * @static
         * @param {string[]} images - The array of image urls the AnimatedSprite will use as its texture frames.
         * @return {PIXI.AnimatedSprite} The new animate sprite with the specified images as frames.
         */
        static fromImages(images: string[]): AnimatedSprite;
        /**
         * The total number of frames in the AnimatedSprite. This is the same as number of textures
         * assigned to the AnimatedSprite.
         *
         * @readonly
         * @member {number}
         * @default 0
         */
        get totalFrames(): number;
        /**
         * The array of textures used for this AnimatedSprite.
         *
         * @member {PIXI.Texture[]}
         */
        get textures(): Texture[] | FrameObject[];
        set textures(value: Texture[] | FrameObject[]);
        /**
        * The AnimatedSprites current frame index.
        *
        * @member {number}
        * @readonly
        */
        get currentFrame(): number;
        /**
         * Indicates if the AnimatedSprite is currently playing.
         *
         * @member {boolean}
         * @readonly
         */
        get playing(): boolean;
        /**
         * Whether to use PIXI.Ticker.shared to auto update animation time
         *
         * @member {boolean}
         */
        get autoUpdate(): boolean;
        set autoUpdate(value: boolean);
    }
    interface FrameObject {
        texture: Texture;
        time: number;
    }
    /**.AnimatedSprite
     * @typedef {object} FrameObject
     * @type {object}
     * @property {PIXI.Texture} texture - The {@link PIXI.Texture} of the frame
     * @property {number} time - the duration of the frame in ms
     */
}
declare namespace feng3d.pixi {
    interface Container extends Partial<pixi.IAccessibleTarget> {
    }
    type PointerEvents = 'auto' | 'none' | 'visiblePainted' | 'visibleFill' | 'visibleStroke' | 'visible' | 'painted' | 'fill' | 'stroke' | 'all' | 'inherit';
    interface IAccessibleTarget {
        accessible: boolean;
        accessibleTitle: string;
        accessibleHint: string;
        tabIndex: number;
        _accessibleActive: boolean;
        _accessibleDiv: IAccessibleHTMLElement;
        accessibleType: string;
        accessiblePointerEvents: PointerEvents;
        accessibleChildren: true;
        renderId: number;
    }
    interface IAccessibleHTMLElement extends HTMLElement {
        type?: string;
        displayObject?: Container;
    }
    /**
     * Default property values of accessible objects
     * used by {@link PIXI.AccessibilityManager}.
     *
     * @private
     * @function accessibleTarget
     * @type {Object}
     * @example
     *      function MyObject() {}
     *
     *      Object.assign(
     *          MyObject.prototype,
     *          PIXI.accessibleTarget
     *      );
     */
    const accessibleTarget: IAccessibleTarget;
}
declare namespace feng3d.pixi {
    /**
     * The Accessibility manager recreates the ability to tab and have content read by screen readers.
     * This is very important as it can possibly help people with disabilities access PixiJS content.
     *
     * A Node2D can be made accessible just like it can be made interactive. This manager will map the
     * events as if the mouse was being used, minimizing the effort required to implement.
     *
     * An instance of this class is automatically created by default, and can be found at `renderer.plugins.accessibility`
     *
     */
    class AccessibilityManager {
        /** Setting this to true will visually show the divs. */
        debug: boolean;
        /**
         * The renderer this accessibility manager works for.
         *
         * @type {PIXI.CanvasRenderer|PIXI.Renderer}
         */
        renderer: Renderer;
        /** Internal variable, see isActive getter. */
        private _isActive;
        /** Internal variable, see isMobileAccessibility getter. */
        private _isMobileAccessibility;
        /** Button element for handling touch hooks. */
        private _hookDiv;
        /** This is the dom element that will sit over the PixiJS element. This is where the div overlays will go. */
        private div;
        /** A simple pool for storing divs. */
        private pool;
        /** This is a tick used to check if an object is no longer being rendered. */
        private renderId;
        /** The array of currently active accessible items. */
        private children;
        /** Count to throttle div updates on android devices. */
        private androidUpdateCount;
        /**  The frequency to update the div elements. */
        private androidUpdateFrequency;
        /**
         * @param {PIXI.CanvasRenderer|PIXI.Renderer} renderer - A reference to the current renderer
         */
        constructor(renderer: Renderer);
        /**
         * Value of `true` if accessibility is currently active and accessibility layers are showing.
         * @member {boolean}
         * @readonly
         */
        get isActive(): boolean;
        /**
         * Value of `true` if accessibility is enabled for touch devices.
         * @member {boolean}
         * @readonly
         */
        get isMobileAccessibility(): boolean;
        /**
         * Creates the touch hooks.
         *
         * @private
         */
        private createTouchHook;
        /**
         * Destroys the touch hooks.
         *
         * @private
         */
        private destroyTouchHook;
        /**
         * Activating will cause the Accessibility layer to be shown.
         * This is called when a user presses the tab key.
         *
         * @private
         */
        private activate;
        /**
         * Deactivating will cause the Accessibility layer to be hidden.
         * This is called when a user moves the mouse.
         *
         * @private
         */
        private deactivate;
        /**
         * This recursive function will run through the scene graph and add any new accessible objects to the DOM layer.
         *
         * @private
         * @param {PIXI.Container} displayObject - The Node2D to check.
         */
        private updateAccessibleObjects;
        /**
         * Before each render this function will ensure that all divs are mapped correctly to their DisplayObjects.
         *
         * @private
         */
        private update;
        /**
         * private function that will visually add the information to the
         * accessability div
         *
         * @param {HTMLElement} div
         */
        updateDebugHTML(div: IAccessibleHTMLElement): void;
        /**
         * Adjust the hit area based on the bounds of a display object
         *
         * @param {PIXI.Rectangle} hitArea - Bounds of the child
         */
        capHitArea(hitArea: Rectangle): void;
        /**
         * Adds a Node2D to the accessibility manager
         *
         * @private
         * @param {feng3d.Node2D} displayObject - The child to make accessible.
         */
        private addChild;
        /**
         * Maps the div button press to pixi's InteractionManager (click)
         *
         * @private
         * @param {MouseEvent} e - The click event.
         */
        private _onClick;
        /**
         * Maps the div focus events to pixi's InteractionManager (mouseover)
         *
         * @private
         * @param {FocusEvent} e - The focus event.
         */
        private _onFocus;
        /**
         * Maps the div focus events to pixi's InteractionManager (mouseout)
         *
         * @private
         * @param {FocusEvent} e - The focusout event.
         */
        private _onFocusOut;
        /**
         * Is called when a key is pressed
         *
         * @private
         * @param {KeyboardEvent} e - The keydown event.
         */
        private _onKeyDown;
        /**
         * Is called when the mouse moves across the renderer element
         *
         * @private
         * @param {MouseEvent} e - The mouse event.
         */
        private _onMouseMove;
        /**
         * Destroys the accessibility manager
         *
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * This class provides renderer-specific plugins for exporting content from a renderer.
     * For instance, these plugins can be used for saving an Image, Canvas element or for exporting the raw image data (pixels).
     *
     * Do not instantiate these plugins directly. It is available from the `renderer.plugins` property.
     * See {@link PIXI.CanvasRenderer#plugins} or {@link PIXI.Renderer#plugins}.
     * @example
     * // Create a new app (will auto-add extract plugin to renderer)
     * const app = new PIXI.Application();
     *
     * // Draw a red circle
     * const graphics = new PIXI.Graphics()
     *     .beginFill(0xFF0000)
     *     .drawCircle(0, 0, 50);
     *
     * // Render the graphics as an HTMLImageElement
     * const image = app.renderer.plugins.extract.image(graphics);
     * document.body.appendChild(image);
     */
    class Extract implements IRendererPlugin {
        private renderer;
        /**
         * @param {PIXI.Renderer} renderer - A reference to the current renderer
         */
        constructor(renderer: Renderer);
        /**
         * Will return a HTML Image of the target
         *
         * @param {feng3d.Node2D|PIXI.RenderTexture} target - A displayObject or renderTexture
         *  to convert. If left empty will use the main renderer
         * @param {string} [format] - Image format, e.g. "image/jpeg" or "image/webp".
         * @param {number} [quality] - JPEG or Webp compression from 0 to 1. Default is 0.92.
         * @return {HTMLImageElement} HTML Image of the target
         */
        image(target: Container | RenderTexture, format?: string, quality?: number): HTMLImageElement;
        /**
         * Will return a a base64 encoded string of this target. It works by calling
         *  `Extract.getCanvas` and then running toDataURL on that.
         *
         * @param {feng3d.Node2D|PIXI.RenderTexture} target - A displayObject or renderTexture
         *  to convert. If left empty will use the main renderer
         * @param {string} [format] - Image format, e.g. "image/jpeg" or "image/webp".
         * @param {number} [quality] - JPEG or Webp compression from 0 to 1. Default is 0.92.
         * @return {string} A base64 encoded string of the texture.
         */
        base64(target: Container | RenderTexture, format?: string, quality?: number): string;
        /**
         * Creates a Canvas element, renders this target to it and then returns it.
         *
         * @param {feng3d.Node2D|PIXI.RenderTexture} target - A displayObject or renderTexture
         *  to convert. If left empty will use the main renderer
         * @return {HTMLCanvasElement} A Canvas element with the texture rendered on.
         */
        canvas(target: Container | RenderTexture): HTMLCanvasElement;
        /**
         * Will return a one-dimensional array containing the pixel data of the entire texture in RGBA
         * order, with integer values between 0 and 255 (included).
         *
         * @param {feng3d.Node2D|PIXI.RenderTexture} target - A displayObject or renderTexture
         *  to convert. If left empty will use the main renderer
         * @return {Uint8Array} One-dimensional array containing the pixel data of the entire texture
         */
        pixels(target: Container | RenderTexture): Uint8Array;
        /**
         * Destroys the extract
         *
         */
        destroy(): void;
        /**
         * Takes premultiplied pixel data and produces regular pixel data
         *
         * @private
         * @param {number[] | Uint8Array | Uint8ClampedArray} pixels - array of pixel data
         * @param {number[] | Uint8Array | Uint8ClampedArray} out - output array
         */
        static arrayPostDivide(pixels: number[] | Uint8Array | Uint8ClampedArray, out: number[] | Uint8Array | Uint8ClampedArray): void;
    }
}
declare namespace feng3d.pixi {
    const particles_frag = "\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\n\nuniform sampler2D uSampler;\n\nvoid main(void){\n    vec4 color = texture2D(uSampler, vTextureCoord) * vColor;\n    gl_FragColor = color;\n}";
}
declare namespace feng3d.pixi {
    const particles_vert = "\nattribute vec2 aVertexPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 aColor;\n\nattribute vec2 aPositionCoord;\nattribute float aRotation;\n\nuniform mat3 translationMatrix;\nuniform vec4 uColor;\n\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\n\nvoid main(void){\n    float x = (aVertexPosition.x) * cos(aRotation) - (aVertexPosition.y) * sin(aRotation);\n    float y = (aVertexPosition.x) * sin(aRotation) + (aVertexPosition.y) * cos(aRotation);\n\n    vec2 v = vec2(x, y);\n    v = v + aPositionCoord;\n\n    gl_Position = vec4((translationMatrix * vec3(v, 1.0)).xy, 0.0, 1.0);\n\n    vTextureCoord = aTextureCoord;\n    vColor = aColor * uColor;\n}\n";
}
declare namespace feng3d.pixi {
    /**
     * The particle buffer manages the static and dynamic buffers for a particle container.
     *
     * @private
     */
    class ParticleBuffer {
        geometry: Geometry;
        staticStride: number;
        staticBuffer: Buffer;
        staticData: Float32Array;
        staticDataUint32: Uint32Array;
        dynamicStride: number;
        dynamicBuffer: Buffer;
        dynamicData: Float32Array;
        dynamicDataUint32: Uint32Array;
        _updateID: number;
        indexBuffer: Buffer;
        private size;
        private dynamicProperties;
        private staticProperties;
        /**
         * @private
         * @param {object} properties - The properties to upload.
         * @param {boolean[]} dynamicPropertyFlags - Flags for which properties are dynamic.
         * @param {number} size - The size of the batch.
         */
        constructor(properties: IParticleRendererProperty[], dynamicPropertyFlags: boolean[], size: number);
        /**
         * Sets up the renderer context and necessary buffers.
         *
         * @private
         */
        private initBuffers;
        /**
         * Uploads the dynamic properties.
         *
         * @private
         * @param {feng3d.Node2D[]} children - The children to upload.
         * @param {number} startIndex - The index to start at.
         * @param {number} amount - The number to upload.
         */
        uploadDynamic(children: Container[], startIndex: number, amount: number): void;
        /**
         * Uploads the static properties.
         *
         * @private
         * @param {feng3d.Node2D[]} children - The children to upload.
         * @param {number} startIndex - The index to start at.
         * @param {number} amount - The number to upload.
         */
        uploadStatic(children: Container[], startIndex: number, amount: number): void;
        /**
         * Destroys the ParticleBuffer.
         *
         * @private
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    interface IParticleProperties {
        vertices?: boolean;
        position?: boolean;
        rotation?: boolean;
        uvs?: boolean;
        tint?: boolean;
        alpha?: boolean;
        scale?: boolean;
    }
    /**
     * The ParticleContainer class is a really fast version of the Container built solely for speed,
     * so use when you need a lot of sprites or particles.
     *
     * The tradeoff of the ParticleContainer is that most advanced functionality will not work.
     * ParticleContainer implements the basic object transform (position, scale, rotation)
     * and some advanced functionality like tint (as of v4.5.6).
     *
     * Other more advanced functionality like masking, children, filters, etc will not work on sprites in this batch.
     *
     * It's extremely easy to use:
     * ```js
     * let container = new ParticleContainer();
     *
     * for (let i = 0; i < 100; ++i)
     * {
     *     let sprite = PIXI.Sprite.from("myImage.png");
     *     container.addChild(sprite);
     * }
     * ```
     *
     * And here you have a hundred sprites that will be rendered at the speed of light.
     *
     */
    class ParticleContainer extends Container {
        blendMode: BLEND_MODES;
        autoResize: boolean;
        roundPixels: boolean;
        baseTexture: BaseTexture;
        tintRgb: Float32Array;
        _maxSize: number;
        _buffers: ParticleBuffer[];
        _batchSize: number;
        _properties: boolean[];
        _bufferUpdateIDs: number[];
        _updateID: number;
        private _tint;
        /**
         * @param {number} [maxSize=1500] - The maximum number of particles that can be rendered by the container.
         *  Affects size of allocated buffers.
         * @param {object} [properties] - The properties of children that should be uploaded to the gpu and applied.
         * @param {boolean} [properties.vertices=false] - When true, vertices be uploaded and applied.
         *                  if sprite's ` scale/anchor/trim/frame/orig` is dynamic, please set `true`.
         * @param {boolean} [properties.position=true] - When true, position be uploaded and applied.
         * @param {boolean} [properties.rotation=false] - When true, rotation be uploaded and applied.
         * @param {boolean} [properties.uvs=false] - When true, uvs be uploaded and applied.
         * @param {boolean} [properties.tint=false] - When true, alpha and tint be uploaded and applied.
         * @param {number} [batchSize=16384] - Number of particles per batch. If less than maxSize, it uses maxSize instead.
         * @param {boolean} [autoResize=false] - If true, container allocates more batches in case
         *  there are more than `maxSize` particles.
         */
        constructor(maxSize: number, properties: IParticleProperties, batchSize?: number, autoResize?: boolean);
        /**
         * Sets the private properties array to dynamic / static based on the passed properties object
         *
         * @param {object} properties - The properties to be uploaded
         */
        setProperties(properties: IParticleProperties): void;
        /**
         * Updates the object transform for rendering
         *
         * @private
         */
        updateTransform(): void;
        /**
         * The tint applied to the container. This is a hex value.
         * A value of 0xFFFFFF will remove any tint effect.
         ** IMPORTANT: This is a WebGL only feature and will be ignored by the canvas renderer.
         * @member {number}
         * @default 0xFFFFFF
         */
        get tint(): number;
        set tint(value: number);
        /**
         * Renders the container using the WebGL renderer
         *
         * @private
         * @param {PIXI.Renderer} renderer - The webgl renderer
         */
        render(renderer: Renderer): void;
        /**
         * Set the flag that static data should be updated to true
         *
         * @private
         * @param {number} smallestChildIndex - The smallest child index
         */
        protected onChildrenChange(smallestChildIndex: number): void;
        dispose(): void;
        /**
         * Destroys the container
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
         *  have been set to that value
         * @param {boolean} [options.children=false] - if set to true, all the children will have their
         *  destroy method called as well. 'options' will be passed on to those calls.
         * @param {boolean} [options.texture=false] - Only used for child Sprites if options.children is set to true
         *  Should it destroy the texture of the child sprite
         * @param {boolean} [options.baseTexture=false] - Only used for child Sprites if options.children is set to true
         *  Should it destroy the base texture of the child sprite
         */
        destroy(options?: IDestroyOptions | boolean): void;
    }
}
declare namespace feng3d.pixi {
    interface IParticleRendererProperty {
        attributeName: string;
        size: number;
        type?: TYPES;
        uploadFunction: (...params: any[]) => any;
        offset: number;
    }
    /**
     * Renderer for Particles that is designer for speed over feature set.
     *
     */
    class ParticleRenderer extends ObjectRenderer {
        readonly state: State;
        shader: Shader;
        tempMatrix: Matrix;
        properties: IParticleRendererProperty[];
        /**
         * @param {PIXI.Renderer} renderer - The renderer this sprite batch works for.
         */
        constructor(renderer: Renderer);
        /**
         * Renders the particle container object.
         *
         * @param {PIXI.ParticleContainer} container - The container to render using this ParticleRenderer
         */
        render(container: ParticleContainer): void;
        /**
         * Creates one particle buffer for each child in the container we want to render and updates internal properties
         *
         * @param {PIXI.ParticleContainer} container - The container to render using this ParticleRenderer
         * @return {PIXI.ParticleBuffer[]} The buffers
         * @private
         */
        private generateBuffers;
        /**
         * Creates one more particle buffer, because container has autoResize feature
         *
         * @param {PIXI.ParticleContainer} container - The container to render using this ParticleRenderer
         * @return {PIXI.ParticleBuffer} generated buffer
         * @private
         */
        private _generateOneMoreBuffer;
        /**
         * Uploads the vertices.
         *
         * @param {feng3d.Node2D[]} children - the array of display objects to render
         * @param {number} startIndex - the index to start from in the children array
         * @param {number} amount - the amount of children that will have their vertices uploaded
         * @param {number[]} array - The vertices to upload.
         * @param {number} stride - Stride to use for iteration.
         * @param {number} offset - Offset to start at.
         */
        uploadVertices(children: Container[], startIndex: number, amount: number, array: number[], stride: number, offset: number): void;
        /**
         * Uploads the position.
         *
         * @param {feng3d.Node2D[]} children - the array of display objects to render
         * @param {number} startIndex - the index to start from in the children array
         * @param {number} amount - the amount of children that will have their positions uploaded
         * @param {number[]} array - The vertices to upload.
         * @param {number} stride - Stride to use for iteration.
         * @param {number} offset - Offset to start at.
         */
        uploadPosition(children: Container[], startIndex: number, amount: number, array: number[], stride: number, offset: number): void;
        /**
         * Uploads the rotation.
         *
         * @param {feng3d.Node2D[]} children - the array of display objects to render
         * @param {number} startIndex - the index to start from in the children array
         * @param {number} amount - the amount of children that will have their rotation uploaded
         * @param {number[]} array - The vertices to upload.
         * @param {number} stride - Stride to use for iteration.
         * @param {number} offset - Offset to start at.
         */
        uploadRotation(children: Container[], startIndex: number, amount: number, array: number[], stride: number, offset: number): void;
        /**
         * Uploads the Uvs
         *
         * @param {feng3d.Node2D[]} children - the array of display objects to render
         * @param {number} startIndex - the index to start from in the children array
         * @param {number} amount - the amount of children that will have their rotation uploaded
         * @param {number[]} array - The vertices to upload.
         * @param {number} stride - Stride to use for iteration.
         * @param {number} offset - Offset to start at.
         */
        uploadUvs(children: Container[], startIndex: number, amount: number, array: number[], stride: number, offset: number): void;
        /**
         * Uploads the tint.
         *
         * @param {feng3d.Node2D[]} children - the array of display objects to render
         * @param {number} startIndex - the index to start from in the children array
         * @param {number} amount - the amount of children that will have their rotation uploaded
         * @param {number[]} array - The vertices to upload.
         * @param {number} stride - Stride to use for iteration.
         * @param {number} offset - Offset to start at.
         */
        uploadTint(children: Container[], startIndex: number, amount: number, array: number[], stride: number, offset: number): void;
        /**
         * Destroys the ParticleRenderer.
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    interface IUploadHook {
        (helper: AbstractRenderer | BasePrepare, item: IDisplayObjectExtended): boolean;
    }
    interface IFindHook {
        (item: any, queue: Array<any>): boolean;
    }
    export interface IDisplayObjectExtended extends Container {
        _textures?: Array<Texture>;
        _texture?: Texture;
        style?: TextStyle | Partial<TextStyle>;
    }
    /**
     * The prepare manager provides functionality to upload content to the GPU.
     *
     * BasePrepare handles basic queuing functionality and is extended by
     * {@link PIXI.Prepare} and {@link PIXI.CanvasPrepare}
     * to provide preparation capabilities specific to their respective renderers.
     *
     * @example
     * // Create a sprite
     * const sprite = PIXI.Sprite.from('something.png');
     *
     * // Load object into GPU
     * app.renderer.plugins.prepare.upload(sprite, () => {
     *
     *     //Texture(s) has been uploaded to GPU
     *     app.stage.addChild(sprite);
     *
     * })
     *
     * @abstract
     */
    export class BasePrepare {
        private limiter;
        protected renderer: AbstractRenderer;
        protected uploadHookHelper: any;
        protected queue: Array<any>;
        addHooks: Array<any>;
        uploadHooks: Array<any>;
        completes: Array<any>;
        ticking: boolean;
        private delayedTick;
        /**
         * @param {PIXI.AbstractRenderer} renderer - A reference to the current renderer
         */
        constructor(renderer: AbstractRenderer);
        /**
         * Upload all the textures and graphics to the GPU.
         *
         * @param {Function|PIXI.DisplayObject|PIXI.Container|PIXI.BaseTexture|PIXI.Texture|PIXI.Graphics|PIXI.Text} item -
         *        Either the container or display object to search for items to upload, the items to upload themselves,
         *        or the callback function, if items have been added using `prepare.add`.
         * @param {Function} [done] - Optional callback when all queued uploads have completed
         */
        upload(item: IDisplayObjectExtended | IUploadHook | IFindHook | (() => void), done?: () => void): void;
        /**
         * Handle tick update
         *
         * @private
         */
        tick(): void;
        /**
         * Actually prepare items. This is handled outside of the tick because it will take a while
         * and we do NOT want to block the current animation frame from rendering.
         *
         * @private
         */
        prepareItems(): void;
        /**
         * Adds hooks for finding items.
         *
         * @param {Function} addHook - Function call that takes two parameters: `item:*, queue:Array`
         *          function must return `true` if it was able to add item to the queue.
         * @return {this} Instance of plugin for chaining.
         */
        registerFindHook(addHook: IFindHook): this;
        /**
         * Adds hooks for uploading items.
         *
         * @param {Function} uploadHook - Function call that takes two parameters: `prepare:CanvasPrepare, item:*` and
         *          function must return `true` if it was able to handle upload of item.
         * @return {this} Instance of plugin for chaining.
         */
        registerUploadHook(uploadHook: IUploadHook): this;
        /**
         * Manually add an item to the uploading queue.
         *
         * @param {PIXI.DisplayObject|PIXI.Container|PIXI.BaseTexture|PIXI.Texture|PIXI.Graphics|PIXI.Text|*} item - Object to
         *        add to the queue
         * @return {this} Instance of plugin for chaining.
         */
        add(item: IDisplayObjectExtended | IUploadHook | IFindHook): this;
        /**
         * Destroys the plugin, don't use after this.
         *
         */
        destroy(): void;
    }
    export {};
}
declare namespace feng3d.pixi {
    /**
     * CountLimiter limits the number of items handled by a {@link PIXI.BasePrepare} to a specified
     * number of items per frame.
     *
     */
    class CountLimiter {
        maxItemsPerFrame: number;
        itemsLeft: number;
        /**
         * @param {number} maxItemsPerFrame - The maximum number of items that can be prepared each frame.
         */
        constructor(maxItemsPerFrame: number);
        /**
         * Resets any counting properties to start fresh on a new frame.
         */
        beginFrame(): void;
        /**
         * Checks to see if another item can be uploaded. This should only be called once per item.
         * @return {boolean} If the item is allowed to be uploaded.
         */
        allowedToUpload(): boolean;
    }
}
declare namespace feng3d.pixi {
    /**
     * The prepare plugin provides renderer-specific plugins for pre-rendering DisplayObjects. These plugins are useful for
     * asynchronously preparing and uploading to the GPU assets, textures, graphics waiting to be displayed.
     *
     * Do not instantiate this plugin directly. It is available from the `renderer.plugins` property.
     * See {@link PIXI.CanvasRenderer#plugins} or {@link PIXI.Renderer#plugins}.
     * @example
     * // Create a new application
     * const app = new PIXI.Application();
     * document.body.appendChild(app.view);
     *
     * // Don't start rendering right away
     * app.stop();
     *
     * // create a display object
     * const rect = new PIXI.Graphics()
     *     .beginFill(0x00ff00)
     *     .drawRect(40, 40, 200, 200);
     *
     * // Add to the stage
     * app.stage.addChild(rect);
     *
     * // Don't start rendering until the graphic is uploaded to the GPU
     * app.renderer.plugins.prepare.upload(app.stage, () => {
     *     app.start();
     * });
     *
     */
    class Prepare extends BasePrepare {
        /**
         * @param {PIXI.Renderer} renderer - A reference to the current renderer
         */
        constructor(renderer: Renderer);
    }
}
declare namespace feng3d.pixi {
}
declare namespace feng3d.pixi {
    /**
     * TimeLimiter limits the number of items handled by a {@link PIXI.BasePrepare} to a specified
     * number of milliseconds per frame.
     *
     */
    class TimeLimiter {
        maxMilliseconds: number;
        frameStart: number;
        /**
         * @param {number} maxMilliseconds - The maximum milliseconds that can be spent preparing items each frame.
         */
        constructor(maxMilliseconds: number);
        /**
         * Resets any counting properties to start fresh on a new frame.
         */
        beginFrame(): void;
        /**
         * Checks to see if another item can be uploaded. This should only be called once per item.
         * @return {boolean} If the item is allowed to be uploaded.
         */
        allowedToUpload(): boolean;
    }
}
declare namespace feng3d.pixi {
    /**
     * Constants that define the type of gradient on text.
     *
     * @static
     * @constant
     * @name TEXT_GRADIENT
     * @type {object}
     * @property {number} LINEAR_VERTICAL Vertical gradient
     * @property {number} LINEAR_HORIZONTAL Linear gradient
     */
    enum TEXT_GRADIENT {
        LINEAR_VERTICAL = 0,
        LINEAR_HORIZONTAL = 1
    }
}
declare namespace feng3d.pixi {
    /**
     * A Text Object will create a line or multiple lines of text.
     *
     * The text is created using the [Canvas API](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API).
     *
     * The primary advantage of this class over BitmapText is that you have great control over the style of the text,
     * which you can change at runtime.
     *
     * The primary disadvantages is that each piece of text has it's own texture, which can use more memory.
     * When text changes, this texture has to be re-generated and re-uploaded to the GPU, taking up time.
     *
     * To split a line you can use '\n' in your text string, or, on the `style` object,
     * change its `wordWrap` property to true and and give the `wordWrapWidth` property a value.
     *
     * A Text can be created directly from a string and a style object,
     * which can be generated [here](https://pixijs.io/pixi-text-style).
     *
     * ```js
     * let text = new PIXI.Text('This is a PixiJS text',{fontFamily : 'Arial', fontSize: 24, fill : 0xff1010, align : 'center'});
     * ```
     *
     */
    class Text extends Sprite {
        canvas: HTMLCanvasElement;
        context: CanvasRenderingContext2D;
        localStyleID: number;
        dirty: boolean;
        _resolution: number;
        _autoResolution: boolean;
        protected _text: string;
        protected _font: string;
        protected _style: TextStyle;
        protected _styleListener: () => void;
        private _ownCanvas;
        /**
         * @param {string} text - The string that you would like the text to display
         * @param {object|PIXI.TextStyle} [style] - The style parameters
         * @param {HTMLCanvasElement} [canvas] - The canvas element for drawing text
         */
        constructor(text: string, style?: Partial<ITextStyle> | TextStyle, canvas?: HTMLCanvasElement);
        /**
         * Renders text to its canvas, and updates its texture.
         * By default this is used internally to ensure the texture is correct before rendering,
         * but it can be used called externally, for example from this class to 'pre-generate' the texture from a piece of text,
         * and then shared across multiple Sprites.
         *
         * @param {boolean} respectDirty - Whether to abort updating the text if the Text isn't dirty and the function is called.
         */
        updateText(respectDirty: boolean): void;
        /**
         * Render the text with letter-spacing.
         * @param {string} text - The text to draw
         * @param {number} x - Horizontal position to draw the text
         * @param {number} y - Vertical position to draw the text
         * @param {boolean} [isStroke=false] - Is this drawing for the outside stroke of the
         *  text? If not, it's for the inside fill
         * @private
         */
        private drawLetterSpacing;
        /**
         * Updates texture size based on canvas size
         *
         * @private
         */
        private updateTexture;
        /**
         * Renders the object using the WebGL renderer
         *
         * @param {PIXI.Renderer} renderer - The renderer
         */
        _render(renderer: Renderer): void;
        /**
         * Gets the local bounds of the text object.
         *
         * @param {PIXI.Rectangle} rect - The output rectangle.
         * @return {PIXI.Rectangle} The bounds.
         */
        getLocalBounds(rect: Rectangle): Rectangle;
        /**
         * calculates the bounds of the Text as a rectangle. The bounds calculation takes the worldTransform into account.
         * @protected
         */
        _calculateBounds(): void;
        /**
         * Generates the fill style. Can automatically generate a gradient based on the fill style being an array
         *
         * @private
         * @param {object} style - The style.
         * @param {string[]} lines - The lines of text.
         * @return {string|number|CanvasGradient} The fill style
         */
        private _generateFillStyle;
        /**
         * Destroys this text object.
         * Note* Unlike a Sprite, a Text object will automatically destroy its baseTexture and texture as
         * the majority of the time the texture will not be shared with any other Sprites.
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
         *  have been set to that value
         * @param {boolean} [options.children=false] - if set to true, all the children will have their
         *  destroy method called as well. 'options' will be passed on to those calls.
         * @param {boolean} [options.texture=true] - Should it destroy the current texture of the sprite as well
         * @param {boolean} [options.baseTexture=true] - Should it destroy the base texture of the sprite as well
         */
        destroy(options?: IDestroyOptions | boolean): void;
        /**
         * The width of the Text, setting this will actually modify the scale to achieve the value set
         *
         * @member {number}
         */
        get width(): number;
        set width(value: number);
        /**
         * The height of the Text, setting this will actually modify the scale to achieve the value set
         *
         * @member {number}
         */
        get height(): number;
        set height(value: number);
        /**
         * Set the style of the text. Set up an event listener to listen for changes on the style
         * object and mark the text as dirty.
         *
         * @member {object|PIXI.TextStyle}
         */
        get style(): TextStyle | Partial<ITextStyle>;
        set style(style: TextStyle | Partial<ITextStyle>);
        /**
         * Set the copy for the text object. To split a line you can use '\n'.
         *
         * @member {string}
         */
        get text(): string;
        set text(text: string);
        /**
         * The resolution / device pixel ratio of the canvas.
         * This is set to automatically match the renderer resolution by default, but can be overridden by setting manually.
         * @member {number}
         * @default 1
         */
        get resolution(): number;
        set resolution(value: number);
    }
}
declare namespace feng3d.pixi {
    interface IFontMetrics {
        ascent: number;
        descent: number;
        fontSize: number;
    }
    /**
     * The TextMetrics object represents the measurement of a block of text with a specified style.
     *
     * ```js
     * let style = new PIXI.TextStyle({fontFamily : 'Arial', fontSize: 24, fill : 0xff1010, align : 'center'})
     * let textMetrics = PIXI.TextMetrics.measureText('Your text', style)
     * ```
     *
     */
    export class TextMetrics {
        text: string;
        style: TextStyle;
        width: number;
        height: number;
        lines: string[];
        lineWidths: number[];
        lineHeight: number;
        maxLineWidth: number;
        fontProperties: IFontMetrics;
        static METRICS_STRING: string;
        static BASELINE_SYMBOL: string;
        static BASELINE_MULTIPLIER: number;
        static HEIGHT_MULTIPLIER: number;
        static _canvas: HTMLCanvasElement | OffscreenCanvas;
        static _context: CanvasRenderingContext2D | OffscreenCanvasRenderingContext2D;
        static _fonts: {
            [font: string]: IFontMetrics;
        };
        static _newlines: number[];
        static _breakingSpaces: number[];
        /**
         * @param {string} text - the text that was measured
         * @param {PIXI.TextStyle} style - the style that was measured
         * @param {number} width - the measured width of the text
         * @param {number} height - the measured height of the text
         * @param {string[]} lines - an array of the lines of text broken by new lines and wrapping if specified in style
         * @param {number[]} lineWidths - an array of the line widths for each line matched to `lines`
         * @param {number} lineHeight - the measured line height for this style
         * @param {number} maxLineWidth - the maximum line width for all measured lines
         * @param {Object} fontProperties - the font properties object from TextMetrics.measureFont
         */
        constructor(text: string, style: TextStyle, width: number, height: number, lines: string[], lineWidths: number[], lineHeight: number, maxLineWidth: number, fontProperties: IFontMetrics);
        /**
         * Measures the supplied string of text and returns a Rectangle.
         *
         * @param {string} text - the text to measure.
         * @param {PIXI.TextStyle} style - the text style to use for measuring
         * @param {boolean} [wordWrap] - optional override for if word-wrap should be applied to the text.
         * @param {HTMLCanvasElement} [canvas] - optional specification of the canvas to use for measuring.
         * @return {PIXI.TextMetrics} measured width and height of the text.
         */
        static measureText(text: string, style: TextStyle, wordWrap?: boolean, canvas?: HTMLCanvasElement | OffscreenCanvas): TextMetrics;
        /**
         * Applies newlines to a string to have it optimally fit into the horizontal
         * bounds set by the Text object's wordWrapWidth property.
         *
         * @private
         * @param {string} text - String to apply word wrapping to
         * @param {PIXI.TextStyle} style - the style to use when wrapping
         * @param {HTMLCanvasElement} [canvas] - optional specification of the canvas to use for measuring.
         * @return {string} New string with new lines applied where required
         */
        private static wordWrap;
        /**
         * Convienience function for logging each line added during the wordWrap
         * method
         *
         * @private
         * @param  {string}   line        - The line of text to add
         * @param  {boolean}  newLine     - Add new line character to end
         * @return {string}  A formatted line
         */
        private static addLine;
        /**
         * Gets & sets the widths of calculated characters in a cache object
         *
         * @private
         * @param  {string}                    key            - The key
         * @param  {number}                    letterSpacing  - The letter spacing
         * @param  {object}                    cache          - The cache
         * @param  {CanvasRenderingContext2D}  context        - The canvas context
         * @return {number}                    The from cache.
         */
        private static getFromCache;
        /**
         * Determines whether we should collapse breaking spaces
         *
         * @private
         * @param  {string}   whiteSpace - The TextStyle property whiteSpace
         * @return {boolean}  should collapse
         */
        private static collapseSpaces;
        /**
         * Determines whether we should collapse newLine chars
         *
         * @private
         * @param  {string}   whiteSpace - The white space
         * @return {boolean}  should collapse
         */
        private static collapseNewlines;
        /**
         * trims breaking whitespaces from string
         *
         * @private
         * @param  {string}  text - The text
         * @return {string}  trimmed string
         */
        private static trimRight;
        /**
         * Determines if char is a newline.
         *
         * @private
         * @param  {string}  char - The character
         * @return {boolean}  True if newline, False otherwise.
         */
        private static isNewline;
        /**
         * Determines if char is a breaking whitespace.
         *
         * It allows one to determine whether char should be a breaking whitespace
         * For example certain characters in CJK langs or numbers.
         * It must return a boolean.
         *
         * @param  {string}  char     - The character
         * @param  {string}  [nextChar] - The next character
         * @return {boolean}  True if whitespace, False otherwise.
         */
        static isBreakingSpace(char: string, _nextChar?: string): boolean;
        /**
         * Splits a string into words, breaking-spaces and newLine characters
         *
         * @private
         * @param  {string}  text - The text
         * @return {string[]}  A tokenized array
         */
        private static tokenize;
        /**
         * Overridable helper method used internally by TextMetrics, exposed to allow customizing the class's behavior.
         *
         * It allows one to customise which words should break
         * Examples are if the token is CJK or numbers.
         * It must return a boolean.
         *
         * @param  {string}  token       - The token
         * @param  {boolean}  breakWords - The style attr break words
         * @return {boolean} whether to break word or not
         */
        static canBreakWords(_token: string, breakWords: boolean): boolean;
        /**
         * Overridable helper method used internally by TextMetrics, exposed to allow customizing the class's behavior.
         *
         * It allows one to determine whether a pair of characters
         * should be broken by newlines
         * For example certain characters in CJK langs or numbers.
         * It must return a boolean.
         *
         * @param  {string}  char        - The character
         * @param  {string}  nextChar    - The next character
         * @param  {string}  token       - The token/word the characters are from
         * @param  {number}  index       - The index in the token of the char
         * @param  {boolean}  breakWords - The style attr break words
         * @return {boolean} whether to break word or not
         */
        static canBreakChars(_char: string, _nextChar: string, _token: string, _index: number, _breakWords: boolean): boolean;
        /**
         * Overridable helper method used internally by TextMetrics, exposed to allow customizing the class's behavior.
         *
         * It is called when a token (usually a word) has to be split into separate pieces
         * in order to determine the point to break a word.
         * It must return an array of characters.
         *
         * @example
         * // Correctly splits emojis, eg "🤪🤪" will result in two element array, each with one emoji.
         * TextMetrics.wordWrapSplit = (token) => [...token];
         *
         * @param  {string}  token - The token to split
         * @return {string[]} The characters of the token
         */
        static wordWrapSplit(token: string): string[];
        /**
         * Calculates the ascent, descent and fontSize of a given font-style
         *
         * @static
         * @param {string} font - String representing the style of the font
         * @return {PIXI.IFontMetrics} Font properties object
         */
        static measureFont(font: string): IFontMetrics;
        /**
         * Clear font metrics in metrics cache.
         *
         * @static
         * @param {string} [font] - font name. If font name not set then clear cache for all fonts.
         */
        static clearMetrics(font?: string): void;
    }
    export {};
}
declare namespace feng3d.pixi {
    type TextStyleAlign = 'left' | 'center' | 'right' | 'justify';
    type TextStyleFill = string | string[] | number | number[] | CanvasGradient | CanvasPattern;
    type TextStyleFontStyle = 'normal' | 'italic' | 'oblique';
    type TextStyleFontVariant = 'normal' | 'small-caps';
    type TextStyleFontWeight = 'normal' | 'bold' | 'bolder' | 'lighter' | '100' | '200' | '300' | '400' | '500' | '600' | '700' | '800' | '900';
    type TextStyleLineJoin = 'miter' | 'round' | 'bevel';
    type TextStyleTextBaseline = 'alphabetic' | 'top' | 'hanging' | 'middle' | 'ideographic' | 'bottom';
    type TextStyleWhiteSpace = 'normal' | 'pre' | 'pre-line';
    interface ITextStyle {
        align: TextStyleAlign;
        breakWords: boolean;
        dropShadow: boolean;
        dropShadowAlpha: number;
        dropShadowAngle: number;
        dropShadowBlur: number;
        dropShadowColor: string | number;
        dropShadowDistance: number;
        fill: TextStyleFill;
        fillGradientType: TEXT_GRADIENT;
        fillGradientStops: number[];
        fontFamily: string | string[];
        fontSize: number | string;
        fontStyle: TextStyleFontStyle;
        fontVariant: TextStyleFontVariant;
        fontWeight: TextStyleFontWeight;
        letterSpacing: number;
        lineHeight: number;
        lineJoin: TextStyleLineJoin;
        miterLimit: number;
        padding: number;
        stroke: string | number;
        strokeThickness: number;
        textBaseline: TextStyleTextBaseline;
        trim: boolean;
        whiteSpace: TextStyleWhiteSpace;
        wordWrap: boolean;
        wordWrapWidth: number;
        leading: number;
    }
    /**
     * A TextStyle Object contains information to decorate a Text objects.
     *
     * An instance can be shared between multiple Text objects; then changing the style will update all text objects using it.
     *
     * A tool can be used to generate a text style [here](https://pixijs.io/pixi-text-style).
     *
     */
    class TextStyle implements ITextStyle {
        styleID: number;
        protected _align: TextStyleAlign;
        protected _breakWords: boolean;
        protected _dropShadow: boolean;
        protected _dropShadowAlpha: number;
        protected _dropShadowAngle: number;
        protected _dropShadowBlur: number;
        protected _dropShadowColor: string | number;
        protected _dropShadowDistance: number;
        protected _fill: TextStyleFill;
        protected _fillGradientType: TEXT_GRADIENT;
        protected _fillGradientStops: number[];
        protected _fontFamily: string | string[];
        protected _fontSize: number | string;
        protected _fontStyle: TextStyleFontStyle;
        protected _fontVariant: TextStyleFontVariant;
        protected _fontWeight: TextStyleFontWeight;
        protected _letterSpacing: number;
        protected _lineHeight: number;
        protected _lineJoin: TextStyleLineJoin;
        protected _miterLimit: number;
        protected _padding: number;
        protected _stroke: string | number;
        protected _strokeThickness: number;
        protected _textBaseline: TextStyleTextBaseline;
        protected _trim: boolean;
        protected _whiteSpace: TextStyleWhiteSpace;
        protected _wordWrap: boolean;
        protected _wordWrapWidth: number;
        protected _leading: number;
        /**
         * @param {object} [style] - The style parameters
         * @param {string} [style.align='left'] - Alignment for multiline text ('left', 'center' or 'right'),
         *  does not affect single line text
         * @param {boolean} [style.breakWords=false] - Indicates if lines can be wrapped within words, it
         *  needs wordWrap to be set to true
         * @param {boolean} [style.dropShadow=false] - Set a drop shadow for the text
         * @param {number} [style.dropShadowAlpha=1] - Set alpha for the drop shadow
         * @param {number} [style.dropShadowAngle=Math.PI/6] - Set a angle of the drop shadow
         * @param {number} [style.dropShadowBlur=0] - Set a shadow blur radius
         * @param {string|number} [style.dropShadowColor='black'] - A fill style to be used on the dropshadow e.g 'red', '#00FF00'
         * @param {number} [style.dropShadowDistance=5] - Set a distance of the drop shadow
         * @param {string|string[]|number|number[]|CanvasGradient|CanvasPattern} [style.fill='black'] - A canvas
         *  fillstyle that will be used on the text e.g 'red', '#00FF00'. Can be an array to create a gradient
         *  eg ['#000000','#FFFFFF']
         * {@link https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fillStyle|MDN}
         * @param {number} [style.fillGradientType=PIXI.TEXT_GRADIENT.LINEAR_VERTICAL] - If fill is an array of colours
         *  to create a gradient, this can change the type/direction of the gradient. See {@link PIXI.TEXT_GRADIENT}
         * @param {number[]} [style.fillGradientStops] - If fill is an array of colours to create a gradient, this array can set
         * the stop points (numbers between 0 and 1) for the color, overriding the default behaviour of evenly spacing them.
         * @param {string|string[]} [style.fontFamily='Arial'] - The font family
         * @param {number|string} [style.fontSize=26] - The font size (as a number it converts to px, but as a string,
         *  equivalents are '26px','20pt','160%' or '1.6em')
         * @param {string} [style.fontStyle='normal'] - The font style ('normal', 'italic' or 'oblique')
         * @param {string} [style.fontVariant='normal'] - The font variant ('normal' or 'small-caps')
         * @param {string} [style.fontWeight='normal'] - The font weight ('normal', 'bold', 'bolder', 'lighter' and '100',
         *  '200', '300', '400', '500', '600', '700', '800' or '900')
         * @param {number} [style.leading=0] - The space between lines
         * @param {number} [style.letterSpacing=0] - The amount of spacing between letters, default is 0
         * @param {number} [style.lineHeight] - The line height, a number that represents the vertical space that a letter uses
         * @param {string} [style.lineJoin='miter'] - The lineJoin property sets the type of corner created, it can resolve
         *      spiked text issues. Possible values "miter" (creates a sharp corner), "round" (creates a round corner) or "bevel"
         *      (creates a squared corner).
         * @param {number} [style.miterLimit=10] - The miter limit to use when using the 'miter' lineJoin mode. This can reduce
         *      or increase the spikiness of rendered text.
         * @param {number} [style.padding=0] - Occasionally some fonts are cropped. Adding some padding will prevent this from
         *     happening by adding padding to all sides of the text.
         * @param {string|number} [style.stroke='black'] - A canvas fillstyle that will be used on the text stroke
         *  e.g 'blue', '#FCFF00'
         * @param {number} [style.strokeThickness=0] - A number that represents the thickness of the stroke.
         *  Default is 0 (no stroke)
         * @param {boolean} [style.trim=false] - Trim transparent borders
         * @param {string} [style.textBaseline='alphabetic'] - The baseline of the text that is rendered.
         * @param {string} [style.whiteSpace='pre'] - Determines whether newlines & spaces are collapsed or preserved "normal"
         *      (collapse, collapse), "pre" (preserve, preserve) | "pre-line" (preserve, collapse). It needs wordWrap to be set to true
         * @param {boolean} [style.wordWrap=false] - Indicates if word wrap should be used
         * @param {number} [style.wordWrapWidth=100] - The width at which text will wrap, it needs wordWrap to be set to true
         */
        constructor(style?: Partial<ITextStyle>);
        /**
         * Creates a new TextStyle object with the same values as this one.
         * Note that the only the properties of the object are cloned.
         *
         * @return {PIXI.TextStyle} New cloned TextStyle object
         */
        clone(): TextStyle;
        /**
         * Resets all properties to the defaults specified in TextStyle.prototype._default
         */
        reset(): void;
        /**
         * Alignment for multiline text ('left', 'center' or 'right'), does not affect single line text
         *
         * @member {string}
         */
        get align(): TextStyleAlign;
        set align(align: TextStyleAlign);
        /**
         * Indicates if lines can be wrapped within words, it needs wordWrap to be set to true
         *
         * @member {boolean}
         */
        get breakWords(): boolean;
        set breakWords(breakWords: boolean);
        /**
         * Set a drop shadow for the text
         *
         * @member {boolean}
         */
        get dropShadow(): boolean;
        set dropShadow(dropShadow: boolean);
        /**
         * Set alpha for the drop shadow
         *
         * @member {number}
         */
        get dropShadowAlpha(): number;
        set dropShadowAlpha(dropShadowAlpha: number);
        /**
         * Set a angle of the drop shadow
         *
         * @member {number}
         */
        get dropShadowAngle(): number;
        set dropShadowAngle(dropShadowAngle: number);
        /**
         * Set a shadow blur radius
         *
         * @member {number}
         */
        get dropShadowBlur(): number;
        set dropShadowBlur(dropShadowBlur: number);
        /**
         * A fill style to be used on the dropshadow e.g 'red', '#00FF00'
         *
         * @member {string|number}
         */
        get dropShadowColor(): number | string;
        set dropShadowColor(dropShadowColor: number | string);
        /**
         * Set a distance of the drop shadow
         *
         * @member {number}
         */
        get dropShadowDistance(): number;
        set dropShadowDistance(dropShadowDistance: number);
        /**
         * A canvas fillstyle that will be used on the text e.g 'red', '#00FF00'.
         * Can be an array to create a gradient eg ['#000000','#FFFFFF']
         * {@link https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/fillStyle|MDN}
         *
         * @member {string|string[]|number|number[]|CanvasGradient|CanvasPattern}
         */
        get fill(): TextStyleFill;
        set fill(fill: TextStyleFill);
        /**
         * If fill is an array of colours to create a gradient, this can change the type/direction of the gradient.
         * See {@link PIXI.TEXT_GRADIENT}
         *
         * @member {number}
         */
        get fillGradientType(): TEXT_GRADIENT;
        set fillGradientType(fillGradientType: TEXT_GRADIENT);
        /**
         * If fill is an array of colours to create a gradient, this array can set the stop points
         * (numbers between 0 and 1) for the color, overriding the default behaviour of evenly spacing them.
         *
         * @member {number[]}
         */
        get fillGradientStops(): number[];
        set fillGradientStops(fillGradientStops: number[]);
        /**
         * The font family
         *
         * @member {string|string[]}
         */
        get fontFamily(): string | string[];
        set fontFamily(fontFamily: string | string[]);
        /**
         * The font size
         * (as a number it converts to px, but as a string, equivalents are '26px','20pt','160%' or '1.6em')
         *
         * @member {number|string}
         */
        get fontSize(): number | string;
        set fontSize(fontSize: number | string);
        /**
         * The font style
         * ('normal', 'italic' or 'oblique')
         *
         * @member {string}
         */
        get fontStyle(): TextStyleFontStyle;
        set fontStyle(fontStyle: TextStyleFontStyle);
        /**
         * The font variant
         * ('normal' or 'small-caps')
         *
         * @member {string}
         */
        get fontVariant(): TextStyleFontVariant;
        set fontVariant(fontVariant: TextStyleFontVariant);
        /**
         * The font weight
         * ('normal', 'bold', 'bolder', 'lighter' and '100', '200', '300', '400', '500', '600', '700', 800' or '900')
         *
         * @member {string}
         */
        get fontWeight(): TextStyleFontWeight;
        set fontWeight(fontWeight: TextStyleFontWeight);
        /**
         * The amount of spacing between letters, default is 0
         *
         * @member {number}
         */
        get letterSpacing(): number;
        set letterSpacing(letterSpacing: number);
        /**
         * The line height, a number that represents the vertical space that a letter uses
         *
         * @member {number}
         */
        get lineHeight(): number;
        set lineHeight(lineHeight: number);
        /**
         * The space between lines
         *
         * @member {number}
         */
        get leading(): number;
        set leading(leading: number);
        /**
         * The lineJoin property sets the type of corner created, it can resolve spiked text issues.
         * Default is 'miter' (creates a sharp corner).
         *
         * @member {string}
         */
        get lineJoin(): TextStyleLineJoin;
        set lineJoin(lineJoin: TextStyleLineJoin);
        /**
         * The miter limit to use when using the 'miter' lineJoin mode
         * This can reduce or increase the spikiness of rendered text.
         *
         * @member {number}
         */
        get miterLimit(): number;
        set miterLimit(miterLimit: number);
        /**
         * Occasionally some fonts are cropped. Adding some padding will prevent this from happening
         * by adding padding to all sides of the text.
         *
         * @member {number}
         */
        get padding(): number;
        set padding(padding: number);
        /**
         * A canvas fillstyle that will be used on the text stroke
         * e.g 'blue', '#FCFF00'
         *
         * @member {string|number}
         */
        get stroke(): string | number;
        set stroke(stroke: string | number);
        /**
         * A number that represents the thickness of the stroke.
         * Default is 0 (no stroke)
         *
         * @member {number}
         */
        get strokeThickness(): number;
        set strokeThickness(strokeThickness: number);
        /**
         * The baseline of the text that is rendered.
         *
         * @member {string}
         */
        get textBaseline(): TextStyleTextBaseline;
        set textBaseline(textBaseline: TextStyleTextBaseline);
        /**
         * Trim transparent borders
         *
         * @member {boolean}
         */
        get trim(): boolean;
        set trim(trim: boolean);
        /**
         * How newlines and spaces should be handled.
         * Default is 'pre' (preserve, preserve).
         *
         *  value       | New lines     |   Spaces
         *  ---         | ---           |   ---
         * 'normal'     | Collapse      |   Collapse
         * 'pre'        | Preserve      |   Preserve
         * 'pre-line'   | Preserve      |   Collapse
         *
         * @member {string}
         */
        get whiteSpace(): TextStyleWhiteSpace;
        set whiteSpace(whiteSpace: TextStyleWhiteSpace);
        /**
         * Indicates if word wrap should be used
         *
         * @member {boolean}
         */
        get wordWrap(): boolean;
        set wordWrap(wordWrap: boolean);
        /**
         * The width at which text will wrap, it needs wordWrap to be set to true
         *
         * @member {number}
         */
        get wordWrapWidth(): number;
        set wordWrapWidth(wordWrapWidth: number);
        /**
         * Generates a font style string to use for `TextMetrics.measureFont()`.
         *
         * @return {string} Font style string, for passing to `TextMetrics.measureFont()`
         */
        toFontString(): string;
    }
}
declare namespace feng3d.pixi {
    /**
     * Fill style object for Graphics.
     *
     */
    class FillStyle {
        /**
         * The hex color value used when coloring the Graphics object.
         *
         * @member {number}
         * @default 0xFFFFFF
         */
        color: number;
        /**
         * The alpha value used when filling the Graphics object.
         *
         * @member {number}
         * @default 1
         */
        alpha: number;
        /**
         * The texture to be used for the fill.
         *
         * @member {PIXI.Texture}
         * @default 0
         */
        texture: Texture;
        /**
         * The transform applied to the texture.
         *
         * @member {PIXI.Matrix}
         * @default null
         */
        matrix: Matrix;
        /**
         * If the current fill is visible.
         *
         * @member {boolean}
         * @default false
         */
        visible: boolean;
        constructor();
        /**
         * Clones the object
         *
         * @return {PIXI.FillStyle}
         */
        clone(): FillStyle;
        /**
         * Reset
         */
        reset(): void;
        /**
         * Destroy and don't use after this
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Represents the line style for Graphics.
     */
    class LineStyle extends FillStyle {
        /**
         * The width (thickness) of any lines drawn.
         *
         * @member {number}
         * @default 0
         */
        width: number;
        /**
         * The alignment of any lines drawn (0.5 = middle, 1 = outer, 0 = inner). WebGL only.
         *
         * @member {number}
         * @default 0.5
         */
        alignment: number;
        /**
         * If true the lines will be draw using LINES instead of TRIANGLE_STRIP
         *
         * @member {boolean}
         * @default false
         */
        native: boolean;
        /**
         * Line cap style.
         *
         * @member {PIXI.LINE_CAP}
         * @default PIXI.LINE_CAP.BUTT
         */
        cap: LINE_CAP;
        /**
         * Line join style.
         *
         * @member {PIXI.LINE_JOIN}
         * @default PIXI.LINE_JOIN.MITER
         */
        join: LINE_JOIN;
        /**
         * Miter limit.
         *
         * @member {number}
         * @default 10
         */
        miterLimit: number;
        /**
         * Clones the object
         *
         * @return {PIXI.LineStyle}
         */
        clone(): LineStyle;
        /**
         * Reset the line style to default.
         */
        reset(): void;
    }
}
declare namespace feng3d.pixi {
    interface IArcLikeShape {
        cx: number;
        cy: number;
        radius: number;
        startAngle: number;
        endAngle: number;
        anticlockwise: boolean;
    }
    /**
     * Utilities for arc curves
     * @private
     */
    export class ArcUtils {
        /**
         * The arcTo() method creates an arc/curve between two tangents on the canvas.
         *
         * "borrowed" from https://code.google.com/p/fxcanvas/ - thanks google!
         *
         * @private
         * @param {number} x1 - The x-coordinate of the beginning of the arc
         * @param {number} y1 - The y-coordinate of the beginning of the arc
         * @param {number} x2 - The x-coordinate of the end of the arc
         * @param {number} y2 - The y-coordinate of the end of the arc
         * @param {number} radius - The radius of the arc
         * @return {object} If the arc length is valid, return center of circle, radius and other info otherwise `null`.
         */
        static curveTo(x1: number, y1: number, x2: number, y2: number, radius: number, points: Array<number>): IArcLikeShape;
        /**
         * The arc method creates an arc/curve (used to create circles, or parts of circles).
         *
         * @private
         * @param {number} startX - Start x location of arc
         * @param {number} startY - Start y location of arc
         * @param {number} cx - The x-coordinate of the center of the circle
         * @param {number} cy - The y-coordinate of the center of the circle
         * @param {number} radius - The radius of the circle
         * @param {number} startAngle - The starting angle, in radians (0 is at the 3 o'clock position
         *  of the arc's circle)
         * @param {number} endAngle - The ending angle, in radians
         * @param {boolean} anticlockwise - Specifies whether the drawing should be
         *  counter-clockwise or clockwise. False is default, and indicates clockwise, while true
         *  indicates counter-clockwise.
         * @param {number[]} points - Collection of points to add to
         */
        static arc(_startX: number, _startY: number, cx: number, cy: number, radius: number, startAngle: number, endAngle: number, _anticlockwise: boolean, points: Array<number>): void;
    }
    export {};
}
declare namespace feng3d.pixi {
    /**
     * A structure to hold interim batch objects for Graphics..graphicsUtils
     */
    class BatchPart {
        style: LineStyle | FillStyle;
        start: number;
        size: number;
        attribStart: number;
        attribSize: number;
        constructor();
        /**
         * Begin batch part
         *
         * @param {PIXI.FillStyle | PIXI.LineStyle} style
         * @param {number} startIndex
         * @param {number} attribStart
         */
        begin(style: LineStyle | FillStyle, startIndex: number, attribStart: number): void;
        /**
         * End batch part
         *
         * @param {number} endIndex
         * @param {number} endAttrib
         */
        end(endIndex: number, endAttrib: number): void;
        reset(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Utilities for bezier curves
     * @private
     */
    class BezierUtils {
        /**
         * Calculate length of bezier curve.
         * Analytical solution is impossible, since it involves an integral that does not integrate in general.
         * Therefore numerical solution is used.
         *
         * @private
         * @param {number} fromX - Starting point x
         * @param {number} fromY - Starting point y
         * @param {number} cpX - Control point x
         * @param {number} cpY - Control point y
         * @param {number} cpX2 - Second Control point x
         * @param {number} cpY2 - Second Control point y
         * @param {number} toX - Destination point x
         * @param {number} toY - Destination point y
         * @return {number} Length of bezier curve
         */
        static curveLength(fromX: number, fromY: number, cpX: number, cpY: number, cpX2: number, cpY2: number, toX: number, toY: number): number;
        /**
         * Calculate the points for a bezier curve and then draws it.
         *
         * Ignored from docs since it is not directly exposed.
         *
         * @ignore
         * @param {number} cpX - Control point x
         * @param {number} cpY - Control point y
         * @param {number} cpX2 - Second Control point x
         * @param {number} cpY2 - Second Control point y
         * @param {number} toX - Destination point x
         * @param {number} toY - Destination point y
         * @param {number[]} points - Path array to push points into
         */
        static curveTo(cpX: number, cpY: number, cpX2: number, cpY2: number, toX: number, toY: number, points: Array<number>): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Builds a circle to draw
     *
     * Ignored from docs since it is not directly exposed.
     *
     * @ignore
     * @private
     * @param {PIXI.WebGLGraphicsData} graphicsData - The graphics object to draw
     * @param {object} webGLData - an object containing all the WebGL-specific information to create this shape
     * @param {object} webGLDataNativeLines - an object containing all the WebGL-specific information to create nativeLines
     */
    const buildCircle: IShapeBuildCommand;
}
declare namespace feng3d.pixi {
    /**
     * Builds a line to draw
     *
     * Ignored from docs since it is not directly exposed.
     *
     * @ignore
     * @private
     * @param {PIXI.GraphicsData} graphicsData - The graphics object containing all the necessary properties
     * @param {PIXI.GraphicsGeometry} graphicsGeometry - Geometry where to append output
     */
    function buildLine(graphicsData: GraphicsData, graphicsGeometry: GraphicsGeometry): void;
}
declare namespace feng3d.pixi {
    /**
     * Builds a polygon to draw
     *
     * Ignored from docs since it is not directly exposed.
     *
     * @ignore
     * @private
     * @param {PIXI.WebGLGraphicsData} graphicsData - The graphics object containing all the necessary properties
     * @param {object} webGLData - an object containing all the WebGL-specific information to create this shape
     * @param {object} webGLDataNativeLines - an object containing all the WebGL-specific information to create nativeLines
     */
    const buildPoly: IShapeBuildCommand;
}
declare namespace feng3d.pixi {
    /**
     * Builds a rectangle to draw
     *
     * Ignored from docs since it is not directly exposed.
     *
     * @ignore
     * @private
     * @param {PIXI.WebGLGraphicsData} graphicsData - The graphics object containing all the necessary properties
     * @param {object} webGLData - an object containing all the WebGL-specific information to create this shape
     * @param {object} webGLDataNativeLines - an object containing all the WebGL-specific information to create nativeLines
     */
    const buildRectangle: IShapeBuildCommand;
}
declare namespace feng3d.pixi {
    /**
     * Builds a rounded rectangle to draw
     *
     * Ignored from docs since it is not directly exposed.
     *
     * @ignore
     * @private
     * @param {PIXI.WebGLGraphicsData} graphicsData - The graphics object containing all the necessary properties
     * @param {object} webGLData - an object containing all the WebGL-specific information to create this shape
     * @param {object} webGLDataNativeLines - an object containing all the WebGL-specific information to create nativeLines
     */
    const buildRoundedRectangle: IShapeBuildCommand;
}
declare namespace feng3d.pixi {
    /**
     * Map of fill commands for each shape type.
     *.graphicsUtils
     * @member {Object} FILL_COMMANDS
     */
    const FILL_COMMANDS: Record<SHAPES, IShapeBuildCommand>;
    /**
     * Batch pool, stores unused batches for preventing allocations.
     *.graphicsUtils
     * @member {Array<PIXI.graphicsUtils.BatchPart>} BATCH_POOL
     */
    const BATCH_POOL: Array<BatchPart>;
    /**
     * Draw call pool, stores unused draw calls for preventing allocations.
     *.graphicsUtils
     * @member {Array<PIXI.BatchDrawCall>} DRAW_CALL_POOL
     */
    const DRAW_CALL_POOL: Array<BatchDrawCall>;
}
declare namespace feng3d.pixi {
    interface IShapeBuildCommand {
        build(graphicsData: GraphicsData): void;
        triangulate(graphicsData: GraphicsData, target: GraphicsGeometry): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Utilities for quadratic curves
     * @private
     */
    class QuadraticUtils {
        /**
         * Calculate length of quadratic curve
         * @see {@link http://www.malczak.linuxpl.com/blog/quadratic-bezier-curve-length/}
         * for the detailed explanation of math behind this.
         *
         * @private
         * @param {number} fromX - x-coordinate of curve start point
         * @param {number} fromY - y-coordinate of curve start point
         * @param {number} cpX - x-coordinate of curve control point
         * @param {number} cpY - y-coordinate of curve control point
         * @param {number} toX - x-coordinate of curve end point
         * @param {number} toY - y-coordinate of curve end point
         * @return {number} Length of quadratic curve
         */
        static curveLength(fromX: number, fromY: number, cpX: number, cpY: number, toX: number, toY: number): number;
        /**
         * Calculate the points for a quadratic bezier curve and then draws it.
         * Based on: https://stackoverflow.com/questions/785097/how-do-i-implement-a-bezier-curve-in-c
         *
         * @private
         * @param {number} cpX - Control point x
         * @param {number} cpY - Control point y
         * @param {number} toX - Destination point x
         * @param {number} toY - Destination point y
         * @param {number[]} points - Points to add segments to.
         */
        static curveTo(cpX: number, cpY: number, toX: number, toY: number, points: Array<number>): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Draw a star shape with an arbitrary number of points.
     *
     * @param {number} x - Center X position of the star
     * @param {number} y - Center Y position of the star
     * @param {number} points - The number of points of the star, must be > 1
     * @param {number} radius - The outer radius of the star
     * @param {number} [innerRadius] - The inner radius between points, default half `radius`
     * @param {number} [rotation=0] - The rotation of the star in radians, where 0 is vertical
     * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
     */
    class Star extends Polygon {
        constructor(x: number, y: number, points: number, radius: number, innerRadius: number, rotation?: number);
    }
}
declare namespace feng3d.pixi {
    /**
     * Supported line joints in `PIXI.LineStyle` for graphics.
     *
     * @see PIXI.Graphics#lineStyle
     * @see https://graphicdesign.stackexchange.com/questions/59018/what-is-a-bevel-join-of-two-lines-exactly-illustrator
     *
     * @name LINE_JOIN
     * @static
     * @enum {string}
     * @property {string} MITER - 'miter': make a sharp corner where outer part of lines meet
     * @property {string} BEVEL - 'bevel': add a square butt at each end of line segment and fill the triangle at turn
     * @property {string} ROUND - 'round': add an arc at the joint
     */
    enum LINE_JOIN {
        MITER = "miter",
        BEVEL = "bevel",
        ROUND = "round"
    }
    /**
     * Support line caps in `PIXI.LineStyle` for graphics.
     *
     * @see PIXI.Graphics#lineStyle
     *
     * @name LINE_CAP
     * @static
     * @enum {string}
     * @property {string} BUTT - 'butt': don't add any cap at line ends (leaves orthogonal edges)
     * @property {string} ROUND - 'round': add semicircle at ends
     * @property {string} SQUARE - 'square': add square at end (like `BUTT` except more length at end)
     */
    enum LINE_CAP {
        BUTT = "butt",
        ROUND = "round",
        SQUARE = "square"
    }
    interface IGraphicsCurvesSettings {
        adaptive: boolean;
        maxLength: number;
        minSegments: number;
        maxSegments: number;
        epsilon: number;
        _segmentsCount(length: number, defaultSegments?: number): number;
    }
    /**
     * Graphics curves resolution settings. If `adaptive` flag is set to `true`,
     * the resolution is calculated based on the curve's length to ensure better visual quality.
     * Adaptive draw works with `bezierCurveTo` and `quadraticCurveTo`.
     *
     * @static
     * @constant
     * @name GRAPHICS_CURVES
     * @type {object}
     * @property {boolean} adaptive=true - flag indicating if the resolution should be adaptive
     * @property {number} maxLength=10 - maximal length of a single segment of the curve (if adaptive = false, ignored)
     * @property {number} minSegments=8 - minimal number of segments in the curve (if adaptive = false, ignored)
     * @property {number} maxSegments=2048 - maximal number of segments in the curve (if adaptive = false, ignored)
     */
    const GRAPHICS_CURVES: IGraphicsCurvesSettings;
}
declare namespace feng3d.pixi {
    /**
     * Batch element computed from Graphics geometry
     */
    interface IGraphicsBatchElement {
        vertexData: Float32Array;
        blendMode: BLEND_MODES;
        indices: Uint16Array | Uint32Array;
        uvs: Float32Array;
        alpha: number;
        worldAlpha: number;
        _batchRGB: number[];
        _tintRGB: number;
        _texture: Texture;
    }
    interface IFillStyleOptions {
        color?: number;
        alpha?: number;
        texture?: Texture;
        matrix?: Matrix;
    }
    interface ILineStyleOptions extends IFillStyleOptions {
        width?: number;
        alignment?: number;
        native?: boolean;
        cap?: LINE_CAP;
        join?: LINE_JOIN;
        miterLimit?: number;
    }
    /**
     * The Graphics class is primarily used to render primitive shapes such as lines, circles and
     * rectangles to the display, and to color and fill them.  However, you can also use a Graphics
     * object to build a list of primitives to use as a mask, or as a complex hitArea.
     *
     * Please note that due to legacy naming conventions, the behavior of some functions in this class
     * can be confusing.  Each call to `drawRect()`, `drawPolygon()`, etc. actually stores that primitive
     * in the Geometry class's GraphicsGeometry object for later use in rendering or hit testing - the
     * functions do not directly draw anything to the screen.  Similarly, the `clear()` function doesn't
     * change the screen, it simply resets the list of primitives, which can be useful if you want to
     * rebuild the contents of an existing Graphics object.
     *
     * Once a GraphicsGeometry list is built, you can re-use it in other Geometry objects as
     * an optimization, by passing it into a new Geometry object's constructor.  Because of this
     * ability, it's important to call `destroy()` on Geometry objects once you are done with them, to
     * properly dereference each GraphicsGeometry and prevent memory leaks.
     *
     */
    class Graphics extends Container {
        /**
         * Temporary point to use for containsPoint
         *
         * @static
         * @private
         * @member {PIXI.Point}
         */
        static _TEMP_POINT: Vector2;
        shader: Shader;
        pluginName: string;
        protected currentPath: Polygon;
        protected batches: Array<IGraphicsBatchElement>;
        protected batchTint: number;
        protected batchDirty: number;
        protected vertexData: Float32Array;
        protected _fillStyle: FillStyle;
        protected _lineStyle: LineStyle;
        protected _matrix: Matrix;
        protected _holeMode: boolean;
        protected _transformID: number;
        protected _tint: number;
        private state;
        private _geometry;
        /**
         * Includes vertex positions, face indices, normals, colors, UVs, and
         * custom attributes within buffers, reducing the cost of passing all
         * this data to the GPU. Can be shared between multiple Mesh or Graphics objects.
         *
         * @member {PIXI.GraphicsGeometry}
         * @readonly
         */
        get geometry(): GraphicsGeometry;
        /**
         * @param {PIXI.GraphicsGeometry} [geometry=null] - Geometry to use, if omitted
         *        will create a new GraphicsGeometry instance.
         */
        constructor(geometry?: GraphicsGeometry);
        /**
         * Creates a new Graphics object with the same values as this one.
         * Note that only the geometry of the object is cloned, not its transform (position,scale,etc)
         *
         * @return {PIXI.Graphics} A clone of the graphics object
         */
        clone(): Graphics;
        /**
         * The blend mode to be applied to the graphic shape. Apply a value of
         * `PIXI.BLEND_MODES.NORMAL` to reset the blend mode.  Note that, since each
         * primitive in the GraphicsGeometry list is rendered sequentially, modes
         * such as `PIXI.BLEND_MODES.ADD` and `PIXI.BLEND_MODES.MULTIPLY` will
         * be applied per-primitive.
         *
         * @member {number}
         * @default PIXI.BLEND_MODES.NORMAL;
         * @see PIXI.BLEND_MODES
         */
        set blendMode(value: BLEND_MODES);
        get blendMode(): BLEND_MODES;
        /**
         * The tint applied to each graphic shape. This is a hex value. A value of
         * 0xFFFFFF will remove any tint effect.
         *
         * @member {number}
         * @default 0xFFFFFF
         */
        get tint(): number;
        set tint(value: number);
        /**
         * The current fill style.
         *
         * @member {PIXI.FillStyle}
         * @readonly
         */
        get fill(): FillStyle;
        /**
         * The current line style.
         *
         * @member {PIXI.LineStyle}
         * @readonly
         */
        get line(): LineStyle;
        /**
         * Specifies the line style used for subsequent calls to Graphics methods such as the lineTo()
         * method or the drawCircle() method.
         *
         * @param {number} [width=0] - width of the line to draw, will update the objects stored style
         * @param {number} [color=0x0] - color of the line to draw, will update the objects stored style
         * @param {number} [alpha=1] - alpha of the line to draw, will update the objects stored style
         * @param {number} [alignment=0.5] - alignment of the line to draw, (0 = inner, 0.5 = middle, 1 = outer).
         *        WebGL only.
         * @param {boolean} [native=false] - If true the lines will be draw using LINES instead of TRIANGLE_STRIP
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        lineStyle(width: number, color?: number, alpha?: number, alignment?: number, native?: boolean): this;
        /**
         * Specifies the line style used for subsequent calls to Graphics methods such as the lineTo()
         * method or the drawCircle() method.
         *
         * @param {object} [options] - Line style options
         * @param {number} [options.width=0] - width of the line to draw, will update the objects stored style
         * @param {number} [options.color=0x0] - color of the line to draw, will update the objects stored style
         * @param {number} [options.alpha=1] - alpha of the line to draw, will update the objects stored style
         * @param {number} [options.alignment=0.5] - alignment of the line to draw, (0 = inner, 0.5 = middle, 1 = outer).
         *        WebGL only.
         * @param {boolean} [options.native=false] - If true the lines will be draw using LINES instead of TRIANGLE_STRIP
         * @param {PIXI.LINE_CAP}[options.cap=PIXI.LINE_CAP.BUTT] - line cap style
         * @param {PIXI.LINE_JOIN}[options.join=PIXI.LINE_JOIN.MITER] - line join style
         * @param {number}[options.miterLimit=10] - miter limit ratio
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        lineStyle(options?: ILineStyleOptions): this;
        /**
         * Like line style but support texture for line fill.
         *
         * @param {object} [options] - Collection of options for setting line style.
         * @param {number} [options.width=0] - width of the line to draw, will update the objects stored style
         * @param {PIXI.Texture} [options.texture=PIXI.Texture.WHITE] - Texture to use
         * @param {number} [options.color=0x0] - color of the line to draw, will update the objects stored style.
         *  Default 0xFFFFFF if texture present.
         * @param {number} [options.alpha=1] - alpha of the line to draw, will update the objects stored style
         * @param {PIXI.Matrix} [options.matrix=null] - Texture matrix to transform texture
         * @param {number} [options.alignment=0.5] - alignment of the line to draw, (0 = inner, 0.5 = middle, 1 = outer).
         *        WebGL only.
         * @param {boolean} [options.native=false] - If true the lines will be draw using LINES instead of TRIANGLE_STRIP
         * @param {PIXI.LINE_CAP}[options.cap=PIXI.LINE_CAP.BUTT] - line cap style
         * @param {PIXI.LINE_JOIN}[options.join=PIXI.LINE_JOIN.MITER] - line join style
         * @param {number}[options.miterLimit=10] - miter limit ratio
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        lineTextureStyle(options: ILineStyleOptions): this;
        /**
         * Start a polygon object internally
         * @protected
         */
        protected startPoly(): void;
        /**
         * Finish the polygon object.
         * @protected
         */
        finishPoly(): void;
        /**
         * Moves the current drawing position to x, y.
         *
         * @param {number} x - the X coordinate to move to
         * @param {number} y - the Y coordinate to move to
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        moveTo(x: number, y: number): this;
        /**
         * Draws a line using the current line style from the current drawing position to (x, y);
         * The current drawing position is then set to (x, y).
         *
         * @param {number} x - the X coordinate to draw to
         * @param {number} y - the Y coordinate to draw to
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        lineTo(x: number, y: number): this;
        /**
         * Initialize the curve
         *
         * @protected
         * @param {number} [x=0]
         * @param {number} [y=0]
         */
        protected _initCurve(x?: number, y?: number): void;
        /**
         * Calculate the points for a quadratic bezier curve and then draws it.
         * Based on: https://stackoverflow.com/questions/785097/how-do-i-implement-a-bezier-curve-in-c
         *
         * @param {number} cpX - Control point x
         * @param {number} cpY - Control point y
         * @param {number} toX - Destination point x
         * @param {number} toY - Destination point y
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        quadraticCurveTo(cpX: number, cpY: number, toX: number, toY: number): this;
        /**
         * Calculate the points for a bezier curve and then draws it.
         *
         * @param {number} cpX - Control point x
         * @param {number} cpY - Control point y
         * @param {number} cpX2 - Second Control point x
         * @param {number} cpY2 - Second Control point y
         * @param {number} toX - Destination point x
         * @param {number} toY - Destination point y
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        bezierCurveTo(cpX: number, cpY: number, cpX2: number, cpY2: number, toX: number, toY: number): this;
        /**
         * The arcTo() method creates an arc/curve between two tangents on the canvas.
         *
         * "borrowed" from https://code.google.com/p/fxcanvas/ - thanks google!
         *
         * @param {number} x1 - The x-coordinate of the first tangent point of the arc
         * @param {number} y1 - The y-coordinate of the first tangent point of the arc
         * @param {number} x2 - The x-coordinate of the end of the arc
         * @param {number} y2 - The y-coordinate of the end of the arc
         * @param {number} radius - The radius of the arc
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        arcTo(x1: number, y1: number, x2: number, y2: number, radius: number): this;
        /**
         * The arc method creates an arc/curve (used to create circles, or parts of circles).
         *
         * @param {number} cx - The x-coordinate of the center of the circle
         * @param {number} cy - The y-coordinate of the center of the circle
         * @param {number} radius - The radius of the circle
         * @param {number} startAngle - The starting angle, in radians (0 is at the 3 o'clock position
         *  of the arc's circle)
         * @param {number} endAngle - The ending angle, in radians
         * @param {boolean} [anticlockwise=false] - Specifies whether the drawing should be
         *  counter-clockwise or clockwise. False is default, and indicates clockwise, while true
         *  indicates counter-clockwise.
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        arc(cx: number, cy: number, radius: number, startAngle: number, endAngle: number, anticlockwise?: boolean): this;
        /**
         * Specifies a simple one-color fill that subsequent calls to other Graphics methods
         * (such as lineTo() or drawCircle()) use when drawing.
         *
         * @param {number} [color=0] - the color of the fill
         * @param {number} [alpha=1] - the alpha of the fill
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        beginFill(color?: number, alpha?: number): this;
        /**
         * Begin the texture fill
         *
         * @param {object} [options] - Object object.
         * @param {PIXI.Texture} [options.texture=PIXI.Texture.WHITE] - Texture to fill
         * @param {number} [options.color=0xffffff] - Background to fill behind texture
         * @param {number} [options.alpha=1] - Alpha of fill
         * @param {PIXI.Matrix} [options.matrix=null] - Transform matrix
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        beginTextureFill(options?: IFillStyleOptions): this;
        /**
         * Applies a fill to the lines and shapes that were added since the last call to the beginFill() method.
         *
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        endFill(): this;
        /**
         * Draws a rectangle shape.
         *
         * @param {number} x - The X coord of the top-left of the rectangle
         * @param {number} y - The Y coord of the top-left of the rectangle
         * @param {number} width - The width of the rectangle
         * @param {number} height - The height of the rectangle
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        drawRect(x: number, y: number, width: number, height: number): this;
        /**
         * Draw a rectangle shape with rounded/beveled corners.
         *
         * @param {number} x - The X coord of the top-left of the rectangle
         * @param {number} y - The Y coord of the top-left of the rectangle
         * @param {number} width - The width of the rectangle
         * @param {number} height - The height of the rectangle
         * @param {number} radius - Radius of the rectangle corners
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        drawRoundedRect(x: number, y: number, width: number, height: number, radius: number): this;
        /**
         * Draws a circle.
         *
         * @param {number} x - The X coordinate of the center of the circle
         * @param {number} y - The Y coordinate of the center of the circle
         * @param {number} radius - The radius of the circle
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        drawCircle(x: number, y: number, radius: number): this;
        /**
         * Draws an ellipse.
         *
         * @param {number} x - The X coordinate of the center of the ellipse
         * @param {number} y - The Y coordinate of the center of the ellipse
         * @param {number} width - The half width of the ellipse
         * @param {number} height - The half height of the ellipse
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        drawEllipse(x: number, y: number, width: number, height: number): this;
        drawPolygon(...path: Array<number> | Array<Vector2>): this;
        drawPolygon(path: Array<number> | Array<Vector2> | Polygon): this;
        /**
         * Draw a star shape with an arbitrary number of points.
         *
         * @param {number} x - Center X position of the star
         * @param {number} y - Center Y position of the star
         * @param {number} points - The number of points of the star, must be > 1
         * @param {number} radius - The outer radius of the star
         * @param {number} [innerRadius] - The inner radius between points, default half `radius`
         * @param {number} [rotation=0] - The rotation of the star in radians, where 0 is vertical
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        drawStar(x: number, y: number, points: number, radius: number, innerRadius: number, rotation?: number): this;
        /**
         * Draw any shape.
         *
         * @param {PIXI.Circle|PIXI.Ellipse|PIXI.Polygon|PIXI.Rectangle|PIXI.RoundedRectangle} shape - Shape to draw
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        drawShape(shape: IShape): this;
        /**
         * Clears the graphics that were drawn to this Graphics object, and resets fill and line style settings.
         *
         * @return {PIXI.Graphics} This Graphics object. Good for chaining method calls
         */
        clear(): this;
        /**
         * True if graphics consists of one rectangle, and thus, can be drawn like a Sprite and
         * masked with gl.scissor.
         *
         * @returns {boolean} True if only 1 rect.
         */
        isFastRect(): boolean;
        /**
         * Renders the object using the WebGL renderer
         *
         * @protected
         * @param {PIXI.Renderer} renderer - The renderer
         */
        _render(renderer: Renderer): void;
        /**
         * Populating batches for rendering
         *
         * @protected
         */
        protected _populateBatches(): void;
        /**
         * Renders the batches using the BathedRenderer plugin
         *
         * @protected
         * @param {PIXI.Renderer} renderer - The renderer
         */
        protected _renderBatched(renderer: Renderer): void;
        /**
         * Renders the graphics direct
         *
         * @protected
         * @param {PIXI.Renderer} renderer - The renderer
         */
        protected _renderDirect(renderer: Renderer): void;
        /**
         * Renders specific DrawCall
         *
         * @param {PIXI.Renderer} renderer
         * @param {PIXI.BatchDrawCall} drawCall
         */
        protected _renderDrawCallDirect(renderer: Renderer, drawCall: BatchDrawCall): void;
        /**
         * Resolves shader for direct rendering
         *
         * @protected
         * @param {PIXI.Renderer} renderer - The renderer
         */
        protected _resolveDirectShader(renderer: Renderer): Shader;
        /**
         * Retrieves the bounds of the graphic shape as a rectangle object
         *
         * @protected
         */
        _calculateBounds(): void;
        /**
         * Tests if a point is inside this graphics object
         *
         * @param {PIXI.IPointData} point - the point to test
         * @return {boolean} the result of the test
         */
        containsPoint(point: Vector2): boolean;
        /**
         * Recalculate the tint by applying tint to batches using Graphics tint.
         * @protected
         */
        protected calculateTints(): void;
        /**
         * If there's a transform update or a change to the shape of the
         * geometry, recalculate the vertices.
         * @protected
         */
        protected calculateVertices(): void;
        /**
         * Closes the current path.
         *
         * @return {PIXI.Graphics} Returns itself.
         */
        closePath(): this;
        /**
         * Apply a matrix to the positional data.
         *
         * @param {PIXI.Matrix} matrix - Matrix to use for transform current shape.
         * @return {PIXI.Graphics} Returns itself.
         */
        setMatrix(matrix: Matrix): this;
        /**
         * Begin adding holes to the last draw shape
         * IMPORTANT: holes must be fully inside a shape to work
         * Also weirdness ensues if holes overlap!
         * Ellipses, Circles, Rectangles and Rounded Rectangles cannot be holes or host for holes in CanvasRenderer,
         * please use `moveTo` `lineTo`, `quadraticCurveTo` if you rely on pixi-legacy bundle.
         * @return {PIXI.Graphics} Returns itself.
         */
        beginHole(): this;
        /**
         * End adding holes to the last draw shape
         * @return {PIXI.Graphics} Returns itself.
         */
        endHole(): this;
        /**
         * Destroys the Graphics object.
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all
         *  options have been set to that value
         * @param {boolean} [options.children=false] - if set to true, all the children will have
         *  their destroy method called as well. 'options' will be passed on to those calls.
         * @param {boolean} [options.texture=false] - Only used for child Sprites if options.children is set to true
         *  Should it destroy the texture of the child sprite
         * @param {boolean} [options.baseTexture=false] - Only used for child Sprites if options.children is set to true
         *  Should it destroy the base texture of the child sprite
         */
        destroy(options?: IDestroyOptions | boolean): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * A class to contain data useful for Graphics objects
     *
     */
    class GraphicsData {
        shape: IShape;
        lineStyle: LineStyle;
        fillStyle: FillStyle;
        matrix: Matrix;
        type: SHAPES;
        points: number[];
        holes: Array<GraphicsData>;
        /**
         *
         * @param {PIXI.Circle|PIXI.Ellipse|PIXI.Polygon|PIXI.Rectangle|PIXI.RoundedRectangle} shape - The shape object to draw.
         * @param {PIXI.FillStyle} [fillStyle] - the width of the line to draw
         * @param {PIXI.LineStyle} [lineStyle] - the color of the line to draw
         * @param {PIXI.Matrix} [matrix] - Transform matrix
         */
        constructor(shape: IShape, fillStyle?: FillStyle, lineStyle?: LineStyle, matrix?: Matrix);
        /**
         * Creates a new GraphicsData object with the same values as this one.
         *
         * @return {PIXI.GraphicsData} Cloned GraphicsData object
         */
        clone(): GraphicsData;
        /**
         * Destroys the Graphics data.
         *
         */
        destroy(): void;
    }
}
declare namespace feng3d.pixi {
    type IShape = Circle | Ellipse | Polygon | Rectangle | RoundedRectangle;
    /**
     * The Graphics class contains methods used to draw primitive shapes such as lines, circles and
     * rectangles to the display, and to color and fill them.
     *
     * GraphicsGeometry is designed to not be continually updating the geometry since it's expensive
     * to re-tesselate using **earcut**. Consider using {@link PIXI.Mesh} for this use-case, it's much faster.
     *
     */
    export class GraphicsGeometry extends BatchGeometry {
        /**
         * The maximum number of points to consider an object "batchable",
         * able to be batched by the renderer's batch system.
         *
         * @memberof PIXI.GraphicsGeometry
         * @static
         * @member {number} BATCHABLE_SIZE
         * @default 100
         */
        static BATCHABLE_SIZE: number;
        closePointEps: number;
        boundsPadding: number;
        uvsFloat32: Float32Array;
        indicesUint16: Uint16Array | Uint32Array;
        batchable: boolean;
        points: Array<number>;
        colors: Array<number>;
        uvs: Array<number>;
        indices: Array<number>;
        textureIds: Array<number>;
        graphicsData: Array<GraphicsData>;
        drawCalls: Array<BatchDrawCall>;
        batchDirty: number;
        batches: Array<BatchPart>;
        protected dirty: number;
        protected cacheDirty: number;
        protected clearDirty: number;
        protected shapeIndex: number;
        protected _bounds: Bounds;
        protected boundsDirty: number;
        constructor();
        /**
         * Get the current bounds of the graphic geometry.
         *
         * @member {PIXI.Bounds}
         * @readonly
         */
        get bounds(): Bounds;
        /**
         * Call if you changed graphicsData manually.
         * Empties all batch buffers.
         */
        protected invalidate(): void;
        /**
         * Clears the graphics that were drawn to this Graphics object, and resets fill and line style settings.
         *
         * @return {PIXI.GraphicsGeometry} This GraphicsGeometry object. Good for chaining method calls
         */
        clear(): GraphicsGeometry;
        /**
         * Draws the given shape to this Graphics object. Can be any of Circle, Rectangle, Ellipse, Line or Polygon.
         *
         * @param {PIXI.Circle|PIXI.Ellipse|PIXI.Polygon|PIXI.Rectangle|PIXI.RoundedRectangle} shape - The shape object to draw.
         * @param {PIXI.FillStyle} fillStyle - Defines style of the fill.
         * @param {PIXI.LineStyle} lineStyle - Defines style of the lines.
         * @param {PIXI.Matrix} matrix - Transform applied to the points of the shape.
         * @return {PIXI.GraphicsGeometry} Returns geometry for chaining.
         */
        drawShape(shape: IShape, fillStyle?: FillStyle, lineStyle?: LineStyle, matrix?: Matrix): GraphicsGeometry;
        /**
         * Draws the given shape to this Graphics object. Can be any of Circle, Rectangle, Ellipse, Line or Polygon.
         *
         * @param {PIXI.Circle|PIXI.Ellipse|PIXI.Polygon|PIXI.Rectangle|PIXI.RoundedRectangle} shape - The shape object to draw.
         * @param {PIXI.Matrix} matrix - Transform applied to the points of the shape.
         * @return {PIXI.GraphicsGeometry} Returns geometry for chaining.
         */
        drawHole(shape: IShape, matrix?: Matrix): GraphicsGeometry;
        /**
         * Destroys the GraphicsGeometry object.
         *
         */
        destroy(): void;
        /**
         * Check to see if a point is contained within this geometry.
         *
         * @param {PIXI.IPointData} point - Point to check if it's contained.
         * @return {Boolean} `true` if the point is contained within geometry.
         */
        containsPoint(point: Vector2): boolean;
        /**
         * Generates intermediate batch data. Either gets converted to drawCalls
         * or used to convert to batch objects directly by the Graphics object.
         *
         * @param {boolean} [allow32Indices] - Allow using 32-bit indices for preventing artifacts when more that 65535 vertices
         */
        updateBatches(allow32Indices?: boolean): void;
        /**
         * Affinity check
         *
         * @param {PIXI.FillStyle | PIXI.LineStyle} styleA
         * @param {PIXI.FillStyle | PIXI.LineStyle} styleB
         */
        protected _compareStyles(styleA: FillStyle | LineStyle, styleB: FillStyle | LineStyle): boolean;
        /**
         * Test geometry for batching process.
         *
         * @protected
         */
        protected validateBatching(): boolean;
        /**
         * Offset the indices so that it works with the batcher.
         *
         * @protected
         */
        protected packBatches(): void;
        /**
         * Checks to see if this graphics geometry can be batched.
         * Currently it needs to be small enough and not contain any native lines.
         *
         * @protected
         */
        protected isBatchable(): boolean;
        /**
         * Converts intermediate batches data to drawCalls.
         *
         * @protected
         */
        protected buildDrawCalls(): void;
        /**
         * Packs attributes to single buffer.
         *
         * @protected
         */
        protected packAttributes(): void;
        /**
         * Process fill part of Graphics.
         *
         * @param {PIXI.GraphicsData} data
         * @protected
         */
        protected processFill(data: GraphicsData): void;
        /**
         * Process line part of Graphics.
         *
         * @param {PIXI.GraphicsData} data
         * @protected
         */
        protected processLine(data: GraphicsData): void;
        /**
         * Process the holes data.
         *
         * @param {PIXI.GraphicsData[]} holes - Holes to render
         * @protected
         */
        protected processHoles(holes: Array<GraphicsData>): void;
        /**
         * Update the local bounds of the object. Expensive to use performance-wise.
         *
         * @protected
         */
        protected calculateBounds(): void;
        /**
         * Transform points using matrix.
         *
         * @protected
         * @param {number[]} points - Points to transform
         * @param {PIXI.Matrix} matrix - Transform matrix
         */
        protected transformPoints(points: Array<number>, matrix: Matrix): void;
        /**
         * Add colors.
         *
         * @protected
         * @param {number[]} colors - List of colors to add to
         * @param {number} color - Color to add
         * @param {number} alpha - Alpha to use
         * @param {number} size - Number of colors to add
         */
        protected addColors(colors: Array<number>, color: number, alpha: number, size: number): void;
        /**
         * Add texture id that the shader/fragment wants to use.
         *
         * @protected
         * @param {number[]} textureIds
         * @param {number} id
         * @param {number} size
         */
        protected addTextureIds(textureIds: Array<number>, id: number, size: number): void;
        /**
         * Generates the UVs for a shape.
         *
         * @protected
         * @param {number[]} verts - Vertices
         * @param {number[]} uvs - UVs
         * @param {PIXI.Texture} texture - Reference to Texture
         * @param {number} start - Index buffer start index.
         * @param {number} size - The size/length for index buffer.
         * @param {PIXI.Matrix} [matrix] - Optional transform for all points.
         */
        protected addUvs(verts: Array<number>, uvs: Array<number>, texture: Texture, start: number, size: number, matrix?: Matrix): void;
        /**
         * Modify uvs array according to position of texture region
         * Does not work with rotated or trimmed textures
         *
         * @param {number[]} uvs - array
         * @param {PIXI.Texture} texture - region
         * @param {number} start - starting index for uvs
         * @param {number} size - how many points to adjust
         */
        protected adjustUvs(uvs: Array<number>, texture: Texture, start: number, size: number): void;
    }
    export {};
}
declare namespace feng3d.pixi {
    const graphicsUtils: {
        buildPoly: IShapeBuildCommand;
        buildCircle: IShapeBuildCommand;
        buildRectangle: IShapeBuildCommand;
        buildRoundedRectangle: IShapeBuildCommand;
        buildLine: typeof buildLine;
        ArcUtils: typeof ArcUtils;
        BezierUtils: typeof BezierUtils;
        QuadraticUtils: typeof QuadraticUtils;
        BatchPart: typeof BatchPart;
        FILL_COMMANDS: Record<SHAPES, IShapeBuildCommand>;
        BATCH_POOL: BatchPart[];
        DRAW_CALL_POOL: BatchDrawCall[];
    };
}
declare namespace feng3d.pixi {
    const mesh_frag = "\nvarying vec2 vTextureCoord;\nuniform vec4 uColor;\n\nuniform sampler2D uSampler;\n\nvoid main(void)\n{\n    gl_FragColor = texture2D(uSampler, vTextureCoord) * uColor;\n}\n";
}
declare namespace feng3d.pixi {
    const mesh_vert = "\nattribute vec2 aVertexPosition;\nattribute vec2 aTextureCoord;\n\nuniform mat3 projectionMatrix;\nuniform mat3 translationMatrix;\nuniform mat3 uTextureMatrix;\n\nvarying vec2 vTextureCoord;\n\nvoid main(void)\n{\n    gl_Position = vec4((projectionMatrix * translationMatrix * vec3(aVertexPosition, 1.0)).xy, 0.0, 1.0);\n\n    vTextureCoord = (uTextureMatrix * vec3(aTextureCoord, 1.0)).xy;\n}\n";
}
declare namespace feng3d.pixi {
    interface Mesh {
        _canvasPadding: number;
        canvasPadding: number;
        _cachedTint: number;
        _tintedCanvas: HTMLCanvasElement;
        _cachedTexture: Texture;
    }
    interface NineSlicePlane {
        _cachedTint: number;
        _tintedCanvas: HTMLCanvasElement;
        _canvasUvs: number[];
    }
    /**
     * Base mesh class.
     *
     * This class empowers you to have maximum flexibility to render any kind of WebGL visuals you can think of.
     * This class assumes a certain level of WebGL knowledge.
     * If you know a bit this should abstract enough away to make you life easier!
     *
     * Pretty much ALL WebGL can be broken down into the following:
     * - Geometry - The structure and data for the mesh. This can include anything from positions, uvs, normals, colors etc..
     * - Shader - This is the shader that PixiJS will render the geometry with (attributes in the shader must match the geometry)
     * - State - This is the state of WebGL required to render the mesh.
     *
     * Through a combination of the above elements you can render anything you want, 2D or 3D!
     *
     */
    class Mesh<T extends Shader = MeshMaterial> extends Container {
        geometry: Geometry;
        shader: T;
        state: State;
        drawMode: DRAW_MODES;
        start: number;
        size: number;
        private vertexData;
        private vertexDirty;
        private _transformID;
        private _roundPixels;
        private batchUvs;
        uvs: Float32Array;
        indices: Uint16Array;
        _tintRGB: number;
        _texture: Texture;
        /**
         * @param {PIXI.Geometry} geometry - the geometry the mesh will use
         * @param {PIXI.MeshMaterial} shader - the shader the mesh will use
         * @param {PIXI.State} [state] - the state that the WebGL context is required to be in to render the mesh
         *        if no state is provided, uses {@link PIXI.State.for2d} to create a 2D state for PixiJS.
         * @param {number} [drawMode=PIXI.DRAW_MODES.TRIANGLES] - the drawMode, can be any of the PIXI.DRAW_MODES consts
         */
        constructor(geometry?: Geometry, shader?: T, state?: State, drawMode?: DRAW_MODES);
        /**
         * To change mesh uv's, change its uvBuffer data and increment its _updateID.
         * @member {PIXI.Buffer}
         * @readonly
         */
        get uvBuffer(): Buffer;
        /**
         * To change mesh vertices, change its uvBuffer data and increment its _updateID.
         * Incrementing _updateID is optional because most of Mesh objects do it anyway.
         * @member {PIXI.Buffer}
         * @readonly
         */
        get verticesBuffer(): Buffer;
        /**
         * Alias for {@link PIXI.Mesh#shader}.
         * @member {PIXI.MeshMaterial}
         */
        set material(value: T);
        get material(): T;
        /**
         * The blend mode to be applied to the Mesh. Apply a value of
         * `PIXI.BLEND_MODES.NORMAL` to reset the blend mode.
         *
         * @member {number}
         * @default PIXI.BLEND_MODES.NORMAL;
         * @see PIXI.BLEND_MODES
         */
        set blendMode(value: BLEND_MODES);
        get blendMode(): BLEND_MODES;
        /**
         * If true PixiJS will Math.floor() x/y values when rendering, stopping pixel interpolation.
         * Advantages can include sharper image quality (like text) and faster rendering on canvas.
         * The main disadvantage is movement of objects may appear less smooth.
         * To set the global default, change {@link PIXI.settings.ROUND_PIXELS}
         *
         * @member {boolean}
         * @default false
         */
        set roundPixels(value: boolean);
        get roundPixels(): boolean;
        /**
         * The multiply tint applied to the Mesh. This is a hex value. A value of
         * `0xFFFFFF` will remove any tint effect.
         *
         * Null for non-MeshMaterial shaders
         * @member {number}
         * @default 0xFFFFFF
         */
        get tint(): number;
        set tint(value: number);
        /**
         * The texture that the Mesh uses.
         *
         * Null for non-MeshMaterial shaders
         * @member {PIXI.Texture}
         */
        get texture(): Texture;
        set texture(value: Texture);
        /**
         * Standard renderer draw.
         * @protected
         * @param {PIXI.Renderer} renderer - Instance to renderer.
         */
        _render(renderer: Renderer): void;
        /**
         * Standard non-batching way of rendering.
         * @protected
         * @param {PIXI.Renderer} renderer - Instance to renderer.
         */
        protected _renderDefault(renderer: Renderer): void;
        /**
         * Rendering by using the Batch system.
         * @protected
         * @param {PIXI.Renderer} renderer - Instance to renderer.
         */
        protected _renderToBatch(renderer: Renderer): void;
        /**
         * Updates vertexData field based on transform and vertices
         */
        calculateVertices(): void;
        /**
         * Updates uv field based on from geometry uv's or batchUvs
         */
        calculateUvs(): void;
        /**
         * Updates the bounds of the mesh as a rectangle. The bounds calculation takes the worldTransform into account.
         * there must be a aVertexPosition attribute present in the geometry for bounds to be calculated correctly.
         *
         * @protected
         */
        _calculateBounds(): void;
        /**
         * Tests if a point is inside this mesh. Works only for PIXI.DRAW_MODES.TRIANGLES.
         *
         * @param {PIXI.IPointData} point - the point to test
         * @return {boolean} the result of the test
         */
        containsPoint(point: Vector2): boolean;
        /**
         * Destroys the Mesh object.
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all
         *  options have been set to that value
         * @param {boolean} [options.children=false] - if set to true, all the children will have
         *  their destroy method called as well. 'options' will be passed on to those calls.
         */
        destroy(options?: IDestroyOptions | boolean): void;
        /**
         * The maximum number of vertices to consider batchable. Generally, the complexity
         * of the geometry.
         * @memberof PIXI.Mesh
         * @static
         * @member {number} BATCHABLE_SIZE
         */
        static BATCHABLE_SIZE: number;
    }
}
declare namespace feng3d.pixi {
    /**
     * Class controls cache for UV mapping from Texture normal space to BaseTexture normal space.
     *
     */
    class MeshBatchUvs {
        readonly data: Float32Array;
        uvBuffer: Buffer;
        uvMatrix: TextureMatrix;
        private _bufferUpdateId;
        private _textureUpdateId;
        _updateID: number;
        /**
         * @param {PIXI.Buffer} uvBuffer - Buffer with normalized uv's
         * @param {PIXI.TextureMatrix} uvMatrix - Material UV matrix
         */
        constructor(uvBuffer: Buffer, uvMatrix: TextureMatrix);
        /**
         * updates
         *
         * @param {boolean} [forceUpdate] - force the update
         */
        update(forceUpdate?: boolean): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * Standard 2D geometry used in PixiJS.
     *
     * Geometry can be defined without passing in a style or data if required.
     *
     * ```js
     * const geometry = new PIXI.Geometry();
     *
     * geometry.addAttribute('positions', [0, 0, 100, 0, 100, 100, 0, 100], 2);
     * geometry.addAttribute('uvs', [0,0,1,0,1,1,0,1], 2);
     * geometry.addIndex([0,1,2,1,3,2]);
     *
     * ```
     */
    class MeshGeometry extends Geometry {
        _updateId: number;
        /**
         * @param {Float32Array|number[]} [vertices] - Positional data on geometry.
         * @param {Float32Array|number[]} [uvs] - Texture UVs.
         * @param {Uint16Array|number[]} [index] - IndexBuffer
         */
        constructor(vertices?: IArrayBuffer, uvs?: IArrayBuffer, index?: IArrayBuffer);
        /**
         * If the vertex position is updated.
         * @member {number}
         * @readonly
         * @private
         */
        get vertexDirtyId(): number;
    }
}
declare namespace feng3d.pixi {
    interface IMeshMaterialOptions {
        alpha?: number;
        tint?: number;
        pluginName?: string;
        program?: Program;
        uniforms?: Dict<unknown>;
    }
    /**
     * Slightly opinionated default shader for PixiJS 2D objects.
     */
    class MeshMaterial extends Shader {
        readonly uvMatrix: TextureMatrix;
        batchable: boolean;
        pluginName: string;
        _tintRGB: number;
        private _colorDirty;
        private _alpha;
        private _tint;
        /**
         * @param {PIXI.Texture} uSampler - Texture that material uses to render.
         * @param {object} [options] - Additional options
         * @param {number} [options.alpha=1] - Default alpha.
         * @param {number} [options.tint=0xFFFFFF] - Default tint.
         * @param {string} [options.pluginName='batch'] - Renderer plugin for batching.
         * @param {PIXI.Program} [options.program=0xFFFFFF] - Custom program.
         * @param {object} [options.uniforms] - Custom uniforms.
         */
        constructor(uSampler: Texture, options?: IMeshMaterialOptions);
        /**
         * Reference to the texture being rendered.
         * @member {PIXI.Texture}
         */
        get texture(): Texture;
        set texture(value: Texture);
        /**
         * This gets automatically set by the object using this.
         *
         * @default 1
         * @member {number}
         */
        set alpha(value: number);
        get alpha(): number;
        /**
         * Multiply tint for the material.
         * @member {number}
         * @default 0xFFFFFF
         */
        set tint(value: number);
        get tint(): number;
        /**
         * Gets called automatically by the Mesh. Intended to be overridden for custom
         * MeshMaterial objects.
         */
        update(): void;
    }
}
declare namespace feng3d.pixi {
    class PlaneGeometry extends MeshGeometry {
        segWidth: number;
        segHeight: number;
        width: number;
        height: number;
        constructor(width?: number, height?: number, segWidth?: number, segHeight?: number);
        /**
         * Refreshes plane coordinates
         * @private
         */
        build(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * RopeGeometry allows you to draw a geometry across several points and then manipulate these points.
     *
     * ```js
     * for (let i = 0; i < 20; i++) {
     *     points.push(new feng3d.Vector2(i * 50, 0));
     * };
     * const rope = new PIXI.RopeGeometry(100, points);
     * ```
     *
     *
     */
    class RopeGeometry extends MeshGeometry {
        points: Vector2[];
        readonly textureScale: number;
        _width: number;
        /**
         * @param {number} [width=200] - The width (i.e., thickness) of the rope.
         * @param {PIXI.Point[]} [points] - An array of {@link PIXI.Point} objects to construct this rope.
         * @param {number} [textureScale=0] - By default the rope texture will be stretched to match
         *     rope length. If textureScale is positive this value will be treated as a scaling
         *     factor and the texture will preserve its aspect ratio instead. To create a tiling rope
         *     set baseTexture.wrapMode to {@link PIXI.WRAP_MODES.REPEAT} and use a power of two texture,
         *     then set textureScale=1 to keep the original texture pixel size.
         *     In order to reduce alpha channel artifacts provide a larger texture and downsample -
         *     i.e. set textureScale=0.5 to scale it down twice.
         */
        constructor(width: number, points: Vector2[], textureScale?: number);
        /**
         * The width (i.e., thickness) of the rope.
         * @member {number}
         * @readOnly
         */
        get width(): number;
        /**
         * Refreshes Rope indices and uvs
         * @private
         */
        private build;
        /**
         * refreshes vertices of Rope mesh
         */
        updateVertices(): void;
        update(): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * The Simple Mesh class mimics Mesh in PixiJS v4, providing easy-to-use constructor arguments.
     * For more robust customization, use {@link PIXI.Mesh}.
     *
     */
    class SimpleMesh extends Mesh {
        autoUpdate: boolean;
        /**
         * @param {PIXI.Texture} [texture=Texture.EMPTY] - The texture to use
         * @param {Float32Array} [vertices] - if you want to specify the vertices
         * @param {Float32Array} [uvs] - if you want to specify the uvs
         * @param {Uint16Array} [indices] - if you want to specify the indices
         * @param {number} [drawMode] - the drawMode, can be any of the Mesh.DRAW_MODES consts
         */
        constructor(texture?: Texture, vertices?: IArrayBuffer, uvs?: IArrayBuffer, indices?: IArrayBuffer, drawMode?: DRAW_MODES);
        /**
         * Collection of vertices data.
         * @member {Float32Array}
         */
        get vertices(): ITypedArray;
        set vertices(value: ITypedArray);
        _render(renderer: Renderer): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * The SimplePlane allows you to draw a texture across several points and then manipulate these points
     *
     *```js
     * for (let i = 0; i < 20; i++) {
     *     points.push(new feng3d.Vector2(i * 50, 0));
     * };
     * let SimplePlane = PIXI.SimplePlane.create(PIXI.Texture.from("snake.png"), points);
     *  ```
     *
     *
     */
    class SimplePlane extends Mesh {
        protected _textureID: number;
        /**
         * @param {PIXI.Texture} texture - The texture to use on the SimplePlane.
         * @param {number} verticesX - The number of vertices in the x-axis
         * @param {number} verticesY - The number of vertices in the y-axis
         */
        constructor(texture?: Texture, verticesX?: number, verticesY?: number);
        /**
         * Method used for overrides, to do something in case texture frame was changed.
         * Meshes based on plane can override it and change more details based on texture.
         */
        textureUpdated(): void;
        set texture(value: Texture);
        get texture(): Texture;
        _render(renderer: Renderer): void;
        destroy(options?: IDestroyOptions | boolean): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * The NineSlicePlane allows you to stretch a texture using 9-slice scaling. The corners will remain unscaled (useful
     * for buttons with rounded corners for example) and the other areas will be scaled horizontally and or vertically
     *
     *```js
     * let Plane9 = new PIXI.NineSlicePlane(PIXI.Texture.from('BoxWithRoundedCorners.png'), 15, 15, 15, 15);
     *  ```
     * <pre>
     *      A                          B
     *    +---+----------------------+---+
     *  C | 1 |          2           | 3 |
     *    +---+----------------------+---+
     *    |   |                      |   |
     *    | 4 |          5           | 6 |
     *    |   |                      |   |
     *    +---+----------------------+---+
     *  D | 7 |          8           | 9 |
     *    +---+----------------------+---+
    
     *  When changing this objects width and/or height:
     *     areas 1 3 7 and 9 will remain unscaled.
     *     areas 2 and 8 will be stretched horizontally
     *     areas 4 and 6 will be stretched vertically
     *     area 5 will be stretched both horizontally and vertically
     * </pre>
     *
     *
     */
    class NineSlicePlane extends SimplePlane {
        private _origWidth;
        private _origHeight;
        _leftWidth: number;
        _rightWidth: number;
        _topHeight: number;
        _bottomHeight: number;
        /**
         * @param {PIXI.Texture} texture - The texture to use on the NineSlicePlane.
         * @param {number} [leftWidth=10] - size of the left vertical bar (A)
         * @param {number} [topHeight=10] - size of the top horizontal bar (C)
         * @param {number} [rightWidth=10] - size of the right vertical bar (B)
         * @param {number} [bottomHeight=10] - size of the bottom horizontal bar (D)
         */
        constructor(texture: Texture, leftWidth?: number, topHeight?: number, rightWidth?: number, bottomHeight?: number);
        textureUpdated(): void;
        get vertices(): ITypedArray;
        set vertices(value: ITypedArray);
        /**
         * Updates the horizontal vertices.
         *
         */
        updateHorizontalVertices(): void;
        /**
         * Updates the vertical vertices.
         *
         */
        updateVerticalVertices(): void;
        /**
         * Returns the smaller of a set of vertical and horizontal scale of nine slice corners.
         *
         * @return {number} Smaller number of vertical and horizontal scale.
         * @private
         */
        private _getMinScale;
        /**
         * The width of the NineSlicePlane, setting this will actually modify the vertices and UV's of this plane
         *
         * @member {number}
         */
        get width(): number;
        set width(value: number);
        /**
         * The height of the NineSlicePlane, setting this will actually modify the vertices and UV's of this plane
         *
         * @member {number}
         */
        get height(): number;
        set height(value: number);
        /**
         * The width of the left column
         *
         * @member {number}
         */
        get leftWidth(): number;
        set leftWidth(value: number);
        /**
         * The width of the right column
         *
         * @member {number}
         */
        get rightWidth(): number;
        set rightWidth(value: number);
        /**
         * The height of the top row
         *
         * @member {number}
         */
        get topHeight(): number;
        set topHeight(value: number);
        /**
         * The height of the bottom row
         *
         * @member {number}
         */
        get bottomHeight(): number;
        set bottomHeight(value: number);
        /**
         * Refreshes NineSlicePlane coords. All of them.
         */
        private _refresh;
    }
}
declare namespace feng3d.pixi {
    /**
     * The rope allows you to draw a texture across several points and then manipulate these points
     *
     *```js
     * for (let i = 0; i < 20; i++) {
     *     points.push(new feng3d.Vector2(i * 50, 0));
     * };
     * let rope = new PIXI.SimpleRope();
     * rope.init00(PIXI.Texture.from("snake.png"), points)
     *  ```
     *
     *
     */
    class SimpleRope extends Mesh {
        autoUpdate: boolean;
        /**
         * @param {PIXI.Texture} texture - The texture to use on the rope.
         * @param {PIXI.Point[]} points - An array of {@link PIXI.Point} objects to construct this rope.
         * @param {number} [textureScale=0] - Optional. Positive values scale rope texture
         * keeping its aspect ratio. You can reduce alpha channel artifacts by providing a larger texture
         * and downsampling here. If set to zero, texture will be stretched instead.
         */
        constructor(texture?: Texture, points?: Vector2[], textureScale?: number);
        _render(renderer: Renderer): void;
    }
}
declare namespace feng3d.pixi {
    interface Container {
        cacheAsBitmap?: boolean;
        cacheAsBitmapResolution?: number;
        _cacheAsBitmapResolution?: number;
        _cacheAsBitmap?: boolean;
        _cacheData?: pixi.CacheData;
        _renderCached?: (renderer: pixi.Renderer) => void;
        _initCachedDisplayObject?: (renderer: pixi.Renderer) => void;
        _calculateCachedBounds?: () => void;
        _getCachedLocalBounds?: () => pixi.Rectangle;
        _destroyCachedDisplayObject?: () => void;
        _cacheAsBitmapDestroy?: (options?: IDestroyOptions | boolean) => void;
    }
    /**
     * @ignore
     * @private
     */
    class CacheData {
        textureCacheId: string;
        originalRender: (renderer: Renderer) => void;
        originalCalculateBounds: () => void;
        originalGetLocalBounds: (rect?: Rectangle) => Rectangle;
        originalUpdateTransform: () => void;
        originalDestroy: (options?: IDestroyOptions | boolean) => void;
        originalMask: Container | MaskData;
        originalFilterArea: Rectangle;
        originalContainsPoint: (point: Vector2) => boolean;
        sprite: Sprite;
        constructor();
    }
}
declare namespace feng3d {
    interface Node2D {
        getGlobalPosition?: (point?: Vector2, skipUpdate?: boolean) => Vector2;
    }
}
declare namespace feng3d.pixi {
}
declare namespace feng3d.pixi {
    interface Container {
        getChildByName?(name: string, isRecursive?: boolean): Container;
    }
}
declare namespace feng3d.pixi {
    /**
     * BitmapFont format that's Text-based.
     *
     * @private
     */
    class TextFormat {
        /**
         * Check if resource refers to txt font data.
         *
         * @static
         * @private
         * @param {any} data
         * @return {boolean} True if resource could be treated as font data, false otherwise.
         */
        static test(data: unknown): boolean;
        /**
         * Convert text font data to a javascript object.
         *
         * @static
         * @private
         * @param {string} txt - Raw string data to be converted
         * @return {PIXI.BitmapFontData} Parsed font data
         */
        static parse(txt: string): BitmapFontData;
    }
}
declare namespace feng3d.pixi {
    /**
     * BitmapFont format that's XML-based.
     *
     * @private
     */
    class XMLFormat {
        /**
         * Check if resource refers to xml font data.
         *
         * @static
         * @private
         * @param {any} data
         * @return {boolean} True if resource could be treated as font data, false otherwise.
         */
        static test(data: unknown): boolean;
        /**
         * Convert the XML into BitmapFontData that we can use.
         *
         * @static
         * @private
         * @param {XMLDocument} xml
         * @return {BitmapFontData} Data to use for BitmapFont
         */
        static parse(xml: XMLDocument): BitmapFontData;
    }
}
declare namespace feng3d.pixi {
    /**
     * BitmapFont format that's XML-based.
     *
     * @private
     */
    class XMLStringFormat {
        /**
         * Check if resource refers to text xml font data.
         *
         * @static
         * @private
         * @param {any} data
         * @return {boolean} True if resource could be treated as font data, false otherwise.
         */
        static test(data: unknown): boolean;
        /**
         * Convert the text XML into BitmapFontData that we can use.
         *
         * @static
         * @private
         * @param {string} xmlTxt
         * @return {BitmapFontData} Data to use for BitmapFont
         */
        static parse(xmlTxt: string): BitmapFontData;
    }
}
declare namespace feng3d.pixi {
    const formats: readonly [typeof TextFormat, typeof XMLFormat, typeof XMLStringFormat];
    /**
     * Auto-detect BitmapFont parsing format based on data.
     * @private
     * @param {any} data - Data to detect format
     * @return {any} Format or null
     */
    export function autoDetectFormat(data: unknown): typeof formats[number] | null;
    export {};
}
declare namespace feng3d.pixi {
    /**
     * Draws the glyph `metrics.text` on the given canvas.
     *
     * Ignored because not directly exposed.
     *
     * @ignore
     * @param {HTMLCanvasElement} canvas
     * @param {CanvasRenderingContext2D} context
     * @param {TextMetrics} metrics
     * @param {number} x
     * @param {number} y
     * @param {number} resolution
     * @param {TextStyle} style
     */
    function drawGlyph(canvas: HTMLCanvasElement, context: CanvasRenderingContext2D, metrics: TextMetrics, x: number, y: number, resolution: number, style: TextStyle): void;
}
declare namespace feng3d.pixi {
    /**
     * Generates the fill style. Can automatically generate a gradient based on the fill style being an array
     *
     * @private
     * @param {object} style - The style.
     * @param {string[]} lines - The lines of text.
     * @return {string|number|CanvasGradient} The fill style
     */
    function generateFillStyle(canvas: HTMLCanvasElement, context: CanvasRenderingContext2D, style: TextStyle, resolution: number, lines: string[], metrics: TextMetrics): string | CanvasGradient | CanvasPattern;
}
declare namespace feng3d.pixi {
    /**
     * Processes the passed character set data and returns a flattened array of all the characters.
     *
     * Ignored because not directly exposed.
     *
     * @ignore
     * @param {string | string[] | string[][] } chars
     * @returns {string[]}
     */
    function resolveCharacters(chars: string | (string | string[])[]): string[];
}
declare namespace feng3d.pixi {
    interface IBitmapFontCharacter {
        xOffset: number;
        yOffset: number;
        xAdvance: number;
        texture: Texture;
        page: number;
        kerning: Dict<number>;
    }
    interface IBitmapFontOptions {
        chars?: string | (string | string[])[];
        resolution?: number;
        padding?: number;
        textureWidth?: number;
        textureHeight?: number;
    }
    /**
     * BitmapFont represents a typeface available for use with the BitmapText class. Use the `install`
     * method for adding a font to be used.
     *
     */
    class BitmapFont {
        /**
         * This character set includes all the letters in the alphabet (both lower- and upper- case).
         * @readonly
         * @static
         * @member {string[][]}
         * @example
         * BitmapFont.from("ExampleFont", style, { chars: BitmapFont.ALPHA })
         */
        static readonly ALPHA: (string | string[])[];
        /**
         * This character set includes all decimal digits (from 0 to 9).
         * @readonly
         * @static
         * @member {string[][]}
         * @example
         * BitmapFont.from("ExampleFont", style, { chars: BitmapFont.NUMERIC })
         */
        static readonly NUMERIC: string[][];
        /**
         * This character set is the union of `BitmapFont.ALPHA` and `BitmapFont.NUMERIC`.
         * @readonly
         * @static
         * @member {string[][]}
         */
        static readonly ALPHANUMERIC: (string | string[])[];
        /**
         * This character set consists of all the ASCII table.
         * @readonly
         * @static
         * @member {string[][]}
         * @see http://www.asciitable.com/
         */
        static readonly ASCII: string[][];
        /**
         * Collection of default options when using `BitmapFont.from`.
         *
         * @readonly
         * @static
         * @member {PIXI.IBitmapFontOptions}
         * @property {number} resolution=1
         * @property {number} textureWidth=512
         * @property {number} textureHeight=512
         * @property {number} padding=4
         * @property {string|string[]|string[][]} chars = PIXI.BitmapFont.ALPHANUMERIC
         */
        static readonly defaultOptions: IBitmapFontOptions;
        /**
         * Collection of available/installed fonts.
         *
         * @readonly
         * @static
         * @member {Object.<string, PIXI.BitmapFont>}
         */
        static readonly available: Dict<BitmapFont>;
        readonly font: string;
        readonly size: number;
        readonly lineHeight: number;
        readonly chars: Dict<IBitmapFontCharacter>;
        readonly pageTextures: Dict<Texture>;
        /**
         * @param {PIXI.BitmapFontData} data
         * @param {PIXI.Texture[]|Object.<string, PIXI.Texture>} textures
         */
        constructor(data: BitmapFontData, textures: Texture[] | Dict<Texture>);
        /**
         * Remove references to created glyph textures.
         */
        destroy(): void;
        /**
         * Register a new bitmap font.
         *
         * @static
         * @param {XMLDocument|string|PIXI.BitmapFontData} data - The
         *        characters map that could be provided as xml or raw string.
         * @param {Object.<string, PIXI.Texture>|PIXI.Texture|PIXI.Texture[]}
         *        textures - List of textures for each page.
         * @return {PIXI.BitmapFont} Result font object with font, size, lineHeight
         *         and char fields.
         */
        static install(data: string | XMLDocument | BitmapFontData, textures: Texture | Texture[] | Dict<Texture>): BitmapFont;
        /**
         * Remove bitmap font by name.
         *
         * @static
         * @param {string} name
         */
        static uninstall(name: string): void;
        /**
         * Generates a bitmap-font for the given style and character set. This does not support
         * kernings yet. With `style` properties, only the following non-layout properties are used:
         *
         * - {@link PIXI.TextStyle#dropShadow|dropShadow}
         * - {@link PIXI.TextStyle#dropShadowDistance|dropShadowDistance}
         * - {@link PIXI.TextStyle#dropShadowColor|dropShadowColor}
         * - {@link PIXI.TextStyle#dropShadowBlur|dropShadowBlur}
         * - {@link PIXI.TextStyle#dropShadowAngle|dropShadowAngle}
         * - {@link PIXI.TextStyle#fill|fill}
         * - {@link PIXI.TextStyle#fillGradientStops|fillGradientStops}
         * - {@link PIXI.TextStyle#fillGradientType|fillGradientType}
         * - {@link PIXI.TextStyle#fontFamily|fontFamily}
         * - {@link PIXI.TextStyle#fontSize|fontSize}
         * - {@link PIXI.TextStyle#fontVariant|fontVariant}
         * - {@link PIXI.TextStyle#fontWeight|fontWeight}
         * - {@link PIXI.TextStyle#lineJoin|lineJoin}
         * - {@link PIXI.TextStyle#miterLimit|miterLimit}
         * - {@link PIXI.TextStyle#stroke|stroke}
         * - {@link PIXI.TextStyle#strokeThickness|strokeThickness}
         * - {@link PIXI.TextStyle#textBaseline|textBaseline}
         *
         * @param {string} name - The name of the custom font to use with BitmapText.
         * @param {object|PIXI.TextStyle} [style] - Style options to render with BitmapFont.
         * @param {PIXI.IBitmapFontOptions} [options] - Setup options for font or name of the font.
         * @param {string|string[]|string[][]} [options.chars=PIXI.BitmapFont.ALPHANUMERIC] - characters included
         *      in the font set. You can also use ranges. For example, `[['a', 'z'], ['A', 'Z'], "!@#$%^&*()~{}[] "]`.
         *      Don't forget to include spaces ' ' in your character set!
         * @param {number} [options.resolution=1] - Render resolution for glyphs.
         * @param {number} [options.textureWidth=512] - Optional width of atlas, smaller values to reduce memory.
         * @param {number} [options.textureHeight=512] - Optional height of atlas, smaller values to reduce memory.
         * @param {number} [options.padding=4] - Padding between glyphs on texture atlas.
         * @return {PIXI.BitmapFont} Font generated by style options.
         * @static
         * @example
         * PIXI.BitmapFont.from("TitleFont", {
         *     fontFamily: "Arial",
         *     fontSize: 12,
         *     strokeThickness: 2,
         *     fill: "purple"
         * });
         *
         * const title = new PIXI.BitmapText("This is the title", { fontName: "TitleFont" });
         */
        static from(name: string, textStyle?: TextStyle | Partial<ITextStyle>, options?: IBitmapFontOptions): BitmapFont;
    }
    /**
     * @interface IBitmapFontOptions
     * @property {string | string[] | string[][]} [chars=PIXI.BitmapFont.ALPHANUMERIC] - the character set to generate
     * @property {number} [resolution=1] - the resolution for rendering
     * @property {number} [padding=4] - the padding between glyphs in the atlas
     * @property {number} [textureWidth=512] - the width of the texture atlas
     * @property {number} [textureHeight=512] - the height of the texture atlas
     */
}
declare namespace feng3d.pixi {
    /**
     * Normalized parsed data from .fnt files.
     *
     */
    class BitmapFontData {
        info: IBitmapFontDataInfo[];
        common: IBitmapFontDataCommon[];
        page: IBitmapFontDataPage[];
        char: IBitmapFontDataChar[];
        kerning: IBitmapFontDataKerning[];
        constructor();
    }
    interface IBitmapFontDataInfo {
        face: string;
        size: number;
    }
    interface IBitmapFontDataCommon {
        lineHeight: number;
    }
    interface IBitmapFontDataPage {
        id: number;
        file: string;
    }
    interface IBitmapFontDataChar {
        id: number;
        page: number;
        x: number;
        y: number;
        width: number;
        height: number;
        xoffset: number;
        yoffset: number;
        xadvance: number;
    }
    interface IBitmapFontDataKerning {
        first: number;
        second: number;
        amount: number;
    }
    /**
     * @typedef {object} IBitmapFontDataInfo
     * @property {string} face
     * @property {number} size
     */
    /**
     * @typedef {object} IBitmapFontDataCommon
     * @property {number} lineHeight
     */
    /**
     * @typedef {object} IBitmapFontDataPage
     * @property {number} id
     * @property {string} file
     */
    /**
     * @typedef {object} IBitmapFontDataChar
     * @property {string} id
     * @property {number} page
     * @property {number} x
     * @property {number} y
     * @property {number} width
     * @property {number} height
     * @property {number} xoffset
     * @property {number} yoffset
     * @property {number} xadvance
     */
    /**
     * @typedef {object} IBitmapFontDataKerning
     * @property {number} first
     * @property {number} second
     * @property {number} amount
     */
}
declare namespace feng3d.pixi {
    interface IBitmapFontResource {
        bitmapFont: BitmapFont;
    }
    export interface ILoaderResource extends Partial<IBitmapFontResource> {
    }
    interface IBitmapFontResourceMetadata {
        pageFile: string;
    }
    export interface IResourceMetadata extends Partial<IBitmapFontResourceMetadata> {
    }
    /**
     * {@link PIXI.Loader Loader} middleware for loading
     * bitmap-based fonts suitable for using with {@link PIXI.BitmapText}.
     * @implements PIXI.ILoaderPlugin
     */
    export class BitmapFontLoader {
        /**
         * Called when the plugin is installed.
         *
         * @see PIXI.Loader.registerPlugin
         */
        static add(): void;
        /**
         * Called after a resource is loaded.
         * @see PIXI.Loader.loaderMiddleware
         * @param {PIXI.LoaderResource} resource
         * @param {function} next
         */
        static use(this: Loader, resource: ILoaderResource, next: (...args: any[]) => void): void;
        /**
         * Get folder path from a resource
         * @private
         * @param {PIXI.Loader} loader
         * @param {PIXI.LoaderResource} resource
         * @return {string}
         */
        private static getBaseUrl;
        /**
         * Replacement for NodeJS's path.dirname
         * @private
         * @param {string} url - Path to get directory for
         */
        private static dirname;
    }
    export {};
}
declare namespace feng3d.pixi {
    interface PageMeshData {
        index: number;
        indexCount: number;
        vertexCount: number;
        uvsCount: number;
        total: number;
        mesh: Mesh;
        vertices?: Float32Array;
        uvs?: Float32Array;
        indices?: Uint16Array;
    }
    /**
     * A BitmapText object will create a line or multiple lines of text using bitmap font.
     *
     * The primary advantage of this class over Text is that all of your textures are pre-generated and loading,
     * meaning that rendering is fast, and changing text has no performance implications.
     *
     * Supporting character sets other than latin, such as CJK languages, may be impractical due to the number of characters.
     *
     * To split a line you can use '\n', '\r' or '\r\n' in your string.
     *
     * PixiJS can auto-generate fonts on-the-fly using BitmapFont or use fnt files provided by:
     * http://www.angelcode.com/products/bmfont/ for Windows or
     * http://www.bmglyph.com/ for Mac.
     *
     * A BitmapText can only be created when the font is loaded.
     *
     * ```js
     * // in this case the font is in a file called 'desyrel.fnt'
     * let bitmapText = new PIXI.BitmapText("text using a fancy font!", {font: "35px Desyrel", align: "right"});
     * ```
     *
     */
    export class BitmapText extends Container {
        static styleDefaults: Partial<IBitmapTextStyle>;
        dirty: boolean;
        protected _textWidth: number;
        protected _textHeight: number;
        protected _text: string;
        protected _maxWidth: number;
        protected _maxLineHeight: number;
        protected _letterSpacing: number;
        /**
         * Text anchor. read-only
         */
        protected _anchor: Vector2;
        protected _fontName: string;
        protected _fontSize: number;
        protected _align: TextStyleAlign;
        protected _activePagesMeshData: PageMeshData[];
        protected _tint: number;
        protected _roundPixels: boolean;
        private _textureCache;
        /**
         * @param {string} text - A string that you would like the text to display.
         * @param {object} style - The style parameters.
         * @param {string} style.fontName - The installed BitmapFont name.
         * @param {number} [style.fontSize] - The size of the font in pixels, e.g. 24. If undefined,
         *.     this will default to the BitmapFont size.
         * @param {string} [style.align='left'] - Alignment for multiline text ('left', 'center', 'right' or 'justify'),
         *      does not affect single line text.
         * @param {number} [style.tint=0xFFFFFF] - The tint color.
         * @param {number} [style.letterSpacing=0] - The amount of spacing between letters.
         * @param {number} [style.maxWidth=0] - The max width of the text before line wrapping.
         */
        constructor(text: string, style?: Partial<IBitmapTextStyle>);
        /**
         * Renders text and updates it when needed. This should only be called
         * if the BitmapFont is regenerated.
         */
        updateText(): void;
        /**
         * Updates the transform of this object
         *
         * @private
         */
        updateTransform(): void;
        /**
         * Validates text before calling parent's getLocalBounds
         *
         * @return {PIXI.Rectangle} The rectangular bounding area
         */
        getLocalBounds(): Rectangle;
        /**
         * Updates text when needed
         *
         * @private
         */
        protected validate(): void;
        /**
         * The tint of the BitmapText object.
         *
         * @member {number}
         * @default 0xffffff
         */
        get tint(): number;
        set tint(value: number);
        /**
         * The alignment of the BitmapText object.
         *
         * @member {string}
         * @default 'left'
         */
        get align(): TextStyleAlign;
        set align(value: TextStyleAlign);
        /**
         * The name of the BitmapFont.
         *
         * @member {string}
         */
        get fontName(): string;
        set fontName(value: string);
        /**
         * The size of the font to display.
         *
         * @member {number}
         */
        get fontSize(): number;
        set fontSize(value: number);
        /**
         * The anchor sets the origin point of the text.
         *
         * The default is `(0,0)`, this means the text's origin is the top left.
         *
         * Setting the anchor to `(0.5,0.5)` means the text's origin is centered.
         *
         * Setting the anchor to `(1,1)` would mean the text's origin point will be the bottom right corner.
         */
        get anchor(): Vector2;
        set anchor(value: Vector2);
        /**
         * The text of the BitmapText object.
         *
         * @member {string}
         */
        get text(): string;
        set text(text: string);
        /**
         * The max width of this bitmap text in pixels. If the text provided is longer than the
         * value provided, line breaks will be automatically inserted in the last whitespace.
         * Disable by setting the value to 0.
         *
         * @member {number}
         */
        get maxWidth(): number;
        set maxWidth(value: number);
        /**
         * The max line height. This is useful when trying to use the total height of the Text,
         * i.e. when trying to vertically align.
         *
         * @member {number}
         * @readonly
         */
        get maxLineHeight(): number;
        /**
         * The width of the overall text, different from fontSize,
         * which is defined in the style object.
         *
         * @member {number}
         * @readonly
         */
        get textWidth(): number;
        /**
         * Additional space between characters.
         *
         * @member {number}
         */
        get letterSpacing(): number;
        set letterSpacing(value: number);
        /**
         * If true PixiJS will Math.floor() x/y values when rendering, stopping pixel interpolation.
         * Advantages can include sharper image quality (like text) and faster rendering on canvas.
         * The main disadvantage is movement of objects may appear less smooth.
         * To set the global default, change {@link PIXI.settings.ROUND_PIXELS}
         *
         * @member {boolean}
         * @default PIXI.settings.ROUND_PIXELS
         */
        get roundPixels(): boolean;
        set roundPixels(value: boolean);
        /**
         * The height of the overall text, different from fontSize,
         * which is defined in the style object.
         *
         * @member {number}
         * @readonly
         */
        get textHeight(): number;
        destroy(options?: boolean | IDestroyOptions): void;
    }
    export {};
}
declare namespace feng3d.pixi {
    interface IBitmapTextStyle {
        fontName: string;
        fontSize: number;
        tint: number;
        align: TextStyleAlign;
        letterSpacing: number;
        maxWidth: number;
    }
    interface IBitmapTextFontDescriptor {
        name: string;
        size: number;
    }
}
declare namespace feng3d.pixi {
    /**
     * WebGL internal formats, including compressed texture formats provided by extensions
     *
     * @static
     * @name INTERNAL_FORMATS
     * @enum {number}
     * @property {number} COMPRESSED_RGB_S3TC_DXT1_EXT=0x83F0
     * @property {number} COMPRESSED_RGBA_S3TC_DXT1_EXT=0x83F1
     * @property {number} COMPRESSED_RGBA_S3TC_DXT3_EXT=0x83F2
     * @property {number} COMPRESSED_RGBA_S3TC_DXT5_EXT=0x83F3
     * @property {number} COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT=35917
     * @property {number} COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT=35918
     * @property {number} COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT=35919
     * @property {number} COMPRESSED_SRGB_S3TC_DXT1_EXT=35916
     * @property {number} COMPRESSED_R11_EAC=0x9270
     * @property {number} COMPRESSED_SIGNED_R11_EAC=0x9271
     * @property {number} COMPRESSED_RG11_EAC=0x9272
     * @property {number} COMPRESSED_SIGNED_RG11_EAC=0x9273
     * @property {number} COMPRESSED_RGB8_ETC2=0x9274
     * @property {number} COMPRESSED_RGBA8_ETC2_EAC=0x9275
     * @property {number} COMPRESSED_SRGB8_ETC2=0x9276
     * @property {number} COMPRESSED_SRGB8_ALPHA8_ETC2_EAC=0x9277
     * @property {number} COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2=0x9278
     * @property {number} COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2= 0x9279
     * @property {number} COMPRESSED_RGB_PVRTC_4BPPV1_IMG=0x8C00
     * @property {number} COMPRESSED_RGBA_PVRTC_4BPPV1_IMG=0x8C02
     * @property {number} COMPRESSED_RGB_PVRTC_2BPPV1_IMG=0x8C01
     * @property {number} COMPRESSED_RGBA_PVRTC_2BPPV1_IMG=0x8C03
     * @property {number} COMPRESSED_RGB_ETC1_WEBGL=0x8D64
     * @property {number} COMPRESSED_RGB_ATC_WEBGL=0x8C92
     * @property {number} COMPRESSED_RGBA_ATC_EXPLICIT_ALPHA_WEBGL=0x8C92
     * @property {number} COMPRESSED_RGBA_ATC_INTERPOLATED_ALPHA_WEBGL=0x87EE
     */
    enum INTERNAL_FORMATS {
        COMPRESSED_RGB_S3TC_DXT1_EXT = 33776,
        COMPRESSED_RGBA_S3TC_DXT1_EXT = 33777,
        COMPRESSED_RGBA_S3TC_DXT3_EXT = 33778,
        COMPRESSED_RGBA_S3TC_DXT5_EXT = 33779,
        COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT = 35917,
        COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT = 35918,
        COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT = 35919,
        COMPRESSED_SRGB_S3TC_DXT1_EXT = 35916,
        COMPRESSED_R11_EAC = 37488,
        COMPRESSED_SIGNED_R11_EAC = 37489,
        COMPRESSED_RG11_EAC = 37490,
        COMPRESSED_SIGNED_RG11_EAC = 37491,
        COMPRESSED_RGB8_ETC2 = 37492,
        COMPRESSED_RGBA8_ETC2_EAC = 37493,
        COMPRESSED_SRGB8_ETC2 = 37494,
        COMPRESSED_SRGB8_ALPHA8_ETC2_EAC = 37495,
        COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 37496,
        COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2 = 37497,
        COMPRESSED_RGB_PVRTC_4BPPV1_IMG = 35840,
        COMPRESSED_RGBA_PVRTC_4BPPV1_IMG = 35842,
        COMPRESSED_RGB_PVRTC_2BPPV1_IMG = 35841,
        COMPRESSED_RGBA_PVRTC_2BPPV1_IMG = 35843,
        COMPRESSED_RGB_ETC1_WEBGL = 36196,
        COMPRESSED_RGB_ATC_WEBGL = 35986,
        COMPRESSED_RGBA_ATC_EXPLICIT_ALPHA_WEBGL = 35986,
        COMPRESSED_RGBA_ATC_INTERPOLATED_ALPHA_WEBGL = 34798
    }
    /**
     * Maps the compressed texture formats in {@link PIXI.INTERNAL_FORMATS} to the number of bytes taken by
     * each texel.
     *
     * @static
     * @ignore
     */
    const INTERNAL_FORMAT_TO_BYTES_PER_PIXEL: {
        [id: number]: number;
    };
}
declare namespace feng3d.pixi {
    /**
     * Schema for compressed-texture manifests
     *
     * @ignore
     * @see PIXI.CompressedTextureLoader
     */
    type CompressedTextureManifest = {
        textures: Array<{
            src: string;
            format?: keyof INTERNAL_FORMATS;
        }>;
        cacheID: string;
    };
    /**
     * Compressed texture extensions
     */
    type CompressedTextureExtensions = {
        s3tc?: WEBGL_compressed_texture_s3tc;
        s3tc_sRGB: WEBGL_compressed_texture_s3tc_srgb;
        etc: any;
        etc1: any;
        pvrtc: any;
        atc: any;
        astc: WEBGL_compressed_texture_astc;
    };
    type CompressedTextureExtensionRef = keyof CompressedTextureExtensions;
    /**
     * Loader plugin for handling compressed textures for all platforms.
     *
     * @implements PIXI.ILoaderPlugin
     */
    class CompressedTextureLoader {
        static textureExtensions: Partial<CompressedTextureExtensions>;
        static textureFormats: {
            [P in keyof INTERNAL_FORMATS]?: number;
        };
        /**
         * Called after a compressed-textures manifest is loaded.
         *
         * This will then load the correct compression format for the device. Your manifest should adhere
         * to the following schema:
         *
         * ```js
         * import { INTERNAL_FORMATS } from '@pixi/constants';
         *
         * // The following should be present in a *.compressed-texture.json file!
         * const manifest = JSON.stringify({
         *   COMPRESSED_RGBA_S3TC_DXT5_EXT: "asset.s3tc.ktx",
         *   COMPRESSED_RGBA8_ETC2_EAC: "asset.etc.ktx",
         *   RGBA_PVRTC_4BPPV1_IMG: "asset.pvrtc.ktx",
         *   textureID: "asset.png",
         *   fallback: "asset.png"
         * });
         * ```
         *
         * @param resource
         * @param next
         */
        static use(resource: ILoaderResource, next: (...args: any[]) => void): void;
        /**
         * Detects the available compressed texture extensions on the device.
         *
         * @param extensions - extensions provided by a WebGL context
         * @ignore
         */
        static autoDetectExtensions(extensions?: Partial<CompressedTextureExtensions>): void;
    }
}
declare namespace feng3d.pixi {
    /**
     * @implements PIXI.ILoaderPlugin
     * @see https://docs.microsoft.com/en-us/windows/win32/direct3ddds/dx-graphics-dds-pguide
     */
    class DDSLoader {
        static use(resource: ILoaderResource, next: (...args: any[]) => void): void;
        /**
         * Parses the DDS file header, generates base-textures, and puts them into the texture
         * cache.
         */
        private static parse;
    }
}
declare namespace feng3d.pixi {
    /**
     * Maps {@link PIXI.TYPES} to the bytes taken per component, excluding those ones that are bit-fields.
     *
     * @ignore
     */
    const TYPES_TO_BYTES_PER_COMPONENT: {
        [id: number]: number;
    };
    /**
     * Number of components in each {@link PIXI.FORMATS}
     *
     * @ignore
     */
    const FORMATS_TO_COMPONENTS: {
        [id: number]: number;
    };
    /**
     * Number of bytes per pixel in bit-field types in {@link PIXI.TYPES}
     *
     * @ignore
     */
    const TYPES_TO_BYTES_PER_PIXEL: {
        [id: number]: number;
    };
    /**
     * Loader plugin for handling KTX texture container files.
     *
     * This KTX loader does not currently support the following features:
     * * cube textures
     * * 3D textures
     * * vendor-specific key/value data parsing
     * * endianness conversion for big-endian machines
     * * embedded *.basis files
     *
     * It does supports the following features:
     * * multiple textures per file
     * * mipmapping
     *
     * @implements PIXI.ILoaderPlugin
     */
    class KTXLoader {
        /**
         * Called after a KTX file is loaded.
         *
         * This will parse the KTX file header and add a {@code BaseTexture} to the texture
         * cache.
         *
         * @see PIXI.Loader.loaderMiddleware
         * @param {PIXI.LoaderResource} resource
         * @param {function} next
         */
        static use(resource: ILoaderResource, next: (...args: any[]) => void): void;
        /**
         * Parses the KTX file header, generates base-textures, and puts them into the texture
         * cache.
         */
        private static parse;
        /**
         * Checks whether the arrayBuffer contains a valid *.ktx file.
         */
        private static validate;
    }
}
declare namespace feng3d.pixi {
    /**
     * Creates base-textures and textures for each compressed-texture resource and adds them into the global
     * texture cache. The first texture has two IDs - `${url}`, `${url}-1`; while the rest have an ID of the
     * form `${url}-i`.
     *
     * @param url - the original address of the resources
     * @param resources - the resources backing texture data
     * @ignore
     */
    function registerCompressedTextures(url: string, resources: CompressedTextureResource[]): void;
}
declare namespace feng3d.pixi {
    interface IBlobOptions {
        autoLoad?: boolean;
        width: number;
        height: number;
    }
    /**
     * Resource that fetches texture data over the network and stores it in a buffer.
     *
     */
    export abstract class BlobResource extends BufferResource {
        protected origin: string;
        protected buffer: ViewableBuffer;
        protected loaded: boolean;
        /**
         * @param {string} url - the URL of the texture file
         * @param {boolean}[autoLoad] - whether to fetch the data immediately;
         *  you can fetch it later via {@link BlobResource#load}
         */
        constructor(source: string | Uint8Array | Uint32Array | Float32Array, options?: IBlobOptions);
        protected onBlobLoaded(_data: ArrayBuffer): void;
        /**
         * Loads the blob
         */
        load(): Promise<Resource>;
    }
    export {};
}
declare namespace feng3d.pixi {
    /**
     * @ignore
     */
    type CompressedLevelBuffer = {
        levelID: number;
        levelWidth: number;
        levelHeight: number;
        levelBuffer: Uint8Array;
    };
    /**
     * @ignore
     */
    interface ICompressedTextureResourceOptions {
        format: INTERNAL_FORMATS;
        width: number;
        height: number;
        levels?: number;
        levelBuffers?: CompressedLevelBuffer[];
    }
    /**
     * Resource for compressed texture formats, as follows: S3TC/DXTn (& their sRGB formats), ATC, ASTC, ETC 1/2, PVRTC.
     *
     * Compressed textures improve performance when rendering is texture-bound. The texture data stays compressed in
     * graphics memory, increasing memory locality and speeding up texture fetches. These formats can also be used to store
     * more detail in the same amount of memory.
     *
     * For most developers, container file formats are a better abstraction instead of directly handling raw texture
     * data. PixiJS provides native support for the following texture file formats (via {@link PIXI.Loader}):
     *
     * * **.dds** - the DirectDraw Surface file format stores DXTn (DXT-1,3,5) data. See {@link PIXI.DDSLoader}
     * * **.ktx** - the Khronos Texture Container file format supports storing all the supported WebGL compression formats.
     *  See {@link PIXI.KTXLoader}.
     * * **.basis** - the BASIS supercompressed file format stores texture data in an internal format that is transcoded
     *  to the compression format supported on the device at _runtime_. It also supports transcoding into a uncompressed
     *  format as a fallback; you must install the `@pixi/basis-loader`, `@pixi/basis-transcoder` packages separately to
     *  use these files. See {@link PIXI.BasisLoader}.
     *
     * The loaders for the aforementioned formats use `CompressedTextureResource` internally. It is strongly suggested that
     * they be used instead.
     *
     * ## Working directly with CompressedTextureResource
     *
     * Since `CompressedTextureResource` inherits `BlobResource`, you can provide it a URL pointing to a file containing
     * the raw texture data (with no file headers!):
     *
     * ```js
     * // The resource backing the texture data for your textures.
     * // NOTE: You can also provide a ArrayBufferView instead of a URL. This is used when loading data from a container file
     * //   format such as KTX, DDS, or BASIS.
     * const compressedResource = new PIXI.CompressedTextureResource("bunny.dxt5", {
     *   format: PIXI.INTERNAL_FORMATS.COMPRESSED_RGBA_S3TC_DXT5_EXT,
     *   width: 256,
     *   height: 256
     * });
     *
     * // You can create a base-texture to the cache, so that future `Texture`s can be created using the `Texture.from` API.
     * const baseTexture = new PIXI.BaseTexture(compressedResource, { pmaMode: PIXI.ALPHA_MODES.NPM });
     *
     * // Create a Texture to add to the TextureCache
     * const texture = new PIXI.Texture(baseTexture);
     *
     * // Add baseTexture & texture to the global texture cache
     * PIXI.BaseTexture.addToCache(baseTexture, "bunny.dxt5");
     * PIXI.Texture.addToCache(texture, "bunny.dxt5");
     * ```
     *
     */
    class CompressedTextureResource extends BlobResource {
        format: INTERNAL_FORMATS;
        levels: number;
        private _extension;
        private _levelBuffers;
        /**
         * @param source - the buffer/URL holding the compressed texture data
         * @param options
         * @param {PIXI.INTERNAL_FORMATS} options.format - the compression format
         * @param {number} options.width - the image width in pixels.
         * @param {number} options.height - the image height in pixels.
         * @param {number}[options.level=1] - the mipmap levels stored in the compressed texture, including level 0.
         * @param {number}[options.levelBuffers] - the buffers for each mipmap level. `CompressedTextureResource` can allows you
         *      to pass `null` for `source`, for cases where each level is stored in non-contiguous memory.
         */
        constructor(source: string | Uint8Array | Uint32Array, options: ICompressedTextureResourceOptions);
        /**
         * @override
         * @param renderer
         * @param _texture
         * @param _glTexture
         */
        upload(renderer: Renderer, _texture: BaseTexture, _glTexture: GLTexture): boolean;
        /**
         * @protected
         */
        protected onBlobLoaded(): void;
        /**
         * Returns the key (to ContextSystem#extensions) for the WebGL extension supporting the compression format
         *
         * @private
         * @param {PIXI.INTERNAL_FORMATS} format
         * @return {string}
         */
        private static _formatToExtension;
        /**
         * Pre-creates buffer views for each mipmap level
         *
         * @private
         * @param {Uint8Array} buffer
         * @param {PIXI.INTERNAL_FORMATS} format
         * @param {number} levels
         * @param {number} blockWidth
         * @param {number} blockHeight
         * @param {number} imageWidth
         * @param {number} imageHeight
         */
        private static _createLevelBuffers;
    }
}
declare namespace feng3d.pixi {
    /**
     * String of the current PIXI version.
     *
     * @static
     * @constant
     * @name VERSION
     * @type {string}
     */
    const VERSION = "$_VERSION";
    /**
     * @namespace feng3d.pixi
     */
    /**
     * This namespace contains WebGL-only display filters that can be applied
     * to DisplayObjects using the {@link PIXI.DisplayObject#filters filters} property.
     *
     * Since PixiJS only had a handful of built-in filters, additional filters
     * can be downloaded {@link https://github.com/pixijs/pixi-filters here} from the
     * PixiJS Filters repository.
     *
     * All filters must extend {@link PIXI.Filter}.
     *
     * @example
     * // Create a new application
     * const app = new PIXI.Application();
     *
     * // Draw a green rectangle
     * const rect = new PIXI.Graphics()
     *     .beginFill(0x00ff00)
     *     .drawRect(40, 40, 200, 200);
     *
     * // Add a blur filter
     * rect.filters = [new PIXI.filters.BlurFilter()];
     *
     * // Display rectangle
     * app.stage.addChild(rect);
     * document.body.appendChild(app.view);
     * @namespace feng3d.pixi.filters
     */
    const filters: {
        AlphaFilter: typeof AlphaFilter;
        BlurFilter: typeof BlurFilter;
        BlurFilterPass: typeof BlurFilterPass;
        ColorMatrixFilter: typeof ColorMatrixFilter;
        DisplacementFilter: typeof DisplacementFilter;
        FXAAFilter: typeof FXAAFilter;
        NoiseFilter: typeof NoiseFilter;
    };
}
declare const PIXI: typeof feng3d.pixi;
//# sourceMappingURL=index.d.ts.map