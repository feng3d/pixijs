namespace feng3d.pixi
{


    /*
     * @description Complex shape type
     */
    export type IShape = Circle | Ellipse | Polygon | Rectangle | RoundedRectangle;

    export interface ISize
    {
        width: number;
        height: number;
    }
}