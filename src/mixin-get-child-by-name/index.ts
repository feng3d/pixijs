namespace feng3d.pixi
{
    export interface Container
    {
        getChildByName?(name: string, isRecursive?: boolean): Container;
    }

    /**
     * Returns the display object in the container.
     *
     * Recursive searches are done in a preorder traversal.
     *
     * @method getChildByName.Node2D#
     * @param {string} name - Instance name.
     * @param {boolean}[deep=false] - Whether to search recursively
     * @return {feng3d.Node2D} The child with the specified name.
     */
    Container.prototype.getChildByName = function getChildByName(name: string, deep?: boolean): Container
    {
        for (let i = 0, j = this.children.length; i < j; i++)
        {
            if (this.children[i].name === name)
            {
                return this.children[i];
            }
        }

        if (deep)
        {
            for (let i = 0, j = this.children.length; i < j; i++)
            {
                const child = (this.children[i] as Container);

                if (!child.getChildByName)
                {
                    continue;
                }

                const target = (this.children[i] as Container).getChildByName(name, true);

                if (target)
                {
                    return target;
                }
            }
        }

        return null;
    };
}