namespace feng3d.pixi
{
    function sortChildren(a: Container, b: Container): number
    {
        if (a.zIndex === b.zIndex)
        {
            return a._lastSortedIndex - b._lastSortedIndex;
        }

        return a.zIndex - b.zIndex;
    }

    export class Container extends feng3d.Node2D
    {
        @AddEntityMenu("Node2D/Container")
        static create(name = 'Container')
        {
            const container = new Entity().addComponent(Container);
            container.name = name;
            return container;
        }
        
        parent: Container;
        children: Container[];

        private _mask: Container | MaskData = null;

        /**
         * Sets a mask for the displayObject. A mask is an object that limits the visibility of an
         * object to the shape of the mask applied to it. In PixiJS a regular mask must be a
         * {@link PIXI.Graphics} or a {@link PIXI.Sprite} object. This allows for much faster masking in canvas as it
         * utilities shape clipping. To remove a mask, set this property to `null`.
         *
         * For sprite mask both alpha and red channel are used. Black mask is the same as transparent mask.
         *
         * @example
         * const graphics = new PIXI.Graphics();
         * graphics.beginFill(0xFF3300);
         * graphics.drawRect(50, 250, 100, 100);
         * graphics.endFill();
         *
         * const sprite = new PIXI.Sprite(texture);
         * sprite.mask = graphics;
         *
         * @todo At the moment, PIXI.CanvasRenderer doesn't support PIXI.Sprite as mask.
         * @member {PIXI.Container|PIXI.MaskData|null}
         */
        get mask(): Container | MaskData | null
        {
            return this._mask;
        }

        set mask(value: Container | MaskData | null)
        {
            if (this._mask)
            {
                const maskObject = ((this._mask as MaskData).maskObject || this._mask) as Container;

                maskObject.renderable = true;
                maskObject.isMask = false;
            }

            this._mask = value;

            if (this._mask)
            {
                const maskObject = ((this._mask as MaskData).maskObject || this._mask) as Container;

                maskObject.renderable = false;
                maskObject.isMask = true;
            }
        }

        constructor()
        {
            super();
            // new Entity().addComponentAt(this, 0);
        }

        /**
         * @protected
         * @member {PIXI.Container}
         */
        get _tempDisplayObjectParent()
        {
            if (this.tempDisplayObjectParent === null)
            {
                // eslint-disable-next-line @typescript-eslint/no-use-before-define
                this.tempDisplayObjectParent = new Container();
            }

            return this.tempDisplayObjectParent;
        }

        /**
         * Can this object be rendered, if false the object will not be drawn but the updateTransform
         * methods will still be called.
         *
         * Only affects recursive calls from parent. You can ask for bounds manually.
         *
         * @member {boolean}
         */
        renderable = true;


        /**
         * The area the filter is applied to. This is used as more of an optimization
         * rather than figuring out the dimensions of the displayObject each frame you can set this rectangle.
         *
         * Also works as an interaction mask.
         *
         * @member {?PIXI.Rectangle}
         */
        filterArea: pixi.Rectangle = null;

        /**
         * Sets the filters for the displayObject.
         * * IMPORTANT: This is a WebGL only feature and will be ignored by the canvas renderer.
         * To remove filters simply set this property to `'null'`.
         *
         * @member {?PIXI.Filter[]}
         */
        filters: pixi.Filter[] = null;


        /**
         * used to fast check if a sprite is.. a sprite!
         * @member {boolean}
         */
        isSprite = false;

        /**
         * Does any other displayObject use this object as a mask?
         * @member {boolean}
         */
        isMask = false;

        /**
         * Which index in the children array the display component was before the previous zIndex sort.
         * Used by containers to help sort objects with the same zIndex, by using previous array index as the decider.
         *
         * @member {number}
         * @protected
         */
        _lastSortedIndex = 0;

        /**
         * The bounds object, this is used to calculate and store the bounds of the displayObject.
         *
         * @member {PIXI.Bounds}
         */
        _bounds = new Bounds();

        /**
         * Local bounds object, swapped with `_bounds` when using `getLocalBounds()`.
         *
         * @member {PIXI.Bounds}
         */
        _localBounds: pixi.Bounds = null;

        /**
         * If set to true, the container will sort its children by zIndex value
         * when updateTransform() is called, or manually if sortChildren() is called.
         *
         * This actually changes the order of elements in the array, so should be treated
         * as a basic solution that is not performant compared to other solutions,
         * such as @link https://github.com/pixijs/pixi-display
         *
         * Also be aware of that this may not work nicely with the addChildAt() function,
         * as the zIndex sorting may cause the child to automatically sorted to another position.
         *
         * @see PIXI.settings.SORTABLE_CHILDREN
         *
         * @member {boolean}
         */
        sortableChildren = settings.SORTABLE_CHILDREN;

        /**
         * Should children be sorted by zIndex at the next updateTransform call.
         *
         * Will get automatically set to true if a new child is added, or if a child's zIndex changes.
         *
         * @member {boolean}
         */
        sortDirty = false;
        containerUpdateTransform: () => void;

        /**
         * The zIndex of the displayObject.
         *
         * If a container has the sortableChildren property set to true, children will be automatically
         * sorted by zIndex value; a higher value will mean it will be moved towards the end of the array,
         * and thus rendered on top of other display objects within the same container.
         *
         * @member {number}
         * @see PIXI.Container#sortableChildren
         */
        get zIndex(): number
        {
            return this._zIndex;
        }

        set zIndex(value: number)
        {
            this._zIndex = value;
            if (this.parent)
            {
                this.parent.sortDirty = true;
            }
        }

        /**
         * The zIndex of the displayObject.
         * A higher value will mean it will be rendered on top of other displayObjects within the same container.
         *
         * @member {number}
         * @protected
         */
        protected _zIndex = 0;

        get width(): number
        {
            return this.scaleX * this.getLocalBounds().width;
        }
        set width(value: number)
        {
            const width = this.getLocalBounds().width;

            if (width !== 0)
            {
                this.scaleX = value / width;
            }
            else
            {
                this.scaleX = 1;
            }

            this._width = value;
        }

        get height(): number
        {
            return this.scaleY * this.getLocalBounds().height;
        }
        set height(value: number)
        {
            const height = this.getLocalBounds().height;

            if (height !== 0)
            {
                this.scaleY = value / height;
            }
            else
            {
                this.scaleY = 1;
            }
            this._height = value;
        }

        _width: number;
        _height: number;

        /**
         * Currently enabled filters
         * @member {PIXI.Filter[]}
         * @protected
         */
        protected _enabledFilters: pixi.Filter[] = null;

        /**
         * Flags the cached bounds as dirty.
         *
         * @member {number}
         * @protected
         */
        protected _boundsID = 0;

        /**
         * Cache of this display-object's bounds-rectangle.
         *
         * @member {PIXI.Bounds}
         * @protected
         */
        protected _boundsRect: pixi.Rectangle = null;

        /**
         * Cache of this display-object's local-bounds rectangle.
         *
         * @member {PIXI.Bounds}
         * @protected
         */
        protected _localBoundsRect: pixi.Rectangle = null;

        /**
         * Calculates and returns the (world) bounds of the display object as a [Rectangle]{@link PIXI.Rectangle}.
         *
         * This method is expensive on containers with a large subtree (like the stage). This is because the bounds
         * of a container depend on its children's bounds, which recursively causes all bounds in the subtree to
         * be recalculated. The upside, however, is that calling `getBounds` once on a container will indeed update
         * the bounds of all children (the whole subtree, in fact). This side effect should be exploited by using
         * `displayObject._bounds.getRectangle()` when traversing through all the bounds in a scene graph. Otherwise,
         * calling `getBounds` on each object in a subtree will cause the total cost to increase quadratically as
         * its height increases.
         *
         * * The transforms of all objects in a container's **subtree** and of all **ancestors** are updated.
         * * The world bounds of all display objects in a container's **subtree** will also be recalculated.
         *
         * The `_bounds` object stores the last calculation of the bounds. You can use to entirely skip bounds
         * calculation if needed.
         *
         * ```js
         * const lastCalculatedBounds = displayObject._bounds.getRectangle(optionalRect);
         * ```
         *
         * Do know that usage of `getLocalBounds` can corrupt the `_bounds` of children (the whole subtree, actually). This
         * is a known issue that has not been solved. See [getLocalBounds]{@link feng3d.Node2D#getLocalBounds} for more
         * details.
         *
         * `getBounds` should be called with `skipUpdate` equal to `true` in a render() call. This is because the transforms
         * are guaranteed to be update-to-date. In fact, recalculating inside a render() call may cause corruption in certain
         * cases.
         *
         * @param {boolean} [skipUpdate] - Setting to `true` will stop the transforms of the scene graph from
         *  being updated. This means the calculation returned MAY be out of date BUT will give you a
         *  nice performance boost.
         * @param {PIXI.Rectangle} [rect] - Optional rectangle to store the result of the bounds calculation.
         * @return {PIXI.Rectangle} The minimum axis-aligned rectangle in world space that fits around this object.
         */
        getBounds(skipUpdate?: boolean, rect?: Rectangle): Rectangle
        {
            if (!skipUpdate)
            {
                if (!this.parent)
                {
                    this._setParent(this._tempDisplayObjectParent);
                    this.updateTransform();
                    this._setParent(null);
                }
                else
                {
                    this._recursivePostUpdateTransform();
                    this.updateTransform();
                }
            }

            if (this._bounds.updateID !== this._boundsID)
            {
                this.calculateBounds();
                this._bounds.updateID = this._boundsID;
            }

            if (!rect)
            {
                if (!this._boundsRect)
                {
                    this._boundsRect = new Rectangle();
                }

                rect = this._boundsRect;
            }

            return this._bounds.getRectangle(rect);
        }

        /**
         * Used in Renderer, cacheAsBitmap and other places where you call an `updateTransform` on root
         *
         * ```
         * const cacheParent = elem.enableTempParent();
         * elem.updateTransform();
         * elem.disableTempParent(cacheParent);
         * ```
         *
         * @returns {feng3d.Node2D} current parent
         */
        enableTempParent(): Container
        {
            const myParent = this.parent;

            this._setParent(this._tempDisplayObjectParent);

            return <any>myParent;
        }

        /**
         * Pair method for `enableTempParent`
         *
         * @param {feng3d.Node2D} cacheParent - Actual parent of element
         */
        disableTempParent(cacheParent: Container): void
        {
            this._setParent(cacheParent);
        }

        /**
         * Sorts children by zIndex. Previous order is maintained for 2 children with the same zIndex.
         */
        sortChildren(): void
        {
            let sortRequired = false;

            for (let i = 0, j = this.children.length; i < j; ++i)
            {
                const child = this.children[i];

                child._lastSortedIndex = i;

                if (!sortRequired && child.zIndex !== 0)
                {
                    sortRequired = true;
                }
            }

            if (sortRequired && this.children.length > 1)
            {
                this.children.sort(sortChildren);
            }

            this.sortDirty = false;
        }

        updateTransform(): void
        {
            if (this.sortableChildren && this.sortDirty)
            {
                this.sortChildren();
            }

            this._boundsID++;

            super.updateTransform();
        }

        /**
         * Recalculates the bounds of the container.
         *
         * This implementation will automatically fit the children's bounds into the calculation. Each child's bounds
         * is limited to its mask's bounds or filterArea, if any is applied.
         */
        calculateBounds(): void
        {
            this._bounds.clear();

            this._calculateBounds();

            for (let i = 0; i < this.children.length; i++)
            {
                const child = this.children[i];

                if (!child.visible || !child.renderable)
                {
                    continue;
                }

                child.calculateBounds();

                // TODO: filter+mask, need to mask both somehow
                const mask = child.mask;
                if (mask)
                {
                    const maskObject = ((mask as MaskData).maskObject || mask) as Container;

                    maskObject.calculateBounds();
                    this._bounds.addBoundsMask(child._bounds, maskObject._bounds);
                }
                else if (child.filterArea)
                {
                    this._bounds.addBoundsArea(child._bounds, child.filterArea);
                }
                else
                {
                    this._bounds.addBounds(child._bounds);
                }
            }

            this._bounds.updateID = this._boundsID;
        }

        /**
         * Retrieves the local bounds of the displayObject as a rectangle object.
         *
         * Calling `getLocalBounds` may invalidate the `_bounds` of the whole subtree below. If using it inside a render()
         * call, it is advised to call `getBounds()` immediately after to recalculate the world bounds of the subtree.
         *
         * @param {PIXI.Rectangle} [rect] - Optional rectangle to store the result of the bounds calculation.
         * @param {boolean} [skipChildrenUpdate=false] - Setting to `true` will stop re-calculation of children transforms,
         *  it was default behaviour of pixi 4.0-5.2 and caused many problems to users.
         * @return {PIXI.Rectangle} The rectangular bounding area.
         */
        getLocalBounds(rect?: Rectangle, skipChildrenUpdate = false): Rectangle
        {
            if (!rect)
            {
                if (!this._localBoundsRect)
                {
                    this._localBoundsRect = new Rectangle();
                }

                rect = this._localBoundsRect;
            }

            if (!this._localBounds)
            {
                this._localBounds = new Bounds();
            }

            const transformRef = this.transform;
            const parentRef = this.parent;

            this._setParent(null);
            this.transform = this._tempDisplayObjectParent.transform;

            const worldBounds = this._bounds;
            const worldBoundsID = this._boundsID;

            this._bounds = this._localBounds;

            const result = this.getBounds(false, rect);

            this._setParent(parentRef);
            this.transform = transformRef;

            this._bounds = worldBounds;
            this._bounds.updateID += this._boundsID - worldBoundsID;// reflect side-effects

            if (!skipChildrenUpdate)
            {
                for (let i = 0, j = this.children.length; i < j; ++i)
                {
                    const child = this.children[i];

                    if (child.visible)
                    {
                        child.updateTransform();
                    }
                }
            }

            return result;
        }


        /**
         * Recalculates the content bounds of this object. This should be overriden to
         * calculate the bounds of this specific object (not including children).
         *
         * @protected
         */
        protected _calculateBounds(): void
        {
            // FILL IN//
        }

        /**
         * Renders the object using the WebGL renderer.
         *
         * The [_render]{@link PIXI.Container#_render} method is be overriden for rendering the contents of the
         * container itself. This `render` method will invoke it, and also invoke the `render` methods of all
         * children afterward.
         *
         * If `renderable` or `visible` is false or if `worldAlpha` is not positive, this implementation will entirely
         * skip rendering. See {@link feng3d.Node2D} for choosing between `renderable` or `visible`. Generally,
         * setting alpha to zero is not recommended for purely skipping rendering.
         *
         * When your scene becomes large (especially when it is larger than can be viewed in a single screen), it is
         * advised to employ **culling** to automatically skip rendering objects outside of the current screen. The
         * [@pixi-essentials/cull]{@link https://www.npmjs.com/package/@pixi-essentials/cull} and
         * [pixi-cull]{@link https://www.npmjs.com/package/pixi-cull} packages do this out of the box.
         *
         * The [renderAdvanced]{@link PIXI.Container#renderAdvanced} method is internally used when when masking or
         * filtering is applied on a container. This does, however, break batching and can affect performance when
         * masking and filtering is applied extensively throughout the scene graph.
         *
         * @param {PIXI.Renderer} renderer - The renderer
         */
        render(renderer: Renderer): void
        {
            // if the object is not visible or the alpha is 0 then no need to render this element
            if (!this.visible || this.worldAlpha <= 0 || !this.renderable)
            {
                return;
            }

            // do a quick check to see if this element has a mask or a filter.
            if (this.mask || (this.filters && this.filters.length))
            {
                this.renderAdvanced(renderer);
            }
            else
            {
                this._render(renderer);

                // simple render children!
                for (let i = 0, j = this.children.length; i < j; ++i)
                {
                    this.children[i].render(renderer);
                }
            }
        }

        /**
         * Render the object using the WebGL renderer and advanced features.
         *
         * @protected
         * @param {PIXI.Renderer} renderer - The renderer
         */
        renderAdvanced(renderer: Renderer): void
        {
            renderer.batch.flush();

            const filters = this.filters;
            const mask = this.mask;

            // push filter first as we need to ensure the stencil buffer is correct for any masking
            if (filters)
            {
                if (!this._enabledFilters)
                {
                    this._enabledFilters = [];
                }

                this._enabledFilters.length = 0;

                for (let i = 0; i < filters.length; i++)
                {
                    if (filters[i].enabled)
                    {
                        this._enabledFilters.push(filters[i]);
                    }
                }

                if (this._enabledFilters.length)
                {
                    renderer.filter.push(this, this._enabledFilters);
                }
            }

            if (mask)
            {
                renderer.mask.push(this, mask);
            }

            // add this object to the batch, only rendered if it has a texture.
            this._render(renderer);

            // now loop through the children and make sure they get rendered
            for (let i = 0, j = this.children.length; i < j; i++)
            {
                this.children[i].render(renderer);
            }

            renderer.batch.flush();

            if (mask)
            {
                renderer.mask.pop(this);
            }

            if (filters && this._enabledFilters && this._enabledFilters.length)
            {
                renderer.filter.pop();
            }
        }

        /**
         *
         * @param {PIXI.Renderer} renderer - The renderer
         */
        _render(_renderer: Renderer): void // eslint-disable-line no-unused-vars
        {
            // this is where content itself gets rendered...
        }

        /**
         * Removes all internal references and listeners as well as removes children from the display list.
         * Do not use a Container after calling `destroy`.
         *
         * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
         *  have been set to that value
         * @param {boolean} [options.children=false] - if set to true, all the children will have their destroy
         *  method called as well. 'options' will be passed on to those calls.
         * @param {boolean} [options.texture=false] - Only used for child Sprites if options.children is set to true
         *  Should it destroy the texture of the child sprite
         * @param {boolean} [options.baseTexture=false] - Only used for child Sprites if options.children is set to true
         *  Should it destroy the base texture of the child sprite
         */
        destroy(options?: IDestroyOptions | boolean): void
        {
            super.destroy(options);
            this._bounds = null;

            this.filters = null;
            this.filterArea = null;
            this.hitArea = null;

            this.interactive = false;
            this.interactiveChildren = false;

            this.sortDirty = false;

            this.mask = null;
        }

        containsPoint(point: Vector2): boolean
        {
            return false;
        }

        /**
         * Mixes all enumerable properties and methods from a source object to Node2D.
         *
         * @param {object} source - The source of properties and methods to mix in.
         */
        static mixin(source: pixi.Dict<any>): void
        {
            // in ES8/ES2017, this would be really easy:
            // Object.defineProperties(Node2D.prototype, Object.getOwnPropertyDescriptors(source));

            // get all the enumerable property keys
            const keys = Object.keys(source);

            // loop through properties
            for (let i = 0; i < keys.length; ++i)
            {
                const propertyName = keys[i];

                // Set the property using the property descriptor - this works for accessors and normal value properties
                Object.defineProperty(
                    Container.prototype,
                    propertyName,
                    Object.getOwnPropertyDescriptor(source, propertyName)
                );
            }
        }
    }

    /**
     * Container default updateTransform, does update children of container.
     * Will crash if there's no parent element.
     *.Container#
     * @method containerUpdateTransform
     */
    Container.prototype.containerUpdateTransform = Container.prototype.updateTransform;

}