namespace feng3d.pixi
{
    export interface Container
    {
        cacheAsBitmap?: boolean;
        cacheAsBitmapResolution?: number;
        _cacheAsBitmapResolution?: number;
        _cacheAsBitmap?: boolean;
        _cacheData?: pixi.CacheData;
        _renderCached?: (renderer: pixi.Renderer) => void;
        _initCachedDisplayObject?: (renderer: pixi.Renderer) => void;
        _calculateCachedBounds?: () => void;
        _getCachedLocalBounds?: () => pixi.Rectangle;
        _destroyCachedDisplayObject?: () => void;
        _cacheAsBitmapDestroy?: (options?: IDestroyOptions | boolean) => void;
    }
    const _tempMatrix = new Matrix();

    Container.prototype._cacheAsBitmap = false;
    Container.prototype._cacheData = null;
    Container.prototype._cacheAsBitmapResolution = null;

    // figured there's no point adding ALL the extra variables to prototype.
    // this model can hold the information needed. This can also be generated on demand as
    // most objects are not cached as bitmaps.
    /**
     * @ignore
     * @private
     */
    export class CacheData
    {
        public textureCacheId: string;
        public originalRender: (renderer: Renderer) => void;
        public originalCalculateBounds: () => void;
        public originalGetLocalBounds: (rect?: Rectangle) => Rectangle;
        public originalUpdateTransform: () => void;
        public originalDestroy: (options?: IDestroyOptions | boolean) => void;
        public originalMask: Container | MaskData;
        public originalFilterArea: Rectangle;
        public originalContainsPoint: (point: Vector2) => boolean;
        public sprite: Sprite;

        constructor()
        {
            this.textureCacheId = null;

            this.originalRender = null;
            this.originalCalculateBounds = null;
            this.originalGetLocalBounds = null;

            this.originalUpdateTransform = null;
            this.originalDestroy = null;
            this.originalMask = null;
            this.originalFilterArea = null;
            this.originalContainsPoint = null;
            this.sprite = null;
        }
    }

    Object.defineProperties(Container.prototype, {
        /**
         * The resolution to use for cacheAsBitmap. By default this will use the renderer's resolution
         * but can be overriden for performance. Lower values will reduce memory usage at the expense
         * of render quality. A falsey value of `null` or `0` will default to the renderer's resolution.
         * If `cacheAsBitmap` is set to `true`, this will re-render with the new resolution.
         *
         * @member {number} cacheAsBitmapResolution
         * @memberof feng3d.Node2D#
         * @default null
         */
        cacheAsBitmapResolution: {
            get(): number
            {
                return this._cacheAsBitmapResolution;
            },
            set(resolution: number): void
            {
                if (resolution === this._cacheAsBitmapResolution)
                {
                    return;
                }

                this._cacheAsBitmapResolution = resolution;

                if (this.cacheAsBitmap)
                {
                    // Toggle to re-render at the new resolution
                    this.cacheAsBitmap = false;
                    this.cacheAsBitmap = true;
                }
            },
        },

        /**
         * Set this to true if you want this display object to be cached as a bitmap.
         * This basically takes a snap shot of the display object as it is at that moment. It can
         * provide a performance benefit for complex static displayObjects.
         * To remove simply set this property to `false`
         *
         * IMPORTANT GOTCHA - Make sure that all your textures are preloaded BEFORE setting this property to true
         * as it will take a snapshot of what is currently there. If the textures have not loaded then they will not appear.
         *
         * @member {boolean}
         * @memberof feng3d.Node2D#
         */
        cacheAsBitmap: {
            get(this: Container)
            {
                return this._cacheAsBitmap;
            },
            set(this: Container, value: boolean): void
            {
                if (this._cacheAsBitmap === value)
                {
                    return;
                }

                this._cacheAsBitmap = value;

                let data: CacheData;

                if (value)
                {
                    if (!this._cacheData)
                    {
                        this._cacheData = new CacheData();
                    }

                    data = this._cacheData;

                    data.originalRender = this.render;

                    data.originalUpdateTransform = this.updateTransform;
                    data.originalCalculateBounds = this.calculateBounds;
                    data.originalGetLocalBounds = this.getLocalBounds;

                    data.originalDestroy = this.destroy;

                    data.originalContainsPoint = this.containsPoint;

                    data.originalMask = this.mask;
                    data.originalFilterArea = this.filterArea;

                    this.render = this._renderCached;

                    this.destroy = this._cacheAsBitmapDestroy;
                }
                else
                {
                    data = this._cacheData;

                    if (data.sprite)
                    {
                        this._destroyCachedDisplayObject();
                    }

                    this.render = data.originalRender;
                    this.calculateBounds = data.originalCalculateBounds;
                    this.getLocalBounds = data.originalGetLocalBounds;

                    this.destroy = data.originalDestroy;

                    this.updateTransform = data.originalUpdateTransform;
                    this.containsPoint = data.originalContainsPoint;

                    if (data.originalMask)
                    {
                        this.mask = data.originalMask;
                    }
                    this.filterArea = data.originalFilterArea;
                }
            },
        },
    });

    /**
     * Renders a cached version of the sprite with WebGL
     *
     * @private
     * @method _renderCached.Node2D#
     * @param {PIXI.Renderer} renderer - the WebGL renderer
     */
    Container.prototype._renderCached = function _renderCached(this: Container, renderer: Renderer): void
    {
        if (!this.visible || this.worldAlpha <= 0 || !this.renderable)
        {
            return;
        }

        this._initCachedDisplayObject(renderer);

        this._cacheData.sprite.transform._worldID = this.transform._worldID;
        this._cacheData.sprite.worldAlpha = this.worldAlpha;
        this._cacheData.sprite._render(renderer);
    };

    /**
     * Prepares the WebGL renderer to cache the sprite
     *
     * @private
     * @method _initCachedDisplayObject.Node2D#
     * @param {PIXI.Renderer} renderer - the WebGL renderer
     */
    Container.prototype._initCachedDisplayObject = function _initCachedDisplayObject(this: Container, renderer: Renderer): void
    {
        if (this._cacheData && this._cacheData.sprite)
        {
            return;
        }

        // make sure alpha is set to 1 otherwise it will get rendered as invisible!
        const cacheAlpha = this.alpha;

        this.alpha = 1;

        // first we flush anything left in the renderer (otherwise it would get rendered to the cached texture)
        renderer.batch.flush();
        // this.filters= [];

        // next we find the dimensions of the untransformed object
        // this function also calls updatetransform on all its children as part of the measuring.
        // This means we don't need to update the transform again in this function
        // TODO pass an object to clone too? saves having to create a new one each time!
        const bounds = (this as Container).getLocalBounds(null, true).clone();

        // add some padding!
        if (this.filters)
        {
            const padding = this.filters[0].padding;

            bounds.pad(padding);
        }

        bounds.ceil(settings.RESOLUTION);

        // for now we cache the current renderTarget that the WebGL renderer is currently using.
        // this could be more elegant..
        const cachedRenderTexture = renderer.renderTexture.current;
        const cachedSourceFrame = renderer.renderTexture.sourceFrame.clone();
        const cachedProjectionTransform = renderer.projection.transform;

        // We also store the filter stack - I will definitely look to change how this works a little later down the line.
        // const stack = renderer.filterManager.filterStack;

        // this renderTexture will be used to store the cached Node2D
        const renderTexture = RenderTexture.create({
            width: bounds.width,
            height: bounds.height,
            resolution: this.cacheAsBitmapResolution || renderer.resolution,
        });

        const textureCacheId = `cacheAsBitmap_${uid()}`;

        this._cacheData.textureCacheId = textureCacheId;

        BaseTexture.addToCache(renderTexture.baseTexture, textureCacheId);
        Texture.addToCache(renderTexture, textureCacheId);

        // need to set //
        const m = this.transform.localTransform.copyTo(_tempMatrix).invert().translate(-bounds.x, -bounds.y);

        // set all properties to there original so we can render to a texture
        this.render = this._cacheData.originalRender;

        renderer.render(<any>this, { renderTexture, clear: true, transform: m, skipUpdateTransform: false });

        // now restore the state be setting the new properties
        renderer.projection.transform = cachedProjectionTransform;
        renderer.renderTexture.bind(cachedRenderTexture, cachedSourceFrame);

        // renderer.filterManager.filterStack = stack;

        this.render = this._renderCached;
        // the rest is the same as for Canvas
        this.updateTransform = this.displayObjectUpdateTransform;
        this.calculateBounds = this._calculateCachedBounds;
        this.getLocalBounds = this._getCachedLocalBounds;

        this.mask = null;
        this.filterArea = null;

        // create our cached sprite
        const cachedSprite = new Sprite();
        cachedSprite.texture = renderTexture;

        cachedSprite.transform.worldTransform = this.transform.worldTransform;
        cachedSprite.anchorX = -(bounds.x / bounds.width);
        cachedSprite.anchorY = -(bounds.y / bounds.height);
        cachedSprite.alpha = cacheAlpha;
        cachedSprite._bounds = this._bounds;

        this._cacheData.sprite = cachedSprite;

        this.transform._parentID = -1;
        // restore the transform of the cached sprite to avoid the nasty flicker..
        if (!this.parent)
        {
            this.enableTempParent();
            this.updateTransform();
            this.disableTempParent(null);
        }
        else
        {
            this.updateTransform();
        }

        // map the hit test..
        (<any>this as Sprite).containsPoint = cachedSprite.containsPoint.bind(cachedSprite);
    };

    /**
     * Calculates the bounds of the cached sprite
     *
     * @private
     * @method
     */
    Container.prototype._calculateCachedBounds = function _calculateCachedBounds(this: Container): void
    {
        this._bounds.clear();
        this._cacheData.sprite.transform._worldID = this.transform._worldID;
        (this._cacheData.sprite as any)._calculateBounds();
        this._bounds.updateID = (this as any)._boundsID;
    };

    /**
     * Gets the bounds of the cached sprite.
     *
     * @private
     * @method
     * @return {Rectangle} The local bounds.
     */
    Container.prototype._getCachedLocalBounds = function _getCachedLocalBounds(this: Container): Rectangle
    {
        return this._cacheData.sprite.getLocalBounds(null);
    };

    /**
     * Destroys the cached sprite.
     *
     * @private
     * @method
     */
    Container.prototype._destroyCachedDisplayObject = function _destroyCachedDisplayObject(this: Container): void
    {
        this._cacheData.sprite._texture.destroy(true);
        this._cacheData.sprite = null;

        BaseTexture.removeFromCache(this._cacheData.textureCacheId);
        Texture.removeFromCache(this._cacheData.textureCacheId);

        this._cacheData.textureCacheId = null;
    };

    /**
     * Destroys the cached object.
     *
     * @private
     * @method
     * @param {object|boolean} [options] - Options parameter. A boolean will act as if all options
     *  have been set to that value.
     *  Used when destroying containers, see the Container.destroy method.
     */
    Container.prototype._cacheAsBitmapDestroy = function _cacheAsBitmapDestroy(this: Container, options?: IDestroyOptions | boolean): void
    {
        this.cacheAsBitmap = false;
        this.destroy(options);
    };
}