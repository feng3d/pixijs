namespace feng3d.pixi
{
    /**
     * Represents the update priorities used by internal PIXI classes when registered with
     * the {@link PIXI.Ticker} object. Higher priority items are updated first and lower
     * priority items, such as render, should go later.
     *
     * @static
     * @constant
     * @name UPDATE_PRIORITY
     * @enum {number}
     * @property {number} INTERACTION=50 Highest priority, used for {@link PIXI.InteractionManager}
     * @property {number} HIGH=25 High priority updating, {@link PIXI.VideoBaseTexture} and {@link PIXI.AnimatedSprite}
     * @property {number} NORMAL=0 Default priority for ticker events, see {@link PIXI.Ticker#add}.
     * @property {number} LOW=-25 Low priority used for {@link PIXI.Application} rendering.
     * @property {number} UTILITY=-50 Lowest priority used for {@link PIXI.BasePrepare} utility.
     */
    export enum UPDATE_PRIORITY
    {
        INTERACTION = 50,
        HIGH = 25,
        NORMAL = 0,
        LOW = -25,
        UTILITY = -50,
    }

    export interface Application
    {
        ticker: Ticker;
        stop(): void;
        start(): void;
    }

    export interface IApplicationOptions
    {
        autoStart?: boolean;
        sharedTicker?: boolean;
    }
}