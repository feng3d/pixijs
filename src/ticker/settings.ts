namespace feng3d.pixi
{
    /**
     * Target frames per millisecond.
     *
     * @static
     * @name TARGET_FPMS.settings
     * @type {number}
     * @default 0.06
     */
    settings.TARGET_FPMS = 0.06;

}