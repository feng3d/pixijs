namespace feng3d.pixi
{
    /**
     * Constants that define the type of gradient on text.
     *
     * @static
     * @constant
     * @name TEXT_GRADIENT
     * @type {object}
     * @property {number} LINEAR_VERTICAL Vertical gradient
     * @property {number} LINEAR_HORIZONTAL Linear gradient
     */
    export enum TEXT_GRADIENT
    {
        LINEAR_VERTICAL = 0,
        LINEAR_HORIZONTAL = 1
    }
}