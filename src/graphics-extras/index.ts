namespace feng3d.pixi
{

    export interface IGraphicsExtras
    {
        drawTorus: typeof drawTorus;
        drawChamferRect: typeof drawChamferRect;
        drawFilletRect: typeof drawFilletRect;
        drawRegularPolygon: typeof drawRegularPolygon;
        drawStar: typeof drawStar;
    }

    // Assign extras to Graphics
    Object.defineProperties(Graphics.prototype, {
        drawTorus: { value: drawTorus },
        drawChamferRect: { value: drawChamferRect },
        drawFilletRect: { value: drawFilletRect },
        drawRegularPolygon: { value: drawRegularPolygon },
        drawStar: { value: drawStar },
    });
}