namespace feng3d.pixi
{

    INSTALLED.push(
        ImageResource,
        ImageBitmapResource,
        CanvasResource,
        VideoResource,
        SVGResource,
        BufferResource,
        CubeResource,
        ArrayResource
    );

    export const resources = { Resource };
}