namespace feng3d.pixi
{

    export interface IFilterTarget
    {
        filterArea: Rectangle;
        getBounds(skipUpdate?: boolean): Rectangle;
    }
}