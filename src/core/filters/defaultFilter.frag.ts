namespace feng3d.pixi
{
    export const defaultFilter_frag =
        `
varying vec2 vTextureCoord;

uniform sampler2D uSampler;

void main(void){
   gl_FragColor = texture2D(uSampler, vTextureCoord);
}
`
}