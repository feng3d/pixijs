namespace feng3d.pixi
{


    // If obj.hasOwnProperty has been overridden, then calling
    // obj.hasOwnProperty(prop) will break.
    // See: https://github.com/joyent/node/issues/1707
    function hasOwnProperty(obj, prop)
    {
        return Object.prototype.hasOwnProperty.call(obj, prop);
    }

    /**
     * parses a URL query string into a collection of key and value pairs
     *
     * @param qs The URL query string to parse
     * @param sep The substring used to delimit key and value pairs in the query string
     * @param eq The substring used to delimit keys and values in the query string
     * @param options.decodeURIComponent The function to use when decoding percent-encoded characters in the query string
     * @param options.maxKeys Specifies the maximum number of keys to parse. Specify 0 to remove key counting limitations default 1000
     */
    function decodeFuncType(qs?: string,
        sep?: string,
        eq?: string,
        options?: {
            decodeURIComponent?: Function;
            maxKeys?: number;
        }): Record<any, unknown>
    {
        sep = sep || '&';
        eq = eq || '=';
        var obj = {};

        if (typeof qs !== 'string' || qs.length === 0)
        {
            return obj;
        }

        var regexp = /\+/g;
        qs = <any>qs.split(sep);

        var maxKeys = 1000;
        if (options && typeof options.maxKeys === 'number')
        {
            maxKeys = options.maxKeys;
        }

        var len = qs.length;
        // maxKeys <= 0 means that we should not limit keys count
        if (maxKeys > 0 && len > maxKeys)
        {
            len = maxKeys;
        }

        for (var i = 0; i < len; ++i)
        {
            var x = qs[i].replace(regexp, '%20'),
                idx = x.indexOf(eq),
                kstr, vstr, k, v;

            if (idx >= 0)
            {
                kstr = x.substr(0, idx);
                vstr = x.substr(idx + 1);
            } else
            {
                kstr = x;
                vstr = '';
            }

            k = decodeURIComponent(kstr);
            v = decodeURIComponent(vstr);

            if (!hasOwnProperty(obj, k))
            {
                obj[k] = v;
            } else if (Array.isArray(obj[k]))
            {
                obj[k].push(v);
            } else
            {
                obj[k] = [obj[k], v];
            }
        }

        return obj;
    };

    function stringifyPrimitive(v)
    {
        switch (typeof v)
        {
            case 'string':
                return v;

            case 'boolean':
                return v ? 'true' : 'false';

            case 'number':
                return isFinite(v) ? v : '';

            default:
                return '';
        }
    };

    /**
    * It serializes passed object into string
    * The numeric values must be finite.
    * Any other input values will be coerced to empty strings.
    *
    * @param obj The object to serialize into a URL query string
    * @param sep The substring used to delimit key and value pairs in the query string
    * @param eq The substring used to delimit keys and values in the query string
    * @param name
    */
    function encodeFuncType(
        obj?: Record<any, unknown>,
        sep?: string,
        eq?: string,
        name?: any): string
    {
        sep = sep || '&';
        eq = eq || '=';
        if (obj === null)
        {
            obj = undefined;
        }

        if (typeof obj === 'object')
        {
            return Object.keys(obj).map(function (k)
            {
                var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
                if (Array.isArray(obj[k]))
                {
                    return (<any>obj[k]).map(function (v)
                    {
                        return ks + encodeURIComponent(stringifyPrimitive(v));
                    }).join(sep);
                } else
                {
                    return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
                }
            }).filter(Boolean).join(sep);

        }

        if (!name) return '';
        return encodeURIComponent(stringifyPrimitive(name)) + eq +
            encodeURIComponent(stringifyPrimitive(obj));
    };

    export const querystring =
    {
        decode: decodeFuncType,
        parse: decodeFuncType,
        encode: encodeFuncType,
        stringify: encodeFuncType,
    }

}