namespace feng3d.pixi
{
    export const utils = {
        rgb2hex: rgb2hex,
        hex2rgb: hex2rgb,
        premultiplyTint: premultiplyTint,
        premultiplyTintToRgba: premultiplyTintToRgba,
        correctBlendMode: correctBlendMode,
        getResolutionOfUrl: getResolutionOfUrl,
        premultiplyBlendMode: premultiplyBlendMode,
        EventEmitter: EventEmitter,
    };
}