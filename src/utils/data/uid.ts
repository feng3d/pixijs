namespace feng3d.pixi
{
    let nextUid = 0;

    /**
     * Gets the next unique identifier
     *.utils
     * @function uid
     * @return {number} The next unique identifier to use.
     */
    export function uid(): number
    {
        return ++nextUid;
    }
}