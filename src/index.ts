namespace feng3d.pixi
{
    //
    classUtils.addClassNameSpace("feng3d.pixi");
    // Install renderer plugins
    Renderer.registerPlugin('accessibility', AccessibilityManager);
    Renderer.registerPlugin('extract', Extract);
    Renderer.registerPlugin('interaction', InteractionManager);
    Renderer.registerPlugin('particle', ParticleRenderer);
    Renderer.registerPlugin('prepare', Prepare);
    Renderer.registerPlugin('batch', BatchRenderer);
    Renderer.registerPlugin('tilingSprite', TilingSpriteRenderer);

    // Install loader plugins
    Loader.registerPlugin(BitmapFontLoader);
    Loader.registerPlugin(CompressedTextureLoader);
    Loader.registerPlugin(DDSLoader);
    Loader.registerPlugin(KTXLoader);
    Loader.registerPlugin(SpritesheetLoader);

    // Install application plugins
    Application.registerPlugin(TickerPlugin);
    Application.registerPlugin(AppLoaderPlugin);

    /**
     * String of the current PIXI version.
     *
     * @static
     * @constant
     * @name VERSION
     * @type {string}
     */
    export const VERSION = '$_VERSION';

    /**
     * @namespace feng3d.pixi
     */

    /**
     * This namespace contains WebGL-only display filters that can be applied
     * to DisplayObjects using the {@link PIXI.DisplayObject#filters filters} property.
     *
     * Since PixiJS only had a handful of built-in filters, additional filters
     * can be downloaded {@link https://github.com/pixijs/pixi-filters here} from the
     * PixiJS Filters repository.
     *
     * All filters must extend {@link PIXI.Filter}.
     *
     * @example
     * // Create a new application
     * const app = new PIXI.Application();
     *
     * // Draw a green rectangle
     * const rect = new PIXI.Graphics()
     *     .beginFill(0x00ff00)
     *     .drawRect(40, 40, 200, 200);
     *
     * // Add a blur filter
     * rect.filters = [new PIXI.filters.BlurFilter()];
     *
     * // Display rectangle
     * app.stage.addChild(rect);
     * document.body.appendChild(app.view);
     * @namespace feng3d.pixi.filters
     */
    export const filters = {
        AlphaFilter,
        BlurFilter,
        BlurFilterPass,
        ColorMatrixFilter,
        DisplacementFilter,
        FXAAFilter,
        NoiseFilter,
    };
}

const PIXI = feng3d.pixi;