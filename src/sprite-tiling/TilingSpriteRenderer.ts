namespace feng3d.pixi
{
    const tempMat = new Matrix();

    /**
     * WebGL renderer plugin for tiling sprites
     *
     */
    export class TilingSpriteRenderer extends ObjectRenderer
    {
        public shader: Shader;
        public simpleShader: Shader;
        public quad: QuadUv;
        public readonly state: State;

        /**
         * constructor for renderer
         *
         * @param {PIXI.Renderer} renderer - The renderer this tiling awesomeness works for.
         */
        constructor(renderer: Renderer)
        {
            super(renderer);

            const uniforms = { globals: this.renderer.globalUniforms };

            this.shader = Shader.from(tilingSprite_vert, tilingSprite_frag, uniforms);

            this.simpleShader = Shader.from(tilingSprite_vert, tilingSprite_simple_frag, uniforms);

            this.quad = new QuadUv();

            /**
             * The WebGL state in which this renderer will work.
             *
             * @member {PIXI.State}
             * @readonly
             */
            this.state = State.for2d();
        }

        /**
         *
         * @param {PIXI.TilingSprite} ts - tilingSprite to be rendered
         */
        public render(ts: TilingSprite): void
        {
            const renderer = this.renderer;
            const quad = this.quad;

            let vertices = quad.vertices;

            vertices[0] = vertices[6] = (ts._width) * -ts.anchorX;
            vertices[1] = vertices[3] = ts._height * -ts.anchorY;

            vertices[2] = vertices[4] = (ts._width) * (1.0 - ts.anchorX);
            vertices[5] = vertices[7] = ts._height * (1.0 - ts.anchorY);

            const anchorX = ts.uvRespectAnchor ? ts.anchorX : 0;
            const anchorY = ts.uvRespectAnchor ? ts.anchorY : 0;

            vertices = quad.uvs;

            vertices[0] = vertices[6] = -anchorX;
            vertices[1] = vertices[3] = -anchorY;

            vertices[2] = vertices[4] = 1.0 - anchorX;
            vertices[5] = vertices[7] = 1.0 - anchorY;

            quad.invalidate();

            const tex = ts._texture;
            const baseTex = tex.baseTexture;
            const lt = ts.tileTransform.localTransform;
            const uv = ts.uvMatrix;
            let isSimple = baseTex.isPowerOfTwo
                && tex.frame.width === baseTex.width && tex.frame.height === baseTex.height;

            // auto, force repeat wrapMode for big tiling textures
            if (isSimple)
            {
                if (!baseTex._glTextures[renderer.CONTEXT_UID])
                {
                    if (baseTex.wrapMode === WRAP_MODES.CLAMP)
                    {
                        baseTex.wrapMode = WRAP_MODES.REPEAT;
                    }
                }
                else
                {
                    isSimple = baseTex.wrapMode !== WRAP_MODES.CLAMP;
                }
            }

            const shader = isSimple ? this.simpleShader : this.shader;

            const w = tex.width;
            const h = tex.height;
            const W = ts._width;
            const H = ts._height;

            tempMat.set(lt.a * w / W,
                lt.b * w / H,
                lt.c * h / W,
                lt.d * h / H,
                lt.tx / W,
                lt.ty / H);

            // that part is the same as above:
            // tempMat.identity();
            // tempMat.scale(tex.width, tex.height);
            // tempMat.prepend(lt);
            // tempMat.scale(1.0 / ts._width, 1.0 / ts._height);

            tempMat.invert();
            if (isSimple)
            {
                tempMat.prepend(uv.mapCoord);
            }
            else
            {
                shader.uniforms.uMapCoord = uv.mapCoord.toArray(true);
                shader.uniforms.uClampFrame = uv.uClampFrame;
                shader.uniforms.uClampOffset = uv.uClampOffset;
            }

            shader.uniforms.uTransform = tempMat.toArray(true);
            shader.uniforms.uColor = premultiplyTintToRgba(ts.tint, ts.worldAlpha,
                shader.uniforms.uColor, baseTex.alphaMode as any);
            shader.uniforms.translationMatrix = ts.transform.worldTransform.toArray(true);
            shader.uniforms.uSampler = tex;

            renderer.shader.bind(shader);
            renderer.geometry.bind(quad);

            this.state.blendMode = correctBlendMode(ts.blendMode, baseTex.alphaMode as any);
            renderer.state.set(this.state);
            renderer.geometry.draw(this.renderer.gl.TRIANGLES, 6, 0);
        }
    }
}