// References:
// https://github.com/sindresorhus/object-assign
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign

namespace feng3d.pixi
{
    if (!Object.assign)
    {
        Object.assign = objectAssign;
    }
}