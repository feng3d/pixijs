namespace feng3d
{
    export interface Node2D
    {
        getGlobalPosition?: (point?: Vector2, skipUpdate?: boolean) => Vector2;
    }
}
namespace feng3d.pixi
{

    /**
     * Returns the global position of the displayObject. Does not depend on object scale, rotation and pivot.
     *
     * @method getGlobalPosition.DisplayObject#
     * @param {PIXI.Point} [point=new feng3d.Vector2()] - The point to write the global value to.
     * @param {boolean} [skipUpdate=false] - Setting to true will stop the transforms of the scene graph from
     *  being updated. This means the calculation returned MAY be out of date BUT will give you a
     *  nice performance boost.
     * @return {PIXI.Point} The updated point.
     */
    Container.prototype.getGlobalPosition = function getGlobalPosition(point: Vector2 = new Vector2(), skipUpdate = false): Vector2
    {
        if (this.parent)
        {
            this.parent.toGlobal(this.position, point, skipUpdate);
        }
        else
        {
            point.x = this.position.x;
            point.y = this.position.y;
        }

        return point;
    };
}