namespace feng3d.pixi
{

    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    export interface Application
    {
        resizeTo: Window | HTMLElement;
        resize(): void;
    }

    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    export interface IApplicationOptions
    {
        resizeTo?: Window | HTMLElement;
    }


    Application.registerPlugin(ResizePlugin);

}