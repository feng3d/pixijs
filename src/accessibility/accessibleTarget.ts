namespace feng3d.pixi
{
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    export interface Container extends Partial<pixi.IAccessibleTarget>
    {
    
    }

    export type PointerEvents = 'auto'
        | 'none'
        | 'visiblePainted'
        | 'visibleFill'
        | 'visibleStroke'
        | 'visible'
        | 'painted'
        | 'fill'
        | 'stroke'
        | 'all'
        | 'inherit';

    export interface IAccessibleTarget
    {
        accessible: boolean;
        accessibleTitle: string;
        accessibleHint: string;
        tabIndex: number;
        _accessibleActive: boolean;
        _accessibleDiv: IAccessibleHTMLElement;
        accessibleType: string;
        accessiblePointerEvents: PointerEvents;
        accessibleChildren: true;
        renderId: number;
    }

    export interface IAccessibleHTMLElement extends HTMLElement
    {
        type?: string;
        displayObject?: Container;
    }

    /**
     * Default property values of accessible objects
     * used by {@link PIXI.AccessibilityManager}.
     *
     * @private
     * @function accessibleTarget
     * @type {Object}
     * @example
     *      function MyObject() {}
     *
     *      Object.assign(
     *          MyObject.prototype,
     *          PIXI.accessibleTarget
     *      );
     */
    export const accessibleTarget: IAccessibleTarget = {
        /**
         *  Flag for if the object is accessible. If true AccessibilityManager will overlay a
         *   shadow div with attributes set
         *
         * @member {boolean}
         * @memberof feng3d.Node2D#
         */
        accessible: false,

        /**
         * Sets the title attribute of the shadow div
         * If accessibleTitle AND accessibleHint has not been this will default to 'displayObject [tabIndex]'
         *
         * @member {?string}
         * @memberof feng3d.Node2D#
         */
        accessibleTitle: null,

        /**
         * Sets the aria-label attribute of the shadow div
         *
         * @member {string}
         * @memberof feng3d.Node2D#
         */
        accessibleHint: null,

        /**
         * @member {number}
         * @memberof feng3d.Node2D#
         * @private
         * @todo Needs docs.
         */
        tabIndex: 0,

        /**
         * @member {boolean}
         * @memberof feng3d.Node2D#
         * @todo Needs docs.
         */
        _accessibleActive: false,

        /**
         * @member {boolean}
         * @memberof feng3d.Node2D#
         * @todo Needs docs.
         */
        _accessibleDiv: null,

        /**
         * Specify the type of div the accessible layer is. Screen readers treat the element differently
         * depending on this type. Defaults to button.
         *
         * @member {string}
         * @memberof feng3d.Node2D#
         * @default 'button'
         */
        accessibleType: 'button',

        /**
         * Specify the pointer-events the accessible div will use
         * Defaults to auto.
         *
         * @member {string}
         * @memberof feng3d.Node2D#
         * @default 'auto'
         */
        accessiblePointerEvents: 'auto',

        /**
         * Setting to false will prevent any children inside this container to
         * be accessible. Defaults to true.
         *
         * @member {boolean}
         * @memberof feng3d.Node2D#
         * @default true
         */
        accessibleChildren: true,

        renderId: -1,
    };
}