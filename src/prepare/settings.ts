namespace feng3d.pixi
{
    /**
     * Default number of uploads per frame using prepare plugin.
     *
     * @static.settings
     * @name UPLOADS_PER_FRAME
     * @type {number}
     * @default 4
     */
    settings.UPLOADS_PER_FRAME = 4;

}