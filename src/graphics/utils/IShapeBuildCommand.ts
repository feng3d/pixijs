namespace feng3d.pixi
{

    export interface IShapeBuildCommand
    {
        build(graphicsData: GraphicsData): void;
        triangulate(graphicsData: GraphicsData, target: GraphicsGeometry): void;
    }
}