namespace feng3d.pixi
{


    /**
     * Map of fill commands for each shape type.
     *.graphicsUtils
     * @member {Object} FILL_COMMANDS
     */
    export const FILL_COMMANDS: Record<SHAPES, IShapeBuildCommand> = {
        [SHAPES.POLY]: buildPoly,
        [SHAPES.CIRC]: buildCircle,
        [SHAPES.ELIP]: buildCircle,
        [SHAPES.RECT]: buildRectangle,
        [SHAPES.RREC]: buildRoundedRectangle,
    };

    /**
     * Batch pool, stores unused batches for preventing allocations.
     *.graphicsUtils
     * @member {Array<PIXI.graphicsUtils.BatchPart>} BATCH_POOL
     */
    export const BATCH_POOL: Array<BatchPart> = [];

    /**
     * Draw call pool, stores unused draw calls for preventing allocations.
     *.graphicsUtils
     * @member {Array<PIXI.BatchDrawCall>} DRAW_CALL_POOL
     */
    export const DRAW_CALL_POOL: Array<BatchDrawCall> = [];
}