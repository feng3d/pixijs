namespace feng3d.pixi
{

    export const graphicsUtils = {
        buildPoly: buildPoly as IShapeBuildCommand,
        buildCircle: buildCircle as IShapeBuildCommand,
        buildRectangle: buildRectangle as IShapeBuildCommand,
        buildRoundedRectangle: buildRoundedRectangle as IShapeBuildCommand,
        buildLine,
        ArcUtils,
        BezierUtils,
        QuadraticUtils,
        BatchPart,
        FILL_COMMANDS: FILL_COMMANDS as Record<SHAPES, IShapeBuildCommand>,
        BATCH_POOL: BATCH_POOL as Array<BatchPart>,
        DRAW_CALL_POOL: DRAW_CALL_POOL as Array<BatchDrawCall>
    };
}